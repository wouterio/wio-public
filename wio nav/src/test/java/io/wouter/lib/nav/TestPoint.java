package io.wouter.lib.nav;


public class TestPoint {

  public final float x, y;



  public TestPoint(final float x, final float y) {
    this.x = x;
    this.y = y;
  }



  @Override
  public String toString() {
    return "(" + x + ',' + y + ')'; //$NON-NLS-1$
  }
}
