package io.wouter.lib.nav;

import static org.testng.Assert.assertEquals;

import java.util.Deque;
import java.util.LinkedList;

import org.testng.annotations.Test;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Pos;

public class PathInterpolationTest {

  private static final float ErrorMargin = .001f;

  private final Model<Object, Pos, Dis> model = new TestModel(11, 11);
  private final AStar<Object, Pos, Dis> astar = new AStar<>(model, true);



  @Test
  public void interpolationTest1() {
    final Pos beg = Pos.get(0, 0);
    final Pos end = Pos.get(3, 0);
    final Deque<Step<Pos, Dis>> path = new LinkedList<>();
    final boolean found = astar.findPath(null, beg, end, 10f, path);
    final AStar.Metrics metrics = astar.getMetrics();
    System.out.println(path);
    System.out.println(metrics);
    assertEquals(found, true);
    assertEquals(path.size(), 3);

    final PathInterpolation<Pos, Dis, TestPoint> ip = new PathInterpolation<>((step, u) -> interpolate(step, u), path);
    TestPoint p;

    p = ip.next(0.75f);
    assertEquals(p.x, 0.75f, ErrorMargin);
    assertEquals(p.y, 0f, ErrorMargin);

    p = ip.next(0.75f);
    assertEquals(p.x, 1.5f, ErrorMargin);
    assertEquals(p.y, 0f, ErrorMargin);

    p = ip.next(0.75f);
    assertEquals(p.x, 2.25f, ErrorMargin);
    assertEquals(p.y, 0f, ErrorMargin);

    p = ip.next(0.75f);
    assertEquals(p.x, 3f, ErrorMargin);
    assertEquals(p.y, 0f, ErrorMargin);

    assertEquals(ip.hasNext(), false);
  }

  @Test
  public void interpolationTest2() {
    final Pos beg = Pos.get(0, 0);
    final Pos end = Pos.get(3, 3);
    final Deque<Step<Pos, Dis>> path = new LinkedList<>();
    final boolean found = astar.findPath(null, beg, end, 10f, path);
    final AStar.Metrics metrics = astar.getMetrics();
    System.out.println(path);
    System.out.println(metrics);
    assertEquals(found, true);
    assertEquals(path.size(), 3);

    final PathInterpolation<Pos, Dis, TestPoint> ip = new PathInterpolation<>((step, u) -> interpolate(step, u), path);
    TestPoint p;

    p = ip.next(0.75f);
    assertEquals(p.x, 0.5303301f, ErrorMargin);
    assertEquals(p.y, 0.5303301f, ErrorMargin);

    p = ip.next(0.75f);
    assertEquals(p.x, 1.0606602f, ErrorMargin);
    assertEquals(p.y, 1.0606602f, ErrorMargin);

    p = ip.next(0.75f);
    assertEquals(p.x, 1.5909903f, ErrorMargin);
    assertEquals(p.y, 1.5909903f, ErrorMargin);

    p = ip.next(0.75f);
    assertEquals(p.x, 2.1213205f, ErrorMargin);
    assertEquals(p.y, 2.1213205f, ErrorMargin);

    p = ip.next(0.75f);
    assertEquals(p.x, 2.6516504f, ErrorMargin);
    assertEquals(p.y, 2.6516504f, ErrorMargin);

    p = ip.next(0.75f);
    assertEquals(p.x, 3f, ErrorMargin);
    assertEquals(p.y, 3f, ErrorMargin);

    assertEquals(ip.hasNext(), false);
  }



  static TestPoint interpolate(final Step<Pos, ?> step, final float u) {
    final Pos a = step.from();
    final Pos b = step.to();
    final float x = interpolate(a.i, b.i, u);
    final float y = interpolate(a.j, b.j, u);
    return new TestPoint(x, y);
  }

  static float interpolate(final int a, final int b, final float u) {
    return a + (b - a) * u; // (1f - u) * a + u * b;
  }
}
