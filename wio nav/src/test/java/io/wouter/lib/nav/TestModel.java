package io.wouter.lib.nav;

import static io.wouter.lib.grid.Pos.diagdist;

import java.util.function.BiConsumer;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Grid;
import io.wouter.lib.grid.Pos;

public class TestModel implements Grid, Model<Object, Pos, Dis> {

  private final int w;
  private final int h;



  public TestModel(final int w, final int h) {
    this.w = w;
    this.h = h;
  }



  @Override
  public int getWidth() {
    return w;
  }

  @Override
  public int getHeight() {
    return h;
  }

  @Override
  public boolean isAccessible(final Object agent, final Pos state) {
    return includes(state);
  }



  @Override
  public void expand(final Object agent, final Pos current, final BiConsumer<Dis, Pos> expand) {
    for (final Dis dis : Dis.values()) {
      final Pos next = current.add(dis);
      if (isAccessible(agent, next))
        expand.accept(dis, next);
    }
  }



  @Override
  public float computeTransitionCost(final Object agent, final Pos from, final Dis trans, final Pos to) {
    return trans.dist;
  }

  @Override
  public float computeHeuristicCost(final Object agent, final Pos from, final Pos end) {
    return diagdist(from, end);
  }
}
