package io.wouter.lib.nav;

import static java.lang.Float.MAX_VALUE;
import static java.lang.Math.abs;
import static java.lang.Math.max;
import static org.testng.Assert.assertEquals;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

import org.testng.annotations.Test;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Pos;

public class AStarTest {

  private final Model<Object, Pos, Dis> model = new TestModel(11, 11);
  private final AStar<Object, Pos, Dis> astar = new AStar<>(model, true);



  @Test
  public void test_NotAccessible() {
    final Pos beg = Pos.get(5, 5);
    final Pos end = Pos.get(5, 11);
    final Deque<Step<Pos, Dis>> path = new LinkedList<>();
    final boolean found = astar.findPath(null, beg, end, MAX_VALUE, path);
    final AStar.Metrics metrics = astar.getMetrics();
    System.out.println(path);
    System.out.println(metrics);
    assertEquals(found, false);
    assertEquals(path.size(), 0);
    assertEquals(metrics.getVisited(), 0);
    assertEquals(metrics.getAdded(), 0);
    assertEquals(metrics.getPolled(), 0);
    assertEquals(metrics.getOpenPeak(), 0);
  }

  @Test
  public void test_MaxCost() {
    final Pos beg = Pos.get(5, 5);
    final Pos end = Pos.get(5, 7);
    final Deque<Step<Pos, Dis>> path = new LinkedList<>();
    final boolean found = astar.findPath(null, beg, end, 1f, path);
    final AStar.Metrics metrics = astar.getMetrics();
    System.out.println(path);
    System.out.println(metrics);
    assertEquals(found, false);
    assertEquals(path.size(), 0);
    assertEquals(metrics.getVisited(), 0);
    assertEquals(metrics.getAdded(), 0);
    assertEquals(metrics.getPolled(), 0);
    assertEquals(metrics.getOpenPeak(), 0);
  }

  @Test
  public void test_1Step() {
    final Pos beg = Pos.get(5, 5);
    final Pos end = Pos.get(6, 5);
    final Deque<Step<Pos, Dis>> path = new LinkedList<>();
    final boolean found = astar.findPath(null, beg, end, MAX_VALUE, path);
    final AStar.Metrics metrics = astar.getMetrics();
    System.out.println(path);
    System.out.println(metrics);
    assertEquals(found, true);
    assertEquals(path.size(), 1);
    assertEquals(path.getFirst().from(), beg);
    assertEquals(path.getLast().to(), end);
  }

  @Test
  public void test_5Step() {
    final Pos beg = Pos.get(5, 5);
    final Pos end = Pos.get(5, 10);
    final Deque<Step<Pos, Dis>> path = new LinkedList<>();
    final boolean found = astar.findPath(null, beg, end, MAX_VALUE, path);
    final AStar.Metrics metrics = astar.getMetrics();
    System.out.println(path);
    System.out.println(metrics);
    assertEquals(found, true);
    assertEquals(path.size(), max(abs(beg.i - end.i), abs(beg.j - end.j)));
    assertEquals(path.getFirst().from(), beg);
    assertEquals(path.getLast().to(), end);
    final Iterator<Step<Pos, Dis>> it = path.iterator();
    for (Step<Pos, Dis> prev = it.next(), next = it.next(); it.hasNext(); prev = next, next = it.next())
      assertEquals(prev.to(), next.from());
  }
}
