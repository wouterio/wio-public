package io.wouter.lib.nav;


public interface Step<S, T> {

  S from();

  T transition();

  S to();

  float cost();
}
