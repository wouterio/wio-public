package io.wouter.lib.nav;

import java.util.function.BiConsumer;

public interface Model<A, S, T> {

  /**
   * Check whether agent can access state.
   *
   * @param agent
   * @param state
   * @return Whether agent can access state.
   */
  boolean isAccessible(final A agent, final S state);

  /**
   * Expand current state with next states.
   *
   * @param agent
   * @param current
   * @param expand
   */

  void expand(final A agent, S current, BiConsumer<T, S> expand);

  /**
   * Transition cost function.
   *
   * @param agent
   * @param from
   * @param trans
   * @param to
   * @return Transition cost.
   */

  float computeTransitionCost(final A agent, S from, T trans, S to);

  /**
   * Heuristic cost function. Should be admissable. That is, should never overestimate cost to reach goal.
   *
   * @param agent
   * @param from
   * @param end
   * @return Heuristic cost.
   */

  float computeHeuristicCost(final A agent, S from, S end);
}
