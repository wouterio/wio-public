package io.wouter.lib.nav;

import static java.util.Objects.requireNonNull;

import java.util.Deque;

public class PathInterpolation<S, T, I> {

  private final InterpolationFunction<S, T, I> fun;
  private final Deque<Step<S, T>> path;



  private float cumdelta = 0f;



  public PathInterpolation(final InterpolationFunction<S, T, I> fun, final Deque<Step<S, T>> path) {
    this.fun = requireNonNull(fun);
    this.path = requireNonNull(path);
  }



  public boolean hasNext() {
    return !path.isEmpty();
  }

  public I next(final float delta) {
    float pathcost = 0f;
    cumdelta += delta;

    while (true) {
      final Step<S, T> step = path.element();
      final float stepcost = step.cost();
      pathcost += stepcost;

      if (cumdelta < pathcost)
        return fun.interpolate(step, cumdelta / stepcost);

      path.remove();
      cumdelta -= stepcost;

      if (path.isEmpty())
        return fun.interpolate(step, 1f);
    }
  }



  public void reset() {
    cumdelta = 0f;
  }



  @FunctionalInterface
  public interface InterpolationFunction<S, T, I> {

    I interpolate(Step<S, T> step, float u);
  }
}
