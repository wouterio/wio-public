package io.wouter.lib.nav;

import static java.lang.Float.compare;
import static java.lang.Math.max;
import static java.util.Comparator.comparing;
import static java.util.Objects.requireNonNull;

import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public final class AStar<A, S, T> {

  private final Map<S, Node> nodes = new HashMap<>();
  private final Comparator<S> comp = comparing(s -> nodes.get(s));
  private final Queue<S> open = new PriorityQueue<>(40, comp);
  private final Model<A, S, T> model;



  private Metrics metrics;



  public AStar(final Model<A, S, T> model, final boolean metrics) {
    this.model = requireNonNull(model);
    this.metrics = metrics ?
        new Metrics() :
        null;
  }



  public Metrics getMetrics() {
    return metrics;
  }



  public boolean findPath(final A agent, final S beg, final S end, final float maxcost, final Deque<Step<S, T>> path) {
    if (metrics != null)
      metrics = new Metrics();

    nodes.clear();
    open.clear();
    path.clear();

    final float beghcost = model.computeHeuristicCost(agent, beg, end);
    if (!model.isAccessible(agent, end) || beghcost > maxcost)
      return false;

    final Node begnode = new Node(beg, beghcost);
    add(beg, begnode, false);

    final Node endnode = search(agent, end, maxcost);
    if (endnode == null)
      return false;

    buildPath(endnode, path);
    return true;
  }



  private Node search(final A agent, final S end, final float maxcost) {
    do {
      if (metrics != null)
        metrics.polled();

      final S poll = open.poll();
      final Node pollnode = nodes.get(poll);

      if (poll.equals(end))
        return pollnode;

      pollnode.closed = true;

      model.expand(agent, poll, (trans, next) -> {
        if (metrics != null)
          metrics.visited();

        final Node oldnextnode = nodes.get(next);
        if (oldnextnode != null && oldnextnode.closed)
          return; // exit lambda

        final float nexttcost = model.computeTransitionCost(agent, poll, trans, next);

        final float nextgcost = pollnode.gcost + nexttcost;
        if (nextgcost > maxcost)
          return; // exit lambda

        if (oldnextnode != null && nextgcost >= oldnextnode.gcost)
          return; // exit lambda

        final float nexthcost = oldnextnode != null ?
            oldnextnode.fcost - oldnextnode.gcost : // reuse hcost instead of computing it again
            model.computeHeuristicCost(agent, next, end);

        final float nextfcost = max(nextgcost + nexthcost, pollnode.fcost); // pathmax
        if (nextfcost > maxcost)
          return; // exit lambda

        final Node newnextnode = new Node(poll, trans, next, nexttcost, nextgcost, nextfcost);
        add(next, newnextnode, oldnextnode != null);
      });
    } while (!open.isEmpty());

    return null;
  }



  private void buildPath(final Node endnode, final Deque<Step<S, T>> path) {
    for (Node n = endnode; n.from != null; n = nodes.get(n.from))
      path.addFirst(n);
  }



  private void add(final S state, final Node node, final boolean remove) {
    if (metrics != null)
      metrics.added(open.size());

    nodes.put(state, node);
    if (remove)
      open.remove(state);
    open.add(state);
  }



  //



  private final class Node implements Step<S, T>, Comparable<Node> {

    final S from;
    final T trans;
    final S to;
    final float tcost;
    final float gcost;
    final float fcost;



    boolean closed = false;



    Node(final S ibeg, final float ihcost) {
      //
      from = null;
      trans = null;
      to = ibeg;
      tcost = 0f;
      gcost = 0f;
      fcost = ihcost;
    }

    Node(final S ifrom, final T itrans, final S ito, final float itcost, final float igcost, final float ifcost) {
      from = ifrom;
      trans = itrans;
      to = ito;
      tcost = itcost;
      gcost = igcost;
      fcost = ifcost;
    }



    @Override
    public S from() {
      return from;
    }

    @Override
    public T transition() {
      return trans;
    }

    @Override
    public S to() {
      return to;
    }

    @Override
    public float cost() {
      return tcost;
    }



    @Override
    public int compareTo(final Node other) {
      return compare(fcost, other.fcost);
    }



    @Override
    public String toString() {
      return new StringBuilder() //
          .append('<').append(getClass().getSimpleName()) //
          .append(" from=").append(from) // //$NON-NLS-1$
          .append(" trans=").append(trans) // //$NON-NLS-1$
          .append(" to=").append(to) // //$NON-NLS-1$
          .append(" cost=").append(tcost) // //$NON-NLS-1$
          .append('>').toString(); //
    }
  }



  //



  public static final class Metrics {

    private int visited = 0;
    private int added = 0;
    private int polled = 0;
    private int openpeak = 0;



    Metrics() {
      // empty
    }



    public int getVisited() {
      return visited;
    }

    public int getAdded() {
      return added;
    }

    public int getPolled() {
      return polled;
    }

    public int getOpenPeak() {
      return openpeak;
    }



    void visited() {
      ++visited;
    }

    void added(final int opensize) {
      ++added;
      openpeak = max(openpeak, opensize);
    }

    void polled() {
      ++polled;
    }



    @Override
    public String toString() {
      return new StringBuilder() //
          .append('<').append(getClass().getSimpleName()) //
          .append(" visited=").append(visited) // //$NON-NLS-1$
          .append(" added=").append(added) // //$NON-NLS-1$
          .append(" polled=").append(polled) // //$NON-NLS-1$
          .append(" openpeak=").append(openpeak) // //$NON-NLS-1$
          .append('>').toString(); //
    }
  }
}
