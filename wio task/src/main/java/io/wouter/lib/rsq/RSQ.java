package io.wouter.lib.rsq;

import static java.lang.System.arraycopy;
import static java.util.Arrays.copyOf;
import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.function.Consumer;

class RSQ<E> implements Serializable {

  private static final long serialVersionUID = 8254827733207723459L;



  private static final int MinInitCap = 8;



  E[] elements;
  int mask;
  int head = 0;
  int tail = 0;



  public RSQ(final int initcap) {
    this.elements = (E[]) new Object[computeCap(initcap)];
    this.mask = elements.length - 1;
  }

  public RSQ(final RSQ<E> other) {
    this.elements = copyOf(other.elements, other.elements.length);
    this.mask = other.mask;
    this.head = other.head;
    this.tail = other.tail;
  }



  public boolean isEmpty() {
    return tail == head;
  }

  public int size() {
    return tail - head & mask;
  }

  public boolean contains(final Object o) {
    if (o == null)
      return false;
    for (int i = head; i != tail; ++i, i &= mask)
      if (o.equals(elements[i]))
        return true;
    return false;
  }



  public void offer(final E element) {
    requireNonNull(element);
    elements[tail] = element;
    tailFw();
    if (tail == head)
      doubleCap();
  }

  public E peek() {
    return elements[head]; // null if empty
  }

  public E poll() {
    final E poll = peek();
    if (poll == null)
      return null;
    headFw();
    tailFw();
    return poll;
  }

  public E remove() {
    final E remove = peek();
    if (remove == null)
      return null;
    elements[head] = null;
    headFw();
    return remove;
  }



  public void clear() {
    if (!isEmpty())
      for (int i = head; i != tail; ++i, i &= mask)
        elements[i] = null;
    head = tail = 0;
  }



  public Iterator<E> iterator() {
    return new Itr();
  }

  public Spliterator<E> spliterator() {
    return new RSQ.Splitr<>(this, -1, -1);
  }



  private void headFw() {
    ++head;
    head &= mask;
  }

  private void tailFw() {
    ++tail;
    tail &= mask;
  }

  private void tailBw() {
    --tail;
    tail &= mask;
  }



  private void doubleCap() {
    assert head == tail;

    final int oldcap = elements.length;
    final int newcap = oldcap << 1;

    if (newcap < 0)
      throw new IllegalStateException("Size too big"); //$NON-NLS-1$

    final E[] newarr = (E[]) new Object[newcap];
    final int remain = oldcap - head; // elements between head and array end
    arraycopy(elements, head, newarr, 0, remain);
    arraycopy(elements, 0, newarr, remain, head);

    elements = newarr;
    mask = newarr.length - 1;
    head = 0;
    tail = oldcap;
  }



  boolean delete(final int i) {
    final int front = i - head & mask;
    final int back = tail - i & mask;

    if (front >= (tail - head & mask))
      throw new ConcurrentModificationException();

    return front < back ?
        deleteFw(i, front) :
        deleteBw(i, back);
  }

  private boolean deleteFw(final int i, final int front) {
    if (head <= i)
      arraycopy(elements, head, elements, head + 1, front);
    else { // wrap around
      arraycopy(elements, 0, elements, 1, i);
      elements[0] = elements[mask];
      arraycopy(elements, head, elements, head + 1, mask - head);
    }
    elements[head] = null;
    headFw();
    return false;
  }

  private boolean deleteBw(final int i, final int back) {
    if (i < tail) { // copy the null tail as well
      arraycopy(elements, i + 1, elements, i, back);
      --tail;
    } else { // wrap around
      arraycopy(elements, i + 1, elements, i, mask - i);
      elements[mask] = elements[0];
      arraycopy(elements, 1, elements, 0, tail);
      tailBw();
    }
    return true;
  }



  private static int computeCap(final int cap) {
    int initcap = MinInitCap;
    if (cap >= initcap) { // find best power of two for capacity
      initcap = cap;
      initcap |= initcap >>> 1;
      initcap |= initcap >>> 2;
      initcap |= initcap >>> 4;
      initcap |= initcap >>> 8;
      initcap |= initcap >>> 16;
      initcap++;
      if (initcap < 0) // too many
        initcap >>>= 1; // 2 ^ 30 elements
    }
    return initcap;
  }



  private void writeObject(final java.io.ObjectOutputStream s) throws java.io.IOException {
    s.defaultWriteObject();
    final int size = size();
    s.writeInt(size);
    for (int i = head; i != tail; ++i, i &= mask)
      s.writeObject(elements[i]);
  }

  private void readObject(final java.io.ObjectInputStream s) throws java.io.IOException, ClassNotFoundException {
    s.defaultReadObject();
    final int size = s.readInt();
    elements = (E[]) new Object[computeCap(size)];
    mask = elements.length - 1;
    head = 0;
    tail = size;
    for (int i = 0; i < size; i++)
      elements[i] = (E) s.readObject();
  }



  private class Itr implements Iterator<E> {

    private int cursor = head;
    private int fence = tail;
    private int last = -1;



    Itr() {}



    @Override
    public boolean hasNext() {
      return cursor != fence;
    }

    @Override
    public E next() {
      if (cursor == fence)
        throw new NoSuchElementException();
      final E next = elements[cursor];
      if (tail != fence || next == null)
        throw new ConcurrentModificationException();
      last = cursor;
      cursorFw();
      return next;
    }

    @Override
    public void remove() {
      if (last < 0)
        throw new IllegalStateException();
      final boolean bw = delete(last);
      last = -1;
      if (bw) { // if delete shifts backwards, undo increment in next()
        cursorBw();
        fence = tail;
      }
    }

    @Override
    public void forEachRemaining(final Consumer<? super E> action) {
      requireNonNull(action);
      while (cursor != fence) {
        final E next = elements[cursor];
        if (next == null)
          throw new ConcurrentModificationException();
        cursorFw();
        action.accept(next);
      }
    }



    private void cursorFw() {
      ++cursor;
      cursor &= mask;
    }

    private void cursorBw() {
      --cursor;
      cursor &= mask;
    }
  }


  private static final class Splitr<E> implements Spliterator<E> {

    private final RSQ<E> queue;
    private int fence; // -1 until first use
    private int index; // current index, modified on traverse/split



    Splitr(final RSQ<E> queue, final int origin, final int fence) {
      this.queue = queue;
      this.index = origin;
      this.fence = fence;
    }



    @Override
    public long estimateSize() {
      int n = getFence() - index;
      if (n < 0)
        n += queue.elements.length;
      return n;
    }



    @Override
    public Spliterator<E> trySplit() {
      int t = getFence();
      final int h = index;
      final int n = queue.elements.length;
      if (h != t && (h + 1 & n - 1) != t) {
        if (h > t)
          t += n;
        final int m = h + t >>> 1 & n - 1;
        return new RSQ.Splitr<>(queue, h, index = m);
      }
      return null;
    }


    @Override
    public boolean tryAdvance(final Consumer<? super E> consumer) {
      if (consumer == null)
        throw new NullPointerException();
      final E[] a = queue.elements;
      final int m = a.length - 1;
      final int f = getFence();
      final int i = index;
      if (i != fence) {
        final E e = a[i];
        index = i + 1 & m;
        if (e == null)
          throw new ConcurrentModificationException();
        consumer.accept(e);
        return true;
      }
      return false;
    }



    @Override
    public void forEachRemaining(final Consumer<? super E> action) {
      requireNonNull(action);
      final E[] a = queue.elements;
      final int m = a.length - 1;
      final int f = getFence();
      int i = index;
      index = f;
      while (i != f) {
        final E e = a[i];
        i = i + 1 & m;
        if (e == null)
          throw new ConcurrentModificationException();
        action.accept(e);
      }
    }



    @Override
    public int characteristics() {
      return Spliterator.ORDERED | Spliterator.SIZED | Spliterator.NONNULL | Spliterator.SUBSIZED;
    }



    private int getFence() { // force initialization
      int t;
      if ((t = fence) < 0) {
        t = fence = queue.tail;
        index = queue.head;
      }
      return t;
    }
  }
}
