package io.wouter.lib.rsq;

import static java.util.Objects.requireNonNull;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * RSQueue works as a Queue that tests upon retrieval whether an element should be removed, shifted back towards the end of the queue, or
 * actually retrieved. Elements removed or shifted are not retrievable. Elements shifted may still become retrievable later.<br>
 * <br>
 * An RSQueue without retrievable elements is not necessarily empty and a non-empty RSQueue not necessarily contains retrievable elements.
 * Due to this nature, it is unknown beforehand whether the RSQueue contains retrievable elements.<br>
 * <br>
 * Multiple instances with shared elements and (partially) overlapping filters may interfere with each other.<br>
 * RSQueue does not accept null elements, as it already uses null to signal that an element is not retrievable.<br>
 *
 * @param <E>
 */

public final class RSQueue<E> implements Queue<E> {

  public static <E> RSQueue<E> newRSQueue(final Predicate<? super E> remove, final Predicate<? super E> shift) {
    return newRSQueue(new ArrayDeque<>(), e -> {/* empty */}, remove, shift);
  }

  public static <E> RSQueue<E> newRSQueue(final Consumer<? super E> update, final Predicate<? super E> remove, final Predicate<? super E> shift) {
    return newRSQueue(new ArrayDeque<>(), update, remove, shift);
  }

  public static <E> RSQueue<E> newRSQueue(final Queue<E> queue, final Predicate<? super E> remove, final Predicate<? super E> shift) {
    return newRSQueue(queue, e -> {/* empty */}, remove, shift);
  }

  public static <E> RSQueue<E> newRSQueue(final Queue<E> queue, final Consumer<? super E> update, final Predicate<? super E> remove, final Predicate<? super E> shift) {
    return new RSQueue<>(queue, update, remove, shift);
  }



  private final Queue<E> queue;
  private final Consumer<? super E> update;
  private final Predicate<? super E> remove;
  private final Predicate<? super E> shift;



  private RSQueue(final Queue<E> queue, final Consumer<? super E> update, final Predicate<? super E> remove, final Predicate<? super E> shift) {
    this.queue = requireAllNonNull(queue);
    this.update = requireNonNull(update);
    this.remove = requireNonNull(remove);
    this.shift = requireNonNull(shift);
  }



  /**
   * Retrieves the next retrievable element of this queue and then shifts it to the end of the queue.<br>
   * Returns null if this queue is empty or has no retrievable elements.<br>
   * That is, all elements were elligable for removal or shifting according to the given predicates.<br>
   * Note that an RSQueue without retrievable elements is not necessarily empty.<br>
   *
   * @return First retrievable element. Null if queue empty or no elements retrievable.
   */

  public E findAndPoll() {
    final E head = findAndPeek();
    if (head == null)
      return null;
    queue.remove(); // shifts head ...
    queue.add(head); // ... to tail
    return head;
  }

  /**
   * Retrieves the next retrievable element of this queue but does not shift it to the end of the queue.<br>
   * Returns null if this queue is empty or has no retrievable elements.<br>
   * That is, all elements were elligable for removal or shifting according to the given predicates.<br>
   * Note that an RSQueue without retrievable elements is not necessarily empty.<br>
   *
   * @return First retrievable element. Null if queue empty or no elements retrievable.
   */

  public E findAndPeek() {
    final int size = queue.size(); // size must be fixed during for-loop
    for (int i = 0; i < size; ++i) { // index loop prevents perpetual loop and concurrent modification
      final E head = peek();
      if (head != null)
        return head;
    }
    return null;
  }



  /**
   * Shifts the head to the end of the queue.
   */

  public void shift() {
    final E head = queue.poll(); // shifts head ...
    if (head != null)
      queue.offer(head); // ... to tail
  }



  /**
   * Removes all elements elligable for removal.
   *
   * @return Whether any element was removed.
   */

  public boolean clean() {
    return queue.removeIf(remove);
  }



  /**
   * Note that a non-empty RSQueue not necessarily contains retrievable elements.<br>
   *
   * @see java.util.Collection#isEmpty()
   */

  @Override
  public boolean isEmpty() {
    return queue.isEmpty();
  }

  /**
   * Note that a non-empty RSQueue not necessarily contains retrievable elements.<br>
   *
   * @see java.util.Collection#size()
   */

  @Override
  public int size() {
    return queue.size();
  }

  /*
   * (non-Javadoc)
   * @see java.util.Collection#contains(java.lang.Object)
   */

  @Override
  public boolean contains(final Object o) {
    return queue.contains(o);
  }

  /*
   * (non-Javadoc)
   * @see java.util.Collection#containsAll(java.util.Collection)
   */

  @Override
  public boolean containsAll(final Collection<?> c) {
    return queue.containsAll(c);
  }

  /*
   * (non-Javadoc)
   * @see java.util.Queue#remove()
   */

  @Override
  public E remove() {
    return queue.remove();
  }

  /**
   * Retrieves the head of this queue and then shifts it to the end of the queue.<br>
   * Returns null if the head is not retrievable.<br>
   * That is, the head was elligable for removal or shifting according to the given predicates.<br>
   *
   * @return First element. Null if queue empty or element not retrievable.
   * @see java.util.Queue#poll()
   */

  @Override
  public E poll() {
    final E head = peek();
    if (head == null)
      return null;
    queue.remove(); // shifts head ...
    queue.add(head); // ... to tail
    return head;
  }

  /*
   * (non-Javadoc)
   * @see java.util.Queue#element()
   */

  @Override
  public E element() {
    return queue.element();
  }

  /**
   * Retrieves the head of this queue but does not shift it to the end of the queue.<br>
   * Returns null if the head is not retrievable.<br>
   * That is, the head was elligable for removal or shifting according to the given predicates.<br>
   *
   * @return First element. Null if queue empty or element not retrievable.
   * @see java.util.Queue#peek()
   */

  @Override
  public E peek() {
    final E head = queue.peek();

    if (head == null)
      return null;

    update.accept(head);

    if (remove.test(head)) {
      queue.remove(); // removes head
      return null;
    }

    if (shift.test(head)) {
      queue.remove(); // shifts head ...
      queue.add(head); // ... to tail
      return null;
    }

    return head;
  }

  /*
   * (non-Javadoc)
   * @see java.util.Queue#add(java.lang.Object)
   */

  @Override
  public boolean add(final E e) {
    return queue.add(requireNonNull(e));
  }

  /*
   * (non-Javadoc)
   * @see java.util.Queue#offer(java.lang.Object)
   */

  @Override
  public boolean offer(final E e) {
    return queue.offer(requireNonNull(e));
  }

  /*
   * (non-Javadoc)
   * @see java.util.Collection#remove(java.lang.Object)
   */

  @Override
  public boolean remove(final Object o) {
    return queue.remove(o);
  }



  /*
   * (non-Javadoc)
   * @see java.util.Collection#addAll(java.util.Collection)
   */

  @Override
  public boolean addAll(final Collection<? extends E> c) {
    return queue.addAll(requireAllNonNull(c));
  }

  /*
   * (non-Javadoc)
   * @see java.util.Collection#removeAll(java.util.Collection)
   */

  @Override
  public boolean removeAll(final Collection<?> c) {
    return queue.removeAll(c);
  }

  /*
   * (non-Javadoc)
   * @see java.util.Collection#retainAll(java.util.Collection)
   */

  @Override
  public boolean retainAll(final Collection<?> c) {
    return queue.retainAll(c);
  }

  /*
   * (non-Javadoc)
   * @see java.util.Collection#removeIf(java.util.function.Predicate)
   */

  @Override
  public boolean removeIf(final Predicate<? super E> filter) {
    return queue.removeIf(filter);
  }



  /*
   * (non-Javadoc)
   * @see java.util.Collection#clear()
   */

  @Override
  public void clear() {
    queue.clear();
  }



  /*
   * (non-Javadoc)
   * @see java.util.Collection#iterator()
   */

  @Override
  public Iterator<E> iterator() {
    return queue.iterator();
  }

  /*
   * (non-Javadoc)
   * @see java.util.Collection#toArray()
   */

  @Override
  public Object[] toArray() {
    return queue.toArray();
  }

  /*
   * (non-Javadoc)
   * @see java.util.Collection#toArray(java.lang.Object[])
   */

  @Override
  public <T> T[] toArray(final T[] a) {
    return queue.toArray(a);
  }



  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */

  @Override
  public String toString() {
    return queue.toString();
  }



  private <T extends Collection<? extends E>> T requireAllNonNull(final T c) {
    c.forEach(Objects::requireNonNull);
    return c;
  }
}
