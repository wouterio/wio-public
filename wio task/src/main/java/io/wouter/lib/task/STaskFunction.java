package io.wouter.lib.task;

import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Working;

public interface STaskFunction<C, A> extends TaskFunction<A> {

  /**
   * Perform task.
   * <p>
   * If status is Pending, task is initialized using {@link #init(Object)}. Status becomes {@link #init(Object)}'s return value.
   * <p>
   * If status then is Working, task is performed using {@link #step(Object)}. Status becomes {@link #step(Object)}'s return value.
   * <p>
   * If status then is not active, task is finished up using {@link #exit(Object)}. Status becomes {@link #exit(Object)}'s return value.
   * <p>
   * A task may consecutively be initialized, performed, and finished in just one call.
   * <p>
   * Setting a task's status to Suspended also results in calling {@link #exit(Object)}. A suspended task remains available.
   *
   * @param status Current task status.
   * @param agent Agent performing task.
   * @param delta Time delta
   * @return status.
   */

  @Override
  default TaskStatus apply(final TaskStatus status, final A agent, final float delta) {
    // XXX Returning immediatly if not active would cause problems with finishing collective tasks.

    TaskStatus newstatus = status;

    if (newstatus == Pending)
      newstatus = init(agent);

    if (newstatus == Working)
      newstatus = step(agent, delta);

    if (!newstatus.isActive()) // includes Suspended, Expired, Completed
      newstatus = exit(agent);

    return newstatus;
  }



  /**
   * Initialize task. For instance, check whether task can be performed.
   *
   * @param agent Agent performing task
   * @return Task status after initializating
   */

  TaskStatus init(A agent);

  /**
   * Perform step in task.
   *
   * @param agent Agent performing task
   * @param delta Time delta
   * @return Task status after performing step
   */

  TaskStatus step(A agent, float delta);

  /**
   * Exit task. After task status turned inactive, exit is invoked to wrap up.
   *
   * @param agent Agent performing task
   * @return Task status after exiting
   */

  TaskStatus exit(A agent);
}
