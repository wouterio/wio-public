package io.wouter.lib.task;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;

final class TaskI<C, A> implements Task<C, A>, Serializable { // default serialization form

  private static final long serialVersionUID = -1242938478515245689L;



  private final TaskFunction<A> function;
  private final C client;



  private TaskStatus status;



  TaskI(final TaskFunction<A> function, final C client, final TaskStatus status) {
    this.function = requireNonNull(function);
    this.client = client;
    this.status = requireNonNull(status);
  }



  @Override
  public C getClient() {
    return client;
  }



  @Override
  public TaskStatus getStatus() {
    return status;
  }

  @Override
  public boolean setStatus(final TaskStatus newstatus) {
    if (newstatus.equals(status))
      return false;
    status = status.checkValidTransition(newstatus);
    return true;
  }



  @Override
  public Task<C, A> perform(final A agent, final float delta) {
    final TaskStatus newstatus = function.apply(status, agent, delta);
    setStatus(newstatus);
    return this;
  }



  @Override
  public String toString() {
    return new StringBuilder(getClass().getSimpleName()) //
        .append('-').append(client) //
        .append('-').append(status) //
        .toString();
  }
}
