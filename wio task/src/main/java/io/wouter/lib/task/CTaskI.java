package io.wouter.lib.task;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

final class CTaskI<C, A> implements CTask<C, A>, Serializable { // default serialization form

  private static final long serialVersionUID = -9173182610153155674L;



  private final Task<C, A> subtask;
  private final Function<A, Object> getagentid;
  private final Set<Object> agentids = new HashSet<>();



  private int minagents;
  private int maxagents;



  CTaskI(final Task<C, A> subtask, final Function<A, Object> getagentid, final int minagents, final int maxagents) {
    checkMinMaxAgents(minagents, maxagents);
    this.subtask = requireNonNull(subtask);
    this.getagentid = requireNonNull(getagentid);
    this.minagents = minagents;
    this.maxagents = maxagents;
  }



  @Override
  public C getClient() {
    return subtask.getClient();
  }



  @Override
  public boolean isPresent() {
    return subtask.isPresent();
  }

  @Override
  public boolean isActive() {
    return subtask.isActive();
  }



  @Override
  public TaskStatus getStatus() {
    return subtask.getStatus();
  }

  @Override
  public boolean setStatus(final TaskStatus newstatus) {
    return subtask.setStatus(newstatus);
  }



  @Override
  public boolean hasMinAgents() {
    return agentids.size() >= minagents;
  }

  @Override
  public boolean hasMaxAgents() {
    return agentids.size() >= maxagents;
  }

  @Override
  public int getMinAgents() {
    return minagents;
  }

  @Override
  public int getMaxAgents() {
    return maxagents;
  }

  @Override
  public void setMinAgents(final int minagents) {
    checkMinMaxAgents(minagents, maxagents);
    this.minagents = minagents;
  }

  @Override
  public void setMaxAgents(final int maxagents) {
    checkMinMaxAgents(minagents, maxagents);
    this.maxagents = maxagents;
  }

  @Override
  public int getAgents() {
    return agentids.size();
  }



  @Override
  public boolean register(final A agent) {
    checkMaxAgents();
    return agentids.add(getagentid.apply(agent));
  }

  @Override
  public boolean deregister(final A agent) {
    return agentids.remove(getagentid.apply(agent));
  }

  @Override
  public boolean isRegistered(final A agent) {
    return agentids.contains(getagentid.apply(agent));
  }



  @Override
  public CTask<C, A> perform(final A agent, final float delta) {
    checkRegistered(agent);
    if (hasMinAgents())
      subtask.perform(agent, delta);
    return this;
  }



  @Override
  public String toString() {
    return new StringBuilder(CTaskI.class.getSimpleName()) //
        .append('-').append(minagents).append("<=").append(agentids.size()).append("<=").append(maxagents) //$NON-NLS-1$ //$NON-NLS-2$
        .append('-').append(subtask) //
        .toString(); //
  }



  private static void checkMinMaxAgents(final int minagents, final int maxagents) {
    if (minagents > maxagents || maxagents < 1)
      throw new IllegalArgumentException("Illegal values for minimum and maximum agents."); //$NON-NLS-1$
  }

  private void checkMaxAgents() {
    if (hasMaxAgents())
      throw new IllegalStateException("This task already has maximum agents."); //$NON-NLS-1$
  }

  private void checkRegistered(final A agent) {
    if (!isRegistered(agent))
      throw new IllegalStateException("Unregistered agents may not perform this task."); //$NON-NLS-1$
  }
}
