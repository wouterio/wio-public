package io.wouter.lib.task;

import static io.wouter.lib.task.TaskStatus.Completed;
import static io.wouter.lib.task.TaskStatus.Working;
import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;

import io.wouter.lib.rsq.RSQueue;

final class QTaskI<C, A> implements QTask<C, A>, Serializable { // default serialization form

  private static final long serialVersionUID = -7436881147027117972L;



  private final RSQueue<Task<C, A>> subtasks;
  private final C client;



  private TaskStatus status;
  private Task<C, A> peek = null;
  private boolean shouldpeek = true;



  QTaskI(final RSQueue<Task<C, A>> subtasks, final C client, final TaskStatus status) {
    this.client = client;
    this.subtasks = checkTasks(subtasks);
    this.status = requireNonNull(status);
  }



  // Task methods



  @Override
  public C getClient() {
    return client;
  }



  @Override
  public TaskStatus getStatus() {
    return status;
  }

  @Override
  public boolean setStatus(final TaskStatus newstatus) {
    if (newstatus.equals(status))
      return false;
    status = status.checkValidTransition(newstatus);
    return true;
  }



  @Override
  public boolean isActive() {
    if (!QTask.super.isActive())
      return false;

    if (!hasPeek()) // no check for shouldpeek here
      peek = subtasks.findAndPeek();

    shouldpeek = false;
    return hasPeek() || subtasks.isEmpty();
  }



  @Override
  public Task<C, A> perform(final A agent, final float delta) {
    if (!QTask.super.isActive()) {
      peek = null;
      shouldpeek = true;
      return this;
    }

    if (shouldpeek)
      peek = subtasks.findAndPeek();

    if (!hasPeek()) {
      if (subtasks.isEmpty())
        status = Completed;
      peek = null;
      shouldpeek = true;
      return this;
    }

    status = Working;
    if (peek.perform(agent, delta).isActive()) { // peek is still available ...
      shouldpeek = false; // ... no need to peek next round
      return this;
    }

    // since peek is not available anymore ...
    peek = subtasks.findAndPeek(); // ... always peek here

    if (!hasPeek()) {
      if (subtasks.isEmpty())
        status = Completed;
      peek = null;
      shouldpeek = true;
      return this;
    }

    // new peek is still available ...
    shouldpeek = false; // ... no need to peek next round
    return this;
  }



  private boolean hasPeek() {
    return peek != null && peek.isActive();
  }



  // Queue methods



  @Override
  public boolean isEmpty() {
    return subtasks.isEmpty();
  }

  @Override
  public boolean contains(final Object o) {
    return subtasks.contains(o);
  }

  @Override
  public boolean containsAll(final Collection<?> c) {
    return subtasks.containsAll(c);
  }

  @Override
  public int size() {
    return subtasks.size();
  }



  @Override
  public boolean offer(final Task<C, A> t) {
    return QTask.super.isPresent() && subtasks.offer(checkTask(t)); // do not add if task not present
  }

  @Override
  public boolean add(final Task<C, A> t) {
    return QTask.super.isPresent() && subtasks.add(checkTask(t)); // do not add if task not present
  }

  @Override
  public boolean addAll(final Collection<? extends Task<C, A>> c) {
    return QTask.super.isPresent() && subtasks.addAll(checkTasks(c)); // do not add if task not present
  }



  @Override
  public Task<C, A> peek() {
    return subtasks.findAndPeek();
  }

  @Override
  public Task<C, A> poll() {
    return subtasks.findAndPoll();
  }



  @Override
  public Task<C, A> element() {
    return subtasks.element();
  }

  @Override
  public Task<C, A> remove() {
    return subtasks.remove();
  }



  @Override
  public boolean remove(final Object o) {
    return subtasks.remove(o);
  }

  @Override
  public boolean removeAll(final Collection<?> c) {
    return subtasks.removeAll(c);
  }

  @Override
  public boolean retainAll(final Collection<?> c) {
    return subtasks.retainAll(c);
  }

  @Override
  public void clear() {
    subtasks.clear();
  }



  @Override
  public Iterator<Task<C, A>> iterator() {
    return subtasks.iterator();
  }

  @Override
  public Object[] toArray() {
    return subtasks.toArray();
  }

  @Override
  public <T> T[] toArray(final T[] array) {
    return subtasks.toArray(array);
  }



  private Task<C, A> checkTask(final Task<C, A> task) {
    if (!Objects.equals(client, task.getClient()))
      throw new IllegalArgumentException("Cannot add task with different client: " + client + " - " + task.getClient()); //$NON-NLS-1$
    return task;
  }

  private <T extends Iterable<? extends Task<C, A>>> T checkTasks(final T tasks) {
    for (final Task<C, A> t : tasks)
      checkTask(t);
    return tasks;
  }



  // Object methods



  @Override
  public String toString() {
    return new StringBuilder(QTaskI.class.getSimpleName()) //
        .append('-').append(client) //
        .append('-').append(status) //
        .append('-').append(subtasks) //
        .toString(); //
  }
}
