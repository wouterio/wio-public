package io.wouter.lib.task;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

/**
 * Status of task.
 */

public enum TaskStatus {

  /**
   * Pending: No one is working on the task yet.
   */

  Pending(true, true),

  /**
   * Working: Someone is working on the task.
   */

  Working(true, true),

  /**
   * Suspended: The task was suspended.
   */

  Suspended(true, false),

  /**
   * Completed: The task completed succesfully.
   */

  Completed(false, false),

  /**
   * Failed: The task failed.
   */

  Failed(true, false),

  /**
   * Expired: The task became unneeded.
   */

  Expired(false, false);



  private static Map<TaskStatus, Set<TaskStatus>> transitions = new EnumMap<>(TaskStatus.class);

  static {
    transitions.put(Pending, EnumSet.of(Pending, Working, Suspended, Completed, Failed, Expired));
    transitions.put(Working, EnumSet.of(Working, Suspended, Completed, Failed, Expired));
    transitions.put(Suspended, EnumSet.of(Suspended, Pending, Expired));
    transitions.put(Completed, EnumSet.of(Completed));
    transitions.put(Failed, EnumSet.of(Failed, Pending, Expired));
    transitions.put(Expired, EnumSet.of(Expired));
  }



  private final boolean present;
  private final boolean active;



  private TaskStatus(final boolean present, final boolean active) {
    this.present = present;
    this.active = active;
  }



  /**
   * @return Whether task is present (Pending, Working, Suspended) or not (Expired, Failed, Completed).
   */

  public boolean isPresent() {
    return present;
  }

  /**
   * @return Whether task is active (Pending, Working) or not (Suspended, Expired, Failed, Completed).
   */

  public boolean isActive() {
    return active;
  }



  /**
   * Valid transitions are:<br>
   * - Pending => Pending, Working, Suspended, Expired<br>
   * - Working => Working, Suspended, Completed, Failed, Expired<br>
   * - Suspended => Suspended, Pending, Expired<br>
   * - Completed => Completed<br>
   * - Failed => Failed, Pending, Expired<br>
   * - Expired => Expired<br>
   *
   * @param next
   * @return Whether transition from this status to next status is valid.
   * @see TaskStatus#checkValidTransition(TaskStatus)
   */

  public boolean isValidTransition(final TaskStatus next) {
    return transitions.get(this).contains(next);
  }

  /**
   * Valid transitions are:<br>
   * - Pending => Pending, Working, Suspended, Expired<br>
   * - Working => Working, Suspended, Completed, Failed, Expired<br>
   * - Suspended => Suspended, Pending, Expired<br>
   * - Completed => Completed<br>
   * - Failed => Failed, Pending, Expired<br>
   * - Expired => Expired<br>
   *
   * @param next
   * @return parameter next
   * @throws IllegalStateException If transition from this status to next status is invalid.
   * @see TaskStatus#isValidTransition(TaskStatus)
   */

  public TaskStatus checkValidTransition(final TaskStatus next) {
    if (!isValidTransition(next))
      throw new IllegalStateException("Illegal transition: " + this + " -> " + next); //$NON-NLS-1$ //$NON-NLS-2$
    return next;
  }
}
