package io.wouter.lib.task;

import static io.wouter.lib.rsq.RSQueue.newRSQueue;
import static io.wouter.lib.task.TaskStatus.Pending;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;
import java.util.function.Function;

import io.wouter.lib.rsq.RSQueue;

public final class Tasks {

  /**
   * Task with no client and task status Pending.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param function
   * @return New task
   */

  public static <C, A> Task<C, A> newTask(final TaskFunction<A> function) {
    return newTask(function, null, Pending);
  }

  /**
   * Task with no client and provided task status.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param function
   * @param status
   * @return New task
   */

  public static <C, A> Task<C, A> newTask(final TaskFunction<A> function, final TaskStatus status) {
    return newTask(function, null, status);
  }

  /**
   * Task with provided client and task status Pending.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param function
   * @param client
   * @return New task
   */

  public static <C, A> Task<C, A> newTask(final TaskFunction<A> function, final C client) {
    return newTask(function, client, Pending);
  }

  /**
   * Task with provided client and task status.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param function
   * @param client
   * @param status
   * @return New task
   */

  public static <C, A> Task<C, A> newTask(final TaskFunction<A> function, final C client, final TaskStatus status) {
    return new TaskI<>(function, client, status);
  }



  /**
   * Queue task with {@link ArrayDeque} and provided client and task status. Does not accept null elements.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param client
   * @param status
   * @return New queue task
   */

  public static <C, A> QTask<C, A> newQueueTask(final C client, final TaskStatus status) {
    return newQueueTask(newTaskDistribQueue(), client, status);
  }

  /**
   * Queue task for provided subtask collection with provided client and task status. Subtasks will be added to an RSQueue. Does not accept
   * null elements.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param subtasks
   * @param client
   * @param status
   * @return New queue task
   */

  public static <C, A> QTask<C, A> newQueueTask(final Collection<Task<C, A>> subtasks, final C client, final TaskStatus status) {
    return newQueueTask(newTaskDistribQueue(subtasks), client, status);
  }

  /**
   * Queue task for provided subtask queue with provided client and task status. Subtask queue will be wrapped in an RSQueue. Does not
   * accept null elements.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param subtasks
   * @param client
   * @param status
   * @return New queue task
   */

  public static <C, A> QTask<C, A> newQueueTask(final Queue<Task<C, A>> subtasks, final C client, final TaskStatus status) {
    return newQueueTask(newTaskDistribQueue(subtasks), client, status);
  }

  /**
   * Queue task for provided subtask queue with provided client and task status. Does not accept null elements.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param subtasks
   * @param client
   * @param status
   * @return New queue task
   */

  public static <C, A> QTask<C, A> newQueueTask(final RSQueue<Task<C, A>> subtasks, final C client, final TaskStatus status) {
    return new QTaskI<>(subtasks, client, status);
  }



  /**
   * Collective task for the provided subtask with provided minimum and maximum agents.
   *
   * @param <C> client type
   * @param <A> agent type
   * @param subtask
   * @param getagentid
   * @param minagents
   * @param maxagents
   * @return New collective task
   */

  public static <C, A> CTask<C, A> newCollectiveTask(final Task<C, A> subtask, final Function<A, Object> getagentid, final int minagents, final int maxagents) {
    return new CTaskI<>(subtask, getagentid, minagents, maxagents);
  }


  /**
   * Collective task for the provided subtasks with provided minimum and maximum agents. Creates an intermediate queue task if and only if
   * more than one subtask. Does not accept empty subtasks. Does not accept null subtasks.
   * @param getagentid
   * @param minagents
   * @param maxagents
   * @param status queue task status
   * @param subtask
   *
   * @param <C> client type
   * @param <A> agent type
   * @return New collective task
   */

  public static <C, A> CTask<C, A> newCollectiveTask(final Collection<Task<C, A>> subtasks, final Function<A, Object> getagentid, final int minagents, final int maxagents, final TaskStatus status) {
    if (subtasks.size() == 1)
      return newCollectiveTask(subtasks.iterator().next(), getagentid, minagents, maxagents);
    if (subtasks.size() > 1)
      return newCollectiveTask(newQueueTask(subtasks, subtasks.iterator().next().getClient(), status), getagentid, minagents, maxagents);
    throw new IllegalArgumentException("subtasks should not be empty"); //$NON-NLS-1$
  }



  /**
   * Task distribution queue with {@link ArrayDeque}. Removes task if task is set to not present. Shifts task to back of the queue if not
   * active.
   *
   * @param <T> task type
   * @return New task distribution queue
   * @see TaskBase#isPresent()
   * @see TaskBase#isActive()
   */

  public static <T extends TaskBase> RSQueue<T> newTaskDistribQueue() {
    return newTaskDistribQueue(new ArrayDeque<>());
  }

  /**
   * Task distribution queue with {@link ArrayDeque}. Removes task if task is set to not present. Shifts task to back of the queue if not
   * active.
   *
   * @param <T> task type
   * @param tasks
   * @return New task distribution queue
   * @see TaskBase#isPresent()
   * @see TaskBase#isActive()
   */

  public static <T extends TaskBase> RSQueue<T> newTaskDistribQueue(final Collection<T> tasks) {
    return newTaskDistribQueue(new ArrayDeque<>(tasks));
  }

  /**
   * Task distribution queue wrapping provided queue. Removes task if task is set to not present. Shifts task to back of the queue if not
   * active.
   *
   * @param <T> task type
   * @param q
   * @return New task distribution queue
   * @see TaskBase#isPresent()
   * @see TaskBase#isActive()
   */

  public static <T extends TaskBase> RSQueue<T> newTaskDistribQueue(final Queue<T> q) {
    return newRSQueue(q, t -> !t.isPresent(), t -> !t.isActive());
  }



  /**
   * Collective task distribution queue with {@link ArrayDeque}. Removes task if task is set to not present. Shifts task to back of the
   * queue if not available.
   *
   * @param <T> collective task type
   * @return New task distribution queue
   * @see TaskBase#isPresent()
   * @see TaskBase#isAvailable()
   */

  public static <T extends CTask<?, ?>> RSQueue<T> newCollectiveTaskDistribQueue() {
    return newCollectiveTaskDistribQueue(new ArrayDeque<>());
  }

  /**
   * Collective task distribution queue with {@link ArrayDeque}. Removes task if task is set to not present. Shifts task to back of the
   * queue if not available.
   *
   * @param <T> collective task type
   * @param tasks
   * @return New task distribution queue
   * @see TaskBase#isPresent()
   * @see TaskBase#isAvailable()
   */

  public static <T extends CTask<?, ?>> RSQueue<T> newCollectiveTaskDistribQueue(final Collection<T> tasks) {
    return newCollectiveTaskDistribQueue(new ArrayDeque<>(tasks));
  }

  /**
   * Collective task distribution queue wrapping provided queue. Removes task if task is set to not present. Shifts task to back of the
   * queue if not available.
   *
   * @param <T> collective task type
   * @param q
   * @return New task distribution queue
   * @see TaskBase#isPresent()
   * @see TaskBase#isAvailable()
   */

  public static <T extends CTask<?, ?>> RSQueue<T> newCollectiveTaskDistribQueue(final Queue<T> q) {
    return newRSQueue(q, t -> !t.isPresent(), t -> !t.isAvailable());
  }



  private Tasks() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
