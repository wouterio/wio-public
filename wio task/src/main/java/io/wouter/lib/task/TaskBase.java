package io.wouter.lib.task;

public interface TaskBase {

  /**
   * @return Whether task is present. Default implementation checks if task status is present (Pending, Working, Suspended) or not (Expired, Failed, Completed).
   * @see TaskBase#getStatus()
   * @see TaskStatus#isPresent()
   */

  default boolean isPresent() {
    return getStatus().isPresent();
  }

  /**
   * @return Whether task is active. Default implementation checks if task status is active (Pending, Working) or not (Suspended, Expired, Failed, Completed).
   * @see TaskBase#getStatus()
   * @see TaskStatus#isActive()
   */

  default boolean isActive() {
    return getStatus().isActive();
  }



  /**
   * @return Task status.
   */

  TaskStatus getStatus();

  /**
   * @param newstatus New task status.
   * @return Whether task status changed.
   */

  boolean setStatus(TaskStatus newstatus);
}
