package io.wouter.lib.task;

@FunctionalInterface
public interface TaskFunction<A> {

  TaskStatus apply(TaskStatus status, A agent, float delta);
}
