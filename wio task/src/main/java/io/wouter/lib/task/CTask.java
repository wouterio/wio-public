package io.wouter.lib.task;

/**
 * Collective task. The CTask interface purposely does not extend the Task interface, because the work flow is different.<br>
 * This prevents adding a CTask to a QTask.
 *
 * @param <C>
 * @param <A>
 */

public interface CTask<C, A> extends TaskBase {

  /**
   * @return Task client.
   */

  C getClient();



  /**
   * @return Whether minimum amount of agents has registred for this task.
   */

  boolean hasMinAgents();

  /**
   * @return Whether maximum amount of agents has registred for this task.
   */

  boolean hasMaxAgents();

  /**
   * @return Minimum amount of agents to perform this task.
   */

  int getMinAgents();

  /**
   * @return Maximum amount of agents to perform this task.
   */

  int getMaxAgents();

  /**
   * Set minimum amount of agents to perform this task.
   *
   * @param minagents
   */

  void setMinAgents(int minagents);

  /**
   * Set maximum amount of agents to perform this task.
   *
   * @param maxagents
   */

  void setMaxAgents(int maxagents);

  /**
   * @return Amount of agents performing this task.
   */

  int getAgents();



  /**
   * @return Whether this task is available. Default implementation returns <code>!hasMaxAgents() && isActive()</code>.
   */

  default boolean isAvailable() {
    return !hasMaxAgents() && isActive();
  }



  /**
   * Register agent for task.
   *
   * @param agent Agent registering
   * @return Whether agent registered successfully
   * @throws IllegalStateException If this task already has maximum agents
   */

  boolean register(A agent);

  /**
   * Deregister agent from task.
   *
   * @param agent Agent deregistering
   * @return Whether agent deregistered successfully
   */

  boolean deregister(A agent);

  /**
   * @param agent
   * @return Whether agent is registered with this task
   */

  boolean isRegistered(A agent);



  /**
   * Perform task.
   *
   * @param agent Agent performing task
   * @param delta Time delta
   * @return This task
   * @throws IllegalStateException If an unregistered agent attempts to perform this task
   */

  CTask<C, A> perform(A agent, final float delta);
}
