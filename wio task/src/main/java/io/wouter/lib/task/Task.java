package io.wouter.lib.task;

/**
 * Task to be performed by an agent.
 *
 * @param <C> Client type
 * @param <A> Agent type
 */

public interface Task<C, A> extends TaskBase {

  /**
   * @return Task client
   */

  C getClient();



  /**
   * Perform task.
   *
   * @param agent Agent performing task
   * @param delta Time delta
   * @return This task
   */

  Task<C, A> perform(A agent, float delta);
}
