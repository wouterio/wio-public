package io.wouter.lib.task;

import java.util.Queue;

public interface QTask<C, A> extends Task<C, A>, Queue<Task<C, A>> {

}
