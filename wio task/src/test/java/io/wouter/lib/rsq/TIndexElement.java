package io.wouter.lib.rsq;

public class TIndexElement {

  private int index = 0;



  void update() {
    ++index;
  }

  boolean shift() {
    return index < 2;
  }

  boolean remove() {
    return index > 3;
  }
}
