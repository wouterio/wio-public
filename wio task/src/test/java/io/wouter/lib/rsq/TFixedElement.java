package io.wouter.lib.rsq;


public class TFixedElement {

  private final boolean remove;
  private final boolean shift;



  TFixedElement(final boolean remove, final boolean shift) {
    this.shift = shift;
    this.remove = remove;
  }


  public void update() {
    //
  }

  public boolean remove() {
    return remove;
  }

  public boolean shift() {
    return shift;
  }
}
