package io.wouter.lib.rsq;

import static org.testng.Assert.assertEquals;

import java.util.ArrayDeque;

import org.testng.annotations.Test;

import io.wouter.lib.rsq.RSQueue;

public class RSQueueTests {

  @Test
  public void findAndPeek_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e = new TFixedElement(false, false);
    rsq.add(e);

    assertEquals(rsq.findAndPeek(), e);
    assertEquals(rsq.contains(e), true);
  }

  @Test
  public void findAndPeek_NoRemoveShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e = new TFixedElement(false, true);
    rsq.add(e);

    assertEquals(rsq.findAndPeek(), null);
    assertEquals(rsq.contains(e), true);
  }

  @Test
  public void findAndPeek_RemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e = new TFixedElement(true, false);
    rsq.add(e);

    assertEquals(rsq.findAndPeek(), null);
    assertEquals(rsq.contains(e), false);
  }

  @Test
  public void findAndPeek_RemoveShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e = new TFixedElement(true, true);
    rsq.add(e);

    assertEquals(rsq.findAndPeek(), null);
    assertEquals(rsq.contains(e), false);
  }



  @Test
  public void findAndPoll_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e = new TFixedElement(false, false);
    rsq.add(e);

    assertEquals(rsq.findAndPoll(), e);
    assertEquals(rsq.contains(e), true); // still in queue but shifted back
  }

  @Test
  public void findAndPoll_NoRemoveShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e = new TFixedElement(false, true);
    rsq.add(e);

    assertEquals(rsq.findAndPoll(), null);
    assertEquals(rsq.contains(e), true);
  }

  @Test
  public void findAndPoll_RemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e = new TFixedElement(true, false);
    rsq.add(e);

    assertEquals(rsq.findAndPoll(), null);
    assertEquals(rsq.contains(e), false);
  }

  @Test
  public void findAndPoll_RemoveShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e = new TFixedElement(true, true);
    rsq.add(e);

    assertEquals(rsq.findAndPoll(), null);
    assertEquals(rsq.contains(e), false);
  }



  @Test
  public void findAndPeek_NoRemoveNoShift_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e1 = new TFixedElement(false, false);
    final TFixedElement e2 = new TFixedElement(false, false);
    rsq.add(e1);
    rsq.add(e2);

    assertEquals(rsq.findAndPeek(), e1);
    assertEquals(rsq.findAndPeek(), e1);
    assertEquals(rsq.contains(e1), true);
    assertEquals(rsq.contains(e2), true);
  }

  @Test
  public void findAndPeek_NoRemoveShift_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e1 = new TFixedElement(false, true);
    final TFixedElement e2 = new TFixedElement(false, false);
    rsq.add(e1);
    rsq.add(e2);

    assertEquals(rsq.findAndPeek(), e2);
    assertEquals(rsq.findAndPeek(), e2);
    assertEquals(rsq.contains(e1), true);
    assertEquals(rsq.contains(e2), true);
  }

  @Test
  public void findAndPeek_RemoveNoShift_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e1 = new TFixedElement(true, false);
    final TFixedElement e2 = new TFixedElement(false, false);
    rsq.add(e1);
    rsq.add(e2);

    assertEquals(rsq.findAndPeek(), e2);
    assertEquals(rsq.findAndPeek(), e2);
    assertEquals(rsq.contains(e1), false);
    assertEquals(rsq.contains(e2), true);
  }

  @Test
  public void findAndPeek_RemoveShift_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e1 = new TFixedElement(true, true);
    final TFixedElement e2 = new TFixedElement(false, false);
    rsq.add(e1);
    rsq.add(e2);

    assertEquals(rsq.findAndPeek(), e2);
    assertEquals(rsq.findAndPeek(), e2);
    assertEquals(rsq.contains(e1), false);
    assertEquals(rsq.contains(e2), true);
  }



  @Test
  public void findAndPoll_NoRemoveNoShift_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e1 = new TFixedElement(false, false);
    final TFixedElement e2 = new TFixedElement(false, false);
    rsq.add(e1);
    rsq.add(e2);

    assertEquals(rsq.findAndPoll(), e1);
    assertEquals(rsq.findAndPoll(), e2);
    assertEquals(rsq.contains(e1), true); // poll does not remove element but shifts it back to tail
    assertEquals(rsq.contains(e2), true);
  }

  @Test
  public void findAndPoll_NoRemoveShift_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e1 = new TFixedElement(false, true);
    final TFixedElement e2 = new TFixedElement(false, false);
    rsq.add(e1);
    rsq.add(e2);

    assertEquals(rsq.findAndPoll(), e2);
    assertEquals(rsq.findAndPoll(), e2);
    assertEquals(rsq.contains(e1), true);
    assertEquals(rsq.contains(e2), true);
  }

  @Test
  public void findAndPoll_RemoveNoShift_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e1 = new TFixedElement(true, false);
    final TFixedElement e2 = new TFixedElement(false, false);
    rsq.add(e1);
    rsq.add(e2);

    assertEquals(rsq.findAndPoll(), e2);
    assertEquals(rsq.findAndPoll(), e2);
    assertEquals(rsq.contains(e1), false);
    assertEquals(rsq.contains(e2), true);
  }

  @Test
  public void findAndPoll_RemoveShift_NoRemoveNoShift() {
    final RSQueue<TFixedElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TFixedElement::update, TFixedElement::remove, TFixedElement::shift);
    final TFixedElement e1 = new TFixedElement(true, true);
    final TFixedElement e2 = new TFixedElement(false, false);
    rsq.add(e1);
    rsq.add(e2);

    assertEquals(rsq.findAndPoll(), e2);
    assertEquals(rsq.findAndPoll(), e2);
    assertEquals(rsq.contains(e1), false);
    assertEquals(rsq.contains(e2), true);
  }



  @Test
  public void findAndPeek_Indexed() {
    final RSQueue<TIndexElement> rsq = RSQueue.newRSQueue(new ArrayDeque<>(), TIndexElement::update, TIndexElement::remove, TIndexElement::shift);
    final TIndexElement e = new TIndexElement();
    rsq.add(e);

    assertEquals(rsq.findAndPeek(), null); // update function increases element count
    assertEquals(rsq.contains(e), true);
    assertEquals(rsq.findAndPeek(), e);
    assertEquals(rsq.contains(e), true);
    assertEquals(rsq.findAndPeek(), e);
    assertEquals(rsq.contains(e), true);
    assertEquals(rsq.findAndPeek(), null);
    assertEquals(rsq.contains(e), false);
  }
}
