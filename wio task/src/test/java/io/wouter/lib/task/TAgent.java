package io.wouter.lib.task;

import java.util.concurrent.atomic.AtomicLong;

public class TAgent {

  private static final AtomicLong Index = new AtomicLong();



  private final long id = Index.getAndIncrement();



  private CTask<Object, TAgent> task = null;



  public TAgent() {}



  public long getId() {
    return id;
  }



  public boolean hasTask() {
    return task != null;
  }

  public void setTask(final CTask<Object, TAgent> newtask) {
    if (hasTask())
      task.deregister(this);
    if (newtask != null)
      newtask.register(this);
    this.task = newtask;
  }



  public void perform(final float delta) {
    if (hasTask() && !task.perform(this, delta).isAvailable()) {
      task.deregister(this);
      task = null;
    }
  }
}
