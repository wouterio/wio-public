package io.wouter.lib.task;

import static io.wouter.lib.task.TaskStatus.Completed;
import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Suspended;
import static io.wouter.lib.task.TaskStatus.Working;
import static io.wouter.lib.task.Tasks.newQueueTask;
import static io.wouter.lib.task.Tasks.newTask;
import static org.testng.Assert.assertEquals;

import java.util.ArrayDeque;

import org.testng.annotations.Test;

public class QTaskTests {

  @Test
  void testQTask_0SubTask_Pending_Perform_Completed() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Pending);

    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Pending);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Completed);
    assertEquals(qtask.isPresent(), false);
    assertEquals(qtask.isActive(), false);
  }

  @Test
  void testQTask_0SubTask_Working_Perform_Completed() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Working);

    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Completed);
    assertEquals(qtask.isPresent(), false);
    assertEquals(qtask.isActive(), false);
  }

  @Test
  void testQTask_1SubTask_Pending_Perform_Completed() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> task = newTask((status, agent, delta) -> Completed);
    qtask.add(task);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Pending);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Completed);
    assertEquals(qtask.isPresent(), false);
    assertEquals(qtask.isActive(), false);
  }

  @Test
  void testQTask_1SubTask_Working_Perform_Completed() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Working);
    final Task<Object, TAgent> task = newTask((status, agent, delta) -> Completed);
    qtask.add(task);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Completed);
    assertEquals(qtask.isPresent(), false);
    assertEquals(qtask.isActive(), false);
  }

  @Test
  void testQTask_2SubTask_Pending_Perform_Working() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> task1 = newTask((status, agent, delta) -> Completed);
    final Task<Object, TAgent> task2 = newTask((status, agent, delta) -> Completed);
    qtask.add(task1);
    qtask.add(task2);

    assertEquals(qtask.size(), 2);
    assertEquals(qtask.getStatus(), Pending);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
  }

  @Test
  void testQTask_2SubTask_Working_Perform_Working() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Working);
    final Task<Object, TAgent> task1 = newTask((status, agent, delta) -> Completed);
    final Task<Object, TAgent> task2 = newTask((status, agent, delta) -> Completed);
    qtask.add(task1);
    qtask.add(task2);

    assertEquals(qtask.size(), 2);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
  }



  @Test
  void testQTask_0SubTask_Suspended_Perform_Suspended() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Suspended);

    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Suspended);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false);
    qtask.perform(null, 1f); // XXX actually should not perform task that's not active
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Suspended);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false);
  }

  @Test
  void testQTask_1SubTask_Suspended_Perform_Suspended() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Suspended);
    final Task<Object, TAgent> task = newTask((status, agent, delta) -> Completed);
    qtask.add(task);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Suspended);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false);
    qtask.perform(null, 1f); // XXX actually should not perform task that's not active
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Suspended);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false);
  }



  @Test
  void testQTask_1SubTaskSuspended_Pending_Perform_Working() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> task = newTask((status, agent, delta) -> Completed, Suspended);
    qtask.add(task);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Pending);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
    qtask.perform(null, 1f); // XXX actually should not perform task that's not active
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Pending); // XXX not Working
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
  }

  @Test
  void testQTask_1SubTaskSuspended_Working_Perform_Working() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Working);
    final Task<Object, TAgent> task = newTask((status, agent, delta) -> Completed, Suspended);
    qtask.add(task);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
    qtask.perform(null, 1f); // XXX actually should not perform task that's not active
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
  }



  //



  @Test
  void testQTask_1SubQTask_0SubSubTask_Pending_Perform_Completed() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    qtask.add(subqtask);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Pending);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    assertEquals(subqtask.size(), 0);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Completed);
    assertEquals(qtask.isPresent(), false);
    assertEquals(qtask.isActive(), false);
    assertEquals(subqtask.size(), 0);
    assertEquals(subqtask.getStatus(), Completed);
    assertEquals(subqtask.isPresent(), false);
    assertEquals(subqtask.isActive(), false);
  }

  @Test
  void testQTask_1SubQTask_0SubSubTask_Working_Perform_Completed() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Working);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    qtask.add(subqtask);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    assertEquals(subqtask.size(), 0);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Completed);
    assertEquals(qtask.isPresent(), false);
    assertEquals(qtask.isActive(), false);
    assertEquals(subqtask.size(), 0);
    assertEquals(subqtask.getStatus(), Completed);
    assertEquals(subqtask.isPresent(), false);
    assertEquals(subqtask.isActive(), false);
  }

  @Test
  void testQTask_1SubQTask_1SubSubTask_Pending_Perform_Completed() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> subsubtask = newTask((status, agent, delta) -> Completed);
    qtask.add(subqtask);
    subqtask.add(subsubtask);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Pending);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Completed);
    assertEquals(qtask.isPresent(), false);
    assertEquals(qtask.isActive(), false);
    assertEquals(subqtask.size(), 0);
    assertEquals(subqtask.getStatus(), Completed);
    assertEquals(subqtask.isPresent(), false);
    assertEquals(subqtask.isActive(), false);
  }

  @Test
  void testQTask_1SubQTask_1SubSubTask_Working_Perform_Completed() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Working);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> subsubtask = newTask((status, agent, delta) -> Completed);
    qtask.add(subqtask);
    subqtask.add(subsubtask);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 0);
    assertEquals(qtask.getStatus(), Completed);
    assertEquals(qtask.isPresent(), false);
    assertEquals(qtask.isActive(), false);
    assertEquals(subqtask.size(), 0);
    assertEquals(subqtask.getStatus(), Completed);
    assertEquals(subqtask.isPresent(), false);
    assertEquals(subqtask.isActive(), false);
  }

  @Test
  void testQTask_1SubQTask_2SubSubTask_Pending_Perform_Working() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> subsubtask1 = newTask((status, agent, delta) -> Completed);
    final Task<Object, TAgent> subsubtask2 = newTask((status, agent, delta) -> Completed);
    qtask.add(subqtask);
    subqtask.add(subsubtask1);
    subqtask.add(subsubtask2);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Pending);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    assertEquals(subqtask.size(), 2);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Working);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
  }

  @Test
  void testQTask_1SubQTask_2SubSubTask_Working_Perform_Working() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Working);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> subsubtask1 = newTask((status, agent, delta) -> Completed);
    final Task<Object, TAgent> subsubtask2 = newTask((status, agent, delta) -> Completed);
    qtask.add(subqtask);
    subqtask.add(subsubtask1);
    subqtask.add(subsubtask2);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    assertEquals(subqtask.size(), 2);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
    qtask.perform(null, 1f);
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), true);
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Working);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
  }



  @Test
  void testQTask_1SubQTask_0SubSubTask_Suspended_Perform_Suspended() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Suspended);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    qtask.add(subqtask);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Suspended);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false);
    assertEquals(subqtask.size(), 0);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
    qtask.perform(null, 1f); // XXX actually should not perform task that's not active
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Suspended);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false);
    assertEquals(subqtask.size(), 0);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
  }

  @Test
  void testQTask_1SubQTask_1SubSubTask_Suspended_Perform_Suspended() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Suspended);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> subsubtask = newTask((status, agent, delta) -> Completed);
    qtask.add(subqtask);
    subqtask.add(subsubtask);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Suspended);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false);
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
    qtask.perform(null, 1f); // XXX actually should not perform task that's not active
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Suspended);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false);
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(subqtask.isActive(), true);
  }



  @Test
  void testQTask_1SubQTask_1SubSubTaskSuspended_Pending_Perform_Working() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> subsubtask = newTask((status, agent, delta) -> Completed, Suspended);
    qtask.add(subqtask);
    subqtask.add(subsubtask);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Pending);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
    qtask.perform(null, 1f); // XXX actually should not perform task that's not active
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Pending); // XXX not Working
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Pending); // XXX not Working
    assertEquals(subqtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
  }

  @Test
  void testQTask_1SubQTask_1SubSubTaskSuspended_Working_Perform_Working() {
    final QTask<Object, TAgent> qtask = newQueueTask(new ArrayDeque<>(), null, Working);
    final QTask<Object, TAgent> subqtask = newQueueTask(new ArrayDeque<>(), null, Pending);
    final Task<Object, TAgent> subsubtask = newTask((status, agent, delta) -> Completed, Suspended);
    qtask.add(subqtask);
    subqtask.add(subsubtask);

    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Pending);
    assertEquals(subqtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
    qtask.perform(null, 1f); // XXX actually should not perform task that's not active
    assertEquals(qtask.size(), 1);
    assertEquals(qtask.getStatus(), Working);
    assertEquals(qtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
    assertEquals(subqtask.size(), 1);
    assertEquals(subqtask.getStatus(), Pending); // XXX not Working
    assertEquals(subqtask.isPresent(), true);
    assertEquals(qtask.isActive(), false); // was true
  }
}
