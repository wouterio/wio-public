package io.wouter.lib.task;

import static io.wouter.lib.task.TaskStatus.Completed;
import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.Tasks.newCollectiveTask;
import static io.wouter.lib.task.Tasks.newTask;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class CTaskTests {

  @Test
  void testCTask_1Agent_Min1Max1_Pending_Perform_Completed() {
    final Task<Object, TAgent> task = newTask((status, agent, delta) -> Completed);
    final CTask<Object, TAgent> ctask = newCollectiveTask(task, TAgent::getId, 1, 1);
    final TAgent agent = new TAgent();

    assertEquals(ctask.getStatus(), Pending);
    assertEquals(ctask.isPresent(), true);
    assertEquals(ctask.isActive(), true);
    assertEquals(ctask.isAvailable(), true);
    assertEquals(ctask.getAgents(), 0);
    assertEquals(ctask.hasMinAgents(), false);
    assertEquals(ctask.hasMaxAgents(), false);
    assertEquals(ctask.isRegistered(agent), false);
    assertEquals(agent.hasTask(), false);
    agent.setTask(ctask);
    assertEquals(ctask.getStatus(), Pending);
    assertEquals(ctask.isPresent(), true);
    assertEquals(ctask.isActive(), true);
    assertEquals(ctask.isAvailable(), false);
    assertEquals(ctask.getAgents(), 1);
    assertEquals(ctask.hasMinAgents(), true);
    assertEquals(ctask.hasMaxAgents(), true);
    assertEquals(ctask.isRegistered(agent), true);
    assertEquals(agent.hasTask(), true);
    agent.perform(1f);
    assertEquals(ctask.getStatus(), Completed);
    assertEquals(ctask.isPresent(), false);
    assertEquals(ctask.isActive(), false);
    assertEquals(ctask.isAvailable(), false);
    assertEquals(ctask.getAgents(), 0);
    assertEquals(ctask.hasMinAgents(), false);
    assertEquals(ctask.hasMaxAgents(), false);
    assertEquals(ctask.isRegistered(agent), false);
    assertEquals(agent.hasTask(), false);
  }

  @Test
  void testCTask_1Agent_Min2Max2_Pending_Perform_Completed() {
    final Task<Object, TAgent> task = newTask((status, agent, delta) -> Completed);
    final CTask<Object, TAgent> ctask = newCollectiveTask(task, TAgent::getId, 2, 2);
    final TAgent agent = new TAgent();

    assertEquals(ctask.getStatus(), Pending);
    assertEquals(ctask.isPresent(), true);
    assertEquals(ctask.isActive(), true);
    assertEquals(ctask.isAvailable(), true);
    assertEquals(ctask.getAgents(), 0);
    assertEquals(ctask.hasMinAgents(), false);
    assertEquals(ctask.hasMaxAgents(), false);
    assertEquals(ctask.isRegistered(agent), false);
    assertEquals(agent.hasTask(), false);
    agent.setTask(ctask);
    assertEquals(ctask.getStatus(), Pending);
    assertEquals(ctask.isPresent(), true);
    assertEquals(ctask.isActive(), true);
    assertEquals(ctask.isAvailable(), true);
    assertEquals(ctask.getAgents(), 1);
    assertEquals(ctask.hasMinAgents(), false);
    assertEquals(ctask.hasMaxAgents(), false);
    assertEquals(ctask.isRegistered(agent), true);
    assertEquals(agent.hasTask(), true);
    agent.perform(1f);
    assertEquals(ctask.getStatus(), Pending);
    assertEquals(ctask.isPresent(), true);
    assertEquals(ctask.isActive(), true);
    assertEquals(ctask.isAvailable(), true);
    assertEquals(ctask.getAgents(), 1);
    assertEquals(ctask.hasMinAgents(), false);
    assertEquals(ctask.hasMaxAgents(), false);
    assertEquals(ctask.isRegistered(agent), true);
    assertEquals(agent.hasTask(), true);
  }

  @Test
  void testCTask_2Agent_Min2Max2_Pending_Perform_Completed() {
    final Task<Object, TAgent> task = newTask((status, agent, delta) -> Completed);
    final CTask<Object, TAgent> ctask = newCollectiveTask(task, TAgent::getId, 2, 2);
    final TAgent agent1 = new TAgent();
    final TAgent agent2 = new TAgent();

    assertEquals(ctask.getStatus(), Pending);
    assertEquals(ctask.isPresent(), true);
    assertEquals(ctask.isActive(), true);
    assertEquals(ctask.isAvailable(), true);
    assertEquals(ctask.getAgents(), 0);
    assertEquals(ctask.hasMinAgents(), false);
    assertEquals(ctask.hasMaxAgents(), false);
    assertEquals(ctask.isRegistered(agent1), false);
    assertEquals(ctask.isRegistered(agent2), false);
    assertEquals(agent1.hasTask(), false);
    assertEquals(agent2.hasTask(), false);
    agent1.setTask(ctask);
    agent2.setTask(ctask);
    assertEquals(ctask.getStatus(), Pending);
    assertEquals(ctask.isPresent(), true);
    assertEquals(ctask.isActive(), true);
    assertEquals(ctask.isAvailable(), false);
    assertEquals(ctask.getAgents(), 2);
    assertEquals(ctask.hasMinAgents(), true);
    assertEquals(ctask.hasMaxAgents(), true);
    assertEquals(ctask.isRegistered(agent1), true);
    assertEquals(ctask.isRegistered(agent2), true);
    assertEquals(agent1.hasTask(), true);
    assertEquals(agent2.hasTask(), true);
    agent1.perform(1f);
    agent2.perform(1f);
    assertEquals(ctask.getStatus(), Completed);
    assertEquals(ctask.isPresent(), false);
    assertEquals(ctask.isActive(), false);
    assertEquals(ctask.isAvailable(), false);
    assertEquals(ctask.getAgents(), 0);
    assertEquals(ctask.hasMinAgents(), false);
    assertEquals(ctask.hasMaxAgents(), false);
    assertEquals(ctask.isRegistered(agent1), false);
    assertEquals(ctask.isRegistered(agent2), false);
    assertEquals(agent1.hasTask(), false);
    assertEquals(agent2.hasTask(), false);
  }
}
