package io.wouter.gdx.desktop;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import io.wouter.won.gdx.WonGame;

public class DesktopLauncher {

  public static void main(final String[] arg) {
    final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    final ApplicationListener game = new WonGame();
    // final ApplicationListener game = new TreeTest();
    new LwjglApplication(game, config);
  }
}
