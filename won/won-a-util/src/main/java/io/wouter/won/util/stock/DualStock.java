package io.wouter.won.util.stock;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

final class DualStock implements Stock, Serializable {

  private static final long serialVersionUID = -3647866483540331783L;



  private final Stock superstock;
  private final Stock substock;



  DualStock(final Stock superstock, final Stock substock) {
    this.superstock = superstock;
    this.substock = substock;
  }



  @Override
  public boolean hasSoftCapacity() {
    return substock.hasSoftCapacity();
  }

  @Override
  public void setSoftCapacity(final boolean softcap) {
    substock.setSoftCapacity(softcap);
  }



  @Override
  public int getCapacity() {
    return Math.min(superstock.getCapacity(), substock.getCapacity());
  }

  @Override
  public int getLoCount() {
    return substock.getLoCount();
  }

  @Override
  public int getLoSpace() {
    return substock.getLoSpace();
  }



  @Override
  public void setCapacity(final int cap) {
    substock.setCapacity(cap);
  }

  @Override
  public void setLoCount(final int locount) {
    substock.setLoCount(locount);
  }

  @Override
  public void setLoSpace(final int lospace) {
    substock.setLoSpace(lospace);
  }



  @Override
  public int getCount() {
    return substock.getCount();
  }

  @Override
  public int getSpace() {
    return Math.min(superstock.getSpace(), substock.getSpace());
  }



  @Override
  public boolean hasLoCount() {
    return substock.hasLoCount();
  }

  @Override
  public boolean hasLoSpace() {
    return superstock.hasLoSpace() || substock.hasLoSpace();
  }



  @Override
  public boolean isEmpty() {
    return substock.isEmpty();
  }

  @Override
  public boolean isFull() {
    return superstock.isFull() || substock.isFull();
  }



  @Override
  public int change(final int amount) {
    superstock.change(amount);
    return substock.change(amount);
  }

  @Override
  public int clear() {
    final int amount = substock.clear();
    superstock.change(-amount);
    return amount;
  }



  @Override
  public String toString() {
    return substock.toString();
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  private static class Serial implements Serializable {

    private static final long serialVersionUID = -6085218917947263250L;



    private final Stock superstock, substock;



    Serial(final DualStock obj) {
      superstock = obj.superstock;
      substock = obj.substock;
    }



    private Object readResolve() {
      return new DualStock(superstock, substock);
    }
  }
}
