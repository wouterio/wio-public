package io.wouter.won.util.string;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

public interface WonInfoBuilder {

  static <T, R> R mapNullable(final T t, final Function<T, R> f) {
    return t == null ?
        null :
        f.apply(t);
  }



  //



  default WonInfoBuilder collect(final Collect value) {
    value.collect(this);
    return this;
  }

  default <T> WonInfoBuilder collect(final T value, final Function<T, Collect> mapper) {
    mapper.apply(value).collect(this);
    return this;
  }

  default WonInfoBuilder collectAll(final Collection<? extends Collect> values) {
    boolean sep = false;
    for (final Collect value : values) {
      if (sep)
        separator();
      collect(value);
      sep = true;
    }
    return this;
  }

  default <T> WonInfoBuilder collectAll(final Collection<? extends T> values, final Function<T, Collect> mapper) {
    boolean sep = false;
    for (final T value : values) {
      if (sep)
        separator();
      collect(value, mapper);
      sep = true;
    }
    return this;
  }



  //



  WonInfoBuilder child(String name);

  <C extends Collect> WonInfoBuilder child(C value, Function<C, String> mapper);

  WonInfoBuilder parent();

  WonInfoBuilder header(String name);

  <C extends Collect> WonInfoBuilder header(C value, Function<C, String> mapper);

  WonInfoBuilder separator();

  WonInfoBuilder field(String name);

  WonInfoBuilder value(String value);

  WonInfoBuilder action(Runnable r);

  <C extends Collect> WonInfoBuilder link(C value, Function<C, String> mapper);



  default WonInfoBuilder value(final boolean value) {
    return value(Boolean.toString(value));
  }

  default WonInfoBuilder value(final char value) {
    return value(Character.toString(value));
  }

  default WonInfoBuilder value(final int value) {
    return value(Integer.toString(value));
  }

  default WonInfoBuilder value(final long value) {
    return value(Long.toString(value));
  }

  default WonInfoBuilder value(final float value) {
    return value(Float.toString(value));
  }

  default WonInfoBuilder value(final double value) {
    return value(Double.toString(value));
  }

  default WonInfoBuilder value(final Object value) {
    return value(value.toString());
  }

  default <T> WonInfoBuilder value(final T value, final Function<T, String> mapper) {
    return value(mapper.apply(value));
  }

  default WonInfoBuilder valueOptional(final Optional<?> value) {
    return value.isPresent() ?
        value(value.get()) :
        this;
  }

  default <T> WonInfoBuilder valueOptional(final Optional<T> value, final Function<T, String> mapper) {
    return value.isPresent() ?
        value(value.get(), mapper) :
        this;
  }

  default <T> WonInfoBuilder valueAll(final Collection<T> values, final Function<T, String> mapper) {
    values.forEach(v -> value(v, mapper));
    return this;
  }



  default <C extends Collect> WonInfoBuilder linkOptional(final Optional<C> value, final Function<C, String> mapper) {
    return value.isPresent() ?
        link(value.get(), mapper) :
        this;
  }

  default <C extends Collect> WonInfoBuilder linkAll(final Collection<C> values, final Function<C, String> mapper) {
    values.forEach(v -> link(v, mapper));
    return this;
  }



  WonInfoBuilder valueBoolean(GetBoolean get);

  WonInfoBuilder valueInt(GetInt get);

  WonInfoBuilder valueLong(GetLong get);

  WonInfoBuilder valueFloat(GetFloat get);

  WonInfoBuilder valueDouble(GetDouble get);

  WonInfoBuilder valueString(GetObject<String> get);

  <T> WonInfoBuilder valueObject(GetObject<T> get, final Function<T, String> mapper);



  WonInfoBuilder valueBoolean(GetBoolean get, SetBoolean set);

  WonInfoBuilder valueInt(GetInt get, SetInt set, int min, int max, int step);

  WonInfoBuilder valueLong(GetLong get, SetLong set, long min, long max, long step);

  WonInfoBuilder valueFloat(GetFloat get, SetFloat set, float min, float max, float step);

  WonInfoBuilder valueDouble(GetDouble get, SetDouble set, double min, double max, double step);

  WonInfoBuilder valueString(GetObject<String> get, SetObject<String> set);

  <T> WonInfoBuilder valueObject(GetObject<T> get, SetObject<T> set, Set<T> options, final Function<T, String> mapper);



  //



  @FunctionalInterface
  interface GetBoolean {

    boolean get();
  }

  @FunctionalInterface
  interface SetBoolean {

    void set(boolean b);
  }



  @FunctionalInterface
  interface GetInt {

    int get();
  }

  @FunctionalInterface
  interface SetInt {

    void set(int i);
  }



  @FunctionalInterface
  interface GetLong {

    long get();
  }

  @FunctionalInterface
  interface SetLong {

    void set(long l);
  }



  @FunctionalInterface
  interface GetFloat {

    float get();
  }

  @FunctionalInterface
  interface SetFloat {

    void set(float f);
  }



  @FunctionalInterface
  interface GetDouble {

    double get();
  }

  @FunctionalInterface
  interface SetDouble {

    void set(double d);
  }



  @FunctionalInterface
  interface GetObject<T> {

    T get();
  }

  @FunctionalInterface
  interface SetObject<T> {

    void set(T t);
  }



  @FunctionalInterface
  interface Collect {

    WonInfoBuilder collect(WonInfoBuilder info);
  }
}
