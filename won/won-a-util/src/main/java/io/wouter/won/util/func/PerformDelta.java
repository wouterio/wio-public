package io.wouter.won.util.func;

public interface PerformDelta {

  void perform(float delta);
}
