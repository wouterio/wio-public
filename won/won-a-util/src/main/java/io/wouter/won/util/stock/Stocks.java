package io.wouter.won.util.stock;


public class Stocks {

  public static MetaStock newMetaStock(final int cap, final int locount, final int lospace, final boolean softcap) {
    return new MetaStock(new MonoStock(cap, locount, lospace, softcap));
  }

  public static AccountStock newAccountStock(final int cap, final int locount, final int lospace, final boolean softcap) {
    return new AccountStock(new MonoStock(cap, locount, lospace, softcap));
  }
}
