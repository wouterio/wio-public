package io.wouter.won.util.counter;

import java.util.Collection;

public interface CounterM<T> extends CounterC<T> {

  CounterC<T> getConst();



  @Override
  CountM<T> get(T key);

  CountM<T> ensure(T key);



  CounterM<T> putAll(CounterC<T> other);

  CounterM<T> cleanup();

  CounterM<T> clear();



  Collection<CountM<T>> getModValues();



  //



  public interface CountM<T> extends CountC<T> {

    CountC<T> getConst();



    void set(int count);

    int incr();

    int decr();

    int incr(int count);

    int decr(int count);

    int clear();
  }
}
