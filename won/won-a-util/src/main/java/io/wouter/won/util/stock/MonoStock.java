package io.wouter.won.util.stock;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

final class MonoStock implements Stock, Serializable {

  private static final long serialVersionUID = -9014919813021054968L;



  private boolean softcap;
  private int cap;
  private int locount;
  private int lospace;
  private int count = 0;



  MonoStock(final int cap, final int locount, final int lospace, final boolean softcap) {
    this.softcap = softcap;
    this.cap = cap;
    this.locount = locount;
    this.lospace = lospace;
    assert assertCapArgs(this);
  }



  @Override
  public boolean hasSoftCapacity() {
    return softcap;
  }

  @Override
  public void setSoftCapacity(final boolean softcap) {
    this.softcap = softcap;
  }



  @Override
  public int getCapacity() {
    return cap;
  }

  @Override
  public int getLoCount() {
    return locount;
  }

  @Override
  public int getLoSpace() {
    return lospace;
  }



  @Override
  public void setCapacity(final int cap) {
    this.cap = max(0, max(cap, locount + lospace)); // this.cap = cap;
    assert assertCapArgs(this);
  }

  @Override
  public void setLoCount(final int locount) {
    this.locount = min(locount, cap - lospace); // this.locount = locount;
    assert assertCapArgs(this);
  }

  @Override
  public void setLoSpace(final int lospace) {
    this.lospace = min(lospace, cap - locount); // this.lospace = lospace;
    assert assertCapArgs(this);
  }



  @Override
  public int getCount() {
    return count;
  }

  @Override
  public int getSpace() {
    return cap - count;
  }



  @Override
  public boolean hasLoCount() {
    return count <= locount;
  }

  @Override
  public boolean hasLoSpace() {
    return getSpace() <= lospace;
  }



  @Override
  public boolean isEmpty() {
    return count <= 0;
  }

  @Override
  public boolean isFull() {
    return count >= cap; // getSpace() <= 0;
  }



  @Override
  public int change(final int amount) {
    count += amount;
    assert assertCount(this);
    return count;
  }

  @Override
  public int clear() {
    final int n = count;
    change(-n);
    return n;
  }



  @Override
  public String toString() {
    return new StringBuilder().append(count).append('/').append(cap).toString();
  }



  private static boolean assertCapArgs(final MonoStock s) {
    assert s.cap >= 0 : "Capacity should be >= 0."; //$NON-NLS-1$
    assert s.locount <= s.cap - s.lospace : "LoCount should be <= Capacity - LoSpace."; //$NON-NLS-1$
    return true;
  }

  private static boolean assertCount(final MonoStock s) {
    assert s.count >= 0 : "Count should be >= 0"; //$NON-NLS-1$
    assert s.softcap || s.count <= s.cap : "Count should be <= Capacity."; //$NON-NLS-1$
    return true;
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  private static class Serial implements Serializable {

    private static final long serialVersionUID = 437468279508639337L;



    private final boolean softcap;
    private final int cap;
    private final int locount;
    private final int lospace;
    private final int count;



    Serial(final MonoStock obj) {
      softcap = obj.softcap;
      cap = obj.cap;
      locount = obj.locount;
      lospace = obj.lospace;
      count = obj.count;
    }



    private Object readResolve() {
      final MonoStock obj = new MonoStock(cap, locount, lospace, softcap);
      obj.count = count;
      return obj;
    }
  }
}
