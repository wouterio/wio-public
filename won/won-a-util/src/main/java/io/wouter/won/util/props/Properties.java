package io.wouter.won.util.props;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class Properties {


  private final Map<String, Object> props = new HashMap<>();



  public boolean containsKey(final String key) {
    return props.containsKey(key);
  }



  public Object get(final String key) {
    final Object value = props.get(key);
    if (value == null)
      throw new NoSuchElementException(key);
    return value;
  }

  public <T> T get(final String key, final Class<T> clazz) {
    final Object value = get(key);
    return clazz.cast(value);
  }

  public String getString(final String key) {
    final Object value = get(key);
    return String.valueOf(value);
  }



  public boolean getBoolean(final String key) {
    return get(key, Boolean.class).booleanValue();
  }

  public int getInt(final String key) {
    return get(key, Integer.class).intValue();
  }

  public long getLong(final String key) {
    return get(key, Long.class).longValue();
  }

  public float getFloat(final String key) {
    return get(key, Float.class).floatValue();
  }

  public double getDouble(final String key) {
    return get(key, Double.class).doubleValue();
  }



  public void put(final String key, final Object value) {
    if (value == null)
      throw new IllegalArgumentException("Value should not be null."); //$NON-NLS-1$
    props.put(key, value);
  }

  public void putAll(final Properties other) {
    this.props.putAll(other.props);
  }



  public void remove(final String key) {
    props.remove(key);
  }

  public void clear() {
    props.clear();
  }
}
