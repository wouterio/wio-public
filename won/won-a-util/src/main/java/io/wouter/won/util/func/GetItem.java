package io.wouter.won.util.func;

public interface GetItem<I> {

  I getItem();
}
