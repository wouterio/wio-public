package io.wouter.won.util.stock;

import static java.lang.Math.min;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public final class AccountStock implements Serializable {

  private static int transfer(final AccountStock src, final AccountStock tar, final int amount) {
    final int n = min(amount, min(src.intern.getCount(), tar.intern.getSpace()));
    if (n < 1)
      return 0;
    src.intern.change(-n);
    tar.intern.change(n);
    return n;
  }



  private static final long serialVersionUID = -5840536981796471889L;



  private final Stock intern;



  AccountStock(final Stock intern) {
    this.intern = intern;
  }



  public boolean hasSoftCapacity() {
    return intern.hasSoftCapacity();
  }

  public void setSoftCapacity(final boolean softcap) {
    intern.setSoftCapacity(softcap);
  }



  public int getCapacity() {
    return intern.getCapacity();
  }

  public int getLoCount() {
    return intern.getLoCount();
  }

  public int getLoSpace() {
    return intern.getLoSpace();
  }



  public void setCapacity(final int cap) {
    intern.setCapacity(cap);
  }

  public void setLoCount(final int locount) {
    intern.setLoCount(locount);
  }

  public void setLoSpace(final int lospace) {
    intern.setLoSpace(lospace);
  }



  public int getCount() {
    return intern.getCount();
  }

  public int getSpace() {
    return intern.getSpace();
  }



  public boolean hasLoCount() {
    return intern.hasLoCount();
  }

  public boolean hasLoSpace() {
    return intern.hasLoSpace();
  }



  public boolean isEmpty() {
    return intern.isEmpty();
  }

  public boolean isFull() {
    return intern.isFull();
  }



  public int change(final int amount) {
    return intern.change(amount);
  }

  public int clear() {
    return intern.clear();
  }

  public int transfer(final AccountStock tar, final int amount) {
    return transfer(this, tar, amount);
  }



  @Override
  public String toString() {
    return intern.toString();
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  private static class Serial implements Serializable {

    private static final long serialVersionUID = -2567963055755195836L;



    private final Stock intern;



    Serial(final AccountStock obj) {
      intern = obj.intern;
    }



    private Object readResolve() {
      return new AccountStock(intern);
    }
  }
}
