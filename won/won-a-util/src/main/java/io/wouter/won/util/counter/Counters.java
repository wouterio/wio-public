package io.wouter.won.util.counter;

import java.util.Comparator;
import java.util.Map;
import java.util.SortedMap;

public final class Counters {

  public static <T> CounterM<T> newCounter() {
    return new CounterMI<>(null);
  }

  public static <T> CounterM<T> newCounter(final Comparator<? super T> c) {
    return new CounterMI<>(c);
  }



  public static <T> CounterM<T> of(final Map<T, Integer> map) {
    return of(map, null);
  }

  public static <T> CounterM<T> of(final SortedMap<T, Integer> map) {
    return of(map, map.comparator());
  }

  public static <T> CounterM<T> of(final Map<T, Integer> map, final Comparator<? super T> c) {
    final CounterM<T> counter = newCounter(c);
    map.forEach((key, value) -> counter.ensure(key).set(value.intValue()));
    return counter;
  }



  private Counters() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
