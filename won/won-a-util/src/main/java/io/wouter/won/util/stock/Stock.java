package io.wouter.won.util.stock;

/**
 * @author Wouter
 */

interface Stock {

  /**
   * @return Whether stock count may exceed stock capacity.
   */

  boolean hasSoftCapacity();

  /**
   * @param softcap Whether stock count may exceed stock capacity.
   */

  void setSoftCapacity(boolean softcap);



  /**
   * @return Stock capacity.
   */

  int getCapacity();

  /**
   * @return Parameter determining when stock count is running low.
   */

  int getLoCount();

  /**
   * @return Parameter determining when stock space is running low.
   */

  int getLoSpace();



  /**
   * @param cap Stock capacity.
   */

  void setCapacity(int cap);

  /**
   * @param locount Determines when stock count is running low.
   */

  void setLoCount(int locount);

  /**
   * @param lospace Determines when stock space is running low.
   */

  void setLoSpace(int lospace);



  /**
   * @return Stock count.
   */

  int getCount();

  /**
   * @return Stock remaining space.
   */

  int getSpace();



  /**
   * @return Whether stock count is running low. That is, stock count <= stock locount parameter.
   */

  boolean hasLoCount();

  /**
   * @return Whether stock space is running low. That is, stock space <= stock lospace parameter.
   */

  boolean hasLoSpace();



  /**
   * @return Whether stock is empty.
   */

  boolean isEmpty();

  /**
   * @return Whether stock is full.
   */

  boolean isFull();



  /**
   * Change stock count with amount. Positive amount increases stock count. Negative amount decreases stock count.
   *
   * @param amount
   * @return New stock count.
   */

  int change(int amount);

  /**
   * Clear stock. That is, set stock count to zero.
   *
   * @return Old stock count.
   */

  int clear();
}
