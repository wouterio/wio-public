package io.wouter.won.util.enums;

public enum LinkType {

  Internal,
  External,
  Idle;
}
