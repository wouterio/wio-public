package io.wouter.won.util.func;

public interface PerformRound {

  void perform(long round);
}
