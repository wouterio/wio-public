package io.wouter.won.util.counter;

import static java.util.Collections.unmodifiableSet;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import java.util.function.ObjIntConsumer;

import io.wouter.won.util.counter.CounterM.CountM;

final class CounterCI<T> implements CounterC<T>, Serializable {

  private static final long serialVersionUID = -3858399080160673988L;



  private final CounterM<T> root;
  private final Set<T> ukeys;



  CounterCI(final CounterM<T> root) {
    this.root = root;
    this.ukeys = unmodifiableSet(root.getKeys());
  }



  @Override
  public CounterM<T> getMod() {
    return root.getMod();
  }



  @Override
  public int size() {
    return root.size();
  }

  @Override
  public long total() {
    return root.total();
  }



  @Override
  public boolean isEmpty() {
    return root.isEmpty();
  }

  @Override
  public boolean containsKey(final T key) {
    return root.containsKey(key);
  }



  @Override
  public CountC<T> get(final T key) {
    return root.get(key).getConst();
  }



  @Override
  public Set<T> getKeys() {
    return ukeys;
  }

  @Override
  public Collection<CountC<T>> getConstValues() {
    return root.getConstValues();
  }



  @Override
  public void forEach(final ObjIntConsumer<T> c) {
    root.forEach(c);
  }



  @Override
  public String toString() {
    return root.toString();
  }



  //



  static final class CountCI<T> implements CountC<T>, Serializable {

    private static final long serialVersionUID = 5033538180003734493L;



    private final CountM<T> root;



    CountCI(final CountM<T> root) {
      this.root = root;
    }



    @Override
    public T key() {
      return root.key();
    }

    @Override
    public int count() {
      return root.count();
    }



    @Override
    public String toString() {
      return root.toString();
    }
  }
}
