package io.wouter.won.util.string;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public final class WonStringBuilder {

  private static final char OpeningChar = '{';
  private static final char ClosingChar = '}';
  private static final char SeperatorChar = ';';
  private static final char AssignmentChar = ':';
  private static final char CollectionOpeningChar = '[';
  private static final char CollectionClosingChar = ']';
  private static final char CollectionSeperatorChar = ',';
  private static final String EmptyCollectionString = "" + CollectionOpeningChar + CollectionClosingChar; //$NON-NLS-1$



  private final StringBuilder builder = new StringBuilder();



  public WonStringBuilder(final Class<?> clazz, final int index, final String type) {
    builder.append(clazz.getSimpleName()).append(OpeningChar).append(type).append(AssignmentChar).append(index);
  }



  public WonStringBuilder append(final CharSequence value) {
    builder.append(SeperatorChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String value) {
    builder.append(SeperatorChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final boolean value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final char value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final int value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final long value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final float value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final double value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final CharSequence value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final String value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public WonStringBuilder append(final String key, final Object value) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value);
    return this;
  }

  public <T> WonStringBuilder append(final String key, final T value, final Function<T, String> mapper) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(mapper.apply(value));
    return this;
  }

  public <T> WonStringBuilder appendNullable(final String key, final T value, final Function<T, String> mapper) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(mapNullable(value, mapper));
    return this;
  }

  public <T> WonStringBuilder appendOptional(final String key, final Optional<T> value, final Function<T, String> mapper) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(value.map(mapper).orElse(null));
    return this;
  }

  public <T> WonStringBuilder appendAll(final String key, final Collection<? extends T> col, final Function<T, String> mapper) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(mapCollection(col, mapper));
    return this;
  }

  public <K, V> WonStringBuilder appendAll(final String key, final Map<? extends K, ? extends V> col, final Function<K, String> keymapper, final Function<V, String> valuemapper) {
    builder.append(SeperatorChar).append(key).append(AssignmentChar).append(mapMap(col, keymapper, valuemapper));
    return this;
  }



  @Override
  public String toString() {
    return builder.append(ClosingChar).toString();
  }



  public static <T> String mapNullable(final T value, final Function<T, String> mapper) {
    return value == null ?
        null :
        mapper.apply(value);
  }

  public static <T> String mapCollection(final Collection<? extends T> col, final Function<T, String> mapper) {
    if (col == null)
      return null;
    if (col.isEmpty())
      return EmptyCollectionString;
    final StringBuilder sb = new StringBuilder();
    col.forEach(e -> sb.append(CollectionSeperatorChar).append(mapNullable(e, mapper)));
    sb.deleteCharAt(0).insert(0, CollectionOpeningChar).append(CollectionClosingChar);
    return sb.toString();
  }

  public static <K, V> String mapMap(final Map<? extends K, ? extends V> map, final Function<K, String> keymapper, final Function<V, String> valuemapper) {
    if (map == null)
      return null;
    if (map.isEmpty())
      return EmptyCollectionString;
    final StringBuilder sb = new StringBuilder();
    map.forEach((k, v) -> sb.append(CollectionSeperatorChar).append(mapNullable(k, keymapper)).append(AssignmentChar).append(mapNullable(v, valuemapper)));
    sb.deleteCharAt(0).insert(0, CollectionOpeningChar).append(CollectionClosingChar);
    return sb.toString();
  }
}
