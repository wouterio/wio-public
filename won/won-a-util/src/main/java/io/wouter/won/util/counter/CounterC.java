package io.wouter.won.util.counter;

import java.util.Collection;
import java.util.Set;
import java.util.function.ObjIntConsumer;

public interface CounterC<T> {

  CounterM<T> getMod();



  boolean isEmpty();

  boolean containsKey(T key);



  int size();

  long total();



  CountC<T> get(T key);



  Set<T> getKeys();

  Collection<CountC<T>> getConstValues();



  void forEach(ObjIntConsumer<T> c);



  //



  public interface CountC<T> {

    T key();

    int count();
  }
}
