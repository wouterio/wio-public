package io.wouter.won.util.counter;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.ObjIntConsumer;

import io.wouter.won.util.counter.CounterCI.CountCI;

final class CounterMI<T> implements CounterM<T>, Serializable {

  private static final long serialVersionUID = 6682343434421147211L;



  private final Comparator<? super T> comp;
  private final Map<T, CountM<T>> counts;
  private final CounterC<T> constcounter;



  long total = 0l;



  CounterMI(final Comparator<? super T> comp) {
    this.comp = comp;
    this.counts = comp == null ?
        new HashMap<>() :
        new TreeMap<>(comp);
    this.constcounter = new CounterCI<>(this);
  }

  private CounterMI(final CounterMI<T> other) {
    this(other.comp);
    other.counts.forEach((key, count) -> this.counts.put(key, new CountMI<>(this, count.key(), count.count())));
    assert this.total == other.total;
  }



  @Override
  public CounterC<T> getConst() {
    return constcounter;
  }

  @Override
  public CounterM<T> getMod() {
    return new CounterMI<>(this);
  }



  @Override
  public boolean isEmpty() {
    return counts.isEmpty();
  }

  @Override
  public boolean containsKey(final T key) {
    return counts.containsKey(key);
  }



  @Override
  public int size() {
    return counts.size();
  }

  @Override
  public long total() {
    return total;
  }



  @Override
  public CountM<T> get(final T key) {
    return counts.get(key);
  }

  @Override
  public CountM<T> ensure(final T key) {
    return counts.computeIfAbsent(key, k -> new CountMI<>(this, k));
  }



  @Override
  public Set<T> getKeys() {
    return counts.keySet();
  }



  @Override
  public void forEach(final ObjIntConsumer<T> c) {
    counts.forEach((key, count) -> c.accept(key, count.count()));
  }



  @Override
  public CounterM<T> putAll(final CounterC<T> other) {
    other.forEach((key, count) -> ensure(key).set(count));
    return this;
  }



  @Override
  public CounterM<T> cleanup() {
    counts.keySet().removeIf(key -> get(key).count() == 0);
    // total does not change
    return this;
  }

  @Override
  public CounterM<T> clear() {
    counts.clear();
    total = 0l;
    return this;
  }



  @Override
  public Collection<CountC<T>> getConstValues() {
    return counts.values().stream().map(CountM::getConst).collect(toList());
  }

  @Override
  public Collection<CountM<T>> getModValues() {
    return counts.values();
  }



  @Override
  public String toString() {
    return counts.toString();
  }



  //



  static final class CountMI<T> implements CountM<T>, Serializable {

    private static final long serialVersionUID = 5033538180003734493L;



    private final CounterMI<?> parent;
    private final CountC<T> constcount;
    private final T key;

    private int count = 0;



    CountMI(final CounterMI<T> parent, final T key) {
      this.parent = parent;
      this.key = key;
      this.constcount = new CountCI<>(this);
    }

    CountMI(final CounterMI<T> parent, final T key, final int count) {
      this(parent, key);
      this.count = count;
      this.parent.total += count;
    }



    @Override
    public CountC<T> getConst() {
      return constcount;
    }



    @Override
    public T key() {
      return key;
    }

    @Override
    public int count() {
      return count;
    }

    @Override
    public void set(final int c) {
      parent.total -= count;
      parent.total += c;
      count = c;
    }

    @Override
    public int incr() {
      ++parent.total;
      return ++count;
    }

    @Override
    public int decr() {
      --parent.total;
      return --count;
    }

    @Override
    public int incr(final int c) {
      parent.total += c;
      return count += c;
    }

    @Override
    public int decr(final int c) {
      parent.total -= c;
      return count -= c;
    }

    @Override
    public int clear() {
      parent.total -= count;
      return count = 0;
    }



    @Override
    public String toString() {
      return new StringBuilder().append('<').append(key).append(',').append(Integer.toString(count)).append('>').toString();
    }
  }
}
