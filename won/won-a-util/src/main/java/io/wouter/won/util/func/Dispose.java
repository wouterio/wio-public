package io.wouter.won.util.func;

public interface Dispose {

  void dispose();
}
