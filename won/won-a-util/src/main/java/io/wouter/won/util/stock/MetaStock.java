package io.wouter.won.util.stock;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public final class MetaStock implements Serializable {

  private static final long serialVersionUID = -8325390368885306708L;



  private final Stock intern;



  MetaStock(final Stock intern) {
    this.intern = intern;
  }



  public boolean hasSoftCapacity() {
    return intern.hasSoftCapacity();
  }

  public void setSoftCapacity(final boolean softcap) {
    intern.setSoftCapacity(softcap);
  }



  public int getCapacity() {
    return intern.getCapacity();
  }

  public int getLoCount() {
    return intern.getLoCount();
  }

  public int getLoSpace() {
    return intern.getLoSpace();
  }



  public void setCapacity(final int cap) {
    intern.setCapacity(cap);
  }

  public void setLoCount(final int locount) {
    intern.setLoCount(locount);
  }

  public void setLoSpace(final int lospace) {
    intern.setLoSpace(lospace);
  }



  public int getCount() {
    return intern.getCount();
  }

  public int getSpace() {
    return intern.getSpace();
  }



  public boolean hasLoCount() {
    return intern.hasLoCount();
  }

  public boolean hasLoSpace() {
    return intern.hasLoSpace();
  }



  public boolean isEmpty() {
    return intern.isEmpty();
  }

  public boolean isFull() {
    return intern.isFull();
  }



  public MetaStock newMetaSubStock(final int cap, final int locount, final int lospace, final boolean softcap) {
    return new MetaStock(new DualStock(intern, new MonoStock(cap, locount, lospace, softcap)));
  }

  public AccountStock newAccountSubStock(final int cap, final int locount, final int lospace, final boolean softcap) {
    return new AccountStock(new DualStock(intern, new MonoStock(cap, locount, lospace, softcap)));
  }



  @Override
  public String toString() {
    return intern.toString();
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  private static class Serial implements Serializable {

    private static final long serialVersionUID = 437974147878925653L;



    private final Stock intern;



    Serial(final MetaStock obj) {
      intern = obj.intern;
    }



    private Object readResolve() {
      return new MetaStock(intern);
    }
  }
}
