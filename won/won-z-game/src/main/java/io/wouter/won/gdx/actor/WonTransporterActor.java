package io.wouter.won.gdx.actor;

import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import io.wouter.won.entity.WonTransporter;
import io.wouter.won.gdx.IsometricStaggeredPositionConversion;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class WonTransporterActor extends Actor {

  private static final Texture Dot;

  static {
    final Pixmap pixmap = new Pixmap(4, 4, Pixmap.Format.RGBA8888);
    pixmap.setColor(Color.BLUE);
    pixmap.fillRectangle(0, 0, 4, 4);
    Dot = new Texture(pixmap);
    pixmap.dispose();
  }



  private final WonTransporter<WonNodeDelegate, WonBufferDelegate> transporter;

  private final Sprite sprite;
  private final TextureAtlas atlas;
  private final IsometricStaggeredPositionConversion posconv;
  private final Vector2 screenpos = new Vector2();



  public WonTransporterActor(final WonTransporter<WonNodeDelegate, WonBufferDelegate> transporter, final IsometricStaggeredPositionConversion posconv) {
    this.transporter = requireNonNull(transporter);
    this.atlas = new TextureAtlas("sprites/bluetruck.txt");
    this.sprite = new Sprite(atlas.findRegion("car_E"));
    this.posconv = requireNonNull(posconv);
    posconv.convertWorldToScreen(transporter.getDelegate().getPosition(), screenpos);
    setPosition(screenpos.x, screenpos.y);
  }



  @Override
  public void act(final float delta) {
    super.act(delta);

    posconv.convertWorldToScreen(transporter.getDelegate().getPosition(), screenpos);
    setPosition(screenpos.x, screenpos.y);

    // if (transporter.isDisposed())
    // do something

    sprite.setRegion(atlas.findRegion("car_" + transporter.getDelegate().getOrientation().name()));
  }

  @Override
  public void draw(final Batch batch, final float alpha) {
    super.draw(batch, alpha);
    batch.draw(sprite, getX() + 64 - (sprite.getRegionWidth() >> 1), getY() + 32 - (sprite.getRegionHeight() >> 1));
    batch.draw(Dot, getX() + 62, getY() + 30);
  }
}
