package io.wouter.won.gdx.actor;

import static io.wouter.won.gdx.anim.WonAgentActorAnimations.getAnimation;
import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.gdx.IsometricStaggeredPositionConversion;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;


public class WonAgentActor extends Actor {

  private static final Texture Dot;

  static {
    final Pixmap pixmap = new Pixmap(4, 4, Pixmap.Format.RGBA8888);
    pixmap.setColor(Color.MAGENTA);
    pixmap.fillRectangle(0, 0, 4, 4);
    Dot = new Texture(pixmap);
    pixmap.dispose();
  }



  private static final String Idle = "idle"; //$NON-NLS-1$
  private static final String Walk = "walk"; //$NON-NLS-1$



  private final WonAgent<WonNodeDelegate, WonBufferDelegate> agent;

  private final IsometricStaggeredPositionConversion posconv;
  private final Vector2 screenpos = new Vector2();

  private Animation<TextureRegion> anim;
  private float animtime = 0f;



  public WonAgentActor(final WonAgent<WonNodeDelegate, WonBufferDelegate> agent, final IsometricStaggeredPositionConversion posconv) {
    this.agent = requireNonNull(agent);
    this.posconv = requireNonNull(posconv);
    posconv.convertWorldToScreen(agent.getAgentDelegate().getPosition(), screenpos);
    setPosition(screenpos.x, screenpos.y);
    this.anim = getAnimation(Idle, agent.getAgentDelegate().getOrientation());
  }



  @Override
  public void act(final float delta) {
    super.act(delta);
    agent.getBehavior().perform(delta);

    posconv.convertWorldToScreen(agent.getAgentDelegate().getPosition(), screenpos);
    setPosition(screenpos.x, screenpos.y);

    // if(agent.isDisposed())
    // do something

    animtime += delta;
    if (animtime > anim.getAnimationDuration())
      animtime = 0f;
    anim = getAnimation(Walk, agent.getAgentDelegate().getOrientation());
  }

  @Override
  public void draw(final Batch batch, final float alpha) {
    super.draw(batch, alpha);
    final TextureRegion frame = anim.getKeyFrame(animtime);
    batch.draw(frame, getX() + 56, getY() + 24, 16, 16);
    batch.draw(Dot, getX() + 62, getY() + 30);
  }
}
