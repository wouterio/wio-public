package io.wouter.won.gdx.actor;

import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;

import io.wouter.won.entity.WonStructure;
import io.wouter.won.gdx.IsometricStaggeredPositionConversion;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class WonStructureActor extends Group {

  private static final Texture Dot;

  static {
    final Pixmap pixmap = new Pixmap(4, 4, Pixmap.Format.RGBA8888);
    pixmap.setColor(Color.CORAL);
    pixmap.fillRectangle(0, 0, 4, 4);
    Dot = new Texture(pixmap);
    pixmap.dispose();
  }



  private static final LabelStyle labelstyle = new LabelStyle(new BitmapFont(), Color.BLACK);



  private final WonStructure<WonNodeDelegate, WonBufferDelegate> structure;
  private final VerticalGroup vglabels = new VerticalGroup();
  private final Label ltype = new Label(null, labelstyle);
  private final Label lcredit = new Label(null, labelstyle);
  private final Label listock = new Label(null, labelstyle);
  private final Label lostock = new Label(null, labelstyle);
  private final Sprite sprite;
  private final Vector2 screenpos = new Vector2();



  public WonStructureActor(final WonStructure<WonNodeDelegate, WonBufferDelegate> structure, final Sprite sprite, final IsometricStaggeredPositionConversion posconv) {
    this.structure = requireNonNull(structure);
    this.sprite = requireNonNull(sprite);
    posconv.convertWorldToScreen(structure.getStructureDelegate().getPosition(), screenpos);
    setPosition(screenpos.x, screenpos.y);
    addActor(vglabels);
    vglabels.addActor(ltype);
    vglabels.addActor(lcredit);
    vglabels.addActor(listock);
    vglabels.addActor(lostock);
    vglabels.setY(vglabels.getPrefHeight());
    ltype.setText(structure.getType());
    setWidth(sprite.getWidth());
    setHeight(sprite.getHeight());
  }



  @Override
  public void act(final float delta) {
    super.act(delta);

    // if (structure.isDisposed())
    // do something

    final WonNodeDelegate d = structure.getStructureDelegate();
    lcredit.setText(d.getAccount().getCredits());
    listock.setText(d.getInput().getCount() + "/" + d.getInput().getCapacity());
    lostock.setText(d.getOutput().getCount() + "/" + d.getOutput().getCapacity());
  }



  @Override
  public void draw(final Batch batch, final float alpha) {
    batch.draw(sprite, getX() + 64 - (sprite.getRegionWidth() >> 1), getY() + 32 - (sprite.getRegionHeight() >> 1) + 13);
    super.draw(batch, alpha);
    batch.draw(Dot, getX() + 62, getY() + 30);
  }
}
