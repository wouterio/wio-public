package io.wouter.won.gdx;

import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

import io.wouter.lib.grid.Pos;

public class IsometricStaggeredCellSelection implements InputProcessor {

  private final OrthographicCamera cam;
  private final IsometricStaggeredPositionConversion posconv;
  private final CellSelectionListener listener;
  private final Vector3 temp = new Vector3();



  public IsometricStaggeredCellSelection(final OrthographicCamera cam, final IsometricStaggeredPositionConversion posconv, final CellSelectionListener listener) {
    this.cam = requireNonNull(cam);
    this.posconv = requireNonNull(posconv);
    this.listener = requireNonNull(listener);
  }



  @Override
  public boolean keyDown(final int keycode) {
    return false;
  }

  @Override
  public boolean keyUp(final int keycode) {
    return false;
  }

  @Override
  public boolean keyTyped(final char character) {
    return false;
  }



  @Override
  public boolean touchDown(final int screenX, final int screenY, final int pointer, final int button) {
    temp.set(screenX, screenY, 0f);
    cam.unproject(temp);
    final Pos pos = posconv.asPos(temp);
    return listener.cellSelected(pos);
  }

  @Override
  public boolean touchUp(final int screenX, final int screenY, final int pointer, final int button) {
    return false;
  }

  @Override
  public boolean touchDragged(final int screenX, final int screenY, final int pointer) {
    return false;
  }



  @Override
  public boolean mouseMoved(final int screenX, final int screenY) {
    return false;
  }

  @Override
  public boolean scrolled(final int amount) {
    return false;
  }



  //



  public interface CellSelectionListener {

    boolean cellSelected(Pos pos);
  }
}
