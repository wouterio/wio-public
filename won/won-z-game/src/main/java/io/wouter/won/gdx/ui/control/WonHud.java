package io.wouter.won.gdx.ui.control;

import static io.wouter.won.gdx.ui.control.WonLabel.newLabel;

import java.text.DecimalFormat;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import io.wouter.won.entity.model.WonBuildModel;
import io.wouter.won.object.WonObjects;
import io.wouter.won.object.structure.WonConstructorTemplate;
import io.wouter.won.util.string.WonInfoBuilder.GetFloat;
import io.wouter.won.util.string.WonInfoBuilder.GetInt;
import io.wouter.won.util.string.WonInfoBuilder.SetFloat;
import io.wouter.won.world.delegate.WonAccount;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class WonHud extends Table {

  private static final DecimalFormat Format = new DecimalFormat("#.##"); //$NON-NLS-1$



  public WonHud(final WonBuildModel<WonNodeDelegate, WonBufferDelegate> model, final Skin skin) {
    setDebug(true, true);
    setFillParent(true);

    final WonAccount account = model.getPlayer().getAccount();
    final GetInt getcredits = () -> account.getCredits();
    final GetFloat getrate = () -> account.getRate();
    final SetFloat setrate = rate -> account.setRate(rate);

    final Label lcredits1 = new Label("credits: ", skin); //$NON-NLS-1$
    final Label lcredits2 = newLabel(getcredits, skin);
    final Label lrate1 = new Label("rate: ", skin); //$NON-NLS-1$
    final Label lrate2 = newLabel(getrate, Format, skin);
    final Slider srate = new WonSlider(getrate, setrate, 0f, 1f, 0.01f, skin);
    final HorizontalGroup playergroup = new HorizontalGroup().space(20f);
    playergroup.addActor(lcredits1);
    playergroup.addActor(lcredits2);
    playergroup.addActor(lrate1);
    playergroup.addActor(lrate2);
    playergroup.addActor(srate);

    final HorizontalGroup constructionsgroup = new HorizontalGroup();
    WonObjects.getConstructors().forEach(c -> constructionsgroup.addActor(newConstructionButton(c, model, skin)));
    final ScrollPane scroll = new ScrollPane(constructionsgroup, skin);

    add(playergroup).center();
    row();
    add(new Actor()).expand();
    row();
    add(scroll).center();
  }



  private static Button newConstructionButton(final WonConstructorTemplate c, final WonBuildModel<WonNodeDelegate, WonBufferDelegate> model, final Skin skin) {
    final Button b = new TextButton(c.getConstruction().getStructureTemplate().getType(), skin);
    b.addListener(new ClickListener() {

      @Override
      public void clicked(final InputEvent event, final float x, final float y) {
        model.buildStructure(model.getPlayer(), c);
      }
    });
    return b;
  }
}
