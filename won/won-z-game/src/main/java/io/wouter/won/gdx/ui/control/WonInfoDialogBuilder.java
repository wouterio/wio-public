package io.wouter.won.gdx.ui.control;

import static io.wouter.won.gdx.ui.control.WonLabel.newLabel;
import static java.util.Objects.requireNonNull;

import java.text.DecimalFormat;
import java.util.Set;
import java.util.function.Function;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import io.wouter.won.util.string.WonInfoBuilder;

public class WonInfoDialogBuilder implements WonInfoBuilder {

  private static final DecimalFormat Format = new DecimalFormat("#.00"); //$NON-NLS-1$



  public static Dialog build(final String title, final Collect value, final Skin skin, final Stage stage) {
    final Dialog dialog = new Dialog(title, skin);
    build(dialog, value, skin, stage);
    dialog.setModal(true);
    dialog.setMovable(false);
    dialog.button("close");
    dialog.show(stage);
    return dialog;
  }

  public static void build(final Table table, final Collect value, final Skin skin, final Stage stage) {
    final Tree<WonInfoNode, String> tree = new Tree<>(skin);
    tree.setIconSpacing(1, 0);
    tree.setIndentSpacing(20);
    tree.setPadding(10);
    build(tree, value, skin, stage);
    final ScrollPane scrollpane = new ScrollPane(tree, skin);
    scrollpane.setFillParent(true);
    table.add(scrollpane).fill().expand().maxWidth(Gdx.graphics.getWidth()).maxHeight(Gdx.graphics.getHeight());
  }

  public static void build(final Tree<WonInfoNode, String> tree, final Collect value, final Skin skin, final Stage stage) {
    new WonInfoDialogBuilder(tree, skin, stage).collect(value);
  }



  private final Skin skin;
  private final Stage stage;



  private WonInfoNode current = null;
  private Group field = null;



  private WonInfoDialogBuilder(final Tree<WonInfoNode, String> tree, final Skin skin, final Stage stage) {
    this.skin = requireNonNull(skin);
    this.stage = requireNonNull(stage);
    final Group g = new HorizontalGroup();
    final Label l = new Label("main", skin); //$NON-NLS-1$
    this.current = new WonInfoNode(g);
    g.addActor(l);
    tree.add(current);
  }



  @Override
  public WonInfoDialogBuilder child(final String name) {
    final Group g = new HorizontalGroup();
    final Label l = new Label(name, skin);
    final WonInfoNode child = new WonInfoNode(g);
    l.setColor(Color.LIME);
    g.addActor(l);
    current.add(child);
    current = child;
    field = null;
    return this;
  }

  @Override
  public <C extends Collect> WonInfoBuilder child(final C value, final Function<C, String> mapper) {
    final Group g = new HorizontalGroup();
    final Button b = newLinkButton(value, mapper);
    final WonInfoNode child = new WonInfoNode(g);
    b.setColor(Color.LIME);
    g.addActor(b);
    current.add(child);
    current = child;
    field = null;
    return this;
  }

  @Override
  public WonInfoDialogBuilder parent() {
    if (current.getParent() != null)
      current = current.getParent();
    field = null;
    return this;
  }

  @Override
  public WonInfoBuilder header(final String name) {
    final Group g = new HorizontalGroup();
    final Label l = new Label(name, skin);
    final WonInfoNode header = new WonInfoNode(g);
    l.setColor(Color.ORANGE);
    g.addActor(l);
    current.add(header);
    field = null;
    return this;
  }

  @Override
  public <C extends Collect> WonInfoBuilder header(final C value, final Function<C, String> mapper) {
    final Group g = new HorizontalGroup();
    final Button b = newLinkButton(value, mapper);
    final WonInfoNode header = new WonInfoNode(g);
    b.setColor(Color.ORANGE);
    g.addActor(b);
    current.add(header);
    field = null;
    return this;
  }

  @Override
  public WonInfoBuilder separator() {
    final Group g = new HorizontalGroup();
    final Label l = new Label("", skin); //$NON-NLS-1$
    final WonInfoNode separator = new WonInfoNode(g);
    g.addActor(l);
    current.add(separator);
    field = null;
    return this;
  }

  @Override
  public WonInfoBuilder field(final String name) {
    field = new HorizontalGroup().space(20f);
    final Label l = new Label(name + ':', skin);
    l.setColor(Color.SKY);
    field.addActor(l);
    current.add(new WonInfoNode(field));
    return this;
  }

  @Override
  public WonInfoBuilder value(final String value) {
    final Label l = new Label(value, skin);
    field.addActor(l);
    return this;
  }

  @Override
  public WonInfoBuilder action(final Runnable r) {
    final WonTextButton b = new WonTextButton("    ", skin); //$NON-NLS-1$
    b.addListener(new ClickListener() {

      @Override
      public void clicked(final InputEvent event, final float x, final float y) {
        r.run();
      }
    });
    field.addActor(b);
    return this;
  }



  @Override
  public WonInfoBuilder valueBoolean(final GetBoolean get) {
    final WonCheckbox cb = new WonCheckbox(get, skin);
    field.addActor(cb);
    return this;
  }

  @Override
  public WonInfoBuilder valueInt(final GetInt get) {
    final WonLabel l = newLabel(get, skin);
    field.addActor(l);
    return this;
  }

  @Override
  public WonInfoBuilder valueLong(final GetLong get) {
    final WonLabel l = newLabel(get, skin);
    field.addActor(l);
    return this;
  }

  @Override
  public WonInfoBuilder valueFloat(final GetFloat get) {
    final WonLabel l = newLabel(get, Format, skin);
    field.addActor(l);
    return this;
  }

  @Override
  public WonInfoBuilder valueDouble(final GetDouble get) {
    final WonLabel l = newLabel(get, Format, skin);
    field.addActor(l);
    return this;
  }

  @Override
  public WonInfoBuilder valueString(final GetObject<String> get) {
    final WonLabel l = newLabel(get, skin);
    field.addActor(l);
    return this;
  }

  @Override
  public <T> WonInfoBuilder valueObject(final GetObject<T> get, final Function<T, String> mapper) {
    final WonLabel l = newLabel(get, mapper, skin);
    field.addActor(l);
    return this;
  }



  @Override
  public WonInfoDialogBuilder valueBoolean(final GetBoolean get, final SetBoolean set) {
    final WonCheckbox cb = new WonCheckbox(get, set, skin);
    field.addActor(cb);
    return this;
  }

  @Override
  public WonInfoDialogBuilder valueInt(final GetInt get, final SetInt set, final int min, final int max, final int step) {
    final WonLabel l = newLabel(get, skin);
    final WonSlider s = new WonSlider(get, set, min, max, step, skin);
    field.addActor(l);
    field.addActor(s);
    return this;
  }

  @Override
  public WonInfoDialogBuilder valueLong(final GetLong get, final SetLong set, final long min, final long max, final long step) {
    final WonLabel l = newLabel(get, skin);
    final WonSlider s = new WonSlider(get, set, min, max, step, skin);
    field.addActor(l);
    field.addActor(s);
    return this;
  }

  @Override
  public WonInfoDialogBuilder valueFloat(final GetFloat get, final SetFloat set, final float min, final float max, final float step) {
    final WonLabel l = newLabel(get, Format, skin);
    final WonSlider s = new WonSlider(get, set, min, max, step, skin);
    field.addActor(l);
    field.addActor(s);
    return this;
  }

  @Override
  public WonInfoDialogBuilder valueDouble(final GetDouble get, final SetDouble set, final double min, final double max, final double step) {
    final WonLabel l = newLabel(get, Format, skin);
    final WonSlider s = new WonSlider(get, set, min, max, step, skin);
    field.addActor(l);
    field.addActor(s);
    return this;
  }

  @Override
  public WonInfoBuilder valueString(final GetObject<String> get, final SetObject<String> set) {
    final WonTextField tf = new WonTextField(get, set, skin);
    field.addActor(tf);
    return this;
  }

  @Override
  public <T> WonInfoDialogBuilder valueObject(final GetObject<T> get, final SetObject<T> set, final Set<T> options, final Function<T, String> mapper) {
    final WonSelectBox<T> sb = new WonSelectBox<>(get, set, options, mapper, skin);
    field.addActor(sb);
    return this;
  }



  @Override
  public <C extends Collect> WonInfoBuilder link(final C value, final Function<C, String> mapper) {
    final WonTextButton b = newLinkButton(value, mapper);
    field.addActor(b);
    return this;
  }



  private <C extends Collect> WonTextButton newLinkButton(final C value, final Function<C, String> mapper) {
    final String text = mapper.apply(value);
    final WonTextButton b = new WonTextButton(text, skin);
    b.addListener(new ClickListener() {

      @Override
      public void clicked(final InputEvent event, final float x, final float y) {
        build(text, value, skin, stage);
      }
    });
    return b;
  }



  //



  private static class WonInfoNode extends Tree.Node<WonInfoNode, String, Group> {



    WonInfoNode(final Group group) {
      super(group);
      setExpanded(true);
      setSelectable(false);
    }
  }
}
