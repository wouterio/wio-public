package io.wouter.won.gdx.ui.control;

import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import io.wouter.won.util.string.WonInfoBuilder.GetBoolean;
import io.wouter.won.util.string.WonInfoBuilder.SetBoolean;

public class WonCheckbox extends CheckBox {

  private final GetBoolean get;



  public WonCheckbox(final GetBoolean get, final Skin skin) {
    super(null, skin);
    this.get = requireNonNull(get);
    setDisabled(true);
  }

  public WonCheckbox(final GetBoolean get, final SetBoolean set, final Skin skin) {
    super(null, skin);
    this.get = requireNonNull(get);
    requireNonNull(set);
    setDisabled(false);
    addListener(new ClickListener() {

      @Override
      public void clicked(final InputEvent event, final float x, final float y) {
        super.clicked(event, x, y);
        set.set(isChecked());
      }
    });
  }



  @Override
  public void act(final float delta) {
    super.act(delta);
    setChecked(get.get());
  }
}
