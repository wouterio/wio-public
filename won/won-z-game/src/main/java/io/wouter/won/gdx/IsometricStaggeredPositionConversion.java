package io.wouter.won.gdx;

import static com.badlogic.gdx.math.MathUtils.floor;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import io.wouter.lib.grid.Pos;

public class IsometricStaggeredPositionConversion {

  private final int tilew;
  private final int tileh;



  public IsometricStaggeredPositionConversion(final TiledMap map) {
    final MapProperties props = map.getProperties();
    this.tilew = props.get("tilewidth", Integer.class).intValue(); //$NON-NLS-1$
    this.tileh = props.get("tileheight", Integer.class).intValue(); //$NON-NLS-1$
  }



  public Vector2 convertWorldToScreen(final Vector2 world, final Vector2 screen) {
    screen.set(world.x, world.y);
    screen.x -= 1f;
    screen.x *= 0.5f;
    screen.x *= tilew;
    screen.y -= 1f;
    screen.y *= 0.5f;
    screen.y += 0.5f;
    screen.y *= tileh;
    return screen;
  }

  // public Vector2 convertScreenToWorld( final Vector2 screen, final Vector2 world) {
  // screen.set(world.x, world.y);
  // screen.x -= 1f;
  // screen.x *= 0.5f;
  // screen.x *= tilew;
  // screen.y -= 1f;
  // screen.y *= 0.5f;
  // screen.y += 0.5f;
  // screen.y *= tileh;
  // return screen;
  // }

  public Vector2 asVector(final Pos pos, final Vector2 out) {
    out.set(pos.i, pos.j);
    out.x -= 1f;
    out.x *= 0.5f;
    out.x *= tilew;
    out.y -= 1f;
    out.y *= 0.5f;
    out.y += 0.5f;
    out.y *= tileh;
    return out;
  }

  public Vector3 asVector(final Pos pos, final Vector3 out) {
    out.set(pos.i, pos.j, 0f);
    out.x -= 1f;
    out.x *= 0.5f;
    out.x *= tilew;
    out.y -= 1f;
    out.y *= 0.5f;
    out.y += 0.5f;
    out.y *= tileh;
    return out;
  }



  public Pos asPos(final Vector2 vec) {
    vec.x /= tilew;
    vec.x *= 2f;
    vec.y /= tileh;
    vec.y -= .5f;
    vec.y *= 2f;
    return asPos(vec.x, vec.y);
  }

  public Pos asPos(final Vector3 vec) {
    vec.x /= tilew;
    vec.x *= 2f;
    vec.y /= tileh;
    vec.y -= .5f;
    vec.y *= 2f;
    return asPos(vec.x, vec.y);
  }



  private static Pos asPos(final float x, final float y) {
    final int xfloor = floor(x);
    final int yfloor = floor(y);
    final float xremain = x - xfloor;
    final float yremain = y - yfloor;
    int i;
    int j;

    switch ((xfloor + yfloor) % 2) {

      case 0: // choose between upper-left and lower-right
        if (xremain < yremain) {
          i = xfloor;
          j = yfloor + 1;
        } else {
          i = xfloor + 1;
          j = yfloor;
        }
        break;

      case 1: // choose between lower-left and upper-right
        if (xremain < 1f - yremain) {
          i = xfloor;
          j = yfloor;
        } else {
          i = xfloor + 1;
          j = yfloor + 1;
        }
        break;

      default:
        throw new AssertionError();
    }

    assert (i + j) % 2 != 0;
    return Pos.get(i, j);
  }
}
