package io.wouter.won.gdx.anim;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public final class WonLoadAnimation {

  public static Animation<TextureRegion> loadAnimation(final TextureAtlas textures, final String name, final int rows, final int cols, final float frameDuration) {
    final TextureRegion framesheet = textures.findRegion(name);
    final int tilew = framesheet.getRegionWidth() / cols;
    final int tileh = framesheet.getRegionHeight() / rows;
    System.out.println(tilew);

    int x = framesheet.getRegionX();
    int y = framesheet.getRegionY();
    int index = 0;
    final int startX = x;
    final TextureRegion[] tiles = new TextureRegion[rows * cols];
    for (int row = 0; row < rows; row++, y += tileh) {
      x = startX;
      for (int col = 0; col < cols; col++, x += tilew)
        tiles[index++] = new TextureRegion(framesheet.getTexture(), x, y, tilew, tileh);
    }
    final Animation<TextureRegion> animation = new Animation<>(frameDuration, tiles);
    animation.setPlayMode(Animation.PlayMode.LOOP);
    return animation;
  }



  private WonLoadAnimation() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
