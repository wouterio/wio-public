package io.wouter.won.gdx.ui.control;

import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

import io.wouter.won.util.string.WonInfoBuilder.GetObject;
import io.wouter.won.util.string.WonInfoBuilder.SetObject;

public class WonTextField extends TextField {

  private final GetObject<String> get;



  public WonTextField(final GetObject<String> get, final SetObject<String> set, final Skin skin) {
    super(get.get(), skin);
    this.get = requireNonNull(get);
    requireNonNull(set);
    setTextFieldListener((textfield, c) -> {
      if (c == Input.Keys.ENTER)
        set.set(textfield.getText());
    });
  }



  @Override
  public void act(final float delta) {
    super.act(delta);
    if (!hasKeyboardFocus())
      setText(get.get());
  }
}
