package io.wouter.won.gdx.ui.control;

import static java.lang.Math.round;
import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import io.wouter.won.util.string.WonInfoBuilder.GetDouble;
import io.wouter.won.util.string.WonInfoBuilder.GetFloat;
import io.wouter.won.util.string.WonInfoBuilder.GetInt;
import io.wouter.won.util.string.WonInfoBuilder.GetLong;
import io.wouter.won.util.string.WonInfoBuilder.SetDouble;
import io.wouter.won.util.string.WonInfoBuilder.SetFloat;
import io.wouter.won.util.string.WonInfoBuilder.SetInt;
import io.wouter.won.util.string.WonInfoBuilder.SetLong;

public class WonSlider extends Slider {

  private final FloatSupplier get;



  public WonSlider(final GetInt get, final SetInt set, final int min, final int max, final int step, final Skin skin) {
    super(min, max, step, false, skin);
    requireNonNull(get);
    requireNonNull(set);
    this.get = () -> get.get();
    addListener(new ChangeListener() {

      @Override
      public void changed(final ChangeEvent event, final Actor actor) {
        final float rawvalue = getValue();
        final int rndvalue = round(rawvalue);
        set.set(rndvalue);
      }
    });
  }

  public WonSlider(final GetLong get, final SetLong set, final long min, final long max, final long step, final Skin skin) {
    super(min, max, step, false, skin);
    requireNonNull(get);
    requireNonNull(set);
    this.get = () -> get.get();
    addListener(new ChangeListener() {

      @Override
      public void changed(final ChangeEvent event, final Actor actor) {
        final float rawvalue = getValue();
        final long rndvalue = round(rawvalue);
        set.set(rndvalue);
      }
    });
  }

  public WonSlider(final GetFloat get, final SetFloat set, final float min, final float max, final float step, final Skin skin) {
    super(min, max, step, false, skin);
    requireNonNull(get);
    requireNonNull(set);
    this.get = () -> get.get();
    addListener(new ChangeListener() {

      @Override
      public void changed(final ChangeEvent event, final Actor actor) {
        final float value = getValue();
        set.set(value);
      }
    });
  }

  public WonSlider(final GetDouble get, final SetDouble set, final double min, final double max, final double step, final Skin skin) {
    super((float) min, (float) max, (float) step, false, skin);
    requireNonNull(get);
    requireNonNull(set);
    this.get = () -> (float) get.get();
    addListener(new ChangeListener() {

      @Override
      public void changed(final ChangeEvent event, final Actor actor) {
        final float value = getValue();
        set.set(value);
      }
    });
  }



  @Override
  public void act(final float delta) {
    super.act(delta);
    setValue(get.get());
  }



  //



  private interface FloatSupplier {

    float get();
  }
}
