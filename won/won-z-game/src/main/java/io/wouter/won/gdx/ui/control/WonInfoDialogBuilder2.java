package io.wouter.won.gdx.ui.control;

import static io.wouter.won.gdx.ui.control.WonLabel.newLabel;
import static java.util.Objects.requireNonNull;

import java.text.DecimalFormat;
import java.util.Set;
import java.util.function.Function;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import io.wouter.won.util.string.WonInfoBuilder;

public class WonInfoDialogBuilder2 implements WonInfoBuilder {

  private static final DecimalFormat Format = new DecimalFormat("#.##"); //$NON-NLS-1$



  public static Dialog build(final String title, final Collect subject, final Skin skin, final Stage stage) {
    final Dialog dialog = new Dialog(title, skin);
    WonInfoDialogBuilder2.builder(dialog, skin, stage).collect(subject);
    dialog.setModal(true);
    dialog.setMovable(false);
    dialog.button("close");
    return dialog;
  }

  public static WonInfoBuilder builder(final Table table, final Skin skin, final Stage stage) {
    final Tree<WonInfoNode, String> tree = new Tree<>(skin);
    final ScrollPane scrollpane = new ScrollPane(tree, skin);
    tree.setPadding(10);
    tree.setIndentSpacing(20);
    tree.setIconSpacing(1, 0);
    scrollpane.setFillParent(true);
    table.add(scrollpane).fill().expand().maxWidth(Gdx.graphics.getWidth()).maxHeight(Gdx.graphics.getHeight());
    return new WonInfoDialogBuilder2(tree, skin, stage);
  }



  private final Skin skin;
  private final Stage stage;



  private WonInfoNode current = null;



  public WonInfoDialogBuilder2(final Tree<WonInfoNode, String> tree, final Skin skin, final Stage stage) {
    this.skin = requireNonNull(skin);
    this.stage = requireNonNull(stage);
    final Table t = new Table(skin);
    this.current = new WonInfoNode(t);
    tree.add(current);
  }



  @Override
  public WonInfoBuilder child(final String name) {
    final Table t = new Table(skin);
    final Label l = new Label(name, skin);
    final WonInfoNode child = new WonInfoNode(t);
    t.add(l).left();
    current.add(child);
    current = child;
    return this;
  }

  @Override
  public <C extends Collect> WonInfoBuilder child(final C value, final Function<C, String> mapper) {
    final Table t = new Table(skin);
    final Button b = newLinkButton(value, mapper);
    final WonInfoNode child = new WonInfoNode(t);
    t.add(b).left();
    current.add(child);
    current = child;
    return this;
  }

  @Override
  public WonInfoBuilder parent() {
    if (current.getParent() != null)
      current = current.getParent();
    return this;
  }

  @Override
  public WonInfoBuilder header(final String name) {
    final Label l = new Label(name, skin);
    current.getActor().row();
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public <C extends Collect> WonInfoBuilder header(final C value, final Function<C, String> mapper) {
    final Button b = newLinkButton(value, mapper);
    current.getActor().row();
    current.getActor().add(b).left();
    return this;
  }

  @Override
  public WonInfoBuilder separator() {
    final Label l = new Label("", skin); //$NON-NLS-1$
    current.getActor().row();
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder field(final String name) {
    final Label l = new Label(name + ':', skin);
    current.getActor().row();
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder value(final String value) {
    final Label l = new Label(value, skin);
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder action(final Runnable r) {
    final WonTextButton b = new WonTextButton("   ", skin);
    b.addListener(new ClickListener() {

      @Override
      public void clicked(final InputEvent event, final float x, final float y) {
        r.run();
      }
    });
    current.getActor().add(b).left();
    return this;
  }



  @Override
  public WonInfoBuilder valueBoolean(final GetBoolean get) {
    final WonCheckbox cb = new WonCheckbox(get, skin);
    current.getActor().add(cb).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueInt(final GetInt get) {
    final WonLabel l = newLabel(get, skin);
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueLong(final GetLong get) {
    final WonLabel l = newLabel(get, skin);
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueFloat(final GetFloat get) {
    final WonLabel l = newLabel(get, Format, skin);
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueDouble(final GetDouble get) {
    final WonLabel l = newLabel(get, Format, skin);
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueString(final GetObject<String> get) {
    final WonLabel l = newLabel(get, skin);
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public <T> WonInfoBuilder valueObject(final GetObject<T> get, final Function<T, String> mapper) {
    final WonLabel l = newLabel(get, mapper, skin);
    current.getActor().add(l).left();
    return this;
  }



  @Override
  public WonInfoBuilder valueBoolean(final GetBoolean get, final SetBoolean set) {
    final WonCheckbox cb = new WonCheckbox(get, set, skin);
    current.getActor().add(cb).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueInt(final GetInt get, final SetInt set, final int min, final int max, final int step) {
    final WonSlider s = new WonSlider(get, set, min, max, step, skin);
    final WonLabel l = newLabel(get, skin);
    current.getActor().add(s).left();
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueLong(final GetLong get, final SetLong set, final long min, final long max, final long step) {
    final WonSlider s = new WonSlider(get, set, min, max, step, skin);
    final WonLabel l = newLabel(get, skin);
    current.getActor().add(s).left();
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueFloat(final GetFloat get, final SetFloat set, final float min, final float max, final float step) {
    final WonSlider s = new WonSlider(get, set, min, max, step, skin);
    final WonLabel l = newLabel(get, Format, skin);
    current.getActor().add(s).left();
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueDouble(final GetDouble get, final SetDouble set, final double min, final double max, final double step) {
    final WonSlider s = new WonSlider(get, set, min, max, step, skin);
    final WonLabel l = newLabel(get, Format, skin);
    current.getActor().add(s).left();
    current.getActor().add(l).left();
    return this;
  }

  @Override
  public WonInfoBuilder valueString(final GetObject<String> get, final SetObject<String> set) {
    final WonTextField tf = new WonTextField(get, set, skin);
    current.getActor().add(tf).left();
    return this;
  }

  @Override
  public <T> WonInfoBuilder valueObject(final GetObject<T> get, final SetObject<T> set, final Set<T> options, final Function<T, String> mapper) {
    final WonSelectBox<T> sb = new WonSelectBox<>(get, set, options, mapper, skin);
    current.getActor().add(sb).left();
    return this;
  }



  @Override
  public <C extends Collect> WonInfoBuilder link(final C value, final Function<C, String> mapper) {
    final WonTextButton b = newLinkButton(value, mapper);
    current.getActor().add(b).left();
    return this;
  }



  private <C extends Collect> WonTextButton newLinkButton(final C value, final Function<C, String> mapper) {
    final String text = mapper.apply(value);
    final WonTextButton button = new WonTextButton(text, skin);
    button.addListener(new ClickListener() {

      @Override
      public void clicked(final InputEvent event, final float x, final float y) {
        build(text, value, skin, stage).show(stage);
      }
    });
    return button;
  }



  //



  private static class WonInfoNode extends Tree.Node<WonInfoNode, String, Table> {



    WonInfoNode(final Table table) {
      super(table);
      setExpanded(true);
      setSelectable(false);
    }
  }
}
