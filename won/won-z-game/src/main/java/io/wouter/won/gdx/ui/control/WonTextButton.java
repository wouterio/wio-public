package io.wouter.won.gdx.ui.control;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class WonTextButton extends TextButton {

  public WonTextButton(final String text, final Skin skin) {
    super(text, skin);
  }
}
