package io.wouter.won.gdx.ui.control;

import static java.util.Objects.requireNonNull;

import java.text.DecimalFormat;
import java.util.function.Function;
import java.util.function.Supplier;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import io.wouter.won.util.string.WonInfoBuilder.GetDouble;
import io.wouter.won.util.string.WonInfoBuilder.GetFloat;
import io.wouter.won.util.string.WonInfoBuilder.GetInt;
import io.wouter.won.util.string.WonInfoBuilder.GetLong;
import io.wouter.won.util.string.WonInfoBuilder.GetObject;

public class WonLabel extends Label {

  public static WonLabel newLabel(final GetInt get, final Skin skin) {
    requireNonNull(get);
    final Supplier<String> supplier = new Supplier<String>() {

      int oldvalue = get.get();
      String s = Integer.toString(oldvalue);

      @Override
      public String get() {
        final int newvalue = get.get();
        if (newvalue != oldvalue) {
          oldvalue = newvalue;
          s = Integer.toString(oldvalue);
        }
        return s;
      }
    };
    return new WonLabel(supplier, skin);
  }



  public static WonLabel newLabel(final GetLong get, final Skin skin) {
    requireNonNull(get);
    final Supplier<String> supplier = new Supplier<String>() {

      long oldvalue = get.get();
      String s = Long.toString(oldvalue);

      @Override
      public String get() {
        final long newvalue = get.get();
        if (newvalue != oldvalue) {
          oldvalue = newvalue;
          s = Long.toString(oldvalue);
        }
        return s;
      }
    };
    return new WonLabel(supplier, skin);
  }



  public static WonLabel newLabel(final GetFloat get, final DecimalFormat df, final Skin skin) {
    requireNonNull(get);
    final Supplier<String> supplier = new Supplier<String>() {

      float oldvalue = get.get();
      String s = df.format(oldvalue);

      @Override
      public String get() {
        final float newvalue = get.get();
        if (newvalue != oldvalue) {
          oldvalue = newvalue;
          s = df.format(newvalue);
        }
        return s;
      }
    };
    return new WonLabel(supplier, skin);
  }



  public static WonLabel newLabel(final GetDouble get, final DecimalFormat df, final Skin skin) {
    requireNonNull(get);
    final Supplier<String> supplier = new Supplier<String>() {

      double oldvalue = get.get();
      String s = df.format(oldvalue);

      @Override
      public String get() {
        final double newvalue = get.get();
        if (newvalue != oldvalue) {
          oldvalue = newvalue;
          s = df.format(oldvalue);
        }
        return s;
      }
    };
    return new WonLabel(supplier, skin);
  }



  public static WonLabel newLabel(final GetObject<String> get, final Skin skin) {
    requireNonNull(get);
    return new WonLabel(() -> get.get(), skin); // no need checking whether new string same as old string
  }

  public static <T> WonLabel newLabel(final GetObject<T> get, final Function<T, String> mapper, final Skin skin) {
    requireNonNull(get);
    return new WonLabel(() -> mapper.apply(get.get()), skin); // always update as same object might map to different string
  }



  private final Supplier<String> get;



  private WonLabel(final Supplier<String> get, final Skin skin) {
    super(null, skin);
    this.get = get;
  }

  private WonLabel(final Supplier<String> get, final Skin skin, final String style) {
    super(null, skin, style);
    this.get = get;
  }



  @Override
  public void act(final float delta) {
    super.act(delta);
    setText(get.get());
  }
}
