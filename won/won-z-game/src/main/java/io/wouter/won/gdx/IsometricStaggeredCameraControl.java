package io.wouter.won.gdx;

import static java.lang.Integer.signum;
import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;

public class IsometricStaggeredCameraControl extends InputAdapter {

  private final OrthographicCamera cam;
  private final Vector2 move = new Vector2();

  private final int mapwidth;
  private final int mapheight;
  private final int halftilew;
  private final int halftileh;

  private float movespeed = 1f;
  private float zoomspeed = 0.01f;

  private boolean keyzoomin = false;
  private boolean keyzoomout = false;
  private boolean keymovelf = false;
  private boolean keymoverg = false;
  private boolean keymoveup = false;
  private boolean keymovedn = false;

  private boolean mousezoomin = false;
  private boolean mousezoomout = false;
  private boolean mousemovelf = false;
  private boolean mousemoverg = false;
  private boolean mousemoveup = false;
  private boolean mousemovedn = false;



  public IsometricStaggeredCameraControl(final OrthographicCamera cam, final TiledMap map) {
    this.cam = requireNonNull(cam);
    final MapProperties props = map.getProperties();
    final int mw = props.get("width", Integer.class).intValue(); //$NON-NLS-1$
    final int mh = props.get("height", Integer.class).intValue(); //$NON-NLS-1$
    final int tw = props.get("tilewidth", Integer.class).intValue(); //$NON-NLS-1$
    final int th = props.get("tileheight", Integer.class).intValue(); //$NON-NLS-1$
    this.halftilew = tw >> 1;
    this.halftileh = th >> 1;
    this.mapwidth = mw * tw - halftilew;
    this.mapheight = (mh >> 1) * th - halftileh;
  }



  public float getMoveSpeed() {
    return movespeed;
  }

  public void setMoveSpeed(final float movespeed) {
    this.movespeed = movespeed;
  }

  public float getZoomSpeed() {
    return zoomspeed;
  }

  public void setZoomSpeed(final float zoomspeed) {
    this.zoomspeed = zoomspeed;
  }



  public void update() {
    move.scl(.85f);

    if (keyzoomin || mousezoomin)
      cam.zoom -= zoomspeed;
    if (keyzoomout || mousezoomout)
      cam.zoom += zoomspeed;
    if (keymovelf || mousemovelf)
      move.x -= movespeed;
    if (keymoverg || mousemoverg)
      move.x += movespeed;
    if (keymoveup || mousemoveup)
      move.y += movespeed;
    if (keymovedn || mousemovedn)
      move.y -= movespeed;

    cam.position.x += move.x;
    cam.position.y += move.y;

    if (cam.zoom < .2f) {
      cam.zoom = .2f;
      keyzoomin = false;
      mousezoomin = false;
    }

    final float maxzoom = Math.min(mapwidth / cam.viewportWidth, mapheight / cam.viewportHeight);
    if (cam.zoom > maxzoom) {
      cam.zoom = maxzoom;
      keyzoomout = false;
      mousezoomout = false;
    }

    final float boundlf = .5f * cam.viewportWidth * cam.zoom;
    if (cam.position.x < boundlf) {
      cam.position.x = boundlf;
      move.x = 0f;
      keymovelf = false;
      mousemovelf = false;
    }

    final float boundrg = mapwidth - boundlf;
    if (cam.position.x > boundrg) {
      cam.position.x = boundrg;
      move.x = 0f;
      keymoverg = false;
      mousemoverg = false;
    }

    final float bounddn = .5f * cam.viewportHeight * cam.zoom;
    if (cam.position.y < bounddn + halftileh) {
      cam.position.y = bounddn + halftileh;
      move.y = 0f;
      keymoveup = false;
      mousemoveup = false;
    }

    final float boundup = mapheight - bounddn;
    if (cam.position.y > boundup + halftileh) {
      cam.position.y = boundup + halftileh;
      move.y = 0f;
      keymovedn = false;
      mousemovedn = false;
    }
  }



  @Override
  public boolean keyDown(final int keycode) {
    return processKey(keycode, true);
  }

  @Override
  public boolean keyUp(final int keycode) {
    return processKey(keycode, false);
  }

  @Override
  public boolean keyTyped(final char character) {
    return false;
  }



  private boolean processKey(final int keycode, final boolean pressed) {
    switch (keycode) {

      case Input.Keys.R:
        keyzoomin = pressed;
        return true;

      case Input.Keys.F:
        keyzoomout = pressed;
        return true;

      case Input.Keys.A:
        keymovelf = pressed;
        return true;

      case Input.Keys.D:
        keymoverg = pressed;
        return true;

      case Input.Keys.W:
        keymoveup = pressed;
        return true;

      case Input.Keys.S:
        keymovedn = pressed;
        return true;

      default:
        break;
    }

    return false;
  }



  @Override
  public boolean touchDown(final int screenX, final int screenY, final int pointer, final int button) {
    return false;
  }

  @Override
  public boolean touchUp(final int screenX, final int screenY, final int pointer, final int button) {
    return false;
  }

  @Override
  public boolean touchDragged(final int screenX, final int screenY, final int pointer) {
    return false;
  }



  @Override
  public boolean mouseMoved(final int screenX, final int screenY) {
    final int w = Gdx.graphics.getWidth();
    final int h = Gdx.graphics.getHeight();
    mousemovelf = screenX >= 0f && screenX < .1f * w;
    mousemoverg = screenX >= .9f * w && screenX <= w;
    mousemoveup = screenY >= 0f && screenY < .1f * h;
    mousemovedn = screenY >= .9f * h && screenY <= h;
    return false;
  }

  @Override
  public boolean scrolled(final int amount) {
    switch (signum(amount)) {

      case -1:
        mousezoomin = !mousezoomout;
        mousezoomout = false;
        return true;

      case 1:
        mousezoomout = !mousezoomin;
        mousezoomin = false;
        return true;

      default:
        break;
    }
    return false;
  }
}
