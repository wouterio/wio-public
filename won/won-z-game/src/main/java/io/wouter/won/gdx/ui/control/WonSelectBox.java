package io.wouter.won.gdx.ui.control;

import static java.util.Objects.requireNonNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import io.wouter.won.util.string.WonInfoBuilder.GetObject;
import io.wouter.won.util.string.WonInfoBuilder.SetObject;

public class WonSelectBox<T> extends SelectBox<String> {

  private final Map<String, T> objectmap;
  private final GetObject<T> get;
  private final Function<T, String> mapper;



  public WonSelectBox(final GetObject<T> get, final SetObject<T> set, final Collection<T> options, final Function<T, String> mapper, final Skin skin) {
    super(skin);
    this.objectmap = options.stream().collect(toMap(o -> mapper.apply(o), identity()));
    this.get = requireNonNull(get);
    requireNonNull(set);
    this.mapper = requireNonNull(mapper);
    setItems(objectmap.keySet().toArray(new String[objectmap.size()]));
    setSelected(mapper.apply(get.get()));
    addListener(new ChangeListener() {

      @Override
      public void changed(final ChangeEvent event, final Actor actor) {
        final String s = getSelected();
        final T selected = objectmap.get(s);
        set.set(selected);
      }
    });
  }



  @Override
  public void act(final float delta) {
    super.act(delta);
    final T value = get.get();
    final String s = mapper.apply(value);
    setSelected(s);
  }
}
