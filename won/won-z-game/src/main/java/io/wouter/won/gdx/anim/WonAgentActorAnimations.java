package io.wouter.won.gdx.anim;

import static io.wouter.won.gdx.anim.WonLoadAnimation.loadAnimation;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import io.wouter.lib.grid.Ori;

public final class WonAgentActorAnimations {

  private static final Map<String, Map<Ori, Animation<TextureRegion>>> Animations = new HashMap<>();

  private static final String Idle = "idle"; //$NON-NLS-1$
  private static final String Walk = "walk"; //$NON-NLS-1$



  static {
    final TextureAtlas textures = new TextureAtlas("sprites/farmer2.txt");
    final float frameDuration = 1f / 24f;

    final Map<Ori, Animation<TextureRegion>> idles = new EnumMap<>(Ori.class);
    idles.put(Ori.N, loadAnimation(textures, "farmer-idle0-n", 1, 4, frameDuration));
    idles.put(Ori.NE, loadAnimation(textures, "farmer-idle1-ne", 1, 4, frameDuration));
    idles.put(Ori.E, loadAnimation(textures, "farmer-idle2-e", 1, 4, frameDuration));
    idles.put(Ori.SE, loadAnimation(textures, "farmer-idle3-se", 1, 4, frameDuration));
    idles.put(Ori.S, loadAnimation(textures, "farmer-idle4-s", 1, 4, frameDuration));
    idles.put(Ori.SW, loadAnimation(textures, "farmer-idle5-sw", 1, 4, frameDuration));
    idles.put(Ori.W, loadAnimation(textures, "farmer-idle6-w", 1, 4, frameDuration));
    idles.put(Ori.NW, loadAnimation(textures, "farmer-idle7-nw", 1, 4, frameDuration));
    Animations.put(Idle, idles);

    final Map<Ori, Animation<TextureRegion>> walks = new EnumMap<>(Ori.class);
    walks.put(Ori.N, loadAnimation(textures, "farmer-walk0-n", 1, 15, frameDuration));
    walks.put(Ori.NE, loadAnimation(textures, "farmer-walk1-ne", 1, 15, frameDuration));
    walks.put(Ori.E, loadAnimation(textures, "farmer-walk2-e", 1, 15, frameDuration));
    walks.put(Ori.SE, loadAnimation(textures, "farmer-walk3-se", 1, 15, frameDuration));
    walks.put(Ori.S, loadAnimation(textures, "farmer-walk4-s", 1, 15, frameDuration));
    walks.put(Ori.SW, loadAnimation(textures, "farmer-walk5-sw", 1, 15, frameDuration));
    walks.put(Ori.W, loadAnimation(textures, "farmer-walk6-w", 1, 15, frameDuration));
    walks.put(Ori.NW, loadAnimation(textures, "farmer-walk7-nw", 1, 15, frameDuration));
    Animations.put(Walk, walks);
  }



  public static Animation<TextureRegion> getAnimation(final String action, final Ori ori) {
    return Animations.get(action).get(ori);
  }



  private WonAgentActorAnimations() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
