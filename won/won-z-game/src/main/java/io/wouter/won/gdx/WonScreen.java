package io.wouter.won.gdx;

import static java.util.Objects.requireNonNull;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.IsometricStaggeredTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.entity.model.WonBuildListener;
import io.wouter.won.gdx.IsometricStaggeredCellSelection.CellSelectionListener;
import io.wouter.won.gdx.actor.WonAgentActor;
import io.wouter.won.gdx.actor.WonStructureActor;
import io.wouter.won.gdx.actor.WonTransporterActor;
import io.wouter.won.gdx.ui.control.WonHud;
import io.wouter.won.gdx.ui.control.WonInfoDialogBuilder;
import io.wouter.won.object.WonObject;
import io.wouter.won.world.WonEnvironment;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class WonScreen implements Screen {

  private static final Comparator<Actor> CompareZOrder = (a, b) -> -Float.compare(a.getY(), b.getY());



  private final WonEnvironment env;


  private Skin skin;
  private TextureAtlas buildings;
  private Map<String, Sprite> sprites;
  private TiledMap map;
  private CellSelectionListener cellselected;
  private IsometricStaggeredTiledMapRenderer maprenderer;
  private IsometricStaggeredCameraControl camcontrol;
  private IsometricStaggeredPositionConversion posconv;
  private IsometricStaggeredCellSelection cellselection;
  private WonHud hud;
  private WonBuildListener<WonNodeDelegate, WonBufferDelegate> buildlistener;

  private OrthographicCamera viewcam;
  private OrthographicCamera uicam;
  private Stage view;
  private Stage ui;

  // private final ShapeRenderer sr = new ShapeRenderer(); // TODO remove
  // final int tileWidth = map.getProperties().get("tilewidth", Integer.class).intValue();
  // final int tileHeight = map.getProperties().get("tileheight", Integer.class).intValue();
  // final int mapWidth = map.getProperties().get("width", Integer.class).intValue() * tileWidth;
  // final int mapHeight = map.getProperties().get("height", Integer.class).intValue() * tileHeight;



  public WonScreen(final WonEnvironment env) {
    this.env = requireNonNull(env);
  }



  @Override
  public void show() {
    final DisplayMode dm = Gdx.graphics.getDisplayMode();
    Gdx.graphics.setFullscreenMode(dm);

    skin = new Skin(Gdx.files.internal("data/uiskin.json"));
    map = new TmxMapLoader().load("maps/kenney-iso-128.tmx");
    buildings = new TextureAtlas("sprites/buildings.txt");
    sprites = new HashMap<>();
    for (final AtlasRegion r : buildings.getRegions())
      sprites.computeIfAbsent(r.name, name -> buildings.createSprite(name));

    viewcam = new OrthographicCamera();
    uicam = new OrthographicCamera();
    viewcam.setToOrtho(false);
    uicam.setToOrtho(false);

    cellselected = pos -> {
      env.getBuildModel().getPlayer().setHome(pos);
      return true;
    };
    maprenderer = new IsometricStaggeredTiledMapRenderer(map);
    posconv = new IsometricStaggeredPositionConversion(map);
    camcontrol = new IsometricStaggeredCameraControl(viewcam, map);
    cellselection = new IsometricStaggeredCellSelection(viewcam, posconv, cellselected);
    hud = new WonHud(env.getBuildModel(), skin);
    buildlistener = new WonBuildListenerI();

    view = new Stage(new ScreenViewport(viewcam));
    ui = new Stage(new ScreenViewport(uicam));

    env.getBuildModel().addBuildListener(buildlistener);
    ui.addActor(hud);
    Gdx.input.setInputProcessor(new InputMultiplexer(ui, view, camcontrol, cellselection));
  }



  @Override
  public void hide() {

  }



  @Override
  public void render(final float delta) {
    Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    env.perform(delta);
    camcontrol.update();
    viewcam.update();
    maprenderer.setView(viewcam);
    maprenderer.render();
    view.act(delta);
    view.getActors().sort(CompareZOrder);
    view.draw();
    ui.act(delta);
    ui.draw();

    // sr.setProjectionMatrix(viewcam.combined);
    // sr.begin(ShapeType.Line);
    // for (int x = 0; x < mapWidth; x += .5f * tileWidth)
    // sr.line(x, 0, x, mapHeight);
    // for (int y = 0; y < mapHeight; y += .5f * tileHeight)
    // sr.line(0, y, mapWidth, y);
    // sr.end();
  }



  @Override
  public void resize(final int width, final int height) {
    camcontrol.update();
    viewcam.setToOrtho(false, width, height);
    uicam.setToOrtho(false, width, height);
  }



  @Override
  public void pause() {

  }

  @Override
  public void resume() {

  }



  @Override
  public void dispose() {
    map.dispose();
    maprenderer.dispose();
    buildings.dispose();
    view.dispose();
    ui.dispose();
  }



  //



  private final class WonBuildListenerI implements WonBuildListener<WonNodeDelegate, WonBufferDelegate> {

    WonBuildListenerI() {}



    @Override
    public void agentBuilt(final WonAgent<WonNodeDelegate, WonBufferDelegate> a) {
      final WonAgentActor aa = new WonAgentActor(a, posconv);
      aa.addListener(new WonObjectClickListener(a));
      view.addActor(aa);
    }

    @Override
    public void transporterBuilt(final WonTransporter<WonNodeDelegate, WonBufferDelegate> t) {
      final WonTransporterActor ta = new WonTransporterActor(t, posconv);
      ta.addListener(new WonObjectClickListener(t));
      view.addActor(ta);
    }

    @Override
    public void structureBuilt(final WonStructure<WonNodeDelegate, WonBufferDelegate> s) {
      final WonStructureActor sa = new WonStructureActor(s, sprites.get("building1"), posconv);
      sa.addListener(new WonObjectClickListener(s));
      view.addActor(sa);
    }

    @Override
    public void agentDestroyed(final WonAgent<WonNodeDelegate, WonBufferDelegate> a) {
      // TODO Auto-generated method stub
    }

    @Override
    public void transporterDestroyed(final WonTransporter<WonNodeDelegate, WonBufferDelegate> t) {
      // TODO Auto-generated method stub
    }

    @Override
    public void structureDestroyed(final WonStructure<WonNodeDelegate, WonBufferDelegate> s) {
      // TODO Auto-generated method stub
    }
  }



  //



  private final class WonObjectClickListener extends ClickListener {

    private final WonObject o;



    WonObjectClickListener(final WonObject o) {
      this.o = o;
    }



    @Override
    public void clicked(final InputEvent event, final float x, final float y) {
      WonInfoDialogBuilder.build(o.getType(), o, skin, ui);
    }
  }
}
