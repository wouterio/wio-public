package io.wouter.won.gdx;

import com.badlogic.gdx.Game;

import io.wouter.lib.grid.Pos;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.object.WonObjects;
import io.wouter.won.object.structure.WonStructureTemplate;
import io.wouter.won.world.WonEnvironment;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class WonGame extends Game {

  @Override
  public void create() {
    final WonEnvironment env = new WonEnvironment();
    final WonScreen scr = new WonScreen(env);
    setScreen(scr);

    final WonStructureTemplate tresidence = WonObjects.getStructure("residence");
    final WonStructureTemplate tgather = WonObjects.getStructure("gather");
    final WonStructureTemplate tcartbuilder = WonObjects.getStructure("cart builder");
    final WonStructureTemplate tcart = WonObjects.getStructure("cart");

    final WonNodeDelegate refresidence = new WonNodeDelegate(0, "ref residence", null, null, 0, Pos.get(16, 17));
    final WonNodeDelegate refgather = new WonNodeDelegate(0, "ref gather", null, null, 0, Pos.get(12, 17));
    final WonNodeDelegate refcartbuilder = new WonNodeDelegate(0, "ref cart builder", null, null, 0, Pos.get(12, 21));

    final WonStructure<WonNodeDelegate, WonBufferDelegate> residence = (WonStructure<WonNodeDelegate, WonBufferDelegate>) env.getBuildModel().buildStructure(refresidence, tresidence);
    final WonStructure<WonNodeDelegate, WonBufferDelegate> gather = (WonStructure<WonNodeDelegate, WonBufferDelegate>) env.getBuildModel().buildStructure(refgather, tgather);
    final WonStructure<WonNodeDelegate, WonBufferDelegate> cartbuilder = (WonStructure<WonNodeDelegate, WonBufferDelegate>) env.getBuildModel().buildStructure(refcartbuilder, tcartbuilder);
    final WonTransporter<WonNodeDelegate, WonBufferDelegate> cart = (WonTransporter<WonNodeDelegate, WonBufferDelegate>) env.getBuildModel().buildStructure(cartbuilder.getStructureDelegate(), tcart);
  }
}
