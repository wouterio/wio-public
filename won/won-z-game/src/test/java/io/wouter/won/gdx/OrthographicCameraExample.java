package io.wouter.won.gdx;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.IsometricStaggeredTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;

public class OrthographicCameraExample implements ApplicationListener {

  private TiledMap map;
  private IsometricStaggeredTiledMapRenderer maprenderer;

  private OrthographicCamera cam;
  private float rotationSpeed;

  private int mapw;
  private int maph;



  @Override
  public void create() {
    map = new TmxMapLoader().load("maps/test-isometric.tmx");
    maprenderer = new IsometricStaggeredTiledMapRenderer(map);
    final MapProperties prop = map.getProperties();

    mapw = prop.get("width", Integer.class) * prop.get("tilewidth", Integer.class);
    maph = prop.get("height", Integer.class) * prop.get("tileheight", Integer.class);

    rotationSpeed = 0.5f;

    final float w = Gdx.graphics.getWidth();
    final float h = Gdx.graphics.getHeight();

    // Constructs a new OrthographicCamera, using the given viewport width and height
    // Height is multiplied by aspect ratio.
    cam = new OrthographicCamera(30, 30 * (h / w));

    cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
    cam.update();
  }

  @Override
  public void render() {
    handleInput();
    cam.update();

    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    maprenderer.setView(cam);
    maprenderer.render();
  }

  private void handleInput() {
    if (Gdx.input.isKeyPressed(Input.Keys.A))
      cam.zoom += 0.02;
    if (Gdx.input.isKeyPressed(Input.Keys.Q))
      cam.zoom -= 0.02;
    if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
      cam.translate(-3, 0, 0);
    if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
      cam.translate(3, 0, 0);
    if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
      cam.translate(0, -3, 0);
    if (Gdx.input.isKeyPressed(Input.Keys.UP))
      cam.translate(0, 3, 0);
    if (Gdx.input.isKeyPressed(Input.Keys.W))
      cam.rotate(-rotationSpeed, 0, 0, 1);
    if (Gdx.input.isKeyPressed(Input.Keys.E))
      cam.rotate(rotationSpeed, 0, 0, 1);

    cam.zoom = MathUtils.clamp(cam.zoom, 0.1f, mapw / cam.viewportWidth);

    final float effectiveViewportWidth = cam.viewportWidth * cam.zoom;
    final float effectiveViewportHeight = cam.viewportHeight * cam.zoom;

    cam.position.x = MathUtils.clamp(cam.position.x, effectiveViewportWidth / 2f, mapw - effectiveViewportWidth / 2f);
    cam.position.y = MathUtils.clamp(cam.position.y, effectiveViewportHeight / 2f, maph - effectiveViewportHeight / 2f);
  }

  @Override
  public void resize(final int width, final int height) {
    cam.viewportWidth = 30f;
    cam.viewportHeight = 30f * height / width;
    cam.update();
  }

  @Override
  public void resume() {}

  @Override
  public void dispose() {
    map.dispose();
    maprenderer.dispose();
  }

  @Override
  public void pause() {}
}
