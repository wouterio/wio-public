package io.wouter.won.gdx.obsolete;

import static io.wouter.won.gdx.anim.WonAgentActorAnimations.getAnimation;
import static java.lang.Float.POSITIVE_INFINITY;
import static java.util.Objects.requireNonNull;

import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Ori;
import io.wouter.lib.grid.Pos;
import io.wouter.lib.nav.AStar;
import io.wouter.lib.nav.Step;
import io.wouter.won.gdx.IsometricStaggeredPositionConversion;


public class WonAgentActor1 extends Actor {

  private static final String Idle = "idle"; //$NON-NLS-1$
  private static final String Walk = "walk"; //$NON-NLS-1$



  private final IsometricStaggeredPositionConversion posconv;
  private final Vector2 worldvec = new Vector2();
  private final Vector2 screenvec = new Vector2();

  private final AStar<WonAgentActor1, Pos, Dis> astar;
  private final Deque<Step<Pos, Dis>> path = new LinkedList<>();

  private Pos agentpos;
  private Ori agentori;
  private Pos taskpos;

  private Step<Pos, Dis> step;
  private Animation<TextureRegion> anim;

  private float steptime = 0f;
  private float animtime = 0f;



  public WonAgentActor1(final IsometricStaggeredPositionConversion posconv, final AStar<WonAgentActor1, Pos, Dis> astar, final Pos pos, final Ori ori) {
    this.posconv = requireNonNull(posconv);
    this.astar = requireNonNull(astar);
    this.worldvec.set(pos.i, pos.j);
    this.agentpos = requireNonNull(pos);
    this.agentori = requireNonNull(ori);
    this.anim = getAnimation(Idle, ori);
    posconv.convertWorldToScreen(worldvec, screenvec);
    setPosition(screenvec.x, screenvec.y);
  }


  @Override
  public void act(final float delta) {
    super.act(delta);

    steptime += delta;
    animtime += delta;
    if (animtime > anim.getAnimationDuration())
      animtime = 0f;

    if (taskpos == null) {
      pickTaskPos();
      nextStep();
    }

    final Pos a = step.from();
    final Pos b = step.to();
    final float u = steptime / step.cost();
    worldvec.x = interpolate(a.i, b.i, u);
    worldvec.y = interpolate(a.j, b.j, u);
    posconv.convertWorldToScreen(worldvec, screenvec);
    setPosition(screenvec.x, screenvec.y);

    if (steptime >= step.cost()) {
      agentpos = step.to();
      if (!path.isEmpty())
        nextStep();
      else
        taskpos = null;
    }
  }



  void pickTaskPos() {
    do
      taskpos = randomPos();
    while (agentpos.equals(taskpos) || !astar.findPath(this, agentpos, taskpos, POSITIVE_INFINITY, path));
  }

  void nextStep() {
    step = path.remove();
    steptime = 0f;
    agentori = step.transition().toOri(agentori);
    this.anim = getAnimation(Walk, agentori);
  }



  @Override
  public void draw(final Batch batch, final float alpha) {
    final TextureRegion frame = anim.getKeyFrame(animtime);
    batch.draw(frame, getX(), getY(), 16, 16);
  }



  private static Pos randomPos() {
    int i;
    int j;
    do {
      i = ThreadLocalRandom.current().nextInt(1, 7);
      j = ThreadLocalRandom.current().nextInt(1, 7);
    } while ((i + j) % 2 == 0);
    return Pos.get(i, j);
  }

  private static float interpolate(final int a, final int b, final float u) {
    return a + (b - a) * u; // (1f - u) * a + u * b;
  }
}
