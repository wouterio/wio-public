package io.wouter.won.gdx;

import static com.badlogic.gdx.math.MathUtils.floor;

public class PickingTest {

  public static void main(final String[] args) {
    final float x = 1.55f;
    final float y = 0.35f;
    printFloat("raw", x, y);

    final int xf = floor(x);
    final int yf = floor(y);
    printInt("flr", xf, yf);

    final float xr = x - xf;
    final float yr = y - yf;
    printFloat("rem", xr, yr);

    switch ((xf + yf) % 2) {

      case 0:
        if (xr < yr)
          printInt("uplf", xf, yf + 1);
        else
          printInt("dnrg", xf + 1, yf);
        break;

      case 1:
        if (xr < 1f - yr)
          printInt("dnlf", xf, yf);
        else
          printInt("uprg", xf + 1, yf + 1);
        break;

      default:
        throw new AssertionError();
    }
  }



  static void uplf_dnrg(final float x, final float y) {
    printFloat("raw", x, y);

    final float xr = x % 1f;
    final float yr = y % 1f;
    printFloat("rem", xr, yr);

    if (xr < yr) {
      final float xf = x - xr;
      final float yc = y + (1f - yr);
      printFloat("uplf", xf, yc);

    } else {
      final float xc = x + (1f - xr);
      final float yf = y - yr;
      printFloat("dnrg", xc, yf);
    }
  }

  static void dnlf_uprg(final float x, final float y) {
    printFloat("raw", x, y);

    final float xr = x % 1f;
    final float yr = y % 1f;
    printFloat("rem", xr, yr);

    if (xr < 1f - yr) {
      final float xf = x - xr;
      final float yf = y - yr;
      printFloat("dnlf", xf, yf);

    } else {
      final float xc = x + (1f - xr);
      final float yc = y + (1f - yr);
      printFloat("uprg", xc, yc);
    }
  }


  static void printInt(final String comment, final int x, final int y) {
    System.out.println(comment + ": (" + x + ',' + y + ')');
  }

  static void printFloat(final String comment, final float x, final float y) {
    System.out.println(comment + ": (" + x + ',' + y + ')');
  }
}
