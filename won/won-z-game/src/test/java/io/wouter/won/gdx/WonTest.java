package io.wouter.won.gdx;

import static io.wouter.won.object.WonObject.CompareIndex;
import static io.wouter.won.util.counter.Counters.newCounter;
import static java.util.Arrays.asList;

import java.util.NoSuchElementException;

import com.badlogic.gdx.Game;

import io.wouter.lib.grid.Pos;
import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.entity.structure.handler.WonProductionHandler;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.object.structure.WonProducerTemplate;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.util.counter.CounterC;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.world.WonEnvironment;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class WonTest extends Game {

  @Override
  public void create() {
    final WonEnvironment env = new WonEnvironment();
    final WonScreen scr = new WonScreen(env);
    setScreen(scr);

    //

    final WonItem ia = new WonItem(0, "A", 1);

    final WonProduction p0a = new WonProduction(0, "_->A", toCounter(), toCounter(ia), null, null, 1);
    final WonProduction pa0 = new WonProduction(1, "A->_", toCounter(ia), toCounter(), null, null, 1);

    final WonProducerTemplate rt = new WonProducerTemplate(0, "R", asList(), 1, 1, 0);
    final WonProducerTemplate wt = new WonProducerTemplate(1, "W", asList(p0a), 1, 1, 4);
    final WonProducerTemplate xt = new WonProducerTemplate(2, "X", asList(pa0), 1, 1, 4);
    final WonTransporterTemplate tt = new WonTransporterTemplate(3, "T", 1, 1, 4);

    final WonConstruction rc = new WonConstruction(0, "Rc", toCounter(), rt, 0);
    final WonConstruction wc = new WonConstruction(1, "Wc", toCounter(), wt, 0);
    final WonConstruction xc = new WonConstruction(2, "Xc", toCounter(), xt, 0);
    final WonConstruction tc = new WonConstruction(3, "Tc", toCounter(), tt, 0);

    // build world

    final WonNodeDelegate refpr1 = new WonNodeDelegate(0, "refpr1", null, null, 0, Pos.get(4, 11));
    final WonNodeDelegate refpw2 = new WonNodeDelegate(0, "refpw2", null, null, 0, Pos.get(5, 6));
    final WonNodeDelegate refpx3 = new WonNodeDelegate(0, "refpx3", null, null, 0, Pos.get(8, 5));
    final WonNodeDelegate reftt4 = new WonNodeDelegate(0, "reftt4", null, env.getBuildModel().getPlayer().getAccount(), 0, Pos.get(8, 11));

    final WonProducer<WonNodeDelegate, WonBufferDelegate> pr1 = buildProducer(env, refpr1, rc);
    final WonProducer<WonNodeDelegate, WonBufferDelegate> pw2 = buildProducer(env, refpw2, wc);
    final WonProducer<WonNodeDelegate, WonBufferDelegate> px3 = buildProducer(env, refpx3, xc);
    final WonTransporter<WonNodeDelegate, WonBufferDelegate> tt4 = buildTransporter(env, reftt4, tc);

    final WonAgent<WonNodeDelegate, WonBufferDelegate> aa5 = buildAgent(env, pr1);
    final WonAgent<WonNodeDelegate, WonBufferDelegate> aa6 = buildAgent(env, pr1);
    final WonAgent<WonNodeDelegate, WonBufferDelegate> aa7 = buildAgent(env, pr1);

    getHandler(pw2, "_->A").setEnabled(true); //$NON-NLS-1$
    getHandler(pw2, "_->A").setActive(true); //$NON-NLS-1$

    getHandler(px3, "A->_").setEnabled(true); //$NON-NLS-1$
    getHandler(px3, "A->_").setActive(true); //$NON-NLS-1$
  }



  protected final static WonAgent<WonNodeDelegate, WonBufferDelegate> buildAgent(final WonEnvironment env, final WonProducer<WonNodeDelegate, WonBufferDelegate> residence) {
    return env.getBuildModel().buildAgent(residence.getStructureDelegate());
  }

  protected final static WonTransporter<WonNodeDelegate, WonBufferDelegate> buildTransporter(final WonEnvironment env, final WonNodeDelegate reference, final WonConstruction c) {
    return env.getBuildModel().buildTransporter(reference, (WonTransporterTemplate) c.getStructureTemplate());
  }

  protected final static WonProducer<WonNodeDelegate, WonBufferDelegate> buildProducer(final WonEnvironment env, final WonNodeDelegate reference, final WonConstruction c) {
    return (WonProducer<WonNodeDelegate, WonBufferDelegate>) env.getBuildModel().buildStructure(reference, c.getStructureTemplate());
  }



  protected static WonProductionHandler getHandler(final WonProducer<?, ?> p, final String type) {
    return p.getHandlers().stream() //
        .filter(h -> h.getProduction().getType().equals(type)) //
        .findAny() //
        .orElseThrow(() -> new NoSuchElementException(type)); //
  }

  protected static CounterC<WonItem> toCounter(final WonItem... items) {
    final CounterM<WonItem> counter = newCounter(CompareIndex);
    for (final WonItem i : items)
      counter.ensure(i).incr();
    return counter.getConst();
  }
}
