package io.wouter.won.gdx.obsolete;

import static io.wouter.won.gdx.anim.WonAgentActorAnimations.getAnimation;
import static java.lang.Float.POSITIVE_INFINITY;
import static java.util.Objects.requireNonNull;

import java.util.Deque;
import java.util.LinkedList;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Ori;
import io.wouter.lib.grid.Pos;
import io.wouter.lib.nav.AStar;
import io.wouter.lib.nav.Step;
import io.wouter.won.entity.WonAgent;
import io.wouter.won.gdx.IsometricStaggeredPositionConversion;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;


public class WonAgentActor2 extends Actor {

  private static final String Idle = "idle"; //$NON-NLS-1$
  private static final String Walk = "walk"; //$NON-NLS-1$



  private final WonAgent<WonNodeDelegate, WonBufferDelegate> agent;
  private final AStar<WonAgentActor2, Pos, Dis> astar;
  private final Deque<Step<Pos, Dis>> path = new LinkedList<>();

  private final IsometricStaggeredPositionConversion posconv;
  private final Vector2 worldvec = new Vector2();
  private final Vector2 screenvec = new Vector2();

  private Pos agentpos;
  private Ori agentori;
  private Pos taskpos;

  private Step<Pos, Dis> step;
  private Animation<TextureRegion> anim;

  private float steptime = 0f;
  private float animtime = 0f;



  public WonAgentActor2(final WonAgent<WonNodeDelegate, WonBufferDelegate> agent, final AStar<WonAgentActor2, Pos, Dis> astar, final IsometricStaggeredPositionConversion posconv) {
    this.agent = requireNonNull(agent);
    this.astar = requireNonNull(astar);
    this.posconv = requireNonNull(posconv);
    this.agentpos = asGridPosition(agent.getAgentDelegate().getPosition());
    this.agentori = Ori.S;
    this.anim = getAnimation(Idle, agentori);
    this.worldvec.set(agentpos.i, agentpos.j);
    posconv.convertWorldToScreen(worldvec, screenvec);
    setPosition(screenvec.x, screenvec.y);
  }



  @Override
  public void act(final float delta) {
    super.act(delta);

    steptime += delta;
    animtime += delta;
    if (animtime > anim.getAnimationDuration())
      animtime = 0f;

    if (!agent.getBehavior().hasTask())
      return;

    if (taskpos == null) {
      taskpos = asGridPosition(agent.getBehavior().getTask().getClient().getPosition());
      if (astar.findPath(this, agentpos, taskpos, POSITIVE_INFINITY, path)) {
        step = path.remove();
        steptime = 0f;
        agentori = step.transition().toOri(agentori);
        this.anim = getAnimation(Walk, agentori);
      } else {
        agent.getBehavior().setTask(null);
        taskpos = null;
        step = null;
        this.anim = getAnimation(Idle, agentori);
        return;
      }
    }

    final Pos a = step.from();
    final Pos b = step.to();
    final float u = steptime / step.cost();
    worldvec.x = interpolate(a.i, b.i, u);
    worldvec.y = interpolate(a.j, b.j, u);
    posconv.convertWorldToScreen(worldvec, screenvec);
    setPosition(screenvec.x, screenvec.y);

    if (steptime >= step.cost()) {
      agentpos = step.to();
      if (!path.isEmpty()) {
        step = path.remove();
        steptime = 0f;
        agentori = step.transition().toOri(agentori);
        this.anim = getAnimation(Walk, agentori);
      }
    }
  }

  @Override
  public void draw(final Batch batch, final float alpha) {
    final TextureRegion frame = anim.getKeyFrame(animtime);
    batch.draw(frame, getX(), getY(), 16, 16);
  }



  private static Pos asGridPosition(final Vector2 pos) {
    return Pos.get((int) pos.x, (int) pos.y);
  }

  private static float interpolate(final int a, final int b, final float u) {
    return a + (b - a) * u; // (1f - u) * a + u * b;
  }
}
