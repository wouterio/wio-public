<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.1.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>libgdx</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantMedium</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Grid</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../assets/sprites/farmer.atlas</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">Idle/farmer-idle0-n/farmer-idle0-n-0.png</key>
            <key type="filename">Idle/farmer-idle0-n/farmer-idle0-n-1.png</key>
            <key type="filename">Idle/farmer-idle0-n/farmer-idle0-n-2.png</key>
            <key type="filename">Idle/farmer-idle0-n/farmer-idle0-n-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,31,49,61</rect>
                <key>scale9Paddings</key>
                <rect>25,31,49,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Idle/farmer-idle1-ne/farmer-idle1-ne-0.png</key>
            <key type="filename">Idle/farmer-idle1-ne/farmer-idle1-ne-1.png</key>
            <key type="filename">Idle/farmer-idle1-ne/farmer-idle1-ne-2.png</key>
            <key type="filename">Idle/farmer-idle1-ne/farmer-idle1-ne-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,31,47,63</rect>
                <key>scale9Paddings</key>
                <rect>24,31,47,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Idle/farmer-idle2-e/farmer-idle2-e-0.png</key>
            <key type="filename">Idle/farmer-idle2-e/farmer-idle2-e-1.png</key>
            <key type="filename">Idle/farmer-idle2-e/farmer-idle2-e-2.png</key>
            <key type="filename">Idle/farmer-idle2-e/farmer-idle2-e-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,32,45,63</rect>
                <key>scale9Paddings</key>
                <rect>23,32,45,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Idle/farmer-idle3-se/farmer-idle3-se-0.png</key>
            <key type="filename">Idle/farmer-idle3-se/farmer-idle3-se-1.png</key>
            <key type="filename">Idle/farmer-idle3-se/farmer-idle3-se-2.png</key>
            <key type="filename">Idle/farmer-idle3-se/farmer-idle3-se-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,32,47,64</rect>
                <key>scale9Paddings</key>
                <rect>24,32,47,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Idle/farmer-idle4-s/farmer-idle4-s-0.png</key>
            <key type="filename">Idle/farmer-idle4-s/farmer-idle4-s-1.png</key>
            <key type="filename">Idle/farmer-idle4-s/farmer-idle4-s-2.png</key>
            <key type="filename">Idle/farmer-idle4-s/farmer-idle4-s-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,32,50,63</rect>
                <key>scale9Paddings</key>
                <rect>25,32,50,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Idle/farmer-idle5-sw/farmer-idle5-sw-0.png</key>
            <key type="filename">Idle/farmer-idle5-sw/farmer-idle5-sw-1.png</key>
            <key type="filename">Idle/farmer-idle5-sw/farmer-idle5-sw-2.png</key>
            <key type="filename">Idle/farmer-idle5-sw/farmer-idle5-sw-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,32,49,64</rect>
                <key>scale9Paddings</key>
                <rect>24,32,49,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Idle/farmer-idle6-w/farmer-idle6-w-0.png</key>
            <key type="filename">Idle/farmer-idle6-w/farmer-idle6-w-1.png</key>
            <key type="filename">Idle/farmer-idle6-w/farmer-idle6-w-2.png</key>
            <key type="filename">Idle/farmer-idle6-w/farmer-idle6-w-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,32,46,63</rect>
                <key>scale9Paddings</key>
                <rect>23,32,46,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Idle/farmer-idle7-nw/farmer-idle7-nw-0.png</key>
            <key type="filename">Idle/farmer-idle7-nw/farmer-idle7-nw-1.png</key>
            <key type="filename">Idle/farmer-idle7-nw/farmer-idle7-nw-2.png</key>
            <key type="filename">Idle/farmer-idle7-nw/farmer-idle7-nw-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,31,49,63</rect>
                <key>scale9Paddings</key>
                <rect>24,31,49,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-0.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-1.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-10.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-11.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-12.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-13.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-14.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-2.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-3.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-4.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-5.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-6.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-7.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-8.png</key>
            <key type="filename">Walk/farmer-walk0-n/farmer-walk0-n-9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,34,49,69</rect>
                <key>scale9Paddings</key>
                <rect>25,34,49,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-0.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-1.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-10.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-11.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-12.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-13.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-14.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-2.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-3.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-4.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-5.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-6.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-7.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-8.png</key>
            <key type="filename">Walk/farmer-walk1-ne/farmer-walk1-ne-9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,34,53,67</rect>
                <key>scale9Paddings</key>
                <rect>26,34,53,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-0.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-1.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-10.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-11.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-12.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-13.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-14.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-2.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-3.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-4.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-5.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-6.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-7.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-8.png</key>
            <key type="filename">Walk/farmer-walk2-e/farmer-walk2-e-9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,31,55,62</rect>
                <key>scale9Paddings</key>
                <rect>27,31,55,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-0.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-1.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-10.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-11.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-12.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-13.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-14.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-2.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-3.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-4.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-5.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-6.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-7.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-8.png</key>
            <key type="filename">Walk/farmer-walk3-sw/farmer-walk3-sw-9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,33,53,67</rect>
                <key>scale9Paddings</key>
                <rect>27,33,53,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-0.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-1.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-10.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-11.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-12.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-13.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-14.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-2.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-3.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-4.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-5.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-6.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-7.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-8.png</key>
            <key type="filename">Walk/farmer-walk4-s/farmer-walk4-s-9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,34,51,68</rect>
                <key>scale9Paddings</key>
                <rect>25,34,51,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-0.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-1.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-10.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-11.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-12.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-13.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-14.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-2.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-3.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-4.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-5.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-6.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-7.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-8.png</key>
            <key type="filename">Walk/farmer-walk5-sw/farmer-walk5-sw-9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,33,54,67</rect>
                <key>scale9Paddings</key>
                <rect>27,33,54,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-0.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-1.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-10.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-11.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-12.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-13.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-14.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-2.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-3.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-4.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-5.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-6.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-7.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-8.png</key>
            <key type="filename">Walk/farmer-walk6-w/farmer-walk6-w-9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,31,57,62</rect>
                <key>scale9Paddings</key>
                <rect>29,31,57,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-0.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-1.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-10.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-11.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-12.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-13.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-14.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-2.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-3.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-4.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-5.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-6.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-7.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-8.png</key>
            <key type="filename">Walk/farmer-walk7-nw/farmer-walk7-nw-9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,34,55,67</rect>
                <key>scale9Paddings</key>
                <rect>27,34,55,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
