package io.wouter.won.world.build;

import static java.util.Objects.requireNonNull;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Function;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.stock.WonItemStock;
import io.wouter.won.entity.structure.WonConverter;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.object.WonObjects;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class WonResidenceManager { // TODO dispose if residence is diposed

  private static final float OneMinute = 60f;

  private static final WonItem Food = WonObjects.getItem("food");
  private static final WonItem Drink = WonObjects.getItem("drink");
  private static final WonItem Clothing = WonObjects.getItem("clothing");

  private static final int LifeBalanceLimit = 1000;
  private static final int LifeBalanceDecrease = 20;
  private static final int LifeBalanceIncreaseFood = 14;
  private static final int LifeBalanceIncreaseDrink = 14;
  private static final int LifeBalanceIncreaseClothing = 4;



  public static boolean isResidence(final WonProducer<?, ?> p) {
    return p.hasOutput(Food) && p.hasOutput(Drink) && p.hasOutput(Clothing);
  }

  public static boolean isResidence(final WonConverter<?, ?> p) {
    return p.hasOutput(Food) && p.hasOutput(Drink) && p.hasOutput(Clothing);
  }



  private final WonProducer<WonNodeDelegate, WonBufferDelegate> residence;
  private final Function<WonProducer<WonNodeDelegate, WonBufferDelegate>, WonAgent<WonNodeDelegate, WonBufferDelegate>> newagent;
  private final Queue<WonAgent<WonNodeDelegate, WonBufferDelegate>> agents = new LinkedList<>();
  private final WonItemStock foodstock;
  private final WonItemStock drinkstock;
  private final WonItemStock clothingstock;



  private float interval = Float.POSITIVE_INFINITY;
  private float timer = 0f;
  private int lifebalance = 0;



  public WonResidenceManager(final WonProducer<WonNodeDelegate, WonBufferDelegate> residence, final Function<WonProducer<WonNodeDelegate, WonBufferDelegate>, WonAgent<WonNodeDelegate, WonBufferDelegate>> newagent) {
    this.residence = checkResidence(residence);
    this.newagent = requireNonNull(newagent);
    this.foodstock = residence.getOutputDelegate(Food).getBufferStock();
    this.drinkstock = residence.getOutputDelegate(Drink).getBufferStock();
    this.clothingstock = residence.getOutputDelegate(Clothing).getBufferStock();
    buildAgent();
    buildAgent();
  }



  public void manage() {
    timer += 1f;
    if (timer < interval) // XXX only works up to 60 per minute
      return;
    timer -= interval;

    lifebalance -= LifeBalanceDecrease;

    if (!foodstock.isEmpty()) {
      lifebalance += LifeBalanceIncreaseFood;
      foodstock.consume(1);
      if (foodstock.hasLoCount())
        residence.outputPurged(Food, true);
    }

    if (!drinkstock.isEmpty()) {
      lifebalance += LifeBalanceIncreaseDrink;
      drinkstock.consume(1);
      if (drinkstock.hasLoCount())
        residence.outputPurged(Drink, true);
    }

    if (!clothingstock.isEmpty()) {
      lifebalance += LifeBalanceIncreaseClothing;
      clothingstock.consume(1);
      if (clothingstock.hasLoCount())
        residence.outputPurged(Clothing, true);
    }

    if (lifebalance > LifeBalanceLimit && buildAgent() || lifebalance < -LifeBalanceLimit && destroyAgent())
      lifebalance = 0;

    System.out.println("life balance:  " + lifebalance);
  }



  private boolean buildAgent() {
    if (agents.size() >= residence.getMaxWorkers())
      return false;
    final WonAgent<WonNodeDelegate, WonBufferDelegate> agent = newagent.apply(residence);
    agents.add(agent);
    updateInterval();
    System.out.println("agent born: " + agents.size());
    return true;
  }

  private boolean destroyAgent() {
    if (agents.isEmpty())
      return false;
    agents.remove().dispose();
    updateInterval();
    System.out.println("agent died: " + agents.size());
    return true;
  }



  private void updateInterval() {
    interval = OneMinute / agents.size();
  }



  private static WonProducer<WonNodeDelegate, WonBufferDelegate> checkResidence(final WonProducer<WonNodeDelegate, WonBufferDelegate> residence) {
    if (!isResidence(residence))
      throw new IllegalArgumentException("Provided residence is not valid"); //$NON-NLS-1$
    return residence;
  }
}
