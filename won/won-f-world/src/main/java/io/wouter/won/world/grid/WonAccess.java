package io.wouter.won.world.grid;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;

public final class WonAccess {

  private final Set<WonTerrain.Type> access;
  private final int maxslope;



  public WonAccess(final Collection<WonTerrain.Type> access, final int maxslope) {
    this.access = EnumSet.copyOf(access);
    this.maxslope = maxslope;
  }



  public boolean canAccess(final WonLandscape.Cell c) {
    return c.isAccessible() && canAccess(c.getTerrain());
  }

  public boolean canAccess(final WonTerrain t) {
    return access.contains(t.getType()) && maxslope >= t.getSlope();
  }
}
