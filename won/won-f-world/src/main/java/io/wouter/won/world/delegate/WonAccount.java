package io.wouter.won.world.delegate;

import static java.lang.Math.round;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.WonObject;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonAccount extends WonEntity { // default serialization form

  private static final long serialVersionUID = -2186884286947598932L;



  public static void transfer(final WonAccount src, final WonAccount tar, final int amount) {
    if (src.equals(tar))
      return;
    final float taxrate = tar.tax.getRate();
    final int taxamount = round(taxrate * amount);
    src.credits -= amount;
    tar.credits += amount - taxamount;
    tar.tax.credits += taxamount; // cumulative cascade taxing
  }



  private final WonAccount tax;



  private int credits = 0;
  private float rate;



  public WonAccount(final int index, final String type, final WonAccount tax, final float rate) {
    super(index, type);
    this.tax = tax;
    this.rate = rate;
  }

  public WonAccount(final WonObject reference, final WonAccount tax, final float rate) {
    super(reference);
    this.tax = tax;
    this.rate = rate;
  }



  public int getCredits() {
    return credits;
  }

  public float getRate() {
    return rate;
  }



  public void setRate(final float rate) {
    this.rate = rate;
  }



  @Override
  public void dispose() {
    super.dispose();
    credits = 0;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("credits", credits) //$NON-NLS-1$
        .append("rate", rate); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("credits").valueInt(this::getCredits) //$NON-NLS-1$
        .field("rate").valueFloat(this::getRate, this::setRate, 0f, 10f, .01f); //$NON-NLS-1$
  }
}
