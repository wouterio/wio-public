package io.wouter.won.world.delegate;

import static java.util.Objects.requireNonNull;

import com.badlogic.gdx.math.Vector2;

import io.wouter.lib.grid.Pos;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.stock.WonItemStock;
import io.wouter.won.object.WonObject;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.stock.AccountStock;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonBufferDelegate extends WonEntity {

  private static final long serialVersionUID = 8616828470085029112L;



  private final WonNodeDelegate node;
  private final WonItemStock stock;
  private final Dispose metadispose;



  public WonBufferDelegate(final int index, final String type, final WonNodeDelegate node, final WonItem item, final AccountStock stock, final Dispose metadispose) {
    super(index, type);
    this.node = requireNonNull(node);
    this.stock = new WonItemStock(item, stock);
    this.metadispose = requireNonNull(metadispose);
  }

  public WonBufferDelegate(final WonObject reference, final WonNodeDelegate node, final WonItem item, final AccountStock stock, final Dispose metadispose) {
    super(reference.getIndex(), reference.getType());
    this.node = requireNonNull(node);
    this.stock = new WonItemStock(item, stock);
    this.metadispose = requireNonNull(metadispose);
  }



  public Long getId() {
    return node.getId();
  }

  public Pos getHome() {
    return node.getHome();
  }

  public Vector2 getPosition() {
    return node.getPosition();
  }

  public WonAccount getAccount() {
    return node.getAccount();
  }

  public WonItemStock getBufferStock() {
    return stock;
  }



  @Override
  public void dispose() {
    super.dispose();
    stock.dispose();
    metadispose.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("pos", getPosition()) //$NON-NLS-1$
        .append("account", getAccount()) //$NON-NLS-1$
        .append("bufferstock", getBufferStock()); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("pos").valueObject(this::getPosition, Vector2::toString) //$NON-NLS-1$
        .separator() //
        .collect(getBufferStock()) //
        .separator() //
        .collect(getAccount()); //
  }
}
