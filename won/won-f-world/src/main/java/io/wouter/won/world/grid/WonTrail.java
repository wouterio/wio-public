package io.wouter.won.world.grid;

import static java.util.Objects.requireNonNull;

import java.util.function.LongSupplier;

import io.wouter.lib.props.Config;

public class WonTrail {

  private static final float DecayFtr = Config.getFloat(WonTrail.class, "DecayFtr"); //$NON-NLS-1$



  private final LongSupplier gettime;



  private float mark = 0f;
  private long lasttime = 0l;



  public WonTrail() {
    this.gettime = null;
  }

  public WonTrail(final LongSupplier gettime) {
    this.gettime = requireNonNull(gettime);
  }



  public float peek() {
    final long time = gettime.getAsLong();
    return peek(time);
  }

  public float peek(final long time) {
    decay(time);
    return mark;
  }



  public float mark(final float newmark) {
    final long time = gettime.getAsLong();
    return mark(time, newmark);
  }

  public float mark(final long time, final float newmark) {
    decay(time);
    return mark += newmark;
  }



  private void decay(final long time) {
    if (time <= lasttime)
      return;
    mark -= DecayFtr * (time - lasttime);
    if (mark < 0f)
      mark = 0f;
    lasttime = time;
  }
}
