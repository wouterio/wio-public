package io.wouter.won.world.util;

public final class Griterator {

  public static Griterator grit(final int i, final int j, final int range, final boolean incl) {
    return new Griterator(i, j, range, incl);
  }

  //

  private final int begi, begj, endi, endj;

  //

  private Griterator(final int i, final int j, final int range, final boolean incl) {
    begi = i - range;
    endi = i + range + (incl ? 1 : 0);
    begj = j - range;
    endj = j + range + (incl ? 1 : 0);
  }

  //

  public void forEach(final GridConsumer c) {
    for (int i = begi; i < endi; ++i)
      for (int j = begj; j < endj; ++j)
        c.accept(i, j);
  }

  public boolean anyMatch(final GridPredicate p) {
    for (int i = begi; i < endi; ++i)
      for (int j = begj; j < endj; ++j)
        if (p.test(i, j))
          return true;
    return false;
  }

  public boolean allMatch(final GridPredicate p) {
    for (int i = begi; i < endi; ++i)
      for (int j = begj; j < endj; ++j)
        if (!p.test(i, j))
          return false;
    return true;
  }

  /*
   *
   */

  public static interface GridConsumer {

    void accept(int i, int j);
  }

  /*
   *
   */

  public static interface GridPredicate {

    boolean test(int i, int j);
  }
}
