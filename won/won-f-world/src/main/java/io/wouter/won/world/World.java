// package io.wouter.won.world;
//
// import java.util.Set;
//
// import io.wouter.lib.grid.Pos;
// import io.wouter.won.entity.WonStructure;
// import io.wouter.won.object.product.WonResource;
// import io.wouter.won.util.func.Perform;
// import io.wouter.won.world.delegate.WonBufferDelegate;
// import io.wouter.won.world.delegate.WonNodeDelegate;
// import io.wouter.won.world.enums.Terrain;
// import io.wouter.won.world.enums.Unit;
// import io.wouter.won.world.grid.Landscape;
//
// public interface World extends Perform {
//
// static World newInstance(final Landscape landscape) {
// return new WorldI(landscape);
// }
//
//
//
// int getWidth();
//
// int getHeight();
//
// int getAlt(Pos p);
//
// int getSlope(Pos p);
//
// int getFlow(Pos p);
//
// int getMaxAlt();
//
// int getMaxSlope();
//
// int getMaxFlow();
//
// Terrain getTerrain(Pos p);
//
//
//
// boolean isWithin(Pos p);
//
// boolean isAccessible(Pos p, Unit u);
//
// boolean isAccessible(Pos p, Unit u, int range);
//
//
//
// void markTrail(Pos p);
//
// float peekTrail(Pos p);
//
// Set<Pos> getTrailMarks();
//
//
//
// WonResource getResource(Pos p);
//
// void setResource(Pos p, WonResource r);
//
// float peekResource(Pos p, WonResource r);
//
// float extractResource(Pos p, WonResource r, int amount);
//
// Set<Pos> getResources();
//
//
//
// WonStructure<WonNodeDelegate, WonBufferDelegate> getStructure(Pos p);
//
// void buildStructure(Pos p, int range, WonStructure<WonNodeDelegate, WonBufferDelegate> s);
//
// WonStructure<WonNodeDelegate, WonBufferDelegate> removeStructure(Pos p, int range);
// }
