package io.wouter.won.world.task;

import static io.wouter.lib.task.TaskStatus.Completed;
import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Suspended;
import static io.wouter.lib.task.TaskStatus.Working;
import static java.lang.Float.NaN;
import static java.lang.Float.isNaN;
import static java.util.Objects.requireNonNull;

import com.aoi.won.core.Node;
import com.aoi.won.core.node.Buffer;

import io.wouter.lib.task.TaskStatus;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.model.WonBuilder;
import io.wouter.won.entity.stock.WonItemStock;
import io.wouter.won.entity.task.WonTask;
import io.wouter.won.entity.task.construction.WonConstructionTask;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.counter.Counters;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public final class ConstructionTaskI extends WonTask<WonNodeDelegate> implements WonConstructionTask<WonNodeDelegate> {

  private static final long serialVersionUID = -6027664053625119532L;



  private static final CounterM<WonItem> EmptyCounter = Counters.newCounter();



  private final Node<WonItem, WonNodeDelegate, WonBufferDelegate> node;
  private final WonConstruction construction;
  private final WonBuilder<WonNodeDelegate, WonBufferDelegate> builder;

  private CounterM<WonItem> inputleft = EmptyCounter;
  private float time = NaN;



  public ConstructionTaskI(final Node<WonItem, WonNodeDelegate, WonBufferDelegate> node, final WonConstruction construction, final WonBuilder<WonNodeDelegate, WonBufferDelegate> builder) {
    super(construction, node.getNodeDelegate(), Pending);
    this.node = requireNonNull(node);
    this.construction = requireNonNull(construction);
    this.builder = requireNonNull(builder);
  }



  @Override
  public WonConstruction getConstruction() {
    return construction;
  }



  @Override
  public float getTime() {
    return time;
  }

  @Override
  public float getTimeLeft() {
    return construction.getDuration() - time;
  }



  @Override
  public WonConstructionTask<WonNodeDelegate> perform(final WonNodeDelegate agent, final float delta) {
    if (isNaN(time)) {
      inputleft = construction.getInput().getMod();
      time = 0f;
    }

    if (!processInput()) {
      setStatus(Suspended);
      return this;
    }

    if ((time += delta) < construction.getDuration()) {
      setStatus(Working);
      return this;
    }

    if (!processStructureTemplate()) {
      setStatus(Working); // TODO Working or Suspended?
      return this;
    }

    inputleft = EmptyCounter;
    time = NaN;

    setStatus(Completed);
    return this;
  }



  @Override
  public void dispose() {
    super.dispose();
    inputleft = EmptyCounter;
    time = NaN;
  }



  @Override
  public WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("construction", construction.getType()) //$NON-NLS-1$
        .append("time", getTime()) //$NON-NLS-1$
        .append("timeleft", getTimeLeft()); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("construction").link(construction, WonConstruction::getType) //$NON-NLS-1$
        .field("client").valueObject(this::getClient, WonEntity::getType) //$NON-NLS-1$
        .field("status").valueObject(this::getStatus, TaskStatus::toString) //$NON-NLS-1$
        .field("time").valueFloat(this::getTime) //$NON-NLS-1$
        .field("timeleft").valueFloat(this::getTimeLeft); //$NON-NLS-1$
  }



  private boolean processInput() {
    boolean processed = true;

    for (final WonItem i : inputleft.cleanup().getKeys()) {
      final Buffer<?, WonBufferDelegate> ibuffer = node.getInput().getBuffer(i);
      final WonItemStock istock = ibuffer.getBufferDelegate().getBufferStock();

      if (istock.isEmpty())
        processed = false;
      else {
        istock.consume(1); // consume one item use
        inputleft.get(i).decr(1);
      }
      if (istock.hasLoCount())
        ibuffer.purged(true);
    }
    return processed;
  }

  private boolean processStructureTemplate() {
    return builder.apply(getClient(), construction.getStructureTemplate()) != null;
  }
}
