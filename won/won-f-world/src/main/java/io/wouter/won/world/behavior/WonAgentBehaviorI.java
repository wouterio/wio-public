package io.wouter.won.world.behavior;

import static io.wouter.lib.grid.Ori.S;
import static io.wouter.won.world.delegate.WonAccount.transfer;
import static java.lang.Float.POSITIVE_INFINITY;
import static java.util.Objects.requireNonNull;

import java.util.Deque;
import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Ori;
import io.wouter.lib.grid.Pos;
import io.wouter.lib.nav.AStar;
import io.wouter.lib.nav.Step;
import io.wouter.lib.task.CTask;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.behavior.WonAgentBehavior;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;
import io.wouter.won.world.delegate.WonAccount;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;
import io.wouter.won.world.grid.WonAccess;

public class WonAgentBehaviorI extends WonEntity implements WonAgentBehavior<WonNodeDelegate, WonBufferDelegate> {

  private static final long serialVersionUID = 3788642893404766271L;



  private static final float WagePaymentInterval = 10f;



  private final WonNodeDelegate delegate;
  private final Motion motion;



  private CTask<WonNodeDelegate, WonNodeDelegate> task = null;
  private float wage = 0;



  public WonAgentBehaviorI(final WonNodeDelegate delegate, final AStar<WonAccess, Pos, Dis> astar) {
    super(delegate);
    this.delegate = requireNonNull(delegate);
    this.motion = new Motion(delegate, astar);
  }



  @Override
  public boolean hasTask() {
    return task != null;
  }

  @Override
  public CTask<WonNodeDelegate, WonNodeDelegate> getTask() {
    return task;
  }

  @Override
  public void setTask(final CTask<WonNodeDelegate, WonNodeDelegate> newtask) {
    if (newtask != null) {
      if (hasTask())
        throw new IllegalStateException("Agent already has a task."); //$NON-NLS-1$
      task = newtask;
      task.register(delegate);

    } else if (hasTask()) {
      task.deregister(delegate);
      task = null;
    }
  }



  @Override
  public void perform(final float delta) {
    if (hasTask()) {

      // move to client

      if (!motion.findPath(task.getClient().getHome())) {
        setTask(null);
        return;
      }

      if (!motion.move(delta))
        return;

      // get paid every 10 seconds

      wage += delta;
      if (wage > WagePaymentInterval) {
        final WonAccount agentaccount = delegate.getAccount();
        final WonAccount clientaccount = task.getClient().getAccount();
        final int payment = (int) wage;
        transfer(clientaccount, agentaccount, payment);
        wage -= payment;
      }

      // perform task

      if (!task.perform(delegate, delta).isActive())
        setTask(null); // dismiss task when finished

    } else if (motion.findPath(delegate.getHome())) // move home
      motion.move(delta);
  }



  @Override
  public void dispose() {
    super.dispose();
    motion.dispose();
    setTask(null);
    wage = 0f;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder();
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info;
  }



  //



  private static class Motion implements Dispose {

    private final WonNodeDelegate agent;
    private final AStar<WonAccess, Pos, Dis> astar;
    private final Deque<Step<Pos, Dis>> path = new LinkedList<>();



    private Pos targetpos = null;
    private float cumdelta = 0f;



    Motion(final WonNodeDelegate delegate, final AStar<WonAccess, Pos, Dis> astar) {
      this.agent = requireNonNull(delegate);
      this.astar = requireNonNull(astar);
    }



    boolean findPath(final Pos newtargetpos) {
      if (newtargetpos.equals(targetpos))
        return true;

      final Pos agentpos = asGridPosition(agent.getPosition());

      if (!astar.findPath(agent.getAccess(), agentpos, newtargetpos, POSITIVE_INFINITY, path))
        return false;

      targetpos = newtargetpos;
      cumdelta = 0f;
      return true;
    }



    boolean move(final float delta) {
      if (path.isEmpty())
        return true;

      cumdelta += delta;
      float movecost = 0f;

      while (true) {
        final Step<Pos, Dis> step = path.element();
        final float stepcost = step.cost();
        movecost += stepcost;

        if (cumdelta < movecost) {
          final Pos a = step.from();
          final Pos b = step.to();
          final Dis d = step.transition();
          final float u = cumdelta / stepcost;
          move(agent, a, b, d, u);
          return false;
        }

        path.remove();
        cumdelta -= stepcost;

        if (path.isEmpty()) {
          final Pos b = step.to();
          final Dis d = step.transition();
          move(agent, b, b, d, 1f); // XXX b twice
          return true;
        }
      }
    }



    @Override
    public void dispose() {
      path.clear();
      targetpos = null;
      cumdelta = 0f;
    }



    private static Pos asGridPosition(final Vector2 pos) {
      return Pos.get((int) pos.x, (int) pos.y);
    }

    private static void move(final WonNodeDelegate agent, final Pos a, final Pos b, final Dis d, final float u) {
      final float x = interpolate(a.i, b.i, u);
      final float y = interpolate(a.j, b.j, u);
      final Ori o = d.toOri(S);
      agent.getPosition().x = x;
      agent.getPosition().y = y;
      agent.setOrientation(o);
    }

    private static float interpolate(final int a, final int b, final float u) {
      return a + (b - a) * u; // (1f - u) * a + u * b;
    }
  }
}
