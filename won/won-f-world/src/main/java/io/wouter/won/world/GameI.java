// package io.wouter.won.world;
//
// import java.util.Collection;
// import java.util.List;
// import java.util.concurrent.ThreadLocalRandom;
//
// import io.wouter.lib.grid.Pos;
// import io.wouter.won.entity.WonAgent;
// import io.wouter.won.entity.WonStructure;
// import io.wouter.won.entity.model.WonBuildModel;
// import io.wouter.won.entity.structure.WonConstructor;
// import io.wouter.won.object.WonObjects;
// import io.wouter.won.object.product.WonResource;
// import io.wouter.won.object.structure.WonConstructorTemplate;
// import io.wouter.won.object.structure.WonProducerTemplate;
// import io.wouter.won.object.structure.WonStructureTemplate;
// import io.wouter.won.util.func.PerformRound;
// import io.wouter.won.world.build.WonInfo;
// import io.wouter.won.world.delegate.WonBufferDelegate;
// import io.wouter.won.world.delegate.WonNodeDelegate;
// import io.wouter.won.world.grid.WonUnit;
//
// final class GameI implements Game {
//
// private final WonBuildModel<WonNodeDelegate, WonBufferDelegate> model;
// private final WonObjects factory;
// private final World world;
// private final WonNodeDelegate player;
//
// private final int round = 0;
//
// //
//
// GameI(final WonBuildModel<WonNodeDelegate, WonBufferDelegate> model, final WonObjects factory) {
// this.model = model;
// this.factory = factory;
// this.world = model.getWorld();
//
// // test init:
//
// Pos initpos;
// do
// initpos = Pos.get(ThreadLocalRandom.current().nextInt(world.getWidth()), ThreadLocalRandom.current().nextInt(world.getHeight()));
// while (!world.isAccessible(initpos, WonUnit.Structure, 2));
// player.setHome(initpos);
//
// final WonStructureTemplate st = WonObjects.getConstructor("build residence").getConstruction().getStructureTemplate();
// final WonStructure<WonNodeDelegate, WonBufferDelegate> s = model.newStructure(null, st);
//
// for (int i = 1; i <= 16; ++i)
// model.newAgent(s.getDelegate());
// }
//
// //
//
// @Override
// public Collection<WonAgent<WonNodeDelegate, WonBufferDelegate>> getAgents() {
// return model.getAgents();
// }
//
// @Override
// public Collection<WonStructure<WonNodeDelegate, WonBufferDelegate>> getStructures() {
// return model.getStructures();
// }
//
// //
//
// @Override
// public Info<WonNodeDelegate, WonBufferDelegate> select(final Pos p, final WonInfo info) {
// if (!world.isWithin(p))
// return info;
//
// final WonStructure<WonNodeDelegate, WonBufferDelegate> structure = world.getStructure(p);
// if (structure != null)
// structure.getInfo(info);
// else {
// player.setHome(p); // TODO not a very safe solution
// info.addNodeDelegate(player);
// info.addAlt(world.getAlt(p));
// info.addTerrain(world.getTerrain(p));
// info.addFactory(factory);
// }
// return info;
// }
//
// //
//
// @Override
// public WonConstructor<WonNodeDelegate, WonBufferDelegate> build(final WonConstructorTemplate ct) {
// final WonStructureTemplate st = ct.getConstruction().getStructureTemplate();
// if (!(st instanceof WonProducerTemplate))
// return buildNormal(ct);
//
// final WonProducerTemplate pt = (WonProducerTemplate) st;
// if (pt.getResources().isEmpty())
// return buildNormal(ct);
//
// return buildResource(ct, pt.getResources());
// }
//
// private WonConstructor<WonNodeDelegate, WonBufferDelegate> buildNormal(final WonConstructorTemplate n) {
// final int size = 3;
// final Pos p = player.getHome();
// return world.isAccessible(p, WonUnit.Structure, size + 2) ?
// build(n, size) :
// null;
// }
//
// private WonConstructor<WonNodeDelegate, WonBufferDelegate> buildResource(final WonConstructorTemplate n, final List<WonResource>
// resourcelist) {
// final int size = 1;
// final Pos p = player.getHome();
// return world.getStructure(p) == null && world.getResource(p) != null && resourcelist.contains(world.getResource(p)) ?
// build(n, size) :
// null;
// }
//
// private WonConstructor<WonNodeDelegate, WonBufferDelegate> build(final WonConstructorTemplate ct, final int size) {
// final WonStructure<WonNodeDelegate, WonBufferDelegate> s = resources.newStructure(player, ct);
// }
//
// //
//
// @Override
// public void perform() {
// getAgents().forEach(PerformRound::perform);
// }
//
// //
//
// @Override
// public void dispose() {}
// }
