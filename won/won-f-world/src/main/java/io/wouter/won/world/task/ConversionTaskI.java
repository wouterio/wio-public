package io.wouter.won.world.task;

import static io.wouter.lib.task.TaskStatus.Suspended;
import static io.wouter.lib.task.TaskStatus.Working;
import static java.lang.Float.NaN;
import static java.lang.Float.isNaN;
import static java.util.Objects.requireNonNull;

import com.aoi.won.core.Node;
import com.aoi.won.core.node.Buffer;

import io.wouter.lib.task.TaskStatus;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.model.WonBuilder;
import io.wouter.won.entity.stock.WonItemStock;
import io.wouter.won.entity.task.WonTask;
import io.wouter.won.entity.task.conversion.WonConversionTask;
import io.wouter.won.object.product.WonConversion;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.counter.Counters;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public final class ConversionTaskI extends WonTask<WonNodeDelegate> implements WonConversionTask<WonNodeDelegate> {

  private static final long serialVersionUID = -4014504080513972201L;



  private static final CounterM<WonItem> EmptyCounter = Counters.newCounter();



  private final Node<WonItem, WonNodeDelegate, WonBufferDelegate> node;
  private final WonConversion conversion;
  private final WonBuilder<WonNodeDelegate, WonBufferDelegate> builder;

  private CounterM<WonItem> inputleft = EmptyCounter;
  private CounterM<WonItem> outputleft = EmptyCounter;
  private float cost = NaN;
  private float price = NaN;
  private float time = NaN;



  public ConversionTaskI(final Node<WonItem, WonNodeDelegate, WonBufferDelegate> node, final WonConversion conversion, final WonBuilder<WonNodeDelegate, WonBufferDelegate> builder) {
    super(conversion, node.getNodeDelegate(), Suspended);
    this.node = requireNonNull(node);
    this.conversion = requireNonNull(conversion);
    this.builder = requireNonNull(builder);
  }



  @Override
  public WonConversion getConversion() {
    return conversion;
  }



  @Override
  public float getTime() {
    return time;
  }

  @Override
  public float getTimeLeft() {
    return conversion.getDuration() - time;
  }



  @Override
  public WonConversionTask<WonNodeDelegate> perform(final WonNodeDelegate agent, final float delta) {
    if (isNaN(time)) {
      inputleft = conversion.getInput().getMod();
      outputleft = conversion.getOutput().getMod();
      cost = conversion.getDuration();
      price = NaN;
      time = 0f;
    }

    if (!processInput()) {
      setStatus(Suspended);
      return this;
    }

    if ((time += delta) < conversion.getDuration()) {
      setStatus(Working);
      return this;
    }

    if (isNaN(price))
      price = cost * node.getNodeDelegate().getAccount().getRate() / conversion.getOutput().total();

    if (!processOutput()) {
      setStatus(Suspended);
      return this;
    }
    if (!outputleft.cleanup().isEmpty()) {
      setStatus(Working);
      return this;
    }

    if (!processStructureTemplate()) {
      setStatus(Working);
      return this;
    }

    inputleft = EmptyCounter;
    outputleft = EmptyCounter;
    cost = NaN;
    price = NaN;
    time = NaN;

    setStatus(Working); // perpetual task
    return this;
  }



  @Override
  public void dispose() {
    super.dispose();
    inputleft = EmptyCounter;
    outputleft = EmptyCounter;
    cost = NaN;
    price = NaN;
    time = NaN;
  }



  @Override
  public WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("conversion", conversion) //$NON-NLS-1$
        .append("time", getTime()) //$NON-NLS-1$
        .append("timeleft", getTimeLeft()); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("conversion").link(conversion, WonConversion::getType) //$NON-NLS-1$
        .field("client").valueObject(this::getClient, WonEntity::getType) //$NON-NLS-1$
        .field("status").valueObject(this::getStatus, TaskStatus::toString) //$NON-NLS-1$
        .field("time").valueFloat(this::getTime) //$NON-NLS-1$
        .field("timeleft").valueFloat(this::getTimeLeft); //$NON-NLS-1$
  }



  private boolean processInput() {
    boolean alldone = true;

    for (final WonItem i : inputleft.cleanup().getKeys()) {
      final Buffer<?, WonBufferDelegate> ibuffer = node.getInput().getBuffer(i);
      final WonItemStock istock = ibuffer.getBufferDelegate().getBufferStock();

      if (istock.isEmpty())
        alldone = false;
      else {
        istock.consume(1); // consume one item use
        inputleft.get(i).decr(1);
        cost += istock.getCost();
      }
      if (istock.hasLoCount())
        ibuffer.purged(true);
    }
    return alldone;
  }

  private boolean processOutput() {
    boolean alldone = true;

    for (final WonItem o : outputleft.cleanup().getKeys()) {
      final Buffer<?, WonBufferDelegate> obuffer = node.getOutput().getBuffer(o);
      final WonItemStock ostock = obuffer.getBufferDelegate().getBufferStock();

      if (ostock.isFull())
        alldone = false;
      else {
        ostock.produce(1, price); // produce one item
        outputleft.get(o).decr(1);
      }
      if (ostock.hasLoSpace())
        obuffer.filled(true);
    }
    return alldone;
  }

  private boolean processStructureTemplate() {
    return !conversion.getStructureTemplate().isPresent() || builder.apply(getClient(), conversion.getStructureTemplate().get()) != null;
  }
}
