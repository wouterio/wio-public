package io.wouter.won.world.build;

import static com.aoi.won.core.Link.newInterLink;
import static com.aoi.won.core.Link.newLinkMultiplexer;
import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Suspended;
import static java.util.Objects.requireNonNull;

import java.util.function.Function;

import com.aoi.won.core.Link;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonAgents;
import io.wouter.won.entity.WonConverters;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.entity.WonTransporters;
import io.wouter.won.entity.model.WonGroup;
import io.wouter.won.entity.stock.WonItemStock;
import io.wouter.won.entity.structure.WonConverter;
import io.wouter.won.object.WonObjects;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.world.delegate.WonAccount;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class WonGroupI implements WonGroup<WonNodeDelegate, WonBufferDelegate> {

  private static final float OneMinute = 60f;

  private static final WonItem Food = WonObjects.getItem("food");
  private static final WonItem Drink = WonObjects.getItem("drink");
  private static final WonItem Clothing = WonObjects.getItem("clothing");

  private static final int LifeBalanceLimit = 1000;
  private static final int LifeBalanceDecrease = 20;
  private static final int LifeBalanceIncreaseFood = 14;
  private static final int LifeBalanceIncreaseDrink = 14;
  private static final int LifeBalanceIncreaseClothing = 4;



  private final WonAgents<WonNodeDelegate, WonBufferDelegate> agents = new WonAgents<>();
  private final WonTransporters<WonNodeDelegate, WonBufferDelegate> transporters = new WonTransporters<>();
  private final WonConverters<WonNodeDelegate, WonBufferDelegate> converters = new WonConverters<>();
  private final WonAccount account;
  private final Link<WonItem, WonBufferDelegate> link;

  private final WonConverter<WonNodeDelegate, WonBufferDelegate> residence;
  private final Function<WonConverter<WonNodeDelegate, WonBufferDelegate>, WonAgent<WonNodeDelegate, WonBufferDelegate>> newagent;
  private final WonItemStock foodstock;
  private final WonItemStock drinkstock;
  private final WonItemStock clothingstock;



  private float interval = Float.POSITIVE_INFINITY;
  private float timer = 0f;
  private int lifebalance = 0;



  public WonGroupI(final WonAccount account, final Link<WonItem, WonBufferDelegate> outerlink, final WonConverter<WonNodeDelegate, WonBufferDelegate> residence, final Function<WonConverter<WonNodeDelegate, WonBufferDelegate>, WonAgent<WonNodeDelegate, WonBufferDelegate>> newagent) {
    this.account = requireNonNull(account);
    this.link = newLinkMultiplexer(newInterLink(t -> transporters.offer(t)), outerlink);

    this.residence = checkResidence(residence);
    this.newagent = requireNonNull(newagent);
    this.foodstock = residence.getOutputDelegate(Food).getBufferStock();
    this.drinkstock = residence.getOutputDelegate(Drink).getBufferStock();
    this.clothingstock = residence.getOutputDelegate(Clothing).getBufferStock();

    addAgent();
    addAgent();
    addConverter(residence);
  }



  @Override
  public boolean addAgent(final WonAgent<WonNodeDelegate, WonBufferDelegate> agent) {
    if (!agents.add(agent))
      return false;
    updateInterval();
    agent.getAgentDelegate().setAccount(account);
    return true;
  }

  @Override
  public boolean removeAgent(final WonAgent<WonNodeDelegate, WonBufferDelegate> agent) {
    if (!agents.remove(agent))
      return false;
    updateInterval();
    agent.getBehavior().setTask(null); // stop current task
    agent.getAgentDelegate().setAccount(null);
    return true;
  }



  @Override
  public boolean addConverter(final WonConverter<WonNodeDelegate, WonBufferDelegate> converter) {
    if (!converters.add(checkNoResidence(converter)))
      return false;
    converter.setLink(link);
    converter.getStructureDelegate().setAccount(account);
    converter.getTask().setStatus(Pending);
    agents.offer(converter.getTask());
    return true;
  }

  @Override
  public boolean removeConverter(final WonConverter<WonNodeDelegate, WonBufferDelegate> converter) {
    if (!converters.remove(converter))
      return false;
    agents.remove(converter.getTask());
    converter.getTask().setStatus(Suspended);
    converter.setLink(null);
    converter.getStructureDelegate().setAccount(null);
    return true;
  }



  @Override
  public boolean addTransporter(final WonTransporter<WonNodeDelegate, WonBufferDelegate> transporter) {
    if (!transporters.add(transporter))
      return false;
    transporter.getDelegate().setAccount(account);
    agents.offer(transporter.getTask());
    return true;
  }

  @Override
  public boolean removeTransporter(final WonTransporter<WonNodeDelegate, WonBufferDelegate> transporter) {
    if (!transporters.remove(transporter))
      return false;
    agents.remove(transporter.getTask());
    transporter.getBehavior().setTransfer(null); // stop current transfer
    transporter.getDelegate().setAccount(null);
    return true;
  }



  @Override
  public void perform(final long unused) {
    agents.distribute();
    transporters.distribute();
    timer += 1f;
    while (timer >= interval) {
      timer -= interval;
      manage();
    }
  }



  public void manage() {
    lifebalance -= LifeBalanceDecrease;

    if (!foodstock.isEmpty()) {
      lifebalance += LifeBalanceIncreaseFood;
      foodstock.consume(1);
      if (foodstock.hasLoCount())
        residence.outputPurged(Food, true);
    }

    if (!drinkstock.isEmpty()) {
      lifebalance += LifeBalanceIncreaseDrink;
      drinkstock.consume(1);
      if (drinkstock.hasLoCount())
        residence.outputPurged(Drink, true);
    }

    if (!clothingstock.isEmpty()) {
      lifebalance += LifeBalanceIncreaseClothing;
      clothingstock.consume(1);
      if (clothingstock.hasLoCount())
        residence.outputPurged(Clothing, true);
    }

    if (lifebalance > LifeBalanceLimit && addAgent() || lifebalance < -LifeBalanceLimit && removeAgent())
      lifebalance = 0;

    System.out.println("life balance:  " + lifebalance);
  }



  private boolean addAgent() {
    if (agents.getSize() >= residence.getMaxWorkers())
      return false;
    addAgent(newagent.apply(residence));
    System.out.println("agent born: " + agents.getSize());
    return true;
  }

  private boolean removeAgent() {
    if (agents.isEmpty())
      return false;
    agents.remove().dispose();
    System.out.println("agent died: " + agents.getSize());
    return true;
  }



  private void updateInterval() {
    interval = OneMinute / agents.getSize();
  }



  public static boolean isResidence(final WonConverter<?, ?> c) {
    return c.hasOutput(Food) && c.hasOutput(Drink) && c.hasOutput(Clothing);
  }



  private static WonConverter<WonNodeDelegate, WonBufferDelegate> checkResidence(final WonConverter<WonNodeDelegate, WonBufferDelegate> c) {
    if (!isResidence(c))
      throw new IllegalArgumentException("Provided converter is not a valid residence"); //$NON-NLS-1$
    return c;
  }

  private static WonConverter<WonNodeDelegate, WonBufferDelegate> checkNoResidence(final WonConverter<WonNodeDelegate, WonBufferDelegate> c) {
    if (isResidence(c))
      throw new IllegalArgumentException("Group may contain only one residence"); //$NON-NLS-1$
    return c;
  }
}
