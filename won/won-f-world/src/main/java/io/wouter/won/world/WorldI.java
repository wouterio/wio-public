// package io.wouter.won.world;
//
// import static io.wouter.won.world.util.Griterator.grit;
// import static java.util.Collections.unmodifiableSet;
//
// import java.time.Clock;
// import java.util.Collections;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.Map;
// import java.util.Set;
//
// import io.wouter.lib.grid.Pos;
// import io.wouter.won.entity.WonEntity;
// import io.wouter.won.entity.WonStructure;
// import io.wouter.won.object.product.WonResource;
// import io.wouter.won.world.delegate.WonBufferDelegate;
// import io.wouter.won.world.delegate.WonNodeDelegate;
// import io.wouter.won.world.enums.Terrain;
// import io.wouter.won.world.enums.Unit;
// import io.wouter.won.world.grid.Landscape;
//
// final class WorldI extends WonEntity implements World {
//
// private static final float TrailMark = getParamF("TrailMark"), EvapRate = getParamF("EvapRate"); //$NON-NLS-1$ //$NON-NLS-2$
//
//
//
// private final Clock clock = new Clock();
//
// private final Landscape landscape;
// private final PathGrid paths;
// private final ResourceMapI resources;
// private final StructureMapI structures;
//
//
//
// private final Set<Pos> marks = new HashSet<>();
// private final Set<Pos> umarks = unmodifiableSet(marks);
//
//
//
// WorldI(final Landscape landscape) {
// super(0, "world"); //$NON-NLS-1$
//
// this.landscape = landscape;
// this.paths = PathGrid.newEvapGrid(getWidth(), getHeight(), clock);
// this.paths.setEvaporationRate(EvapRate);
// this.resources = new ResourceMapI(clock);
// this.structures = new StructureMapI(paths);
// }
//
//
//
// @Override
// public int getWidth() {
// return landscape.getWidth();
// }
//
// @Override
// public int getHeight() {
// return landscape.getHeight();
// }
//
// @Override
// public int getAlt(final Pos p) {
// return landscape.getAlt(p.i, p.j);
// }
//
// @Override
// public int getSlope(final Pos p) {
// return landscape.getSlope(p.i, p.j);
// }
//
// @Override
// public int getFlow(final Pos p) {
// return landscape.getFlow(p.i, p.j);
// }
//
// @Override
// public int getMaxAlt() {
// return landscape.getMaxAlt();
// }
//
// @Override
// public int getMaxSlope() {
// return landscape.getMaxSlope();
// }
//
// @Override
// public int getMaxFlow() {
// return landscape.getMaxFlow();
// }
//
// @Override
// public Terrain getTerrain(final Pos p) {
// return landscape.getTerrain(p.i, p.j);
// }
//
//
//
// @Override
// public boolean isWithin(final Pos p) {
// return landscape.isWithin(p);
// }
//
// @Override
// public boolean isAccessible(final Pos p, final Unit u) {
// return isAccessible(p.i, p.j, u);
// }
//
// @Override
// public boolean isAccessible(final Pos p, final Unit u, final int size) {
// return grit(p.i, p.j, size >> 1, true).allMatch((i, j) -> isAccessible(i, j, u));
// }
//
// private boolean isAccessible(final int i, final int j, final Unit u) {
// return paths.isAccessible(i, j) && u.hasAccess(landscape.getTerrain(i, j));
// }
//
//
//
// @Override
// public void markTrail(final Pos p) {
// paths.mark(p.i, p.j, TrailMark);
// marks.add(p);
// }
//
// @Override
// public float peekTrail(final Pos p) {
// return paths.peek(p.i, p.j);
// }
//
// @Override
// public Set<Pos> getTrailMarks() {
// return umarks;
// }
//
//
//
// @Override
// public WonResource getResource(final Pos p) {
// return resources.getResource(p);
// }
//
// @Override
// public void setResource(final Pos p, final WonResource r) {
// resources.setResource(p, r);
// }
//
// @Override
// public float peekResource(final Pos p, final WonResource r) {
// return resources.peek(p, r);
// }
//
// @Override
// public float extractResource(final Pos p, final WonResource r, final int amount) {
// return resources.extract(p, r, amount);
// }
//
// @Override
// public Set<Pos> getResources() {
// return resources.getResources();
// }
//
//
//
// @Override
// public WonStructure<WonNodeDelegate, WonBufferDelegate> getStructure(final Pos p) {
// return structures.getStructure(p);
// }
//
// @Override
// public void buildStructure(final Pos p, final int size, final WonStructure<Delegate> s) {
// structures.setStructure(p, size >> 1, s);
// }
//
// @Override
// public WonStructure<WonNodeDelegate, WonBufferDelegate> removeStructure(final Pos p, final int size) {
// return structures.removeStructure(p, size >> 1);
// }
//
//
//
// @Override
// public void perform() {
// clock.increment(); // paths.update();
// }
//
// /*
// *
// */
//
// private static class ResourceMapI {
//
// private final Map<Pos, ResourceTile> resources = new HashMap<>();
// private final Clock clock;
//
//
//
// ResourceMapI(final Clock clock) {
// this.clock = clock;
// }
//
//
//
// WonResource getResource(final Pos p) {
// return resources.containsKey(p) ?
// resources.get(p).getResource() :
// null;
// }
//
// void setResource(final Pos p, final WonResource r) {
// resources.put(p, new ResourceTile(clock, r));
// }
//
//
//
// float peek(final Pos p, final WonResource r) {
// return resources.get(p).peek(r);
// }
//
// float extract(final Pos p, final WonResource r, final int amount) {
// return resources.get(p).extract(r, amount);
// }
//
//
//
// Set<Pos> getResources() {
// return Collections.unmodifiableSet(resources.keySet());
// }
//
// /*
// *
// */
//
// private static class ResourceTile {
//
// private final Clock clock;
// private final WonResource resource;
//
// private float mark;
// private int marktime;
//
//
//
// ResourceTile(final Clock clock, final WonResource resource) {
// this.resource = resource;
// this.clock = clock;
// }
//
//
//
// WonResource getResource() {
// return resource;
// }
//
//
//
// float peek(final WonResource r) {
// return mark / (1f + resource.getRefillFactor() * (clock.time() - marktime));
// }
//
// float extract(final WonResource r, final int amount) {
// final float peek = peek(r);
// mark = peek + amount * resource.getExtractFactor();
// marktime = clock.time();
// return peek;
// }
//
//
//
// }
// }
//
//
//
// //
//
//
//
// private static class StructureMapI {
//
// private final Map<Pos, WonStructure<WonNodeDelegate, WonBufferDelegate>> structures = new HashMap<>();
// private final PathGrid paths;
//
//
//
// StructureMapI(final PathGrid paths) {
// this.paths = paths;
// }
//
//
//
// WonStructure<WonNodeDelegate, WonBufferDelegate> getStructure(final Pos p) {
// return structures.get(p);
// }
//
//
//
// void setStructure(final Pos p, final int range, final WonStructure<WonNodeDelegate, WonBufferDelegate> s) {
// grit(p.i, p.j, range, true).forEach((i, j) -> {
// structures.put(Pos.get(i, j), s);
// paths.setAccessible(i, j, false);
// });
//
// for (int k = p.j; k >= p.j - range; --k)
// paths.setAccessible(p.i, k, true);
// }
//
//
//
// WonStructure<WonNodeDelegate, WonBufferDelegate> removeStructure(final Pos p, final int range) {
// final WonStructure<WonNodeDelegate, WonBufferDelegate> removed = structures.get(p);
//
// grit(p.i, p.j, range, true).forEach((i, j) -> {
// structures.remove(Pos.get(i, j));
// paths.setAccessible(i, j, true);
// });
//
// return removed;
// }
// }
// }
