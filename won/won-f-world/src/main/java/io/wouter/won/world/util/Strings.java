package io.wouter.won.world.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Strings {

  private static final String BundleName = "game.test.strings"; //$NON-NLS-1$
  private static final ResourceBundle Bundle = ResourceBundle.getBundle(BundleName);

  //

  public static String getString(final String key) {
    try {
      return Bundle.getString(key);
    } catch (final MissingResourceException e) {
      return '!' + key + '!';
    }
  }

  //

  private Strings() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
