package io.wouter.won.world.grid;

import static java.util.Objects.requireNonNull;

import io.wouter.lib.props.Config;

public final class WonTerrain {

  private final WonTerrain.Type type;
  private final int height;
  private final int slope;



  public WonTerrain(final WonTerrain.Type type, final int height, final int slope) {
    this.type = requireNonNull(type);
    this.height = height;
    this.slope = slope;
  }



  public WonTerrain.Type getType() {
    return type;
  }

  public int getHeight() {
    return height;
  }

  public int getSlope() {
    return slope;
  }

  public int getMoveCost() {
    return type.movecost;
  }



  //



  public enum Type {

    SaltWater,
    FreshWater,
    Shore,
    Bank,
    Plain,
    Forest,
    Desert,
    Mountain,
    Glacier;



    final int movecost;



    private Type() {
      this.movecost = Config.getInt(Type.class, name() + ".MoveCost"); //$NON-NLS-1$
    }
  }
}
