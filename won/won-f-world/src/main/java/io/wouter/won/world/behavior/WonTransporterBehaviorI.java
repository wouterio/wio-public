package io.wouter.won.world.behavior;

import static io.wouter.lib.grid.Ori.S;
import static io.wouter.lib.task.TaskStatus.Completed;
import static io.wouter.lib.task.TaskStatus.Failed;
import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Suspended;
import static io.wouter.lib.task.TaskStatus.Working;
import static io.wouter.won.world.delegate.WonAccount.transfer;
import static java.lang.Float.POSITIVE_INFINITY;
import static java.lang.Math.min;
import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Deque;
import java.util.LinkedList;

import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.link.Transfer;
import com.aoi.won.core.node.Buffer;
import com.badlogic.gdx.math.Vector2;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Ori;
import io.wouter.lib.grid.Pos;
import io.wouter.lib.nav.AStar;
import io.wouter.lib.nav.Step;
import io.wouter.won.entity.behavior.WonTransporterBehavior;
import io.wouter.won.entity.task.WonTask;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;
import io.wouter.won.world.delegate.WonAccount;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;
import io.wouter.won.world.grid.WonAccess;

public class WonTransporterBehaviorI extends WonTask<WonNodeDelegate> implements WonTransporterBehavior<WonNodeDelegate, WonBufferDelegate> {

  private static final long serialVersionUID = -51268995349811895L;



  private final WonNodeDelegate delegate;
  private final Motion motion;
  private final int transportercap;



  private Transfer<WonItem, WonBufferDelegate> transfer = null;
  private TransferDetails transferdetails = null;



  public WonTransporterBehaviorI(final WonNodeDelegate delegate, final AStar<WonAccess, Pos, Dis> astar, final int transportercap) {
    super(delegate, delegate, Suspended);
    this.delegate = requireNonNull(delegate);
    this.motion = new Motion(delegate, astar);
    this.transportercap = transportercap;
  }



  @Override
  public boolean hasTransfer() {
    return transfer != null;
  }


  @Override
  public Transfer<WonItem, WonBufferDelegate> getTransfer() {
    return transfer;
  }

  @Override
  public void setTransfer(final Transfer<WonItem, WonBufferDelegate> newtransfer) {
    if (newtransfer != null) {
      if (hasTransfer())
        throw new IllegalStateException("Transporter already has a transfer."); //$NON-NLS-1$
      transfer = newtransfer;
      transferdetails = new TransferDetails(newtransfer, transportercap);
      setStatus(Pending);

    } else if (hasTransfer()) { // newtransfer == null
      transferdetails.dispose();
      transfer = null;
      transferdetails = null;
      setStatus(Suspended);
    }
  }



  @Override
  public WonTransporterBehavior<WonNodeDelegate, WonBufferDelegate> perform(final WonNodeDelegate agent, final float delta) {

    // fetch from src

    if (transferdetails.push.isActive()) {
      if (!motion.findPath(transferdetails.src.getBufferDelegate().getHome())) {
        setTransfer(null);
        return this;
      }

      if (!motion.move(agent, delta)) {
        setStatus(Working);
        return this;
      }

      transferdetails.fetch();
      setStatus(Working);
      return this;
    }

    // bring to tar

    if (transferdetails.pull.isActive()) {
      if (!motion.findPath(transferdetails.tar.getBufferDelegate().getHome())) {
        setTransfer(null);
        return this;
      }

      if (!motion.move(agent, delta)) {
        setStatus(Working);
        return this;
      }

      transferdetails.bring();
      transferdetails.pay();
      setStatus(Working);
      return this;
    }

    // return home

    if (!motion.findPath(delegate.getHome())) {
      setStatus(Working); // TODO what to do here?
      return this;
    }

    if (!motion.move(agent, delta)) {
      setStatus(Working);
      return this;
    }

    // reset

    setTransfer(null);
    return this;
  }



  @Override
  public void dispose() {
    super.dispose();
    motion.dispose();
    if (hasTransfer()) {
      transferdetails.dispose();
      transfer = null;
      transferdetails = null;
    }
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder();
  }

  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return hasTransfer() ?
        info.collect(transferdetails) :
        info;
  }



  //



  private static class TransferDetails implements WonInfoBuilder.Collect, Dispose, Serializable {

    private static final long serialVersionUID = -7427362178641873054L;



    final WonItem item;
    final PushRequest<WonItem, WonBufferDelegate> push;
    final PullRequest<WonItem, WonBufferDelegate> pull;
    final Buffer<WonItem, WonBufferDelegate> src;
    final Buffer<WonItem, WonBufferDelegate> tar;
    final int amount;
    final int price;
    final int payment;



    TransferDetails(final Transfer<WonItem, WonBufferDelegate> transfer, final int transportercap) {
      item = transfer.getItem();
      push = transfer.getPushRequests().findAndPoll();
      pull = transfer.getPullRequests().findAndPoll();
      src = push.getSource();
      tar = pull.getTarget();
      push.setStatus(Working);
      pull.setStatus(Working);

      final int srccount = src.getBufferDelegate().getBufferStock().getCount();
      final int tarspace = tar.getBufferDelegate().getBufferStock().getSpace();
      final int srcprice = src.getBufferDelegate().getBufferStock().getPrice();
      final int tarprice = tar.getBufferDelegate().getBufferStock().getPrice();

      amount = min(transportercap, min(srccount, tarspace));
      price = srcprice <= tarprice ?
          srcprice :
          srcprice + tarprice >> 1;
      payment = amount * price;
    }



    void fetch() {
      src.getBufferDelegate().getBufferStock().remove(amount);
      src.purged(true);
      push.setStatus(Completed);
      // TODO pay transporter
    }

    void bring() {
      tar.getBufferDelegate().getBufferStock().add(amount, price);
      tar.filled(true);
      pull.setStatus(Completed);
      // TODO pay transporter
    }


    void pay() {
      final WonAccount srcaccount = src.getBufferDelegate().getAccount();
      final WonAccount taraccount = tar.getBufferDelegate().getAccount();
      transfer(taraccount, srcaccount, payment); // XXX credits from tar to src
    }



    @Override
    public void dispose() {
      if (push.isActive()) {
        src.purged(false);
        push.setStatus(Failed);
      }
      if (pull.isActive()) {
        tar.filled(false);
        pull.setStatus(Failed);
      }
    }



    @Override
    public String toString() {
      return super.toString();
    }



    @Override
    public WonInfoBuilder collect(final WonInfoBuilder info) {
      return info //
          .field("item").link(item, WonItem::getType) //$NON-NLS-1$
          .field("amount").valueInt(() -> amount) //$NON-NLS-1$
          .separator() //
          .field("source").link(src.getBufferDelegate(), WonBufferDelegate::getType) //$NON-NLS-1$
          .separator() //
          .field("target").link(tar.getBufferDelegate(), WonBufferDelegate::getType); //$NON-NLS-1$
    }
  }



  //



  private static class Motion implements Dispose {

    private final WonNodeDelegate transporter;
    private final AStar<WonAccess, Pos, Dis> astar;
    private final Deque<Step<Pos, Dis>> path = new LinkedList<>();



    private Pos targetpos = null;
    private float cumdelta = 0f;



    Motion(final WonNodeDelegate transporter, final AStar<WonAccess, Pos, Dis> astar) {
      this.transporter = requireNonNull(transporter);
      this.astar = requireNonNull(astar);
    }



    boolean findPath(final Pos newtargetpos) {
      if (newtargetpos.equals(targetpos))
        return true;

      final Pos agentpos = asGridPosition(transporter.getPosition());

      if (!astar.findPath(transporter.getAccess(), agentpos, newtargetpos, POSITIVE_INFINITY, path))
        return false;

      targetpos = newtargetpos;
      cumdelta = 0f;
      return true;
    }



    boolean move(final WonNodeDelegate agent, final float delta) {
      if (path.isEmpty())
        return true;

      cumdelta += delta;
      float movecost = 0f;

      while (true) {
        final Step<Pos, Dis> step = path.element();
        final float stepcost = step.cost();
        movecost += stepcost;

        if (cumdelta < movecost) {
          final Pos a = step.from();
          final Pos b = step.to();
          final Dis d = step.transition();
          final float u = cumdelta / stepcost;
          move(transporter, agent, a, b, d, u);
          return false;
        }

        path.remove();
        cumdelta -= stepcost;

        if (path.isEmpty()) {
          final Pos b = step.to();
          final Dis d = step.transition();
          move(transporter, agent, b, b, d, 1f); // XXX b twice
          return true;
        }
      }
    }



    @Override
    public void dispose() {
      path.clear();
      targetpos = null;
      cumdelta = 0f;
    }



    private static Pos asGridPosition(final Vector2 pos) {
      return Pos.get((int) pos.x, (int) pos.y);
    }

    private static void move(final WonNodeDelegate transporter, final WonNodeDelegate agent, final Pos a, final Pos b, final Dis d, final float u) {
      final float x = interpolate(a.i, b.i, u);
      final float y = interpolate(a.j, b.j, u);
      final Ori o = d.toOri(S);
      transporter.getPosition().x = x;
      transporter.getPosition().y = y;
      transporter.setOrientation(o);
      agent.getPosition().x = x;
      agent.getPosition().y = y;
      agent.setOrientation(o);
    }

    private static float interpolate(final int a, final int b, final float u) {
      return a + (b - a) * u; // (1f - u) * a + u * b;
    }
  }
}
