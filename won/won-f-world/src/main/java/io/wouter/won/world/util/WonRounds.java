package io.wouter.won.world.util;

import java.util.function.LongSupplier;

public final class WonRounds implements LongSupplier {

  private static final float OneSecond = 1f;



  private float cumdelta = OneSecond;
  private boolean nextround = false;
  private long round = 0l;



  public WonRounds() { /* empty */ }



  public void proceed(final float delta) {
    cumdelta += delta;
    nextround = cumdelta >= OneSecond;
    if (nextround) {
      cumdelta -= OneSecond;
      ++round;
    }
  }



  public boolean isNextRound() {
    return nextround;
  }

  public long getRound() {
    return round;
  }

  @Override
  public long getAsLong() {
    return round;
  }
}
