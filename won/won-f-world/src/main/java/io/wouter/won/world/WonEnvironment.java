package io.wouter.won.world;

import java.util.function.Function;

import io.wouter.lib.grid.Pos;
import io.wouter.lib.nav.AStar;
import io.wouter.won.entity.model.WonBuildModel;
import io.wouter.won.object.product.WonResource;
import io.wouter.won.util.func.PerformDelta;
import io.wouter.won.world.build.WonBuildModelI;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;
import io.wouter.won.world.grid.WonLandscape;
import io.wouter.won.world.grid.WonNavModel;
import io.wouter.won.world.grid.WonTerrain;
import io.wouter.won.world.util.WonRounds;

public class WonEnvironment implements PerformDelta {

  private static final int W = 32;
  private static final int H = 64;



  private final WonRounds rounds;
  private final WonLandscape landscape;
  private final WonNavModel navmodel;
  private final WonBuildModel<WonNodeDelegate, WonBufferDelegate> buildmodel;



  public WonEnvironment() {
    final int[][] heightmap = new int[W][H];
    final Function<Pos, WonTerrain.Type> terrainmap = p -> WonTerrain.Type.Plain;
    final Function<Pos, WonResource> resourcemap = p -> new WonResource(0, "[resource]", 0, 0);

    rounds = new WonRounds();
    landscape = new WonLandscape.Builder(rounds, heightmap, terrainmap, resourcemap).build();
    navmodel = new WonNavModel(landscape);
    buildmodel = new WonBuildModelI(new AStar<>(navmodel, false));
  }



  public WonLandscape getLandscape() {
    return landscape;
  }

  public WonBuildModel<WonNodeDelegate, WonBufferDelegate> getBuildModel() {
    return buildmodel;
  }



  @Override
  public void perform(final float delta) {
    rounds.proceed(delta);
    if (rounds.isNextRound())
      buildmodel.perform(rounds.getRound());
  }
}
