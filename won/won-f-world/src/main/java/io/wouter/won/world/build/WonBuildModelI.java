package io.wouter.won.world.build;

import static com.aoi.won.core.Link.getIdleLink;
import static com.aoi.won.core.Link.newInterLink;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;
import com.badlogic.gdx.math.Vector2;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Pos;
import io.wouter.lib.nav.AStar;
import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonAgents;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.WonStructures;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.entity.WonTransporters;
import io.wouter.won.entity.behavior.WonAgentBehavior;
import io.wouter.won.entity.behavior.WonTransporterBehavior;
import io.wouter.won.entity.model.WonBuildListener;
import io.wouter.won.entity.model.WonBuildModel;
import io.wouter.won.entity.structure.WonConstructor;
import io.wouter.won.entity.structure.WonConverter;
import io.wouter.won.entity.structure.WonDistributor;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.entity.task.construction.NewConstructionTask;
import io.wouter.won.entity.task.conversion.NewConversionTask;
import io.wouter.won.entity.task.production.NewProductionTask;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.structure.WonConstructorTemplate;
import io.wouter.won.object.structure.WonConverterTemplate;
import io.wouter.won.object.structure.WonDistributorTemplate;
import io.wouter.won.object.structure.WonProducerTemplate;
import io.wouter.won.object.structure.WonStructureTemplate;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.util.enums.LinkType;
import io.wouter.won.world.behavior.WonAgentBehaviorI;
import io.wouter.won.world.behavior.WonTransporterBehaviorI;
import io.wouter.won.world.delegate.WonAccount;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;
import io.wouter.won.world.grid.WonAccess;
import io.wouter.won.world.grid.WonTerrain;
import io.wouter.won.world.task.ConstructionTaskI;
import io.wouter.won.world.task.ConversionTaskI;
import io.wouter.won.world.task.ProductionTaskI;

public class WonBuildModelI implements WonBuildModel<WonNodeDelegate, WonBufferDelegate> {

  private static final Function<WonNodeDelegate, Object> GetAgentId = WonNodeDelegate::getId; // agent node delegate
  private static final float DefaultTaxRate = 0.15f;
  private static final float DefaultProfitRate = 2f;



  private final AStar<WonAccess, Pos, Dis> astar;
  private final WonAccount playeraccount;
  private final WonNodeDelegate playerdelegate;

  private final WonAgents<WonNodeDelegate, WonBufferDelegate> agents;
  private final WonStructures<WonNodeDelegate, WonBufferDelegate> structures;
  private final WonTransporters<WonNodeDelegate, WonBufferDelegate> transporters;

  private final NewConstructionTask<WonNodeDelegate, WonBufferDelegate> newconstructiontask;
  private final NewProductionTask<WonNodeDelegate, WonBufferDelegate> newproductiontask;
  private final NewConversionTask<WonNodeDelegate, WonBufferDelegate> newconversiontask;

  private final Link<WonItem, WonBufferDelegate> internal;
  private final Link<WonItem, WonBufferDelegate> external;
  private final Link<WonItem, WonBufferDelegate> idle;

  private final List<WonBuildListener<WonNodeDelegate, WonBufferDelegate>> buildlisteners;
  private final List<WonResidenceManager> residencemanagers;



  public WonBuildModelI(final AStar<WonAccess, Pos, Dis> astar) {
    this.astar = requireNonNull(astar);

    playeraccount = new WonAccount(-1, "Player", null, DefaultTaxRate); //$NON-NLS-1$
    playerdelegate = new WonNodeDelegate(playeraccount, new WonAccess(asList(WonTerrain.Type.Plain), MIN_VALUE), playeraccount, 0, Pos.get(0, 0));

    agents = new WonAgents<>();
    structures = new WonStructures<>();
    transporters = new WonTransporters<>();

    newconstructiontask = (n, c) -> new ConstructionTaskI(n, c, (d, t) -> buildStructure(d, t));
    newproductiontask = (n, p) -> new ProductionTaskI(n, p, (d, t) -> buildStructure(d, t));
    newconversiontask = (n, c) -> new ConversionTaskI(n, c, (d, t) -> buildStructure(d, t));

    internal = newInterLink(t -> transporters.offer(t));
    external = null;
    idle = getIdleLink();

    buildlisteners = new ArrayList<>();
    residencemanagers = new ArrayList<>();
  }



  @Override
  public WonNodeDelegate getPlayer() {
    return playerdelegate;
  }



  @Override
  public void addBuildListener(final WonBuildListener<WonNodeDelegate, WonBufferDelegate> l) {
    buildlisteners.add(l);
  }

  @Override
  public void removeBuildListener(final WonBuildListener<WonNodeDelegate, WonBufferDelegate> l) {
    buildlisteners.remove(l);
  }



  @Override
  public void perform(final long time) {
    transporters.distribute();
    agents.distribute();
    residencemanagers.forEach(m -> m.manage());
  }



  @Override
  public WonAgent<WonNodeDelegate, WonBufferDelegate> buildAgent(final WonNodeDelegate residence) {
    final Pos pos = asGridPosition(residence.getPosition());
    final WonAccess access = new WonAccess(asList(WonTerrain.Type.Plain), MAX_VALUE); // TODO
    final WonAccount account = residence.getAccount();
    final WonNodeDelegate delegate = new WonNodeDelegate(0, "Agent", access, account, 0, pos); //$NON-NLS-1$
    final WonAgentBehavior<WonNodeDelegate, WonBufferDelegate> behavior = new WonAgentBehaviorI(delegate, astar);
    final WonAgent<WonNodeDelegate, WonBufferDelegate> a = new WonAgent<>(delegate, behavior);
    agents.add(a);
    fireAgentBuilt(a);
    return a;
  }

  @Override
  public WonTransporter<WonNodeDelegate, WonBufferDelegate> buildTransporter(final WonNodeDelegate reference, final WonTransporterTemplate template) {
    final Pos pos = asGridPosition(reference.getPosition());
    final WonAccess access = new WonAccess(asList(WonTerrain.Type.Plain), MAX_VALUE); // TODO
    final WonAccount account = reference.getAccount();
    final WonNodeDelegate delegate = new WonNodeDelegate(template, access, account, 0, pos);
    final int cap = template.getCapacity();
    final WonTransporterBehavior<WonNodeDelegate, WonBufferDelegate> behavior = new WonTransporterBehaviorI(delegate, astar, cap);
    final WonTransporter<WonNodeDelegate, WonBufferDelegate> t = new WonTransporter<>(template, delegate, behavior, GetAgentId);
    transporters.add(t);
    agents.offer(t.getTask());
    fireTransporterBuilt(t);
    return t;
  }

  @Override
  public WonEntity buildStructure(final WonNodeDelegate reference, final WonStructureTemplate template) {
    if (template instanceof WonConstructorTemplate)
      return newConstructor(reference, (WonConstructorTemplate) template);
    if (template instanceof WonProducerTemplate)
      return newProducer(reference, (WonProducerTemplate) template);
    if (template instanceof WonTransporterTemplate)
      return buildTransporter(reference, (WonTransporterTemplate) template);
    if (template instanceof WonDistributorTemplate)
      return newDistributor(reference, (WonDistributorTemplate) template);
    throw new IllegalArgumentException("Unknown WonStructureTemplate subclass"); //$NON-NLS-1$
  }



  private WonConstructor<WonNodeDelegate, WonBufferDelegate> newConstructor(final WonNodeDelegate reference, final WonConstructorTemplate template) {
    final Pos pos = reference.getHome();
    final Node<WonItem, WonNodeDelegate, WonBufferDelegate> node = newNode(template, playeraccount, pos);
    final WonConstructor<WonNodeDelegate, WonBufferDelegate> c = new WonConstructor<>(template, node, internal, newconstructiontask, GetAgentId);
    structures.add(c);
    agents.offer(c.getTask());
    fireStructureBuilt(c);
    return c;
  }

  private WonProducer<WonNodeDelegate, WonBufferDelegate> newProducer(final WonNodeDelegate reference, final WonProducerTemplate template) {
    final WonAccount account = new WonAccount(template, playeraccount, DefaultProfitRate);
    final Pos pos = reference.getHome();
    final Node<WonItem, WonNodeDelegate, WonBufferDelegate> node = newNode(template, account, pos);
    final WonProducer<WonNodeDelegate, WonBufferDelegate> p = new WonProducer<>(template, node, internal, newproductiontask, GetAgentId);
    structures.add(p);
    agents.offer(p.getTask());
    fireStructureBuilt(p);
    if (WonResidenceManager.isResidence(p)) {
      final WonResidenceManager m = new WonResidenceManager(p, r -> buildAgent(r.getStructureDelegate()));
      residencemanagers.add(m);
    }
    return p;
  }

  private WonConverter<WonNodeDelegate, WonBufferDelegate> newConverter(final WonNodeDelegate reference, final WonConverterTemplate template) {
    final Pos pos = reference.getHome();
    final Node<WonItem, WonNodeDelegate, WonBufferDelegate> node = newNode(template, null, pos);
    final WonConverter<WonNodeDelegate, WonBufferDelegate> c = new WonConverter<>(template, node, newconversiontask, GetAgentId);
    structures.add(c);
    agents.offer(c.getTask());
    fireStructureBuilt(c);
    // if (WonResidenceManager.isResidence(p)) {
    // final WonResidenceManager m = new WonResidenceManager(p, r -> newAgent(r.getNodeDelegate()));
    // residencemanagers.add(m);
    // }
    return c;
  }

  private WonDistributor<WonNodeDelegate, WonBufferDelegate> newDistributor(final WonNodeDelegate reference, final WonDistributorTemplate template) {
    final Pos pos = reference.getHome();
    final LinkType pulltype = template.getPullType();
    final LinkType pushtype = template.getPushType();
    final Link<WonItem, WonBufferDelegate> pulllink = getLink(pulltype);
    final Link<WonItem, WonBufferDelegate> pushlink = getLink(pushtype);
    final Node<WonItem, WonNodeDelegate, WonBufferDelegate> node = newNode(template, playeraccount, pos);
    final WonDistributor<WonNodeDelegate, WonBufferDelegate> d = WonDistributor.newInstance(template, node);
    structures.add(d);
    fireStructureBuilt(d);
    return d;
  }



  private static Node<WonItem, WonNodeDelegate, WonBufferDelegate> newNode(final WonStructureTemplate template, final WonAccount account, final Pos pos) {
    final int nodecap = template.getCapacity();
    final WonAccess access = new WonAccess(asList(WonTerrain.Type.Plain), MAX_VALUE); // TODO
    final WonNodeDelegate nodedelegate = new WonNodeDelegate(template, access, account, nodecap, pos);
    final Function<WonItem, WonBufferDelegate> ensureibufferdelegate = item -> nodedelegate.getInput().ensureBufferDelegate(item);
    final Function<WonItem, WonBufferDelegate> ensureobufferdelegate = item -> nodedelegate.getOutput().ensureBufferDelegate(item);
    return Node.newNode(nodedelegate, ensureibufferdelegate, ensureobufferdelegate);
  }



  private Link<WonItem, WonBufferDelegate> getLink(final LinkType link) {
    switch (link) {
      case Internal:
        return internal;
      case External:
        return external;
      case Idle:
        return idle;
      default:
        throw new AssertionError();
    }
  }



  private void fireAgentBuilt(final WonAgent<WonNodeDelegate, WonBufferDelegate> a) {
    buildlisteners.forEach(l -> l.agentBuilt(a));
  }

  private void fireStructureBuilt(final WonStructure<WonNodeDelegate, WonBufferDelegate> c) {
    buildlisteners.forEach(l -> l.structureBuilt(c));
  }

  private void fireTransporterBuilt(final WonTransporter<WonNodeDelegate, WonBufferDelegate> t) {
    buildlisteners.forEach(l -> l.transporterBuilt(t));
  }



  private static Pos asGridPosition(final Vector2 pos) {
    return Pos.get((int) pos.x, (int) pos.y);
  }
}
