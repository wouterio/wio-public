package io.wouter.won.world.grid;

import static io.wouter.lib.grid.Dis.E;
import static io.wouter.lib.grid.Dis.N;
import static io.wouter.lib.grid.Dis.NE;
import static io.wouter.lib.grid.Dis.NW;
import static io.wouter.lib.grid.Dis.S;
import static io.wouter.lib.grid.Dis.SE;
import static io.wouter.lib.grid.Dis.SW;
import static io.wouter.lib.grid.Dis.W;
import static java.lang.Math.abs;
import static java.util.Objects.requireNonNull;

import java.util.function.Function;
import java.util.function.LongSupplier;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Grid;
import io.wouter.lib.grid.Pos;
import io.wouter.won.object.product.WonResource;

public final class WonLandscape implements Grid {

  private final Cell[][] cells;
  private final int w;
  private final int h;



  WonLandscape(final Cell[][] cells) {
    this.cells = cells;
    this.w = cells.length;
    this.h = cells[0].length;
  }



  @Override
  public int getWidth() {
    return w;
  }

  @Override
  public int getHeight() {
    return h;
  }



  public Cell getCell(final Pos pos) {
    return cells[pos.i][pos.j];
  }



  //



  public static final class Cell {

    private final WonTerrain terrain;
    private final WonResourceLocation loc;
    private final WonTrail trail;



    private boolean access = true;



    public Cell(final WonTerrain terrain, final WonResource resource) {
      this.terrain = requireNonNull(terrain);
      this.loc = new WonResourceLocation(resource);
      this.trail = new WonTrail();
    }

    public Cell(final WonTerrain terrain, final WonResource resource, final LongSupplier gettime) {
      this.terrain = requireNonNull(terrain);
      this.loc = new WonResourceLocation(resource, gettime);
      this.trail = new WonTrail(gettime);
    }



    public WonTerrain getTerrain() {
      return terrain;
    }

    public WonResourceLocation getResource() {
      return loc;
    }

    public WonTrail getTrail() {
      return trail;
    }

    public boolean isAccessible() {
      return access;
    }

    public void setAccessible(final boolean access) {
      this.access = access;
    }
  }



  //



  public static final class Builder implements Grid {

    private static final Dis[] Dispos = {N, NE, E, SE, S, SW, W, NW};
    // private static final Dis[] Dispos2 = {N, NE, E, SE}; // only need half of directions because of abs diff symmetry



    private final LongSupplier gettime;
    private final int[][] heightmap;
    private final Function<Pos, WonTerrain.Type> terrainmap;
    private final Function<Pos, WonResource> resourcemap;
    private final int w;
    private final int h;



    public Builder(final LongSupplier gettime, final int[][] heightmap, final Function<Pos, WonTerrain.Type> terrainmap, final Function<Pos, WonResource> resourcemap) {
      this.gettime = requireNonNull(gettime);
      this.heightmap = requireNonNull(heightmap);
      this.terrainmap = requireNonNull(terrainmap);
      this.resourcemap = requireNonNull(resourcemap);
      this.w = heightmap.length;
      this.h = heightmap[0].length;
    }



    @Override
    public int getWidth() {
      return w;
    }

    @Override
    public int getHeight() {
      return h;
    }



    public WonLandscape build() {
      final Cell[][] cells = new Cell[w][h];
      for (int i = 0; i < w; ++i)
        for (int j = 0; j < h; ++j)
          cells[i][j] = buildCell(Pos.get(i, j));
      return new WonLandscape(cells);
    }



    private Cell buildCell(final Pos p) {
      final int height = heightmap[p.i][p.j];
      final int slope = computeSlope(p);
      final WonTerrain.Type t = terrainmap.apply(p);
      final WonTerrain terrain = new WonTerrain(t, height, slope);
      final WonResource resource = resourcemap.apply(p);
      return new Cell(terrain, resource, gettime);
    }

    private int computeSlope(final Pos p0) {
      final int h0 = heightmap[p0.i][p0.j];
      int hislope = 0;
      for (final Dis d : Dispos) {
        final Pos p1 = p0.add(d);
        if (includes(p1)) {
          final int h1 = heightmap[p1.i][p1.j];
          final int slope = abs(h1 - h0);
          if (slope > hislope)
            hislope = slope;
        }
      }
      return hislope;
    }



    // private Cell buildCell(final int i, final int j) {
    // final int height = heightmap[i][j];
    // final int slope = computeSlope(i, j);
    // final Pos p = Pos.get(i, j);
    // final WonTerrain.Type t = terrainmap.apply(p);
    // final WonTerrain terrain = new WonTerrain(t, height, slope);
    // final WonResource resource = resourcemap.apply(p);
    // return new Cell(terrain, resource, gettime);
    // }
    //
    // private int computeSlope(final int i0, final int j0) {
    // final int h0 = heightmap[i0][j0];
    // int hislope = 0;
    // for (final Dis d : Dispos) {
    // final int i1 = i0 + d.di;
    // final int j1 = j0 + d.dj;
    // if (includes(i1, j1)) {
    // final int h1 = heightmap[i1][j1];
    // final int slope = abs(h1 - h0);
    // if (slope > hislope)
    // hislope = slope;
    // }
    // }
    // return hislope;
    // }
    //
    // private int computeSlope2(final int i0, final int j0) {
    // int hislope = 0;
    //
    // for (final Dis d : Dispos2) {
    // final int i1 = i0 + d.di;
    // final int j1 = j0 + d.dj;
    // final int h1 = isWithin(i1, j1) ?
    // heightmap[i1][j1] :
    // heightmap[i0][j0];
    //
    // final int i2 = i0 - d.di;
    // final int j2 = j0 - d.dj;
    // final int h2 = isWithin(i2, j2) ?
    // heightmap[i2][j2] :
    // heightmap[i0][j0];
    //
    // final int slope = abs(h1 - h2);
    // if (slope > hislope)
    // hislope = slope;
    // }
    //
    // return hislope;
    // }
  }
}
