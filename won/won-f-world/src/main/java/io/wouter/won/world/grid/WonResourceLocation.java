package io.wouter.won.world.grid;

import static java.util.Objects.requireNonNull;

import java.util.function.LongSupplier;

import io.wouter.won.object.product.WonResource;

public class WonResourceLocation {

  private final WonResource resource;
  private final LongSupplier gettime;



  private float mark = 0f;
  private long lasttime = 0l;



  public WonResourceLocation(final WonResource resource) {
    this.resource = requireNonNull(resource);
    this.gettime = null;
  }

  public WonResourceLocation(final WonResource resource, final LongSupplier gettime) {
    this.resource = requireNonNull(resource);
    this.gettime = requireNonNull(gettime);
  }



  public WonResource getResource() {
    return resource;
  }



  public float peek() {
    final long time = gettime.getAsLong();
    return peek(time);
  }

  public float peek(final long time) {
    refill(time);
    return mark;
  }



  public float extract(final int amount) {
    final long time = gettime.getAsLong();
    return extract(time, amount);
  }

  public float extract(final long time, final int amount) {
    refill(time);
    return mark += amount * resource.getExtractFactor();
  }



  private void refill(final long time) {
    if (time <= lasttime)
      return;
    mark -= resource.getRefillFactor() * (time - lasttime);
    if (mark < 0f)
      mark = 0f;
    lasttime = time;
  }
}
