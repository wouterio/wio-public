package io.wouter.won.world.delegate;

import static io.wouter.won.util.stock.Stocks.newMetaStock;
import static java.lang.Float.NaN;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

import com.badlogic.gdx.math.Vector2;

import io.wouter.lib.grid.Ori;
import io.wouter.lib.grid.Pos;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.WonObject;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.stock.AccountStock;
import io.wouter.won.util.stock.MetaStock;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;
import io.wouter.won.world.grid.WonAccess;

public final class WonNodeDelegate extends WonEntity {

  private static final long serialVersionUID = -8982546852239964550L;



  private static final AtomicLong Indexer = new AtomicLong(0l);



  private final Long id = Long.valueOf(Indexer.getAndIncrement());
  private final WonAccess access;
  private final WonBufferDelegates input;
  private final WonBufferDelegates output;



  private final Vector2 pos = new Vector2(NaN, NaN);
  private Pos home;
  private Ori ori = Ori.S;
  private WonAccount account;



  public WonNodeDelegate(final int index, final String type, final WonAccess access, final WonAccount account, final int cap, final Pos home) {
    super(index, type);
    this.access = access;
    this.account = account;
    this.input = new WonBufferDelegates(index, type, newMetaStock(cap, cap >> 2, cap >> 2, true));
    this.output = new WonBufferDelegates(index, type, newMetaStock(cap, cap >> 2, cap >> 2, true));
    this.pos.set(home.i, home.j);
    this.home = home;
  }

  public WonNodeDelegate(final WonObject reference, final WonAccess access, final WonAccount account, final int cap, final Pos home) {
    super(reference);
    this.access = access;
    this.account = account;
    this.input = new WonBufferDelegates(reference, newMetaStock(cap, cap >> 2, cap >> 2, true));
    this.output = new WonBufferDelegates(reference, newMetaStock(cap, cap >> 2, cap >> 2, true));
    this.pos.set(home.i, home.j);
    this.home = home;
  }



  public Long getId() {
    return id;
  }

  public WonAccess getAccess() {
    return access;
  }

  public WonBufferDelegates getInput() {
    return input;
  }

  public WonBufferDelegates getOutput() {
    return output;
  }

  public WonAccount getAccount() {
    return account;
  }



  public Vector2 getPosition() {
    return pos;
  }

  public Pos getHome() {
    return home;
  }

  public Ori getOrientation() {
    return ori;
  }



  public void setAccount(final WonAccount account) {
    this.account = account;
  }

  public void setHome(final Pos newhome) {
    this.home = newhome;
  }

  public void setOrientation(final Ori ori) {
    this.ori = ori;
  }



  @Override
  public void dispose() {
    super.dispose(); // XXX do not dispose account
    input.dispose();
    output.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("pos", pos) //$NON-NLS-1$
        .append("account", account) //$NON-NLS-1$
        .append("input", input) //$NON-NLS-1$
        .append("output", output); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("pos").valueObject(this::getPosition, Vector2::toString) //$NON-NLS-1$
        .collect(account) //
        .separator() //
        .child("input").collect(input).parent() //$NON-NLS-1$
        .separator() //
        .child("output").collect(output).parent(); //$NON-NLS-1$
  }



  //



  public class WonBufferDelegates extends WonEntity implements Iterable<WonBufferDelegate> {

    private static final long serialVersionUID = -5827776730150784898L;



    private final Map<WonItem, WonBufferDelegate> bufferdelegates = new TreeMap<>(WonObject.CompareIndex);
    private final Map<WonItem, WonBufferDelegate> ubufferdelegates = unmodifiableMap(bufferdelegates);
    private final MetaStock meta;



    WonBufferDelegates(final int index, final String type, final MetaStock meta) {
      super(index, type);
      this.meta = requireNonNull(meta);
    }

    WonBufferDelegates(final WonObject reference, final MetaStock meta) {
      super(reference);
      this.meta = requireNonNull(meta);
    }



    public int getCount() {
      return meta.getCount();
    }

    public int getCapacity() {
      return meta.getCapacity();
    }



    public boolean hasBufferDelegates() {
      return !bufferdelegates.isEmpty();
    }

    public boolean hasBufferDelegate(final WonItem item) {
      return bufferdelegates.containsKey(item);
    }

    public WonBufferDelegate ensureBufferDelegate(final WonItem item) {
      return bufferdelegates.computeIfAbsent(item, i -> newBufferDelegate(i));
    }

    public WonBufferDelegate getBufferDelegate(final WonItem item) {
      return bufferdelegates.get(item);
    }

    public WonBufferDelegate removeBufferDelegate(final WonItem item) {
      final WonBufferDelegate removed = bufferdelegates.remove(item);
      removed.dispose();
      return removed;
    }



    public Set<WonItem> getItems() {
      return ubufferdelegates.keySet();
    }

    public Collection<WonBufferDelegate> getBufferDelegates() {
      return ubufferdelegates.values();
    }



    @Override
    public void dispose() {
      super.dispose();
      forEach(WonBufferDelegate::dispose);
    }



    @Override
    protected WonStringBuilder toStringBuilder() {
      return super.toStringBuilder() //
          .append("meta", meta) //$NON-NLS-1$
          .appendAll("bufferdelegates", ubufferdelegates.values(), d -> d.getBufferStock().toString()); //$NON-NLS-1$
    }



    @Override
    public WonInfoBuilder collect(final WonInfoBuilder info) {
      return info //
          .field("metastockcount").valueInt(this::getCount) //$NON-NLS-1$
          .field("metastockcap").valueInt(this::getCapacity) //$NON-NLS-1$
          .separator() //
          .collectAll(ubufferdelegates.values(), WonBufferDelegate::getBufferStock);
    }



    @Override
    public Iterator<WonBufferDelegate> iterator() {
      return getBufferDelegates().iterator();
    }



    private WonBufferDelegate newBufferDelegate(final WonItem item) {
      final int buffercap = getCapacity() >> 2;
      final int bufferlocount = 1;
      final int bufferlospace = 1;
      final AccountStock bufferstock = meta.newAccountSubStock(buffercap, bufferlocount, bufferlospace, true);
      final Dispose metadispose = () -> removeBufferDelegate(item);
      return new WonBufferDelegate(item, WonNodeDelegate.this, item, bufferstock, metadispose);
    }
  }
}
