package io.wouter.won.world.grid;

import static io.wouter.lib.grid.Dis.E;
import static io.wouter.lib.grid.Dis.N;
import static io.wouter.lib.grid.Dis.NE;
import static io.wouter.lib.grid.Dis.NW;
import static io.wouter.lib.grid.Dis.S;
import static io.wouter.lib.grid.Dis.SE;
import static io.wouter.lib.grid.Dis.SW;
import static io.wouter.lib.grid.Dis.W;
import static io.wouter.lib.grid.Pos.diagdist;
import static java.lang.Math.abs;
import static java.util.Objects.requireNonNull;

import java.util.function.BiConsumer;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Pos;
import io.wouter.lib.nav.Model;
import io.wouter.lib.props.Config;
import io.wouter.won.world.grid.WonLandscape.Cell;

public class WonNavModel implements Model<WonAccess, Pos, Dis> {

  private static final Dis[] Moves = {N, NE, E, SE, S, SW, W, NW};
  private static final float SlopeFtr = Config.getFloat(WonNavModel.class, "SlopeFtr"); //$NON-NLS-1$
  private static final float TerrainFtr = Config.getFloat(WonNavModel.class, "TerrainFtr"); //$NON-NLS-1$
  private static final float TrailFtr = Config.getFloat(WonNavModel.class, "TrailFtr"); //$NON-NLS-1$



  private final WonLandscape landscape;



  public WonNavModel(final WonLandscape landscape) {
    this.landscape = requireNonNull(landscape);
  }



  @Override
  public boolean isAccessible(final WonAccess agent, final Pos state) {
    return landscape.includes(state) && agent.canAccess(landscape.getCell(state));
  }



  @Override
  public void expand(final WonAccess agent, final Pos current, final BiConsumer<Dis, Pos> expand) {
    for (final Dis dis : Moves) {
      final Pos next = current.add(dis);
      if (isAccessible(agent, next))
        expand.accept(dis, next);
    }
  }



  @Override
  public float computeTransitionCost(final WonAccess agent, final Pos from, final Dis trans, final Pos to) {
    final Cell f = landscape.getCell(from);
    final Cell t = landscape.getCell(to);
    return trans.dist * (1f + computeTransitionSlopeCost(f, t) + computeTransitionTerrainCost(f, t) + computeTransitionTrailCost(f, t));
  }



  @Override
  public float computeHeuristicCost(final WonAccess agent, final Pos from, final Pos end) {
    final Cell f = landscape.getCell(from);
    final Cell e = landscape.getCell(end);
    return diagdist(from, end) * (1f + computeHeuristicSlopeCost(f, e));
  }



  private static float computeTransitionSlopeCost(final Cell from, final Cell to) {
    return SlopeFtr * .5f * (from.getTerrain().getSlope() + to.getTerrain().getSlope());
  }

  private static float computeTransitionTerrainCost(final Cell from, final Cell to) {
    return TerrainFtr * (from.getTerrain().getMoveCost() + to.getTerrain().getMoveCost());
  }

  private static float computeTransitionTrailCost(final Cell from, final Cell to) {
    return TrailFtr / (1f + from.getTrail().peek() + to.getTrail().peek());
  }

  private static float computeHeuristicSlopeCost(final Cell from, final Cell end) {
    return SlopeFtr * abs(end.getTerrain().getHeight() - from.getTerrain().getHeight());
  }
}
