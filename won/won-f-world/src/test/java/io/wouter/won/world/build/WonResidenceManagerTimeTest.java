package io.wouter.won.world.build;


public class WonResidenceManagerTimeTest implements Runnable {

  private static final float OneHour = 3600f;
  private static final float OneMinute = 60f;


  @Override
  public void run() {
    for (int i = 0; i < OneHour; ++i)
      manage();
    System.out.println("expected:");
    System.out.println(n + " updates per minute");
    System.out.println(n * (OneHour / OneMinute) + " updates per hour");
    System.out.println("actual:");
    System.out.println(k + " updates per hour");
  }



  private final int n = 1000;
  private final float interval = OneMinute / n;



  private float timer = 0f;
  private int k = 0;



  private void manage() {
    timer += 1f;
    while (timer >= interval) {
      timer -= interval;
      ++k;
    }
  }

  private void manage2() {
    timer += 1f;
    if (timer < interval) // XXX only works up to 60 per minute
      return;
    timer -= interval;
    ++k;
  }



  public static void main(final String[] args) {
    new WonResidenceManagerTimeTest().run();
  }
}
