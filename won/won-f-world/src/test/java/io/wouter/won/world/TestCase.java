package io.wouter.won.world;

import static io.wouter.won.object.WonObject.CompareIndex;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.entity.model.WonBuildListener;
import io.wouter.won.entity.model.WonBuildModel;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.entity.structure.handler.WonProductionHandler;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.util.counter.CounterC;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.counter.Counters;
import io.wouter.won.util.func.PerformRound;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public abstract class TestCase implements PerformRound, WonBuildListener<WonNodeDelegate, WonBufferDelegate> {

  private final WonEnvironment env;
  private final Collection<WonEntity> objects = new ArrayList<>();



  protected TestCase(final WonEnvironment env) {
    this.env = requireNonNull(env);
    this.env.getBuildModel().addBuildListener(this);
  }



  @Override
  public void perform(final long time) {
    env.perform(time);
  }



  @Override
  public void agentBuilt(final WonAgent<WonNodeDelegate, WonBufferDelegate> a) {
    objects.add(a);
  }

  @Override
  public void transporterBuilt(final WonTransporter<WonNodeDelegate, WonBufferDelegate> t) {
    objects.add(t);
  }

  @Override
  public void structureBuilt(final WonStructure<WonNodeDelegate, WonBufferDelegate> s) {
    objects.add(s);
  }

  @Override
  public void agentDestroyed(final WonAgent<WonNodeDelegate, WonBufferDelegate> a) {
    objects.remove(a);
  }

  @Override
  public void transporterDestroyed(final WonTransporter<WonNodeDelegate, WonBufferDelegate> t) {
    objects.remove(t);
  }

  @Override
  public void structureDestroyed(final WonStructure<WonNodeDelegate, WonBufferDelegate> s) {
    objects.remove(s);
  }



  public final WonBuildModel<WonNodeDelegate, WonBufferDelegate> getModel() {
    return env.getBuildModel();
  }

  public final Collection<WonEntity> getObjects() {
    return objects;
  }



  protected final WonAgent<WonNodeDelegate, WonBufferDelegate> buildAgent(final WonProducer<WonNodeDelegate, WonBufferDelegate> residence) {
    return env.getBuildModel().buildAgent(residence.getStructureDelegate());
  }

  protected final WonTransporter<WonNodeDelegate, WonBufferDelegate> buildTransporter(final WonConstruction c) {
    return env.getBuildModel().buildTransporter(env.getBuildModel().getPlayer(), (WonTransporterTemplate) c.getStructureTemplate());
  }

  protected final WonProducer<WonNodeDelegate, WonBufferDelegate> buildProducer(final WonConstruction c) {
    return (WonProducer<WonNodeDelegate, WonBufferDelegate>) env.getBuildModel().buildStructure(env.getBuildModel().getPlayer(), c.getStructureTemplate());
  }



  protected static <N extends WonEntity, B extends WonEntity> WonProductionHandler getHandler(final WonProducer<N, B> p, final String type) {
    return p.getHandlers().stream() //
        .filter(h -> h.getProduction().getType().equals(type)) //
        .findAny() //
        .orElseThrow(NoSuchElementException::new); //
  }

  protected static CounterC<WonItem> toCounter(final WonItem... items) {
    final CounterM<WonItem> counter = Counters.newCounter(CompareIndex);
    for (final WonItem i : items)
      counter.ensure(i).incr();
    return counter.getConst();
  }
}
