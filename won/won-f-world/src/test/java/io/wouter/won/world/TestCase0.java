package io.wouter.won.world;

import static java.util.Arrays.asList;

import io.wouter.lib.grid.Pos;
import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.object.structure.WonProducerTemplate;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class TestCase0 extends TestCase {

  public TestCase0(final WonEnvironment env) {
    super(env);

    final WonItem ia = new WonItem(0, "A", 1);

    final WonProduction p0a = new WonProduction(0, "_->A", toCounter(), toCounter(ia), null, null, 1);

    final WonProducerTemplate rt = new WonProducerTemplate(0, "R", asList(), 1, 1, 0);
    final WonProducerTemplate wt = new WonProducerTemplate(1, "W", asList(p0a), 1, 2, 16);

    final WonConstruction rc = new WonConstruction(0, "Rc", toCounter(), rt, 0);
    final WonConstruction wc = new WonConstruction(1, "Wc", toCounter(), wt, 0);

    // build world

    env.getBuildModel().getPlayer().setHome(Pos.get(2, 2));
    final WonProducer<WonNodeDelegate, WonBufferDelegate> pr1 = buildProducer(rc);
    env.getBuildModel().getPlayer().setHome(Pos.get(8, 8));
    final WonProducer<WonNodeDelegate, WonBufferDelegate> pw2 = buildProducer(wc);
    env.getBuildModel().getPlayer().setHome(null);

    final WonAgent<WonNodeDelegate, WonBufferDelegate> aa5 = buildAgent(pr1);
    final WonAgent<WonNodeDelegate, WonBufferDelegate> aa6 = buildAgent(pr1);

    getHandler(pw2, "_->A").setEnabled(true); //$NON-NLS-1$
    getHandler(pw2, "_->A").setActive(true); //$NON-NLS-1$
  }



  public static void main(final String[] args) {
    new WriterTest(new TestCase0(new WonEnvironment())).run();
  }
}
