package io.wouter.won.world.delegate;

import static java.lang.Math.round;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class AccountTest {

  @Test
  public void srcAccountPays() {
    final WonAccount tax = new WonAccount(0, "tax", null, .1f); //$NON-NLS-1$
    final WonAccount src = new WonAccount(1, "src", tax, 1f); //$NON-NLS-1$
    final WonAccount tar = new WonAccount(2, "tar", tax, 1f); //$NON-NLS-1$

    final int beforetotal = src.getCredits() + tar.getCredits() + tax.getCredits();
    System.out.println("before:  src=" + src.getCredits() + " tar=" + tar.getCredits() + " tax=" + tax.getCredits()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    System.out.println("> total: " + beforetotal); //$NON-NLS-1$

    final int amount = 100;
    final float taxrate = tax.getRate();
    final int taxamount = round(taxrate * amount);
    System.out.println("amount:  " + amount); //$NON-NLS-1$
    System.out.println("tax:     " + taxamount); //$NON-NLS-1$

    WonAccount.transfer(src, tar, amount);

    final int aftertotal = src.getCredits() + tar.getCredits() + tax.getCredits();
    System.out.println("after:   src=" + src.getCredits() + " tar=" + tar.getCredits() + " tax=" + tax.getCredits()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    System.out.println("> total: " + aftertotal); //$NON-NLS-1$

    assertEquals(aftertotal, beforetotal);
  }



  @Test
  public void taxAccountPays() {
    final WonAccount tax = new WonAccount(0, "tax", null, .1f); //$NON-NLS-1$
    final WonAccount tar = new WonAccount(2, "tar", tax, 1f); //$NON-NLS-1$

    final int beforetotal = tar.getCredits() + tax.getCredits();
    System.out.println("before:  tar=" + tar.getCredits() + " tax=" + tax.getCredits()); //$NON-NLS-1$ //$NON-NLS-2$
    System.out.println("> total: " + beforetotal); //$NON-NLS-1$

    final int amount = 100;
    final float taxrate = tax.getRate();
    final int taxamount = round(taxrate * amount);
    System.out.println("amount:  " + amount); //$NON-NLS-1$
    System.out.println("tax:     " + taxamount); //$NON-NLS-1$

    WonAccount.transfer(tax, tar, amount);

    final int aftertotal = tar.getCredits() + tax.getCredits();
    System.out.println("after:   tar=" + tar.getCredits() + " tax=" + tax.getCredits()); //$NON-NLS-1$ //$NON-NLS-2$
    System.out.println("> total: " + aftertotal); //$NON-NLS-1$

    assertEquals(aftertotal, beforetotal);
  }
}
