package io.wouter.won.world;

import static java.util.Arrays.asList;

import io.wouter.lib.grid.Pos;
import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.object.structure.WonProducerTemplate;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.world.delegate.WonBufferDelegate;
import io.wouter.won.world.delegate.WonNodeDelegate;

public class TestCase1 extends TestCase {

  public TestCase1(final WonEnvironment env) {
    super(env);

    final WonItem ia = new WonItem(0, "A", 1);

    final WonProduction p_a = new WonProduction(0, "_->A", toCounter(), toCounter(ia), null, null, 1);
    final WonProduction pa_ = new WonProduction(1, "A->_", toCounter(ia), toCounter(), null, null, 1);

    final WonProducerTemplate tr = new WonProducerTemplate(0, "R", asList(), 1, 1, 0);
    final WonProducerTemplate tx = new WonProducerTemplate(1, "X", asList(p_a), 1, 1, 4);
    final WonProducerTemplate ty = new WonProducerTemplate(2, "Y", asList(pa_), 1, 1, 4);
    final WonTransporterTemplate tt = new WonTransporterTemplate(3, "T", 1, 1, 4);

    final WonConstruction cr = new WonConstruction(0, "cR", toCounter(), tr, 0);
    final WonConstruction cx = new WonConstruction(1, "cX", toCounter(), tx, 0);
    final WonConstruction cy = new WonConstruction(2, "cY", toCounter(), ty, 0);
    final WonConstruction ct = new WonConstruction(3, "cT", toCounter(), tt, 0);

    // build world

    env.getBuildModel().getPlayer().setHome(Pos.get(1, 1));
    final WonProducer<WonNodeDelegate, WonBufferDelegate> pr1 = buildProducer(cr);
    env.getBuildModel().getPlayer().setHome(Pos.get(8, 2));
    final WonProducer<WonNodeDelegate, WonBufferDelegate> px2 = buildProducer(cx);
    env.getBuildModel().getPlayer().setHome(Pos.get(7, 7));
    final WonProducer<WonNodeDelegate, WonBufferDelegate> py3 = buildProducer(cy);
    env.getBuildModel().getPlayer().setHome(Pos.get(2, 6));
    final WonTransporter<WonNodeDelegate, WonBufferDelegate> tt4 = buildTransporter(ct);
    env.getBuildModel().getPlayer().setHome(null);

    final WonAgent<WonNodeDelegate, WonBufferDelegate> aa5 = buildAgent(pr1);
    // final WonAgent<WonNodeDelegate, WonBufferDelegate> aa6 = buildAgent(pr1);

    getHandler(px2, "_->A").setEnabled(true); //$NON-NLS-1$
    getHandler(py3, "A->_").setEnabled(true); //$NON-NLS-1$

    getHandler(px2, "_->A").setActive(true); //$NON-NLS-1$
    getHandler(py3, "A->_").setActive(true); //$NON-NLS-1$
  }



  public static void main(final String[] args) {
    new WriterTest(new TestCase1(new WonEnvironment())).run();
  }
}
