Resources:

Water Fruit Fish Game
Potato Rice Cereal Cotton Flax
Firwood Hardwood
Stone Clay Coal Tar Oil
CopperOre TinOre IronOre SilverOre GoldOre

Materials:

Water Milk Wine Beer
Fruit Potato Rice Cereal Flour Bread Fish Game Meat
Cotton Flax Wool Skin Yarn Fabric
FwBranch HwBranch FwLog HwLog
FwStick HwStick FwPlank HwPlank
Stone Flintstone StoneSlab
Clay Brick Coal Tar Oil
CopperOre TinOre IronOre SilverOre GoldOre
CopperBar TinBar BronzeBar IronBar SteelBar SilverBar GoldBar

Tools:

Cloth Rope Net
Furnace Mold Pot
Millstone
Flail Bucket Barrel Reel Loom
Hammer Knife Axe Spear (Stone Bronze Iron Steel)
Chisel Pickaxe Plough Sickle Saw Shuffle (Bronze Iron Steel)