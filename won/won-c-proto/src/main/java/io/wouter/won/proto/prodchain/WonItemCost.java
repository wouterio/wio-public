package io.wouter.won.proto.prodchain;

import static java.lang.Double.NaN;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

import java.util.Map;
import java.util.TreeMap;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;

public class WonItemCost extends WonProtoObject {

  private final WonProtoItem item;
  private final Map<WonProtoProduction, Double> prodcosts = new TreeMap<>(CompareIndex);
  private final Map<WonProtoProduction, Double> uprodcosts = unmodifiableMap(prodcosts);



  private int price = -1;
  private int cost = -1;



  public WonItemCost(final WonProtoItem item) {
    super(item);
    this.item = requireNonNull(item);
  }



  public WonProtoItem getItem() {
    return item;
  }

  public Map<WonProtoProduction, Double> getProductionCosts() {
    return uprodcosts;
  }

  public int getPrice() {
    return price;
  }

  public int getCost() {
    return cost;
  }



  public void updateProductionCosts(final Map<WonProtoProduction, Double> newprodcosts) {
    final double newprice = newprodcosts.values().stream().mapToDouble(Double::doubleValue).average().orElse(NaN);
    final double newcost = newprice / item.getDurability();
    prodcosts.clear();
    prodcosts.putAll(newprodcosts);
    price = (int) newprice;
    cost = (int) newcost;
  }
}
