package io.wouter.won.proto.structure;

import static java.util.Objects.requireNonNull;

import io.wouter.won.proto.WonProtoStructure;
import io.wouter.won.proto.product.WonProtoConstruction;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProtoConstructor extends WonProtoStructure {

  private WonProtoConstruction construction;



  public WonProtoConstructor(final int index, final String type) {
    super(index, type);
  }



  public WonProtoConstructor(final int index, final String type, final WonProtoConstruction construction, final int minworkers, final int maxworkers, final int cap) {
    super(index, type, minworkers, maxworkers, cap);
    setConstruction(construction);
  }



  public WonProtoConstructor setConstruction(final WonProtoConstruction construction) {
    this.construction = requireNonNull(construction);
    return this;
  }



  public WonProtoConstruction getConstruction() {
    return construction;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("construction", construction, WonProtoConstruction::getType); //$NON-NLS-1$
  }
}
