package io.wouter.won.proto.product;

import static io.wouter.won.util.counter.Counters.newCounter;

import java.util.Optional;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.proto.WonProtoStructure;
import io.wouter.won.util.counter.CounterC;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProtoProduction extends WonProtoObject {

  private final CounterM<WonProtoItem> input = newCounter(CompareIndex);
  private final CounterM<WonProtoItem> output = newCounter(CompareIndex);

  private WonProtoResource resource = null;
  private WonProtoStructure structure = null;
  private int duration;
  private boolean includecost = true;



  public WonProtoProduction(final int index, final String type) {
    super(index, type);
  }



  public WonProtoProduction(final int index, final String type, final CounterM<WonProtoItem> input, final CounterM<WonProtoItem> output, final WonProtoResource resource, final WonProtoStructure structure, final int duration, final boolean includecost) {
    super(index, type);
    this.input.putAll(input);
    this.output.putAll(output);
    setResource(resource);
    setStructure(structure);
    setDuration(duration);
    setIncludeCost(includecost);
  }



  public WonProtoProduction setInput(final WonProtoItem item, final int amount) {
    input.ensure(item).set(amount);
    return this;
  }

  public WonProtoProduction setOutput(final WonProtoItem item, final int amount) {
    output.ensure(item).set(amount);
    return this;
  }

  public WonProtoProduction setResource(final WonProtoResource resource) {
    this.resource = resource;
    return this;
  }

  public WonProtoProduction setStructure(final WonProtoStructure structure) {
    this.structure = structure;
    return this;
  }

  public WonProtoProduction setDuration(final int duration) {
    this.duration = duration;
    return this;
  }

  public WonProtoProduction setIncludeCost(final boolean includecost) {
    this.includecost = includecost;
    return this;
  }



  public CounterC<WonProtoItem> getInput() {
    return input.getConst();
  }

  public CounterC<WonProtoItem> getOutput() {
    return output.getConst();
  }

  public Optional<WonProtoResource> getResource() {
    return Optional.ofNullable(resource);
  }

  public Optional<WonProtoStructure> getStructure() {
    return Optional.ofNullable(structure);
  }

  public int getDuration() {
    return duration;
  }

  public boolean shouldIncludeCost() {
    return includecost;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .appendAll("input", input.getKeys(), WonProtoItem::getType) //$NON-NLS-1$
        .appendAll("output", output.getKeys(), WonProtoItem::getType) //$NON-NLS-1$
        .appendNullable("resource", resource, WonProtoResource::getType) //$NON-NLS-1$
        .appendNullable("structure", structure, WonProtoStructure::getType) //$NON-NLS-1$
        .append("duration", duration) //$NON-NLS-1$
        .append("include cost", includecost); //$NON-NLS-1$
  }
}
