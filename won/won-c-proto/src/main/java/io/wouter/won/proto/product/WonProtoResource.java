package io.wouter.won.proto.product;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProtoResource extends WonProtoObject {

  private float extractftr;
  private float refillftr;



  public WonProtoResource(final int index, final String type) {
    super(index, type);
  }

  public WonProtoResource(final int index, final String type, final float extractftr, final float refillftr) {
    super(index, type);
    setExtractFactor(extractftr);
    setRefillFactor(refillftr);
  }



  public WonProtoResource setExtractFactor(final float extractftr) {
    this.extractftr = extractftr;
    return this;
  }

  public WonProtoResource setRefillFactor(final float refillftr) {
    this.refillftr = refillftr;
    return this;
  }



  public float getExtractFactor() {
    return extractftr;
  }

  public float getRefillFactor() {
    return refillftr;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("extractftr", extractftr) //$NON-NLS-1$
        .append("refillftr", refillftr); //$NON-NLS-1$
  }
}
