package io.wouter.won.proto.structure;

import static java.util.Collections.unmodifiableSet;
import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.proto.WonProtoStructure;
import io.wouter.won.proto.product.WonProtoProduction;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProtoProducer extends WonProtoStructure {

  private final Set<WonProtoProduction> productions = new TreeSet<>(WonProtoObject.CompareIndex);



  public WonProtoProducer(final int index, final String type) {
    super(index, type);
  }

  public WonProtoProducer(final int index, final String type, final List<WonProtoProduction> productions, final int minworkers, final int maxworkers, final int cap) {
    super(index, type, minworkers, maxworkers, cap);
    this.productions.addAll(productions);
  }



  public WonProtoProducer addProduction(final WonProtoProduction production) {
    productions.add(requireNonNull(production));
    return this;
  }



  public Set<WonProtoProduction> getProductions() {
    return unmodifiableSet(productions);
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .appendAll("productions", productions, WonProtoProduction::getType); //$NON-NLS-1$
  }
}
