package io.wouter.won.proto;

import static java.util.Objects.requireNonNull;

import java.util.Comparator;

import io.wouter.won.util.string.WonStringBuilder;

public abstract class WonProtoObject {

  public static final Comparator<WonProtoObject> CompareType = (a, b) -> a.getType().compareTo(b.getType());
  public static final Comparator<WonProtoObject> CompareIndex = (a, b) -> Integer.compare(a.getIndex(), b.getIndex());



  private final int index;
  private final String type;



  protected WonProtoObject(final int index, final String type) {
    this.index = index;
    this.type = requireNonNull(type);
  }

  protected WonProtoObject(final WonProtoObject reference) {
    this(reference.index, reference.type);
  }



  public final int getIndex() {
    return index;
  }

  public final String getType() {
    return type;
  }



  @Override
  public final String toString() {
    return toStringBuilder().toString();
  }

  protected WonStringBuilder toStringBuilder() {
    return new WonStringBuilder(getClass(), index, type);
  }



  @Override
  public final boolean equals(final Object o) {
    return super.equals(o);
  }

  @Override
  public final int hashCode() {
    return type.hashCode();
  }
}
