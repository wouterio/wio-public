package io.wouter.won.proto.prodchain.ui;

import static java.util.stream.Collectors.toList;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.proto.prodchain.WonItemCost;
import io.wouter.won.proto.prodchain.ui.binding.CellIntegerBinding;
import io.wouter.won.proto.prodchain.ui.binding.CellStringBinding;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.NumberStringConverter;

public final class WonItemCostTableView extends TableView<WonItemCost> {

  private static final String AlignLf = "-fx-alignment: CENTER-LEFT;"; //$NON-NLS-1$
  private static final String AlignRg = "-fx-alignment: CENTER-RIGHT;"; //$NON-NLS-1$



  public WonItemCostTableView() {
    super();

    setRowFactory(tv -> new TooltipTableRow<>(pc -> null, pc -> new ItemCostTooltipPane(pc)));


    final TableColumn<WonItemCost, String> typecol = new TableColumn<>("type");
    final TableColumn<WonItemCost, Number> durabilitycol = new TableColumn<>("durability");
    final TableColumn<WonItemCost, Number> pricecol = new TableColumn<>("price");
    final TableColumn<WonItemCost, Number> costcol = new TableColumn<>("cost");

    typecol.setMinWidth(320d);
    durabilitycol.setMinWidth(80d);
    pricecol.setMinWidth(80d);
    costcol.setMinWidth(80d);

    typecol.setSortable(false);
    durabilitycol.setSortable(false);
    pricecol.setSortable(false);
    costcol.setSortable(false);

    typecol.setStyle(AlignLf);
    durabilitycol.setStyle(AlignRg);
    pricecol.setStyle(AlignRg);
    costcol.setStyle(AlignRg);

    typecol.setCellValueFactory(param -> new CellStringBinding(() -> param.getValue().getItem().getType()));
    durabilitycol.setCellValueFactory(param -> new CellIntegerBinding(() -> param.getValue().getItem().getDurability()));
    pricecol.setCellValueFactory(param -> new CellIntegerBinding(() -> param.getValue().getPrice()));
    costcol.setCellValueFactory(param -> new CellIntegerBinding(() -> param.getValue().getCost()));

    durabilitycol.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
    durabilitycol.setOnEditCommit(e -> e.getRowValue().getItem().setDurability(e.getNewValue().intValue()));

    getColumns().addAll(typecol, durabilitycol, pricecol, costcol);
    setEditable(true);
    setFixedCellSize(40d);
  }



  //



  private static class ItemCostTooltipPane extends ListView<String> {

    ItemCostTooltipPane(final WonItemCost itemcost) {
      getItems().addAll(itemcost.getProductionCosts().keySet().stream().map(WonProtoObject::getType).collect(toList()));
    }
  }

}
