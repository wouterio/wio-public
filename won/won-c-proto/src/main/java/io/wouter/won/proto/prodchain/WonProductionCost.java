package io.wouter.won.proto.prodchain;

import static java.util.Objects.requireNonNull;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.proto.product.WonProtoProduction;

public class WonProductionCost extends WonProtoObject {

  private final WonProtoProduction prod;



  private int prodcost;
  private int itemprodcost;



  public WonProductionCost(final WonProtoProduction prod) {
    super(prod);
    this.prod = requireNonNull(prod);
  }



  public WonProtoProduction getProduction() {
    return prod;
  }

  public int getProductionCost() {
    return prodcost;
  }

  public int getItemProductionCost() {
    return itemprodcost;
  }



  public void updateProductionCost(final double newprodcost) {
    this.prodcost = (int) newprodcost;
    this.itemprodcost = (int) (newprodcost / prod.getOutput().total());
  }
}
