package io.wouter.won.proto;

import io.wouter.won.util.string.WonStringBuilder;

public abstract class WonProtoStructure extends WonProtoObject {

  private int minworkers;
  private int maxworkers;
  private int cap;



  protected WonProtoStructure(final int index, final String type) {
    super(index, type);
  }

  protected WonProtoStructure(final int index, final String type, final int minworkers, final int maxworkers, final int cap) {
    super(index, type);
    setMinWorkers(minworkers);
    setMaxWorkers(maxworkers);
    setCapacity(cap);
  }



  public int getMinWorkers() {
    return minworkers;
  }

  public int getMaxWorkers() {
    return maxworkers;
  }

  public int getCapacity() {
    return cap;
  }



  public void setMinWorkers(final int minworkers) {
    this.minworkers = minworkers;
  }

  public void setMaxWorkers(final int maxworkers) {
    this.maxworkers = maxworkers;
  }

  public void setCapacity(final int cap) {
    this.cap = cap;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("minworkers", minworkers) //$NON-NLS-1$
        .append("maxworkers", maxworkers) //$NON-NLS-1$
        .append("cap", cap); //$NON-NLS-1$
  }
}
