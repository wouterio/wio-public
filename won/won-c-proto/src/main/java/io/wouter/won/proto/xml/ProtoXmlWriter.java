package io.wouter.won.proto.xml;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import io.wouter.won.proto.product.WonProtoConstruction;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;
import io.wouter.won.proto.product.WonProtoResource;
import io.wouter.won.proto.structure.WonProtoConstructor;
import io.wouter.won.proto.structure.WonProtoDistributor;
import io.wouter.won.proto.structure.WonProtoProducer;
import io.wouter.won.proto.structure.WonProtoTransporter;
import io.wouter.won.util.counter.CounterC;

public class ProtoXmlWriter extends ProtoXmlBase {

  private final Path dir;



  public ProtoXmlWriter(final Path dir) {
    this.dir = requireNonNull(dir);
  }



  public void writeResources(final Stream<WonProtoResource> str) throws IOException {
    final Document doc = new Document();
    doc.setRootElement(new Element(Resources));

    str.forEach(r -> {
      final Element xi = new Element(Resource);
      // xi.setAttribute("index", Integer.toString(r.getIndex()));
      xi.addContent(new Element(Type).setText(r.getType()));
      xi.addContent(new Element(Extract).setText(Float.toString(r.getExtractFactor())));
      xi.addContent(new Element(Refill).setText(Float.toString(r.getRefillFactor())));
      doc.getRootElement().addContent(xi);
    });

    final XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    try (final OutputStream os = Files.newOutputStream(dir.resolve(ResourcesFile))) {
      xout.output(doc, os);
    }
  }



  public void writeItems(final Stream<WonProtoItem> str) throws IOException {
    final Document doc = new Document();
    doc.setRootElement(new Element(Items));

    str.forEach(i -> {
      final Element xi = new Element(Item);
      // xi.setAttribute("index", Integer.toString(i.getIndex()));
      xi.addContent(new Element(Type).setText(i.getType()));
      xi.addContent(new Element(Durability).setText(Integer.toString(i.getDurability())));
      xi.addContent(new Element(Size).setText(Integer.toString(i.getSize())));
      doc.getRootElement().addContent(xi);
    });

    final XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    try (final OutputStream os = Files.newOutputStream(dir.resolve(ItemsFile))) {
      xout.output(doc, os);
    }
  }



  public void writeConstructions(final Stream<WonProtoConstruction> str) throws IOException {
    final Document doc = new Document();
    doc.setRootElement(new Element(Constructions));

    str.forEach(c -> {
      final Element xi = new Element(Construction);
      // xi.setAttribute("index", Integer.toString(c.getIndex()));
      xi.addContent(new Element(Type).setText(c.getType()));
      xi.addContent(new Element(Inputs).addContent(buildItemElements(Input, c.getInput())));
      xi.addContent(new Element(Structure).setText(c.getStructure().getType()));
      xi.addContent(new Element(Duration).setText(Integer.toString(c.getDuration())));
      doc.getRootElement().addContent(xi);
    });

    final XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    try (final OutputStream os = Files.newOutputStream(dir.resolve(ConstructionsFile))) {
      xout.output(doc, os);
    }
  }



  public void writeProductions(final Stream<WonProtoProduction> str) throws IOException {
    final Document doc = new Document();
    doc.setRootElement(new Element(Productions));

    str.forEach(p -> {
      final Element xi = new Element(Production);
      // xi.setAttribute("index", Integer.toString(r.getIndex()));
      xi.addContent(new Element(Type).setText(p.getType()));
      xi.addContent(new Element(Inputs).addContent(buildItemElements(Input, p.getInput())));
      xi.addContent(new Element(Outputs).addContent(buildItemElements(Output, p.getOutput())));
      if (p.getResource().isPresent())
        xi.addContent(new Element(Resource).setText(p.getResource().get().getType()));
      if (p.getStructure().isPresent())
        xi.addContent(new Element(Structure).setText(p.getStructure().get().getType()));
      xi.addContent(new Element(Duration).setText(Integer.toString(p.getDuration())));
      xi.addContent(new Element(ComputeCost).setText(Boolean.toString(p.shouldIncludeCost())));
      doc.getRootElement().addContent(xi);
    });

    final XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    try (final OutputStream os = Files.newOutputStream(dir.resolve(ProductionsFile))) {
      xout.output(doc, os);
    }
  }



  public void writeConstructors(final Stream<WonProtoConstructor> str) throws IOException {
    final Document doc = new Document();
    doc.setRootElement(new Element(Constructors));

    str.forEach(c -> {
      final Element xi = new Element(Constructor);
      // xi.setAttribute("index", Integer.toString(r.getIndex()));
      xi.addContent(new Element(Type).setText(c.getType()));
      xi.addContent(new Element(MinWorkers).setText(Integer.toString(c.getMinWorkers())));
      xi.addContent(new Element(MaxWorkers).setText(Integer.toString(c.getMaxWorkers())));
      xi.addContent(new Element(Capacity).setText(Integer.toString(c.getCapacity())));
      xi.addContent(new Element(Construction).setText(c.getConstruction().getType()));
      doc.getRootElement().addContent(xi);
    });

    final XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    try (final OutputStream os = Files.newOutputStream(dir.resolve(ConstructorsFile))) {
      xout.output(doc, os);
    }
  }



  public void writeProducers(final Stream<WonProtoProducer> str) throws IOException {
    final Document doc = new Document();
    doc.setRootElement(new Element(Producers));

    str.forEach(p -> {
      final Element xi = new Element(Producer);
      // xi.setAttribute("index", Integer.toString(r.getIndex()));
      xi.addContent(new Element(Type).setText(p.getType()));
      xi.addContent(new Element(MinWorkers).setText(Integer.toString(p.getMinWorkers())));
      xi.addContent(new Element(MaxWorkers).setText(Integer.toString(p.getMaxWorkers())));
      xi.addContent(new Element(Capacity).setText(Integer.toString(p.getCapacity())));
      final Element prods = new Element(Productions);
      for (final WonProtoProduction pr : p.getProductions())
        prods.addContent(new Element(Production).setText(pr.getType()));
      xi.addContent(prods);
      doc.getRootElement().addContent(xi);
    });

    final XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    try (final OutputStream os = Files.newOutputStream(dir.resolve(ProducersFile))) {
      xout.output(doc, os);
    }
  }



  public void writeTransporters(final Stream<WonProtoTransporter> str) throws IOException {
    final Document doc = new Document();
    doc.setRootElement(new Element(Transporters));

    str.forEach(t -> {
      final Element xi = new Element(Transporter);
      // xi.setAttribute("index", Integer.toString(r.getIndex()));
      xi.addContent(new Element(Type).setText(t.getType()));
      xi.addContent(new Element(MinWorkers).setText(Integer.toString(t.getMinWorkers())));
      xi.addContent(new Element(MaxWorkers).setText(Integer.toString(t.getMaxWorkers())));
      xi.addContent(new Element(Capacity).setText(Integer.toString(t.getCapacity())));
      doc.getRootElement().addContent(xi);
    });

    final XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    try (final OutputStream os = Files.newOutputStream(dir.resolve(ProducersFile))) {
      xout.output(doc, os);
    }
  }


  public void writeDistributors(final Stream<WonProtoDistributor> str) throws IOException {
    final Document doc = new Document();
    doc.setRootElement(new Element(Distributors));

    str.forEach(d -> {
      final Element e = new Element(Distributor);
      // xi.setAttribute("index", Integer.toString(r.getIndex()));
      e.addContent(new Element(Type).setText(d.getType()));
      e.addContent(new Element(MinWorkers).setText(Integer.toString(d.getMinWorkers())));
      e.addContent(new Element(MaxWorkers).setText(Integer.toString(d.getMaxWorkers())));
      e.addContent(new Element(Capacity).setText(Integer.toString(d.getCapacity())));
      final Element eitems = new Element(Items);
      for (final WonProtoItem pr : d.getItems())
        eitems.addContent(new Element(Item).setText(pr.getType()));
      e.addContent(eitems);
      doc.getRootElement().addContent(e);
    });

    final XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
    try (final OutputStream os = Files.newOutputStream(dir.resolve(ProducersFile))) {
      xout.output(doc, os);
    }
  }



  private static Collection<Element> buildItemElements(final String substr, final CounterC<WonProtoItem> counter) {
    final Collection<Element> xitems = new ArrayList<>(counter.size());
    for (final WonProtoItem i : counter.getKeys()) {
      final Element xitem = new Element(substr);
      xitem.addContent(new Element(Type).setText(i.getType()));
      xitem.addContent(new Element(Amount).setText(Integer.toString(counter.get(i).count())));
      xitems.add(xitem);
    }
    return xitems;
  }
}
