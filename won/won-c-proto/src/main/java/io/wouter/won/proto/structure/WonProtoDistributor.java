package io.wouter.won.proto.structure;

import static java.util.Collections.unmodifiableSet;
import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.proto.WonProtoStructure;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.util.enums.LinkType;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProtoDistributor extends WonProtoStructure {

  private final Set<WonProtoItem> items = new TreeSet<>(WonProtoObject.CompareIndex);



  private LinkType pull;
  private LinkType push;



  public WonProtoDistributor(final int index, final String type) {
    super(index, type);
  }

  public WonProtoDistributor(final int index, final String type, final LinkType pull, final LinkType push, final Collection<WonProtoItem> items, final int minworkers, final int maxworkers, final int cap) {
    super(index, type, minworkers, maxworkers, cap);
    this.items.addAll(items);
    setLinkTypes(pull, push);
  }



  public WonProtoDistributor setLinkTypes(final LinkType pull, final LinkType push) {
    this.pull = requireNonNull(pull);
    this.push = requireNonNull(push);
    return this;
  }

  public WonProtoDistributor addItem(final WonProtoItem item) {
    items.add(requireNonNull(item));
    return this;
  }



  public LinkType getPullLinkType() {
    return pull;
  }

  public LinkType getPushLinkType() {
    return push;
  }

  public Set<WonProtoItem> getItems() {
    return unmodifiableSet(items);
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("pull", pull) //$NON-NLS-1$
        .append("push", push) //$NON-NLS-1$
        .appendAll("items", items, WonProtoItem::getType); //$NON-NLS-1$
  }
}
