package io.wouter.won.proto.product;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProtoItem extends WonProtoObject {

  private int durability = 0;
  private int size = 1;



  public WonProtoItem(final int index, final String type) {
    super(index, type);
  }



  public WonProtoItem(final int index, final String type, final int durability) {
    super(index, type);
    setDurability(durability);
  }



  public WonProtoItem setDurability(final int durability) {
    this.durability = durability;
    return this;
  }

  public WonProtoItem setSize(final int size) {
    this.size = size;
    return this;
  }



  public int getDurability() {
    return durability;
  }

  public int getSize() {
    return size;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("durability", durability) //$NON-NLS-1$
        .append("size", size); //$NON-NLS-1$
  }
}
