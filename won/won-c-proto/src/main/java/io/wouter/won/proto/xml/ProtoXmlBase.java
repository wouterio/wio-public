package io.wouter.won.proto.xml;


public abstract class ProtoXmlBase {

  protected static final String ResourcesFile = "resources.xml"; //$NON-NLS-1$
  protected static final String ItemsFile = "items.xml"; //$NON-NLS-1$
  protected static final String ProductionsFile = "productions.xml"; //$NON-NLS-1$
  protected static final String ConstructionsFile = "constructions.xml"; //$NON-NLS-1$
  protected static final String ProducersFile = "producers.xml"; //$NON-NLS-1$
  protected static final String ConstructorsFile = "constructors.xml"; //$NON-NLS-1$
  protected static final String TransportersFile = "transporters.xml"; //$NON-NLS-1$
  protected static final String DistributorsFile = "distributors.xml"; //$NON-NLS-1$

  protected static final String Type = "type"; //$NON-NLS-1$
  protected static final String Resources = "resources"; //$NON-NLS-1$
  protected static final String Resource = "resource"; //$NON-NLS-1$
  protected static final String Items = "items"; //$NON-NLS-1$
  protected static final String Item = "item"; //$NON-NLS-1$
  protected static final String Productions = "productions"; //$NON-NLS-1$
  protected static final String Production = "production"; //$NON-NLS-1$
  protected static final String Constructions = "constructions"; //$NON-NLS-1$
  protected static final String Construction = "construction"; //$NON-NLS-1$
  protected static final String Producers = "producers"; //$NON-NLS-1$
  protected static final String Producer = "producer"; //$NON-NLS-1$
  protected static final String Constructors = "constructors"; //$NON-NLS-1$
  protected static final String Constructor = "constructor"; //$NON-NLS-1$
  protected static final String Transporters = "transporters"; //$NON-NLS-1$
  protected static final String Transporter = "transporter"; //$NON-NLS-1$
  protected static final String Distributors = "distributors"; //$NON-NLS-1$
  protected static final String Distributor = "distributor"; //$NON-NLS-1$
  protected static final String Refill = "refill"; //$NON-NLS-1$
  protected static final String Extract = "extract"; //$NON-NLS-1$
  protected static final String Durability = "durability"; //$NON-NLS-1$
  protected static final String Size = "size"; //$NON-NLS-1$
  protected static final String Duration = "duration"; //$NON-NLS-1$
  protected static final String ComputeCost = "computecost"; //$NON-NLS-1$
  protected static final String Inputs = "inputs"; //$NON-NLS-1$
  protected static final String Input = "input"; //$NON-NLS-1$
  protected static final String Outputs = "outputs"; //$NON-NLS-1$
  protected static final String Output = "output"; //$NON-NLS-1$
  protected static final String Amount = "amount"; //$NON-NLS-1$
  protected static final String Structure = "structure"; //$NON-NLS-1$
  protected static final String MaxWorkers = "maxworkers"; //$NON-NLS-1$
  protected static final String MinWorkers = "minworkers"; //$NON-NLS-1$
  protected static final String Capacity = "capacity"; //$NON-NLS-1$
  protected static final String Pull = "pull"; //$NON-NLS-1$
  protected static final String Push = "push"; //$NON-NLS-1$



  protected ProtoXmlBase() {}
}
