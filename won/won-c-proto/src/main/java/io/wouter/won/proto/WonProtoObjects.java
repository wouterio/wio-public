package io.wouter.won.proto;

import static io.wouter.won.proto.WonProtoObject.CompareIndex;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntSupplier;

import io.wouter.won.proto.product.WonProtoConstruction;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;
import io.wouter.won.proto.product.WonProtoResource;
import io.wouter.won.proto.structure.WonProtoConstructor;
import io.wouter.won.proto.structure.WonProtoDistributor;
import io.wouter.won.proto.structure.WonProtoProducer;
import io.wouter.won.proto.structure.WonProtoTransporter;
import io.wouter.won.proto.xml.ProtoXmlReader;
import io.wouter.won.proto.xml.ProtoXmlWriter;

public final class WonProtoObjects {

  private static class Holder {

    static final WonProtoObjects Instance = new WonProtoObjects();
  }



  public static WonProtoResource newResource(final String type) throws IllegalStateException {
    return Holder.Instance.buildResource(type);
  }

  public static WonProtoItem newItem(final String type) throws IllegalStateException {
    return Holder.Instance.buildItem(type);
  }

  public static WonProtoConstruction newConstruction(final String type) throws IllegalStateException {
    return Holder.Instance.buildConstruction(type);
  }

  public static WonProtoProduction newProduction(final String type) throws IllegalStateException {
    return Holder.Instance.buildProduction(type);
  }

  public static WonProtoConstructor newConstructor(final String type) throws IllegalStateException {
    return Holder.Instance.buildConstructor(type);
  }

  public static WonProtoProducer newProducer(final String type) throws IllegalStateException {
    return Holder.Instance.buildProducer(type);
  }

  public static WonProtoTransporter newTransporter(final String type) throws IllegalStateException {
    return Holder.Instance.buildTransporter(type);
  }

  public static WonProtoDistributor newDistributor(final String type) throws IllegalStateException {
    return Holder.Instance.buildDistributor(type);
  }



  public static WonProtoResource getResource(final String type) {
    return Holder.Instance.resources.get(type);
  }

  public static WonProtoItem getItem(final String type) {
    return Holder.Instance.items.get(type);
  }

  public static WonProtoConstruction getConstruction(final String type) {
    return Holder.Instance.constructions.get(type);
  }

  public static WonProtoProduction getProduction(final String type) {
    return Holder.Instance.productions.get(type);
  }

  public static WonProtoConstructor getConstructor(final String type) {
    return Holder.Instance.constructors.get(type);
  }

  public static WonProtoProducer getProducer(final String type) {
    return Holder.Instance.producers.get(type);
  }

  public static WonProtoTransporter getTransporter(final String type) {
    return Holder.Instance.transporters.get(type);
  }

  public static WonProtoDistributor getDistributor(final String type) {
    return Holder.Instance.distributors.get(type);
  }



  public static Collection<WonProtoResource> getResources() {
    return Holder.Instance.resources.values().stream().sorted(CompareIndex).collect(toList());
  }

  public static Collection<WonProtoItem> getItems() {
    return Holder.Instance.items.values().stream().sorted(CompareIndex).collect(toList());
  }

  public static Collection<WonProtoConstruction> getConstructions() {
    return Holder.Instance.constructions.values().stream().sorted(CompareIndex).collect(toList());
  }

  public static Collection<WonProtoProduction> getProductions() {
    return Holder.Instance.productions.values().stream().sorted(CompareIndex).collect(toList());
  }

  public static Collection<WonProtoConstructor> getConstructors() {
    return Holder.Instance.constructors.values().stream().sorted(CompareIndex).collect(toList());
  }

  public static Collection<WonProtoProducer> getProducers() {
    return Holder.Instance.producers.values().stream().sorted(CompareIndex).collect(toList());
  }

  public static Collection<WonProtoTransporter> getTransporters() {
    return Holder.Instance.transporters.values().stream().sorted(CompareIndex).collect(toList());
  }

  public static Collection<WonProtoDistributor> getDistributors() {
    return Holder.Instance.distributors.values().stream().sorted(CompareIndex).collect(toList());
  }



  public static Map<WonProtoItem, Set<WonProtoProduction>> computeItemProductionOutputMap() {
    final Map<WonProtoItem, Set<WonProtoProduction>> map = new TreeMap<>(CompareIndex);
    getProductions().forEach(p -> {
      p.getOutput().getKeys().forEach(o -> {
        map.computeIfAbsent(o, ignored -> new TreeSet<>(CompareIndex)).add(p);
      });
    });
    return unmodifiableMap(map);
  }

  public static Map<WonProtoItem, Set<WonProtoProduction>> computeItemProductionInputMap() {
    final Map<WonProtoItem, Set<WonProtoProduction>> map = new TreeMap<>(CompareIndex);
    getProductions().forEach(p -> {
      p.getInput().getKeys().forEach(o -> {
        map.computeIfAbsent(o, ignored -> new TreeSet<>(CompareIndex)).add(p);
      });
    });
    return unmodifiableMap(map);
  }

  public static Map<WonProtoResource, Set<WonProtoProduction>> computeResourceProductionMap() {
    final Map<WonProtoResource, Set<WonProtoProduction>> map = new TreeMap<>(CompareIndex);
    getProductions().forEach(p -> {
      p.getResource().ifPresent(r -> {
        map.computeIfAbsent(r, ignored -> new TreeSet<>(CompareIndex)).add(p);
      });
    });
    return unmodifiableMap(map);
  }

  public static Map<WonProtoStructure, Set<WonProtoProduction>> computeStructureProductionMap() {
    final Map<WonProtoStructure, Set<WonProtoProduction>> map = new TreeMap<>(CompareIndex);
    getProductions().forEach(p -> {
      p.getStructure().ifPresent(st -> {
        map.computeIfAbsent(st, ignored -> new TreeSet<>(CompareIndex)).add(p);
      });
    });
    return unmodifiableMap(map);
  }

  public static Map<WonProtoItem, Set<WonProtoConstruction>> computeItemConstructionMap() {
    final Map<WonProtoItem, Set<WonProtoConstruction>> map = new TreeMap<>(CompareIndex);
    getConstructions().forEach(c -> {
      c.getInput().getKeys().forEach(o -> {
        map.computeIfAbsent(o, ignored -> new TreeSet<>(CompareIndex)).add(c);
      });
    });
    return unmodifiableMap(map);
  }

  public static Map<WonProtoStructure, Set<WonProtoConstruction>> computeStructureConstructionMap() {
    final Map<WonProtoStructure, Set<WonProtoConstruction>> map = new TreeMap<>(CompareIndex);
    getConstructions().forEach(c -> {
      map.computeIfAbsent(c.getStructure(), ignored -> new TreeSet<>(CompareIndex)).add(c);
    });
    return unmodifiableMap(map);
  }

  public static Map<WonProtoProduction, Set<WonProtoProducer>> computeProductionProducerMap() {
    final Map<WonProtoProduction, Set<WonProtoProducer>> map = new TreeMap<>(CompareIndex);
    getProducers().forEach(p -> {
      p.getProductions().forEach(o -> {
        map.computeIfAbsent(o, ignored -> new TreeSet<>(CompareIndex)).add(p);
      });
    });
    return unmodifiableMap(map);
  }

  public static Map<WonProtoConstruction, Set<WonProtoConstructor>> computeConstructionConstructorMap() {
    final Map<WonProtoConstruction, Set<WonProtoConstructor>> map = new TreeMap<>(CompareIndex);
    getConstructors().forEach(c -> {
      map.computeIfAbsent(c.getConstruction(), ignored -> new TreeSet<>(CompareIndex)).add(c);
    });
    return unmodifiableMap(map);
  }



  public static WonProtoObjects clear() {
    final WonProtoObjects objects = Holder.Instance;
    objects.resources.clear();
    objects.items.clear();
    objects.constructions.clear();
    objects.productions.clear();
    objects.constructors.clear();
    objects.producers.clear();
    objects.transporters.clear();
    objects.distributors.clear();
    return objects;
  }

  public static WonProtoObjects load(final Path dir) throws IOException {
    final WonProtoObjects objects = Holder.Instance;
    final ProtoXmlReader reader = new ProtoXmlReader(dir, objects.indexer);
    objects.resources.putAll(reader.getResources());
    objects.items.putAll(reader.getItems());
    objects.constructions.putAll(reader.getConstructions());
    objects.productions.putAll(reader.getProductions());
    objects.constructors.putAll(reader.getConstructors());
    objects.producers.putAll(reader.getProducers());
    objects.transporters.putAll(reader.getTransporters());
    objects.distributors.putAll(reader.getDistributors());
    return objects;
  }

  public static WonProtoObjects save(final Path dir) throws IOException {
    final WonProtoObjects objects = Holder.Instance;
    final ProtoXmlWriter writer = new ProtoXmlWriter(dir);
    writer.writeResources(getResources().stream());
    writer.writeItems(getItems().stream());
    writer.writeConstructions(getConstructions().stream());
    writer.writeProductions(getProductions().stream());
    writer.writeConstructors(getConstructors().stream());
    writer.writeProducers(getProducers().stream());
    writer.writeTransporters(getTransporters().stream());
    writer.writeDistributors(getDistributors().stream());
    return objects;
  }



  private final AtomicInteger index = new AtomicInteger();
  private final IntSupplier indexer = index::incrementAndGet;

  private final Map<String, WonProtoResource> resources = new HashMap<>();
  private final Map<String, WonProtoItem> items = new HashMap<>();
  private final Map<String, WonProtoConstruction> constructions = new HashMap<>();
  private final Map<String, WonProtoProduction> productions = new HashMap<>();
  private final Map<String, WonProtoConstructor> constructors = new HashMap<>();
  private final Map<String, WonProtoProducer> producers = new HashMap<>();
  private final Map<String, WonProtoTransporter> transporters = new HashMap<>();
  private final Map<String, WonProtoDistributor> distributors = new HashMap<>();
  private final Set<String> types = new HashSet<>();



  WonProtoObjects() {}



  private WonProtoResource buildResource(final String type) throws IllegalStateException {
    checkType(type);
    final WonProtoResource r = new WonProtoResource(indexer.getAsInt(), type);
    resources.put(type, r);
    return r;
  }

  private WonProtoItem buildItem(final String type) throws IllegalStateException {
    checkType(type);
    final WonProtoItem i = new WonProtoItem(indexer.getAsInt(), type);
    items.put(type, i);
    return i;
  }

  private WonProtoConstruction buildConstruction(final String type) throws IllegalStateException {
    checkType(type);
    final WonProtoConstruction c = new WonProtoConstruction(indexer.getAsInt(), type);
    constructions.put(type, c);
    return c;
  }

  private WonProtoProduction buildProduction(final String type) throws IllegalStateException {
    checkType(type);
    final WonProtoProduction p = new WonProtoProduction(indexer.getAsInt(), type);
    productions.put(type, p);
    return p;
  }

  private WonProtoConstructor buildConstructor(final String type) throws IllegalStateException {
    checkType(type);
    final WonProtoConstructor t = new WonProtoConstructor(indexer.getAsInt(), type);
    constructors.put(type, t);
    return t;
  }

  private WonProtoProducer buildProducer(final String type) throws IllegalStateException {
    checkType(type);
    final WonProtoProducer p = new WonProtoProducer(indexer.getAsInt(), type);
    producers.put(type, p);
    return p;
  }

  private WonProtoTransporter buildTransporter(final String type) throws IllegalStateException {
    checkType(type);
    final WonProtoTransporter t = new WonProtoTransporter(indexer.getAsInt(), type);
    transporters.put(type, t);
    return t;
  }

  private WonProtoDistributor buildDistributor(final String type) throws IllegalStateException {
    checkType(type);
    final WonProtoDistributor d = new WonProtoDistributor(indexer.getAsInt(), type);
    distributors.put(type, d);
    return d;
  }



  private void checkType(final String type) throws IllegalStateException {
    if (isNull(type) || type.isEmpty())
      throw new IllegalArgumentException("Type should be non-null and non-empty."); //$NON-NLS-1$
    if (!types.add(type))
      throw new IllegalStateException(type + " already exists."); //$NON-NLS-1$
  }
}
