package io.wouter.won.proto.structure;

import io.wouter.won.proto.WonProtoStructure;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProtoTransporter extends WonProtoStructure {

  public WonProtoTransporter(final int index, final String type) {
    super(index, type);
  }

  public WonProtoTransporter(final int index, final String type, final int minworkers, final int maxworkers, final int cap) {
    super(index, type, minworkers, maxworkers, cap);
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder(); //
  }
}
