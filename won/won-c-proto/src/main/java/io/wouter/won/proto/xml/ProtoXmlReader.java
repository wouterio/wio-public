package io.wouter.won.proto.xml;

import static io.wouter.won.proto.WonProtoObject.CompareIndex;
import static io.wouter.won.util.counter.Counters.newCounter;
import static java.lang.Boolean.parseBoolean;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import static java.nio.file.Files.newInputStream;
import static java.util.Collections.unmodifiableMap;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.IntSupplier;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import io.wouter.won.proto.WonProtoStructure;
import io.wouter.won.proto.product.WonProtoConstruction;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;
import io.wouter.won.proto.product.WonProtoResource;
import io.wouter.won.proto.structure.WonProtoConstructor;
import io.wouter.won.proto.structure.WonProtoDistributor;
import io.wouter.won.proto.structure.WonProtoProducer;
import io.wouter.won.proto.structure.WonProtoTransporter;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.enums.LinkType;

public final class ProtoXmlReader extends ProtoXmlBase {

  private final IntSupplier indexer;

  private final Element eresources;
  private final Element eitems;
  private final Element econstructions;
  private final Element eproductions;
  private final Element econstructors;
  private final Element eproducers;
  private final Element etransporters;
  private final Element edistributors;

  private final Map<String, WonProtoResource> resources = new HashMap<>();
  private final Map<String, WonProtoItem> items = new HashMap<>();
  private final Map<String, WonProtoConstruction> constructions = new HashMap<>();
  private final Map<String, WonProtoProduction> productions = new HashMap<>();
  private final Map<String, WonProtoConstructor> constructors = new HashMap<>();
  private final Map<String, WonProtoProducer> producers = new HashMap<>();
  private final Map<String, WonProtoTransporter> transporters = new HashMap<>();
  private final Map<String, WonProtoDistributor> distributors = new HashMap<>();



  public ProtoXmlReader(final Path dir, final IntSupplier indexer) throws IOException {
    this.indexer = indexer;

    eresources = root(dir.resolve(ResourcesFile));
    eitems = root(dir.resolve(ItemsFile));
    econstructions = root(dir.resolve(ConstructionsFile));
    eproductions = root(dir.resolve(ProductionsFile));
    econstructors = root(dir.resolve(ConstructorsFile));
    eproducers = root(dir.resolve(ProducersFile));
    etransporters = root(dir.resolve(TransportersFile));
    edistributors = root(dir.resolve(DistributorsFile));

    // XXX in this order
    eresources.getChildren().forEach(e -> ensure(e, this::readResource, resources));
    eitems.getChildren().forEach(e -> ensure(e, this::readItem, items));
    etransporters.getChildren().forEach(e -> ensure(e, this::readTransporter, transporters));
    edistributors.getChildren().forEach(e -> ensure(e, this::readDistributor, distributors));
    eproductions.getChildren().forEach(e -> ensure(e, this::readProduction, productions));
    eproducers.getChildren().forEach(e -> ensure(e, this::readProducer, producers));
    econstructions.getChildren().forEach(e -> ensure(e, this::readConstruction, constructions));
    econstructors.getChildren().forEach(e -> ensure(e, this::readConstructor, constructors));
  }



  public Map<String, WonProtoResource> getResources() {
    return unmodifiableMap(resources);
  }

  public Map<String, WonProtoItem> getItems() {
    return unmodifiableMap(items);
  }

  public Map<String, WonProtoConstruction> getConstructions() {
    return unmodifiableMap(constructions);
  }

  public Map<String, WonProtoProduction> getProductions() {
    return unmodifiableMap(productions);
  }

  public Map<String, WonProtoConstructor> getConstructors() {
    return unmodifiableMap(constructors);
  }

  public Map<String, WonProtoProducer> getProducers() {
    return unmodifiableMap(producers);
  }

  public Map<String, WonProtoTransporter> getTransporters() {
    return unmodifiableMap(transporters);
  }

  public Map<String, WonProtoDistributor> getDistributors() {
    return unmodifiableMap(distributors);
  }



  private WonProtoResource readResource(final Element e) {
    final String type = readType(e);
    final float extractftr = readFloat(e, Extract);
    final float refillftr = readFloat(e, Refill);
    return new WonProtoResource(indexer.getAsInt(), type, extractftr, refillftr);
  }



  private WonProtoItem readItem(final Element e) {
    final String type = readType(e);
    final int durability = readInt(e, Durability);
    return new WonProtoItem(indexer.getAsInt(), type, durability);
  }



  private WonProtoConstruction readConstruction(final Element e) {
    final String type = readType(e);
    final String structuretype = e.getChildTextTrim(Structure);
    final WonProtoStructure structure = ensureStructure(structuretype);
    final int index = structure.getIndex();

    final CounterM<WonProtoItem> materials = newCounter(CompareIndex);
    for (final Element em : e.getChild(Inputs).getChildren(Input)) {
      final String mtype = readType(em);
      final WonProtoItem m = ensureItem(mtype);
      final int amount = readInt(em, Amount);
      materials.ensure(m).set(amount);
    }

    final int duration = readInt(e, Duration);
    return new WonProtoConstruction(index, type, materials, structure, duration);
  }



  private WonProtoProduction readProduction(final Element e) {
    final String type = readType(e);

    final CounterM<WonProtoItem> input = newCounter(CompareIndex);
    for (final Element ei : e.getChild(Inputs).getChildren(Input)) {
      final String itype = readType(ei);
      final WonProtoItem i = ensureItem(itype);
      final int amount = readInt(ei, Amount);
      input.ensure(i).set(amount);
    }

    final CounterM<WonProtoItem> output = newCounter(CompareIndex);
    for (final Element eo : e.getChild(Outputs).getChildren(Output)) {
      final String otype = readType(eo);
      final WonProtoItem o = ensureItem(otype);
      final int amount = readInt(eo, Amount);
      output.ensure(o).set(amount);
    }

    final String resourcetype = e.getChildTextTrim(Resource);
    final WonProtoResource resource = resourcetype != null ?
        ensureResource(resourcetype) :
        null;

    final String structuretype = e.getChildTextTrim(Structure);
    final WonProtoStructure structure = structuretype != null ?
        ensureStructure(structuretype) :
        null;

    final int duration = readInt(e, Duration);
    final String includecoststr = e.getChildTextTrim(ComputeCost);
    final boolean includecost = includecoststr == null || parseBoolean(includecoststr);
    return new WonProtoProduction(indexer.getAsInt(), type, input, output, resource, structure, duration, includecost);
  }



  private WonProtoConstructor readConstructor(final Element e) {
    final String type = readType(e);
    final String ctype = e.getChildTextTrim(Construction);
    final WonProtoConstruction c = ensureConstruction(ctype);
    final int index = c.getIndex();

    final int cap = readInt(e, Capacity);
    final int minworkers = readInt(e, MinWorkers);
    final int maxworkers = readInt(e, MaxWorkers);
    return new WonProtoConstructor(index, type, c, minworkers, maxworkers, cap);
  }



  private WonProtoProducer readProducer(final Element e) {
    final String type = readType(e);

    final List<WonProtoProduction> plist = new LinkedList<>();
    for (final Element ep : e.getChild(Productions).getChildren(Production)) {
      final String ptype = ep.getTextTrim();
      final WonProtoProduction p = ensureProduction(ptype);
      plist.add(p);
    }

    final int cap = readInt(e, Capacity);
    final int minworkers = readInt(e, MinWorkers);
    final int maxworkers = readInt(e, MaxWorkers);
    return new WonProtoProducer(indexer.getAsInt(), type, plist, minworkers, maxworkers, cap);
  }



  private WonProtoTransporter readTransporter(final Element e) {
    final String type = readType(e);

    final int cap = readInt(e, Capacity);
    final int minworkers = readInt(e, MinWorkers);
    final int maxworkers = readInt(e, MaxWorkers);
    return new WonProtoTransporter(indexer.getAsInt(), type, minworkers, maxworkers, cap);
  }



  private WonProtoDistributor readDistributor(final Element e) {
    final String type = readType(e);
    final LinkType pull = readLinkType(e, Pull);
    final LinkType push = readLinkType(e, Push);

    final List<WonProtoItem> ilist = new LinkedList<>();
    for (final Element ei : e.getChild(Items).getChildren(Item)) {
      final String itype = ei.getTextTrim();
      final WonProtoItem i = ensureItem(itype);
      ilist.add(i);
    }

    final int cap = readInt(e, Capacity);
    final int minworkers = readInt(e, MinWorkers);
    final int maxworkers = readInt(e, MaxWorkers);
    return new WonProtoDistributor(indexer.getAsInt(), type, pull, push, ilist, minworkers, maxworkers, cap);
  }



  private WonProtoResource ensureResource(final String type) {
    return ensure(eresources, type, this::readResource, resources);
  }

  private WonProtoItem ensureItem(final String type) {
    return ensure(eitems, type, this::readItem, items);
  }

  private WonProtoConstruction ensureConstruction(final String type) {
    return ensure(econstructions, type, this::readConstruction, constructions);
  }

  private WonProtoProduction ensureProduction(final String type) {
    return ensure(eproductions, type, this::readProduction, productions);
  }

  private WonProtoStructure ensureStructure(final String type) {
    if (transporters.containsKey(type))
      return transporters.get(type);
    if (distributors.containsKey(type))
      return distributors.get(type);
    if (producers.containsKey(type))
      return producers.get(type);
    if (constructors.containsKey(type))
      return constructors.get(type);

    Optional<Element> oe = findChild(etransporters, type);
    if (oe.isPresent())
      return ensure(oe.get(), this::readTransporter, transporters);

    oe = findChild(edistributors, type);
    if (oe.isPresent())
      return ensure(oe.get(), this::readDistributor, distributors);

    oe = findChild(eproducers, type);
    if (oe.isPresent())
      return ensure(oe.get(), this::readProducer, producers);

    oe = findChild(econstructors, type);
    if (oe.isPresent())
      return ensure(oe.get(), this::readConstructor, constructors);

    throw new NoSuchElementException(type);
  }



  private static <T> T ensure(final Element e, final Function<Element, T> reader, final Map<String, T> map) {
    final String type = readType(e);
    return map.computeIfAbsent(type, t -> reader.apply(e));
  }

  private static <T> T ensure(final Element parent, final String type, final Function<Element, T> reader, final Map<String, T> map) {
    return map.computeIfAbsent(type, t -> {
      final Optional<Element> oe = findChild(parent, type);
      if (oe.isPresent())
        return reader.apply(oe.get());
      throw new NoSuchElementException(type);
    });
  }



  private static String readType(final Element e) {
    return e.getChildTextTrim(Type);
  }

  private static int readInt(final Element e, final String key) {
    return parseInt(e.getChildTextTrim(key));
  }

  private static float readFloat(final Element e, final String key) {
    return parseFloat(e.getChildTextTrim(key));
  }

  private static LinkType readLinkType(final Element e, final String key) {
    return LinkType.valueOf(e.getChildTextTrim(key));
  }



  private static Optional<Element> findChild(final Element parent, final String type) {
    return parent.getChildren().stream().filter(c -> readType(c).equals(type)).findAny();
  }



  private static Element root(final Path file) throws IOException {
    try (InputStream is = newInputStream(file)) {
      return new SAXBuilder().build(is).getDocument().getRootElement();

    } catch (final JDOMException e) {
      throw new IOException(e);
    }
  }
}
