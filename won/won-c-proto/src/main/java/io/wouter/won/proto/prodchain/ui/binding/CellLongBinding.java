package io.wouter.won.proto.prodchain.ui.binding;

import java.util.function.LongSupplier;

import javafx.beans.binding.LongBinding;

public class CellLongBinding extends LongBinding {

  private final LongSupplier get;



  public CellLongBinding(final LongSupplier get) {
    this.get = get;
  }



  @Override
  protected long computeValue() {
    return get.getAsLong();
  }
}
