package io.wouter.won.proto.prodchain.ui.binding;

import java.util.function.IntSupplier;

import javafx.beans.binding.IntegerBinding;

public class CellIntegerBinding extends IntegerBinding {

  private final IntSupplier get;



  public CellIntegerBinding(final IntSupplier get) {
    this.get = get;
  }



  @Override
  protected int computeValue() {
    return get.getAsInt();
  }
}
