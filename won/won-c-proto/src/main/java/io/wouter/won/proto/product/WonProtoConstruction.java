package io.wouter.won.proto.product;

import java.util.Objects;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.proto.WonProtoStructure;
import io.wouter.won.util.counter.CounterC;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.counter.Counters;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProtoConstruction extends WonProtoObject {

  private final CounterM<WonProtoItem> input = Counters.newCounter(CompareIndex);



  private WonProtoStructure structure = null;
  private int duration;



  public WonProtoConstruction(final int index, final String type) {
    super(index, type);
  }



  public WonProtoConstruction(final int index, final String type, final CounterM<WonProtoItem> input, final WonProtoStructure structure, final int duration) {
    super(index, type);
    this.input.putAll(input);
    setStructure(structure);
    setDuration(duration);
  }



  public WonProtoConstruction addMaterial(final WonProtoItem item) {
    return addMaterial(item, item.getDurability());
  }

  public WonProtoConstruction addMaterial(final WonProtoItem item, final int amount) {
    input.ensure(item).incr(amount);
    return this;
  }

  public WonProtoConstruction setStructure(final WonProtoStructure structure) {
    this.structure = Objects.requireNonNull(structure);
    return this;
  }

  public WonProtoConstruction setDuration(final int duration) {
    this.duration = duration;
    return this;
  }



  public CounterC<WonProtoItem> getInput() {
    return input.getConst();
  }

  public WonProtoStructure getStructure() {
    return structure;
  }

  public int getDuration() {
    return duration;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .appendAll("input", input.getKeys(), WonProtoItem::getType) //
        .append("structure", structure, WonProtoStructure::getType) //
        .append("duration", duration); //
  }
}
