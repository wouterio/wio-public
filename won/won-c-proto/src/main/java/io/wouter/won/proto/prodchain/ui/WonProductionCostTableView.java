package io.wouter.won.proto.prodchain.ui;

import io.wouter.won.proto.prodchain.WonProductionCost;
import io.wouter.won.proto.prodchain.ui.binding.CellIntegerBinding;
import io.wouter.won.proto.prodchain.ui.binding.CellStringBinding;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;
import io.wouter.won.util.counter.CounterC.CountC;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.NumberStringConverter;

public final class WonProductionCostTableView extends TableView<WonProductionCost> {

  private static final String AlignLf = "-fx-alignment: CENTER-LEFT;"; //$NON-NLS-1$
  private static final String AlignRg = "-fx-alignment: CENTER-RIGHT;"; //$NON-NLS-1$

  private static final NumberStringConverter Conv = new NumberStringConverter();



  public WonProductionCostTableView() {
    super();

    setRowFactory(tv -> new TooltipTableRow<>(pc -> null, pc -> new ProductionTooltipPane(pc.getProduction())));

    final TableColumn<WonProductionCost, String> typecol = new TableColumn<>("type");
    final TableColumn<WonProductionCost, Number> durationcol = new TableColumn<>("duration");
    final TableColumn<WonProductionCost, Number> prodcostcol = new TableColumn<>("prod cost");
    final TableColumn<WonProductionCost, Number> itemprodcostcol = new TableColumn<>("per item");

    typecol.setMinWidth(320d);
    durationcol.setMinWidth(80d);
    prodcostcol.setMinWidth(80d);
    itemprodcostcol.setMinWidth(80d);

    typecol.setSortable(false);
    durationcol.setSortable(false);
    prodcostcol.setSortable(false);
    itemprodcostcol.setSortable(false);

    typecol.setStyle(AlignLf);
    durationcol.setStyle(AlignRg);
    prodcostcol.setStyle(AlignRg);
    itemprodcostcol.setStyle(AlignRg);

    typecol.setCellValueFactory(param -> new CellStringBinding(() -> param.getValue().getProduction().getType()));
    durationcol.setCellValueFactory(param -> new CellIntegerBinding(() -> param.getValue().getProduction().getDuration()));
    prodcostcol.setCellValueFactory(param -> new CellIntegerBinding(() -> param.getValue().getProductionCost()));
    itemprodcostcol.setCellValueFactory(param -> new CellIntegerBinding(() -> param.getValue().getItemProductionCost()));

    durationcol.setCellFactory(TextFieldTableCell.forTableColumn(Conv));
    durationcol.setOnEditCommit(e -> e.getRowValue().getProduction().setDuration(e.getNewValue().intValue()));

    getColumns().addAll(typecol, durationcol, prodcostcol, itemprodcostcol);
    setEditable(true);
    setFixedCellSize(40d);
  }



  //



  private static class ProductionTooltipPane extends SplitPane {

    ProductionTooltipPane(final WonProtoProduction prod) {
      final WonInputOutputTableView inputview = new WonInputOutputTableView();
      final WonInputOutputTableView outputview = new WonInputOutputTableView();
      inputview.getItems().addAll(prod.getInput().getConstValues());
      outputview.getItems().addAll(prod.getOutput().getConstValues());
      getItems().addAll(inputview, outputview);
    }
  }



  //



  private static class WonInputOutputTableView extends TableView<CountC<WonProtoItem>> {

    WonInputOutputTableView() {
      super();

      final TableColumn<CountC<WonProtoItem>, String> typecol = new TableColumn<>("type");
      final TableColumn<CountC<WonProtoItem>, Number> amountcol = new TableColumn<>("amount");

      typecol.setSortable(false);
      amountcol.setSortable(false);

      typecol.setStyle(AlignLf);
      amountcol.setStyle(AlignRg);

      typecol.setCellValueFactory(param -> new CellStringBinding(() -> param.getValue().key().getType()));
      amountcol.setCellValueFactory(param -> new CellIntegerBinding(() -> param.getValue().count()));

      getColumns().addAll(typecol, amountcol);
      setEditable(false);
      setFixedCellSize(40d);
    }
  }
}
