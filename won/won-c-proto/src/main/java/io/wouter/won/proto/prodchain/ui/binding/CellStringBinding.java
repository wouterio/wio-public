package io.wouter.won.proto.prodchain.ui.binding;

import java.util.function.Supplier;

import javafx.beans.binding.StringBinding;

public class CellStringBinding extends StringBinding {

  private final Supplier<String> get;



  public CellStringBinding(final Supplier<String> get) {
    this.get = get;
  }



  @Override
  protected String computeValue() {
    return get.get();
  }
}
