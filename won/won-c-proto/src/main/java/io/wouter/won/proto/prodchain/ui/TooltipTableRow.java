package io.wouter.won.proto.prodchain.ui;

import static java.util.Objects.requireNonNull;

import java.util.function.Function;

import javafx.scene.Node;
import javafx.scene.control.TableRow;
import javafx.scene.control.Tooltip;

public final class TooltipTableRow<T> extends TableRow<T> {

  private final Tooltip tooltip = new Tooltip();
  private final Function<T, String> gettext;
  private final Function<T, Node> getnode;



  public TooltipTableRow(final Function<T, String> gettext, final Function<T, Node> getnode) {
    this.gettext = requireNonNull(gettext);
    this.getnode = requireNonNull(getnode);
  }



  @Override
  public void updateItem(final T row, final boolean empty) {
    super.updateItem(row, empty);

    if (empty || row == null) {
      tooltip.setText(null);
      tooltip.setGraphic(null);
      setTooltip(null);

    } else {
      final String text = gettext.apply(row);
      final Node node = getnode.apply(row);
      tooltip.setText(text);
      tooltip.setGraphic(node);
      setTooltip(tooltip);
    }
  }
}
