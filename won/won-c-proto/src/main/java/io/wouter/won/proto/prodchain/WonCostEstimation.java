package io.wouter.won.proto.prodchain;

import static io.wouter.won.proto.WonProtoObject.CompareIndex;
import static io.wouter.won.proto.WonProtoObjects.getProductions;
import static java.lang.Double.valueOf;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableMap;

import java.util.Map;
import java.util.TreeMap;

import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;

public final class WonCostEstimation {

  public static Map<WonProtoItem, Map<WonProtoProduction, Double>> estimateCosts() {
    final Map<WonProtoItem, Map<WonProtoProduction, Double>> itemprodcostmap1 = new TreeMap<>(CompareIndex);
    final Map<WonProtoItem, Map<WonProtoProduction, Double>> itemprodcostmap2 = new TreeMap<>(CompareIndex);
    for (int i = 0; i < 100; ++i) {
      getProductions().stream().filter(WonProtoProduction::shouldIncludeCost).forEach(p -> estimateCost(p, itemprodcostmap1, itemprodcostmap2));
      getProductions().stream().filter(WonProtoProduction::shouldIncludeCost).forEach(p -> estimateCost(p, itemprodcostmap2, itemprodcostmap1));
    }
    return unmodifiableMap(itemprodcostmap1);
  }



  private static void estimateCost(final WonProtoProduction p, final Map<WonProtoItem, Map<WonProtoProduction, Double>> itemprodcostmapin, final Map<WonProtoItem, Map<WonProtoProduction, Double>> itemprodcostmapout) {
    double prodcost = p.getDuration();
    for (final WonProtoItem i : p.getInput().getKeys())
      prodcost += itemprodcostmapin.getOrDefault(i, emptyMap()).values().stream().mapToDouble(Double::doubleValue).map(itemprodcost -> p.getInput().get(i).count() * itemprodcost).average().orElse(0d) / i.getDurability();

    final double itemprodcost = prodcost / p.getOutput().total();
    final Double itemprodcostobj = valueOf(itemprodcost);
    for (final WonProtoItem o : p.getOutput().getKeys())
      itemprodcostmapout.computeIfAbsent(o, ignored -> new TreeMap<>(CompareIndex)).put(p, itemprodcostobj);
  }



  private WonCostEstimation() {
    throw new AssertionError("static class."); //$NON-NLS-1$
  }
}
