package io.wouter.won.proto.prodchain;

import static io.wouter.won.proto.WonProtoObjects.computeConstructionConstructorMap;
import static io.wouter.won.proto.WonProtoObjects.computeItemConstructionMap;
import static io.wouter.won.proto.WonProtoObjects.computeItemProductionInputMap;
import static io.wouter.won.proto.WonProtoObjects.computeItemProductionOutputMap;
import static io.wouter.won.proto.WonProtoObjects.computeProductionProducerMap;
import static io.wouter.won.proto.WonProtoObjects.computeResourceProductionMap;
import static io.wouter.won.proto.WonProtoObjects.computeStructureConstructionMap;
import static io.wouter.won.proto.WonProtoObjects.computeStructureProductionMap;
import static io.wouter.won.proto.WonProtoObjects.getConstructions;
import static io.wouter.won.proto.WonProtoObjects.getDistributors;
import static io.wouter.won.proto.WonProtoObjects.getItems;
import static io.wouter.won.proto.WonProtoObjects.getProducers;
import static io.wouter.won.proto.WonProtoObjects.getProductions;
import static io.wouter.won.proto.WonProtoObjects.getResources;
import static io.wouter.won.proto.WonProtoObjects.getTransporters;
import static io.wouter.won.util.string.WonStringBuilder.mapCollection;
import static java.util.Collections.emptySet;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.wouter.won.proto.WonProtoObject;
import io.wouter.won.proto.WonProtoObjects;
import io.wouter.won.proto.WonProtoStructure;
import io.wouter.won.proto.product.WonProtoConstruction;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;
import io.wouter.won.proto.product.WonProtoResource;
import io.wouter.won.proto.structure.WonProtoConstructor;
import io.wouter.won.proto.structure.WonProtoProducer;

public class WonProductionChainTest implements Runnable {

  private final Map<WonProtoItem, Set<WonProtoProduction>> itemproductionoutputmap = computeItemProductionOutputMap();
  private final Map<WonProtoItem, Set<WonProtoProduction>> itemproductioninputmap = computeItemProductionInputMap();
  private final Map<WonProtoResource, Set<WonProtoProduction>> recourceproductionmap = computeResourceProductionMap();
  private final Map<WonProtoStructure, Set<WonProtoProduction>> structureproductionmap = computeStructureProductionMap();
  private final Map<WonProtoItem, Set<WonProtoConstruction>> itemconstructionmap = computeItemConstructionMap();
  private final Map<WonProtoStructure, Set<WonProtoConstruction>> structureconstructionmap = computeStructureConstructionMap();
  private final Map<WonProtoProduction, Set<WonProtoProducer>> productionproducermap = computeProductionProducerMap();
  private final Map<WonProtoConstruction, Set<WonProtoConstructor>> constructionconstructormap = computeConstructionConstructorMap();
  private final PrintStream print;



  private WonProductionChainTest() throws IOException {
    print = new PrintStream(Files.newOutputStream(Paths.get("prodchain.txt")), true); //$NON-NLS-1$
  }



  @Override
  public void run() {
    checkItemsNoOutputProduction();
    checkItemsNoInputProductionOrConstuction();
    checkResourcesNoProduction();
    checkStructuresNoProductionOrConstuction();
    checkProductionsNoProducer();
    checkConstructionsNoConstructor();

    // final Set<WonProtoItem> printeditems = new HashSet<>();
    // final Set<WonProtoProduction> printedprods = new HashSet<>();
    getItems().forEach(i -> printItem(i, "", new HashSet<>(), new HashSet<>())); //$NON-NLS-1$
  }



  private void checkItemsNoOutputProduction() {
    final Collection<WonProtoItem> unreferenced = new ArrayList<>(getItems());
    unreferenced.removeAll(itemproductionoutputmap.keySet());
    System.out.println("Items without production output: " + mapCollection(unreferenced, WonProtoObject::getType));
  }

  private void checkItemsNoInputProductionOrConstuction() {
    final Collection<WonProtoItem> unreferenced = new ArrayList<>(getItems());
    unreferenced.removeAll(itemproductioninputmap.keySet());
    unreferenced.removeAll(itemconstructionmap.keySet());
    System.out.println("Items without production input or construction: " + mapCollection(unreferenced, WonProtoObject::getType));
  }

  private void checkResourcesNoProduction() {
    final Collection<WonProtoResource> unreferenced = new ArrayList<>(getResources());
    unreferenced.removeAll(recourceproductionmap.keySet());
    System.out.println("Resources without production: " + mapCollection(unreferenced, WonProtoObject::getType));
  }

  private void checkStructuresNoProductionOrConstuction() {
    final Collection<WonProtoStructure> unreferenced = new ArrayList<>();
    unreferenced.addAll(getProducers());
    unreferenced.addAll(getTransporters());
    unreferenced.addAll(getDistributors());
    // omit constructors
    unreferenced.removeAll(structureproductionmap.keySet());
    unreferenced.removeAll(structureconstructionmap.keySet());
    System.out.println("Structure templates without production or construction: " + mapCollection(unreferenced, WonProtoObject::getType));
  }

  private void checkProductionsNoProducer() {
    final Collection<WonProtoProduction> unreferenced = new ArrayList<>(getProductions());
    unreferenced.removeAll(productionproducermap.keySet());
    System.out.println("Productions without producer: " + mapCollection(unreferenced, WonProtoObject::getType));
  }

  private void checkConstructionsNoConstructor() {
    final Collection<WonProtoConstruction> unreferenced = new ArrayList<>(getConstructions());
    unreferenced.removeAll(constructionconstructormap.keySet());
    System.out.println("Constructions without constructor: " + mapCollection(unreferenced, WonProtoObject::getType));
  }



  private void printItem(final WonProtoItem item, final String indent, final Set<WonProtoItem> printeditems, final Set<WonProtoProduction> printedprods) {
    print.println(format(indent, item));
    if (printeditems.add(item))
      getProductionsWithOutput(item).forEach(p -> printProduction(p, indent + ' ', printeditems, printedprods));
  }

  private void printProduction(final WonProtoProduction prod, final String indent, final Set<WonProtoItem> printeditems, final Set<WonProtoProduction> printedprods) {
    print.println(format(indent, prod) + "production input: " + mapCollection(prod.getInput().getKeys(), WonProtoItem::getType)); //$NON-NLS-1$
    if (printedprods.add(prod))
      prod.getInput().getKeys().forEach(i -> printItem(i, indent + '|', printeditems, printedprods));
  }



  private Set<WonProtoProduction> getProductionsWithOutput(final WonProtoItem item) {
    return itemproductionoutputmap.getOrDefault(item, emptySet());
  }



  private static String format(final String indent, final WonProtoItem i) {
    return String.format("%-65s", indent + '_' + i.getType().toUpperCase()); //$NON-NLS-1$
  }

  private static String format(final String indent, final WonProtoProduction p) {
    return String.format("%-65s", indent + p.getType()); //$NON-NLS-1$
  }



  public static void main(final String[] args) throws IOException {
    WonProtoObjects.load(Paths.get("xml-20200411"));
    new WonProductionChainTest().run();
  }
}
