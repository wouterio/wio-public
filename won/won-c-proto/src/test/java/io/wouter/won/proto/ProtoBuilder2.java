package io.wouter.won.proto;

import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;
import io.wouter.won.proto.structure.WonProtoProducer;

public class ProtoBuilder2 implements ProtoBuilder {

  @Override
  public void build() {
    final WonProtoItem pia = WonProtoObjects.newItem("A").setDurability(4); // //$NON-NLS-1$
    final WonProtoItem pib = WonProtoObjects.newItem("B").setDurability(1); // //$NON-NLS-1$
    final WonProtoItem pic = WonProtoObjects.newItem("C").setDurability(1); // //$NON-NLS-1$

    final WonProtoProduction pp1 = WonProtoObjects.newProduction("_->A").setDuration(1).setOutput(pia, 1); //$NON-NLS-1$
    final WonProtoProduction pp2 = WonProtoObjects.newProduction("A->B").setDuration(1).setInput(pia, 2).setOutput(pib, 1); //$NON-NLS-1$
    final WonProtoProduction pp3 = WonProtoObjects.newProduction("_->C").setDuration(3).setOutput(pic, 1); //$NON-NLS-1$
    final WonProtoProduction pp4 = WonProtoObjects.newProduction("B->_").setDuration(1).setInput(pib, 1); //$NON-NLS-1$
    final WonProtoProduction pp5 = WonProtoObjects.newProduction("C->_").setDuration(1).setInput(pic, 1); //$NON-NLS-1$

    final WonProtoProducer psr = WonProtoObjects.newProducer("R"); //$NON-NLS-1$
    final WonProtoProducer psw = WonProtoObjects.newProducer("W").addProduction(pp1); //$NON-NLS-1$
    final WonProtoProducer psx = WonProtoObjects.newProducer("X").addProduction(pp2); //$NON-NLS-1$
    final WonProtoProducer psy = WonProtoObjects.newProducer("Y").addProduction(pp3); //$NON-NLS-1$
    final WonProtoProducer psz = WonProtoObjects.newProducer("Z").addProduction(pp4).addProduction(pp5); //$NON-NLS-1$

    WonProtoObjects.newConstruction("Rc").setStructure(psr); //$NON-NLS-1$
    WonProtoObjects.newConstruction("Wc").setStructure(psw); //$NON-NLS-1$
    WonProtoObjects.newConstruction("Xc").setStructure(psx); //$NON-NLS-1$
    WonProtoObjects.newConstruction("Yc").setStructure(psy); //$NON-NLS-1$
    WonProtoObjects.newConstruction("Zc").setStructure(psz); //$NON-NLS-1$
  }
}
