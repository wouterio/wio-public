package io.wouter.won.proto;

public interface ProtoBuilder {

  void build();
}
