package io.wouter.won.proto.prodchain;

import static java.nio.file.Files.newOutputStream;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.Map;

import io.wouter.won.proto.WonProtoObjects;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;

public class WonCostEstimationPrint implements Runnable {

  private final Map<WonProtoItem, Map<WonProtoProduction, Double>> allitemprodcosts = WonCostEstimation.estimateCosts();
  private final PrintStream print;



  private WonCostEstimationPrint() throws IOException {
    print = new PrintStream(newOutputStream(Paths.get("prodcost.txt")), true); //$NON-NLS-1$
  }



  @SuppressWarnings("boxing")
  @Override
  public void run() {
    allitemprodcosts.forEach((i, itemprodcosts) -> {
      print.append(format(i)) //
          .append(" | "); //$NON-NLS-1$
      itemprodcosts.forEach((p, itemprodcost) -> {
        print.append(format(p)) //
            .append(" price: ") //$NON-NLS-1$
            .append(format(itemprodcost)) //
            .append("   cost: ") //$NON-NLS-1$
            .append(format(itemprodcost / i.getDurability())) //
            .append(" | "); //$NON-NLS-1$
      });
      print.append('\n');
    });
  }



  private static String format(final WonProtoItem i) {
    return String.format("%-18s", i.getType()); //$NON-NLS-1$
  }

  private static String format(final WonProtoProduction p) {
    return String.format("%-35s", p.getType()); //$NON-NLS-1$
  }

  private static String format(final Double d) {
    return String.format("% 4.0f", d); //$NON-NLS-1$
  }



  public static void main(final String[] args) throws IOException {
    WonProtoObjects.load(Paths.get("xml-20200411"));
    new WonCostEstimationPrint().run();
  }
}
