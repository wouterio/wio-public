package io.wouter.won.proto;

import static io.wouter.won.proto.WonProtoObjects.getConstructions;
import static io.wouter.won.proto.WonProtoObjects.getConstructors;
import static io.wouter.won.proto.WonProtoObjects.getDistributors;
import static io.wouter.won.proto.WonProtoObjects.getItems;
import static io.wouter.won.proto.WonProtoObjects.getProducers;
import static io.wouter.won.proto.WonProtoObjects.getProductions;
import static io.wouter.won.proto.WonProtoObjects.getResources;
import static io.wouter.won.proto.WonProtoObjects.getTransporters;

import java.io.IOException;
import java.nio.file.Paths;

public class WonProtoObjectsTest implements Runnable {

  @Override
  public void run() {
    getResources().forEach(r -> System.out.println(r));
    System.out.println();
    getItems().forEach(i -> System.out.println(i));
    System.out.println();
    getProductions().forEach(p -> System.out.println(p));
    System.out.println();
    getProducers().forEach(s -> System.out.println(s));
    System.out.println();
    getTransporters().forEach(s -> System.out.println(s));
    System.out.println();
    getDistributors().forEach(s -> System.out.println(s));
    System.out.println();
    getConstructions().forEach(c -> System.out.println(c));
    System.out.println();
    getConstructors().forEach(c -> System.out.println(c));
  }



  public static void main(final String[] args) throws IOException {
    WonProtoObjects.load(Paths.get("xml-20200411"));
    new WonProtoObjectsTest().run();
  }
}
