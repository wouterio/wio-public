package io.wouter.won.proto;

import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;
import io.wouter.won.proto.structure.WonProtoProducer;

public class ProtoBuilder1 implements ProtoBuilder {

  @Override
  public void build() {
    final WonProtoItem pia = WonProtoObjects.newItem("A").setDurability(1); // //$NON-NLS-1$

    final WonProtoProduction pp1 = WonProtoObjects.newProduction("_->A").setDuration(1).setOutput(pia, 1); //$NON-NLS-1$
    final WonProtoProduction pp2 = WonProtoObjects.newProduction("A->_").setDuration(1).setInput(pia, 1); //$NON-NLS-1$

    final WonProtoProducer psr = WonProtoObjects.newProducer("R"); //$NON-NLS-1$
    final WonProtoProducer psw = WonProtoObjects.newProducer("W").addProduction(pp1); //$NON-NLS-1$
    final WonProtoProducer psx = WonProtoObjects.newProducer("X").addProduction(pp2); //$NON-NLS-1$

    WonProtoObjects.newConstruction("Rc").setStructure(psr); //$NON-NLS-1$
    WonProtoObjects.newConstruction("Wc").setStructure(psw); //$NON-NLS-1$
    WonProtoObjects.newConstruction("Xc").setStructure(psx); //$NON-NLS-1$
  }
}
