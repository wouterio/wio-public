package io.wouter.won.proto.xml;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import io.wouter.won.proto.ProtoBuilder2;
import io.wouter.won.proto.WonProtoObjects;

public class ProtoXmlTest implements Runnable {

  @Override
  public void run() {
    try {
      final Path dir = Paths.get("xml", LocalDateTime.now().toString().replace(':', '-')); //$NON-NLS-1$
      if (!Files.exists(dir))
        Files.createDirectories(dir);

      final ProtoXmlWriter ProtoXmlWriter = new ProtoXmlWriter(dir);
      ProtoXmlWriter.writeResources(WonProtoObjects.getResources().stream());
      ProtoXmlWriter.writeItems(WonProtoObjects.getItems().stream());
      ProtoXmlWriter.writeProductions(WonProtoObjects.getProductions().stream());
      ProtoXmlWriter.writeProducers(WonProtoObjects.getProducers().stream());
      ProtoXmlWriter.writeConstructions(WonProtoObjects.getConstructions().stream());
      ProtoXmlWriter.writeConstructors(WonProtoObjects.getConstructors().stream());

    } catch (final IOException e) {
      e.printStackTrace();
    }
  }



  public static void main(final String[] args) {
    new ProtoBuilder2().build();
    new ProtoXmlTest().run();
  }
}
