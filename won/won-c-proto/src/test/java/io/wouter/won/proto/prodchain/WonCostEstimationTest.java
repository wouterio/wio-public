package io.wouter.won.proto.prodchain;

import static io.wouter.won.proto.WonProtoObject.CompareIndex;
import static javafx.geometry.Orientation.VERTICAL;
import static javafx.geometry.Pos.CENTER;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;

import io.wouter.won.proto.WonProtoObjects;
import io.wouter.won.proto.prodchain.ui.WonItemCostTableView;
import io.wouter.won.proto.prodchain.ui.WonProductionCostTableView;
import io.wouter.won.proto.product.WonProtoItem;
import io.wouter.won.proto.product.WonProtoProduction;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class WonCostEstimationTest extends Application {

  private final Map<WonProtoProduction, WonProductionCost> prodcosts = new TreeMap<>(CompareIndex);
  private final Map<WonProtoItem, WonItemCost> itemcosts = new TreeMap<>(CompareIndex);
  private final WonProductionCostTableView prodcostview = new WonProductionCostTableView();
  private final WonItemCostTableView itemcostview = new WonItemCostTableView();

  private final DirectoryChooser dirchooser = new DirectoryChooser();



  public WonCostEstimationTest() {
    dirchooser.setInitialDirectory(new File("."));
  }



  @Override
  public void start(final Stage stage) throws Exception {
    final Button bload = new Button("load...");
    final Button bsave = new Button("save...");
    final Separator sep = new Separator(VERTICAL);
    final Button bcompute = new Button("compute costs");
    final HBox pbuttons = new HBox(40d, bload, bsave, sep, bcompute);
    pbuttons.setAlignment(CENTER);

    bload.setOnAction(e -> {
      final File selected = dirchooser.showDialog(stage);
      if (selected != null)
        load(selected.toPath());
    });
    bsave.setOnAction(e -> {
      final File selected = dirchooser.showDialog(stage);
      if (selected != null)
        save(selected.toPath());
    });
    bcompute.setOnAction(e -> {
      estimateCosts();
    });

    final SplitPane spane = new SplitPane(prodcostview, itemcostview);
    final BorderPane bpane = new BorderPane();
    bpane.setTop(pbuttons);
    bpane.setCenter(spane);
    BorderPane.setMargin(pbuttons, new Insets(10d));
    BorderPane.setMargin(spane, new Insets(10d));
    BorderPane.setAlignment(pbuttons, CENTER);
    BorderPane.setAlignment(spane, CENTER);
    stage.setScene(new Scene(bpane));
    stage.show();
  }


  private void load(final Path dir) {
    try {
      prodcosts.clear();
      itemcosts.clear();
      WonProtoObjects.load(dir);
      WonProtoObjects.getProductions().forEach(p -> prodcosts.put(p, new WonProductionCost(p)));
      WonProtoObjects.getItems().forEach(p -> itemcosts.put(p, new WonItemCost(p)));
      prodcostview.getItems().setAll(prodcosts.values());
      itemcostview.getItems().setAll(itemcosts.values());

    } catch (final IOException exc) {
      exc.printStackTrace();
    }
  }

  private void save(final Path dir) {
    try {
      WonProtoObjects.save(dir);

    } catch (final IOException exc) {
      exc.printStackTrace();
    }
  }

  private void estimateCosts() {
    final Map<WonProtoItem, Map<WonProtoProduction, Double>> itemprodcostmap = WonCostEstimation.estimateCosts();
    final Map<WonProtoProduction, Double> prodcostmap = itemprodcostmap.values().stream().reduce(new TreeMap<>(CompareIndex), (result, m) -> {
      result.putAll(m);
      return result;
    });
    prodcostmap.entrySet().forEach(e -> {
      final WonProtoProduction p = e.getKey();
      final double prodcost = e.getValue().doubleValue();
      prodcosts.get(p).updateProductionCost(prodcost);
    });
    itemprodcostmap.entrySet().forEach(e -> {
      final WonProtoItem i = e.getKey();
      final Map<WonProtoProduction, Double> itemprodcosts = e.getValue();
      itemcosts.get(i).updateProductionCosts(itemprodcosts);
    });
    prodcostview.refresh();
    itemcostview.refresh();
  }



  public static void main(final String[] args) {
    launch(args);
  }
}
