package io.wouter.won.entity.delegate;

import java.util.concurrent.atomic.AtomicLong;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.WonObject;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.stock.AccountStock;
import io.wouter.won.util.stock.MetaStock;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public class TNodeDelegate extends WonEntity {

  private static final long serialVersionUID = 4571370841156504407L;



  private static final AtomicLong Indexer = new AtomicLong(0l); // FIXME resets to 0 after deserialization



  private final Long id = Long.valueOf(Indexer.getAndIncrement());
  private final TAccount account;
  private final MetaStock istock;
  private final MetaStock ostock;



  public TNodeDelegate(final int index, final String type, final TAccount account, final MetaStock istock, final MetaStock ostock) {
    super(index, type);
    this.account = account;
    this.istock = istock;
    this.ostock = ostock;
  }

  public TNodeDelegate(final WonObject reference, final TAccount account, final MetaStock istock, final MetaStock ostock) {
    super(reference);
    this.account = account;
    this.istock = istock;
    this.ostock = ostock;
  }



  public Long getId() {
    return id;
  }

  public TAccount getAccount() {
    return account;
  }

  public MetaStock getInputNodeStock() {
    return istock;
  }

  public MetaStock getOutputNodeStock() {
    return ostock;
  }



  public TBufferDelegate newInputBufferDelegate(final WonItem item) {
    final int cap = istock.getCapacity();
    final int locount = cap >> 2;
    final int lospace = cap >> 2;
    final AccountStock bufferstock = istock.newAccountSubStock(cap, locount, lospace, true);
    return new TBufferDelegate(item, account, bufferstock);
  }

  public TBufferDelegate newOutputBufferDelegate(final WonItem item) {
    final int cap = ostock.getCapacity();
    final int locount = cap >> 2;
    final int lospace = cap >> 2;
    final AccountStock bufferstock = ostock.newAccountSubStock(cap, locount, lospace, true);
    return new TBufferDelegate(item, account, bufferstock);
  }



  @Override
  public void dispose() {
    account.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("account", account) //$NON-NLS-1$
        .append("istock", istock) //$NON-NLS-1$
        .append("ostock", ostock); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info;
  }
}
