package io.wouter.won.entity.delegate;

import static java.util.Objects.requireNonNull;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.stock.WonItemStock;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.stock.AccountStock;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public class TBufferDelegate extends WonEntity {

  private final TAccount account;
  private final WonItemStock stock;



  public TBufferDelegate(final WonItem item, final TAccount account, final AccountStock stock) {
    super(item.getIndex(), item.getType());
    this.account = requireNonNull(account);
    this.stock = new WonItemStock(item, stock);
  }



  public TAccount getAccount() {
    return account;
  }

  public WonItemStock getBufferStock() {
    return stock;
  }



  @Override
  public void dispose() {
    stock.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("account", account) //$NON-NLS-1$
        .append("stock", stock); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info;
  }
}
