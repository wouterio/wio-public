package io.wouter.won.entity.task;

import static io.wouter.lib.task.TaskStatus.Completed;
import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Suspended;
import static io.wouter.lib.task.TaskStatus.Working;
import static java.lang.Float.NaN;
import static java.lang.Float.isNaN;
import static java.util.Objects.requireNonNull;

import com.aoi.won.core.Node;
import com.aoi.won.core.node.Buffer;

import io.wouter.won.entity.delegate.TBufferDelegate;
import io.wouter.won.entity.delegate.TNodeDelegate;
import io.wouter.won.entity.model.WonBuilder;
import io.wouter.won.entity.stock.WonItemStock;
import io.wouter.won.entity.task.construction.WonConstructionTask;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public class TConstructionTask extends WonTask<TNodeDelegate> implements WonConstructionTask<TNodeDelegate> { // , STask<TNodeDelegate,
                                                                                                              // TNodeDelegate> {

  private static final long serialVersionUID = -6027664053625119532L;



  private final Node<WonItem, TNodeDelegate, TBufferDelegate> node;
  private final WonConstruction construction;
  private final WonBuilder<TNodeDelegate, TBufferDelegate> builder;

  private CounterM<WonItem> inputleft = null;
  private float time = NaN;



  public TConstructionTask(final Node<WonItem, TNodeDelegate, TBufferDelegate> node, final WonConstruction construction, final WonBuilder<TNodeDelegate, TBufferDelegate> builder) {
    super(construction, node.getNodeDelegate(), Pending);
    this.node = requireNonNull(node);
    this.construction = requireNonNull(construction);
    this.builder = requireNonNull(builder);
  }



  @Override
  public WonConstruction getConstruction() {
    return construction;
  }



  @Override
  public float getTime() {
    return time;
  }

  @Override
  public float getTimeLeft() {
    return construction.getDuration() - time;
  }



  @Override
  public TConstructionTask perform(final TNodeDelegate agent, final float delta) {
    if (isNaN(time)) {
      inputleft = construction.getInput().getMod();
      time = 0f;
    }

    if (!processInput()) {
      setStatus(Suspended);
      return this;
    }

    if ((time += delta) < construction.getDuration()) {
      setStatus(Working);
      return this;
    }

    if (!processOutput()) {
      setStatus(Working); // TODO Working or Suspended?
      return this;
    }

    inputleft = null;
    time = NaN;

    setStatus(Completed);
    return this;
  }



  @Override
  public void dispose() {
    super.dispose();
    inputleft = null;
    time = NaN;
  }



  @Override
  public WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("construction", construction.getType()); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info;
  }



  private boolean processInput() {
    boolean processed = true;

    for (final WonItem i : inputleft.cleanup().getKeys()) {
      final Buffer<?, TBufferDelegate> ibuffer = node.getInput().getBuffer(i);
      final WonItemStock istock = ibuffer.getBufferDelegate().getBufferStock();

      if (istock.isEmpty())
        processed = false;
      else {
        istock.consume(1); // consume one item use
        inputleft.get(i).decr(1);
      }
      if (istock.hasLoCount())
        ibuffer.purged(true);
    }
    return processed;
  }

  private boolean processOutput() {
    return builder.apply(getClient(), construction.getStructureTemplate()) != null;
  }
}



// @Override
// public TaskStatus init(final TNodeDelegate unused) {
// if (buildtime >= 0) // if resuming task ...
// return Working; // ... leave state as is
//
// inputleft = construction.getMaterial().getMod();
// buildtime = 0;
// return Working;
// }

// @Override
// public TaskStatus step(final TNodeDelegate unused) {
// if (!processInput())
// return Suspended;
//
// if (++buildtime < construction.getConstructionTime())
// return Working;
//
// if (!processOutput())
// return Working;
//
// return Completed;
// }

// @Override
// public TaskStatus exit(final TNodeDelegate unused) { // also invoked if suspended
// if (isPresent()) // if suspended ...
// return getStatus(); // ... leave state as is
//
// inputleft = null;
// buildtime = MIN_VALUE;
// return getStatus();
// }
