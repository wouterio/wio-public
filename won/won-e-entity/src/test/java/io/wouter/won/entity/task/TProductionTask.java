package io.wouter.won.entity.task;

import static io.wouter.lib.task.TaskStatus.Suspended;
import static io.wouter.lib.task.TaskStatus.Working;
import static java.lang.Float.NaN;
import static java.lang.Float.isNaN;
import static java.util.Objects.requireNonNull;

import com.aoi.won.core.Node;
import com.aoi.won.core.node.Buffer;

import io.wouter.won.entity.delegate.TBufferDelegate;
import io.wouter.won.entity.delegate.TNodeDelegate;
import io.wouter.won.entity.stock.WonItemStock;
import io.wouter.won.entity.task.production.WonProductionTask;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public class TProductionTask extends WonTask<TNodeDelegate> implements WonProductionTask<TNodeDelegate> { // , STask<TNodeDelegate,
                                                                                                          // TNodeDelegate> {

  private static final long serialVersionUID = -4014504080513972201L;



  private final Node<WonItem, TNodeDelegate, TBufferDelegate> node;
  private final WonProduction production;

  private CounterM<WonItem> inputleft = null;
  private CounterM<WonItem> outputleft = null;
  private float cost = NaN;
  private float price = NaN;
  private float time = NaN;



  public TProductionTask(final Node<WonItem, TNodeDelegate, TBufferDelegate> node, final WonProduction production) {
    super(production, node.getNodeDelegate(), Suspended);
    this.node = requireNonNull(node);
    this.production = requireNonNull(production);
  }



  @Override
  public WonProduction getProduction() {
    return production;
  }



  @Override
  public float getTime() {
    return time;
  }

  @Override
  public float getTimeLeft() {
    return production.getDuration() - time;
  }



  @Override
  public TProductionTask perform(final TNodeDelegate agent, final float delta) {
    if (isNaN(time)) {
      inputleft = production.getInput().getMod();
      outputleft = production.getOutput().getMod();
      cost = 0f;
      price = NaN;
      time = 0f;
    }

    if (!processInput()) {
      setStatus(Suspended);
      return this;
    }

    if ((time += delta) < production.getDuration()) {
      setStatus(Working);
      return this;
    }

    if (isNaN(price))
      price = cost * node.getNodeDelegate().getAccount().getRate() / production.getOutput().total();

    if (!processOutput()) {
      setStatus(Suspended);
      return this;
    }
    if (!outputleft.cleanup().isEmpty()) {
      setStatus(Working);
      return this;
    }

    inputleft = null;
    outputleft = null;
    cost = NaN;
    price = NaN;
    time = NaN;

    setStatus(Working); // perpetual task
    return this;
  }



  @Override
  public void dispose() {
    super.dispose();
    inputleft = null;
    outputleft = null;
    cost = NaN;
    price = NaN;
    time = NaN;
  }



  @Override
  public WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("production", production.getType()); // //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info;
  }



  private boolean processInput() {
    boolean alldone = true;

    for (final WonItem i : inputleft.cleanup().getKeys()) {
      final Buffer<?, TBufferDelegate> ibuffer = node.getInput().getBuffer(i);
      final WonItemStock istock = ibuffer.getBufferDelegate().getBufferStock();

      if (istock.isEmpty())
        alldone = false;
      else {
        istock.consume(1); // consume one item use
        inputleft.get(i).decr(1);
        cost += istock.getCost();
      }
      if (istock.hasLoCount())
        ibuffer.purged(true);
    }
    return alldone;
  }

  private boolean processOutput() {
    boolean alldone = true;

    for (final WonItem o : outputleft.cleanup().getKeys()) {
      final Buffer<?, TBufferDelegate> obuffer = node.getOutput().getBuffer(o);
      final WonItemStock ostock = obuffer.getBufferDelegate().getBufferStock();

      if (ostock.isFull())
        alldone = false;
      else {
        ostock.produce(1, price); // produce one item
        outputleft.get(o).decr(1);
      }
      if (ostock.hasLoSpace())
        obuffer.filled(true);
    }
    return alldone;
  }
}



// @Override
// public TaskStatus init(final TNodeDelegate unused) {
// if (prodtime >= 0) // if resuming task ...
// return Working; // ... leave state as is
//
// inputleft = production.getInput().getMod();
// outputleft = production.getOutput().getMod();
// prodcost = 0f;
// price = NEGATIVE_INFINITY;
// prodtime = 0;
// return Working;
// }

// @Override
// public TaskStatus step(final TNodeDelegate unused) {
// prodcost += node.getDelegate().getAccount().getRate();
//
// if (!processInput())
// return Suspended;
// if (++prodtime < production.getProductionTime())
// return Working;
//
// if (price < 0f)
// price = prodcost * node.getDelegate().getAccount().getRate() / production.getOutput().total();
//
// if (!processOutput())
// return Suspended;
// if (!outputleft.cleanup().isEmpty())
// return Working;
//
// return Completed;
// }

// @Override
// public TaskStatus exit(final TNodeDelegate unused) { // also invoked if suspended
// if (isPresent()) // if suspended ...
// return getStatus(); // ... leave state as is
//
// inputleft = null;
// outputleft = null;
// prodcost = NEGATIVE_INFINITY;
// price = NEGATIVE_INFINITY;
// prodtime = MIN_VALUE;
// return TaskStatus.Pending; // Pending causes task to be initialized again for next conversion
// }
