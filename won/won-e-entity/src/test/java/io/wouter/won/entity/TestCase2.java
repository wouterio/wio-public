package io.wouter.won.entity;

import io.wouter.won.entity.model.TModel;
import io.wouter.won.entity.model.WonBuildModel;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.object.WonObjects;

public class TestCase2<N extends WonEntity, B extends WonEntity> extends TestCase<N, B> {

  public TestCase2(final WonBuildModel<N, B> model) {
    super(model);

    final WonProducer<N, B> pr1 = buildProducer(WonObjects.getConstruction("R")); //$NON-NLS-1$
    final WonProducer<N, B> pw2 = buildProducer(WonObjects.getConstruction("W")); //$NON-NLS-1$
    final WonProducer<N, B> px3 = buildProducer(WonObjects.getConstruction("X")); //$NON-NLS-1$
    final WonProducer<N, B> py4 = buildProducer(WonObjects.getConstruction("Y")); //$NON-NLS-1$
    final WonProducer<N, B> pz5 = buildProducer(WonObjects.getConstruction("Z")); //$NON-NLS-1$

    final WonTransporter<N, B> tt6 = buildTransporter(WonObjects.getConstruction("T")); //$NON-NLS-1$
    final WonTransporter<N, B> tt7 = buildTransporter(WonObjects.getConstruction("T")); //$NON-NLS-1$

    final WonAgent<N, B> aa8 = buildAgent(pr1);
    final WonAgent<N, B> aa9 = buildAgent(pr1);

    getHandler(pw2, "_->A").setEnabled(true); //$NON-NLS-1$
    getHandler(px3, "A->B").setEnabled(true); //$NON-NLS-1$
    getHandler(py4, "_->C").setEnabled(true); //$NON-NLS-1$
    getHandler(pz5, "B->_").setEnabled(true); //$NON-NLS-1$
    getHandler(pz5, "C->_").setEnabled(true); //$NON-NLS-1$

    getHandler(pw2, "_->A").setActive(true); //$NON-NLS-1$
    getHandler(px3, "A->B").setActive(true); //$NON-NLS-1$
    getHandler(py4, "_->C").setActive(true); //$NON-NLS-1$
    getHandler(pz5, "B->_").setActive(true); //$NON-NLS-1$
    getHandler(pz5, "C->_").setActive(true); //$NON-NLS-1$
  }



  public static void main(final String[] args) {
    new WriterTest(new TestCase2<>(new TModel())).run();
  }
}
