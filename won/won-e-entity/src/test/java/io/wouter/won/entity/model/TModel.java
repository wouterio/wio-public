package io.wouter.won.entity.model;

import static com.aoi.won.core.Link.getIdleLink;
import static com.aoi.won.core.Link.newInterLink;
import static com.aoi.won.core.Node.newNode;
import static io.wouter.won.util.stock.Stocks.newMetaStock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;
import com.aoi.won.core.link.SendTransfer;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonAgents;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.WonStructures;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.entity.WonTransporters;
import io.wouter.won.entity.behavior.WonAgentBehavior;
import io.wouter.won.entity.behavior.WonTransporterBehavior;
import io.wouter.won.entity.delegate.TAccount;
import io.wouter.won.entity.delegate.TBufferDelegate;
import io.wouter.won.entity.delegate.TNodeDelegate;
import io.wouter.won.entity.structure.WonConstructor;
import io.wouter.won.entity.structure.WonDistributor;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.entity.task.TConstructionTask;
import io.wouter.won.entity.task.TProductionTask;
import io.wouter.won.entity.task.construction.NewConstructionTask;
import io.wouter.won.entity.task.production.NewProductionTask;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.structure.WonConstructorTemplate;
import io.wouter.won.object.structure.WonDistributorTemplate;
import io.wouter.won.object.structure.WonProducerTemplate;
import io.wouter.won.object.structure.WonStructureTemplate;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.util.enums.LinkType;
import io.wouter.won.util.stock.MetaStock;

public class TModel implements WonBuildModel<TNodeDelegate, TBufferDelegate> {

  private static final Function<TNodeDelegate, Object> GetAgentId = TNodeDelegate::getId;



  private final List<WonBuildListener<TNodeDelegate, TBufferDelegate>> buildlisteners = new ArrayList<>();

  private final TAccount playeraccount = new TAccount(-1, "Player", 1); //$NON-NLS-1$
  private final TNodeDelegate playerdelegate = new TNodeDelegate(playeraccount, playeraccount, null, null);

  private final WonAgents<TNodeDelegate, TBufferDelegate> agents = new WonAgents<>();
  private final WonStructures<TNodeDelegate, TBufferDelegate> structures = new WonStructures<>();
  private final WonTransporters<TNodeDelegate, TBufferDelegate> transporters = new WonTransporters<>();

  private final WonBuilder<TNodeDelegate, TBufferDelegate> builder = (n, t) -> buildStructure(n, t);
  private final NewConstructionTask<TNodeDelegate, TBufferDelegate> newconstructiontask = (n, c) -> new TConstructionTask(n, c, builder);
  private final NewProductionTask<TNodeDelegate, TBufferDelegate> newproductiontask = (n, p) -> new TProductionTask(n, p);
  private final SendTransfer<WonItem, TBufferDelegate> sendtransfer = t -> transporters.offer(t);

  private final Link<WonItem, TBufferDelegate> internal = newInterLink(sendtransfer);
  private final Link<WonItem, TBufferDelegate> external = null;
  private final Link<WonItem, TBufferDelegate> idle = getIdleLink();



  @Override
  public TNodeDelegate getPlayer() {
    return playerdelegate;
  }



  @Override
  public void perform(final long round) {
    transporters.distribute();
    agents.distribute();
    agents.forEach(a -> a.getBehavior().perform(1f)); // XXX hack for this test
  }



  @Override
  public WonAgent<TNodeDelegate, TBufferDelegate> buildAgent(final TNodeDelegate residence) {
    final TNodeDelegate delegate = new TNodeDelegate(0, "Agent" + ThreadLocalRandom.current().nextInt(1000), residence.getAccount(), null, null); //$NON-NLS-1$
    final WonAgentBehavior<TNodeDelegate, TBufferDelegate> behavior = null;
    final WonAgent<TNodeDelegate, TBufferDelegate> a = new WonAgent<>(delegate, behavior);
    agents.add(a);
    fireAgentBuilt(a);
    return a;
  }

  @Override
  public WonTransporter<TNodeDelegate, TBufferDelegate> buildTransporter(final TNodeDelegate reference, final WonTransporterTemplate template) {
    final TNodeDelegate delegate = new TNodeDelegate(template, playeraccount, null, null);
    final WonTransporterBehavior<TNodeDelegate, TBufferDelegate> behavior = null;
    final WonTransporter<TNodeDelegate, TBufferDelegate> t = new WonTransporter<>(template, delegate, behavior, GetAgentId);
    transporters.add(t);
    agents.offer(t.getTask());
    fireTransporterBuilt(t);
    return t;
  }

  @Override
  public WonEntity buildStructure(final TNodeDelegate reference, final WonStructureTemplate template) {
    if (template instanceof WonConstructorTemplate)
      return newConstructor((WonConstructorTemplate) template);
    if (template instanceof WonProducerTemplate)
      return newProducer((WonProducerTemplate) template);
    if (template instanceof WonTransporterTemplate)
      return buildTransporter(reference, (WonTransporterTemplate) template);
    if (template instanceof WonDistributorTemplate)
      return newDistributor((WonDistributorTemplate) template);
    throw new IllegalArgumentException("Unknown WonStructureTemplate subclass"); //$NON-NLS-1$
  }



  @Override
  public void addBuildListener(final WonBuildListener<TNodeDelegate, TBufferDelegate> l) {
    buildlisteners.add(l);
  }

  @Override
  public void removeBuildListener(final WonBuildListener<TNodeDelegate, TBufferDelegate> l) {
    buildlisteners.remove(l);
  }



  private WonConstructor<TNodeDelegate, TBufferDelegate> newConstructor(final WonConstructorTemplate template) {
    final int cap = template.getCapacity();
    final int locount = cap >> 2;
    final int lospace = cap >> 2;
    final MetaStock inputnodestock = newMetaStock(cap, locount, lospace, true);
    final MetaStock outputnodestock = newMetaStock(cap, locount, lospace, true);
    final TNodeDelegate nodedelegate = new TNodeDelegate(template, playeraccount, inputnodestock, outputnodestock);
    final Function<WonItem, TBufferDelegate> newinputbufferdelegate = item -> nodedelegate.newInputBufferDelegate(item);
    final Function<WonItem, TBufferDelegate> newoutputbufferdelegate = item -> nodedelegate.newOutputBufferDelegate(item);
    final Node<WonItem, TNodeDelegate, TBufferDelegate> node = newNode(nodedelegate, newinputbufferdelegate, newoutputbufferdelegate);
    final WonConstructor<TNodeDelegate, TBufferDelegate> c = new WonConstructor<>(template, node, internal, newconstructiontask, GetAgentId);
    structures.add(c);
    agents.offer(c.getTask());
    fireStructureBuilt(c);
    return c;
  }

  private WonProducer<TNodeDelegate, TBufferDelegate> newProducer(final WonProducerTemplate template) {
    final int cap = template.getCapacity();
    final int locount = cap >> 2;
    final int lospace = cap >> 2;
    final TAccount account = new TAccount(template, 1);
    final MetaStock inputnodestock = newMetaStock(cap, locount, lospace, true);
    final MetaStock outputnodestock = newMetaStock(cap, locount, lospace, true);
    final TNodeDelegate nodedelegate = new TNodeDelegate(template, account, inputnodestock, outputnodestock);
    final Function<WonItem, TBufferDelegate> newinputbufferdelegate = item -> nodedelegate.newInputBufferDelegate(item);
    final Function<WonItem, TBufferDelegate> newoutputbufferdelegate = item -> nodedelegate.newOutputBufferDelegate(item);
    final Node<WonItem, TNodeDelegate, TBufferDelegate> node = newNode(nodedelegate, newinputbufferdelegate, newoutputbufferdelegate);
    final WonProducer<TNodeDelegate, TBufferDelegate> p = new WonProducer<>(template, node, internal, newproductiontask, GetAgentId);
    structures.add(p);
    agents.offer(p.getTask());
    fireStructureBuilt(p);
    return p;
  }

  private WonDistributor<TNodeDelegate, TBufferDelegate> newDistributor(final WonDistributorTemplate template) {
    final int cap = template.getCapacity();
    final int locount = cap >> 2;
    final int lospace = cap >> 2;
    final MetaStock inputnodestock = newMetaStock(cap, locount, lospace, true);
    final MetaStock outputnodestock = newMetaStock(cap, locount, lospace, true);
    final TNodeDelegate nodedelegate = new TNodeDelegate(template, playeraccount, inputnodestock, outputnodestock);
    final Function<WonItem, TBufferDelegate> newinputbufferdelegate = item -> nodedelegate.newInputBufferDelegate(item);
    final Function<WonItem, TBufferDelegate> newoutputbufferdelegate = item -> nodedelegate.newOutputBufferDelegate(item);
    final Node<WonItem, TNodeDelegate, TBufferDelegate> node = newNode(nodedelegate, newinputbufferdelegate, newoutputbufferdelegate);
    final Link<WonItem, TBufferDelegate> pulllink = getLink(template.getPullType());
    final Link<WonItem, TBufferDelegate> pushlink = getLink(template.getPushType());
    final WonDistributor<TNodeDelegate, TBufferDelegate> d = WonDistributor.newInstance(template, node);
    structures.add(d);
    fireStructureBuilt(d);
    return d;
  }



  private Link<WonItem, TBufferDelegate> getLink(final LinkType link) {
    switch (link) {
      case Internal:
        return internal;
      case External:
        return external;
      case Idle:
        return idle;
      default:
        throw new AssertionError();
    }
  }



  private void fireAgentBuilt(final WonAgent<TNodeDelegate, TBufferDelegate> a) {
    buildlisteners.forEach(l -> l.agentBuilt(a));
  }

  private void fireStructureBuilt(final WonStructure<TNodeDelegate, TBufferDelegate> c) {
    buildlisteners.forEach(l -> l.structureBuilt(c));
  }

  private void fireTransporterBuilt(final WonTransporter<TNodeDelegate, TBufferDelegate> t) {
    buildlisteners.forEach(l -> l.transporterBuilt(t));
  }
}
