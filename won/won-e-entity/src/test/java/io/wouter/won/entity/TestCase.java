package io.wouter.won.entity;

import static io.wouter.won.object.WonObject.CompareIndex;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;

import io.wouter.won.entity.model.WonBuildListener;
import io.wouter.won.entity.model.WonBuildModel;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.entity.structure.handler.WonProductionHandler;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.util.counter.CounterC;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.counter.Counters;
import io.wouter.won.util.func.PerformRound;

public abstract class TestCase<N extends WonEntity, B extends WonEntity> implements PerformRound, WonBuildListener<N, B> {

  private final WonBuildModel<N, B> model;
  private final Collection<WonEntity> objects = new ArrayList<>();



  protected TestCase(final WonBuildModel<N, B> model) {
    this.model = requireNonNull(model);
    this.model.addBuildListener(this);
  }



  @Override
  public void perform(final long time) {
    model.perform(time);
  }



  @Override
  public void agentBuilt(final WonAgent<N, B> a) {
    objects.add(a);
  }

  @Override
  public void transporterBuilt(final WonTransporter<N, B> t) {
    objects.add(t);
  }

  @Override
  public void structureBuilt(final WonStructure<N, B> s) {
    objects.add(s);
  }

  @Override
  public void agentDestroyed(final WonAgent<N, B> a) {
    objects.remove(a);
  }

  @Override
  public void transporterDestroyed(final WonTransporter<N, B> t) {
    objects.remove(t);
  }

  @Override
  public void structureDestroyed(final WonStructure<N, B> s) {
    objects.remove(s);
  }



  public final Collection<WonEntity> getObjects() {
    return objects;
  }



  protected final WonAgent<N, B> buildAgent(final WonProducer<N, B> residence) {
    return model.buildAgent(residence.getStructureDelegate());
  }

  protected final WonTransporter<N, B> buildTransporter(final WonConstruction c) {
    return model.buildTransporter(null, (WonTransporterTemplate) c.getStructureTemplate());
  }

  protected final WonProducer<N, B> buildProducer(final WonConstruction c) {
    return (WonProducer<N, B>) model.buildStructure(null, c.getStructureTemplate());
  }



  protected static <N extends WonEntity, B extends WonEntity> WonProductionHandler getHandler(final WonProducer<N, B> p, final String type) {
    return p.getHandlers().stream() //
        .filter(h -> h.getProduction().getType().equals(type)) //
        .findAny() //
        .orElseThrow(NoSuchElementException::new); //
  }

  protected static CounterC<WonItem> toCounter(final WonItem... items) {
    final CounterM<WonItem> counter = Counters.newCounter(CompareIndex);
    for (final WonItem i : items)
      counter.ensure(i).incr();
    return counter.getConst();
  }
}
