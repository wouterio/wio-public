package io.wouter.won.entity;

import static java.util.Arrays.asList;

import io.wouter.won.entity.model.TModel;
import io.wouter.won.entity.model.WonBuildModel;
import io.wouter.won.entity.structure.WonProducer;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.object.structure.WonProducerTemplate;
import io.wouter.won.object.structure.WonTransporterTemplate;

public class TestCase1<N extends WonEntity, B extends WonEntity> extends TestCase<N, B> {

  public TestCase1(final WonBuildModel<N, B> model) {
    super(model);

    final WonItem ia = new WonItem(0, "A", 1);

    final WonProduction p0a = new WonProduction(0, "_->A", toCounter(), toCounter(ia), null, null, 1);
    final WonProduction pa0 = new WonProduction(1, "A->_", toCounter(ia), toCounter(), null, null, 1);

    final WonProducerTemplate rt = new WonProducerTemplate(0, "R", asList(), 1, 1, 0);
    final WonProducerTemplate wt = new WonProducerTemplate(1, "W", asList(p0a), 1, 1, 4);
    final WonProducerTemplate xt = new WonProducerTemplate(2, "X", asList(pa0), 1, 1, 4);
    final WonTransporterTemplate tt = new WonTransporterTemplate(3, "T", 1, 1, 4);

    final WonConstruction rc = new WonConstruction(0, "Rc", toCounter(), rt, 0);
    final WonConstruction wc = new WonConstruction(1, "Wc", toCounter(), wt, 0);
    final WonConstruction xc = new WonConstruction(2, "Xc", toCounter(), xt, 0);
    final WonConstruction tc = new WonConstruction(3, "Tc", toCounter(), tt, 0);

    // build world

    final WonProducer<N, B> pr1 = buildProducer(rc);
    final WonProducer<N, B> pw2 = buildProducer(wc);
    final WonProducer<N, B> px3 = buildProducer(xc);
    final WonTransporter<N, B> tt4 = buildTransporter(tc);

    final WonAgent<N, B> aa5 = buildAgent(pr1);
    // final WonAgent<N, B> aa6 = buildAgent(pr1);

    getHandler(pw2, "_->A").setEnabled(true); //$NON-NLS-1$
    getHandler(px3, "A->_").setEnabled(true); //$NON-NLS-1$

    getHandler(pw2, "_->A").setActive(true); //$NON-NLS-1$
    getHandler(px3, "A->_").setActive(true); //$NON-NLS-1$
  }



  public static void main(final String[] args) {
    new WriterTest(new TestCase1<>(new TModel())).run();
  }
}
