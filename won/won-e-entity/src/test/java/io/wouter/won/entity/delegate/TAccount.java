package io.wouter.won.entity.delegate;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.WonObject;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public class TAccount extends WonEntity { // default serialization form

  private static final long serialVersionUID = -2186884286947598932L;



  private final int rate;



  private int credits = 0;



  public TAccount(final int index, final String type, final int rate) {
    super(index, type);
    this.rate = rate;
  }

  public TAccount(final WonObject reference, final int rate) {
    super(reference);
    this.rate = rate;
  }



  public int getRate() {
    return rate;
  }



  public int getCredits() {
    return credits;
  }

  public void addCredits(final int amount) {
    credits += amount;
  }

  public void removeCredits(final int amount) {
    credits -= amount;
  }



  @Override
  public void dispose() {
    credits = 0;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder().append("$" + getCredits()); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info;
  }
}
