package io.wouter.won.entity;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WriterTest implements Runnable {

  private final TestCase<?, ?> testcase;



  public WriterTest(final TestCase<?, ?> testcase) {
    this.testcase = testcase;
  }



  @Override
  public void run() {
    final String timestamp = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()); //$NON-NLS-1$
    final String filename = testcase.getClass().getSimpleName() + " " + timestamp + ".txt"; //$NON-NLS-1$//$NON-NLS-2$
    final Path outfile = Paths.get("out", filename);

    try (BufferedWriter writer = new BufferedWriter(new FileWriter(outfile.toFile(), false))) {
      writer.write(filename);
      writer.write('\n');
      writer.write('\n');

      final StringBuilder sb = new StringBuilder();
      testcase.getObjects().forEach(p -> sb.append(p).append('\n'));
      writer.write(sb.toString());
      writer.write('\n');
      sb.setLength(0);

      for (int i = 0; i < 100; ++i) {
        final String roundstr = "*** round " + i + " ***"; //$NON-NLS-1$ //$NON-NLS-2$
        System.out.println(roundstr);
        writer.write(roundstr);
        writer.write('\n');
        writer.write('\n');

        testcase.perform(i);
        testcase.getObjects().forEach(p -> sb.append(p).append('\n'));
        writer.write(sb.toString());
        writer.write('\n');
        sb.setLength(0);
      }

    } catch (final IOException e) {
      e.printStackTrace();
    }

    System.out.println("Completed."); //$NON-NLS-1$
  }
}
