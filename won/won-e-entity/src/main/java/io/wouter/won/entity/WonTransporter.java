package io.wouter.won.entity;

import static io.wouter.lib.task.Tasks.newCollectiveTask;
import static java.util.Objects.requireNonNull;

import java.util.function.Function;

import io.wouter.lib.task.CTask;
import io.wouter.won.entity.behavior.WonTransporterBehavior;
import io.wouter.won.entity.model.WonGroup;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonTransporter<N extends WonEntity, B extends WonEntity> extends WonEntity {

  private static final long serialVersionUID = 393453804742828933L;



  private final N delegate;
  private final WonTransporterBehavior<N, B> behavior;
  private final CTask<N, N> ctask;



  private WonGroup<N, B> group = null;



  public WonTransporter(final WonTransporterTemplate template, final N delegate, final WonTransporterBehavior<N, B> behavior, final Function<N, Object> getagentid) {
    super(template);

    final int minworkers = template.getMinWorkers();
    final int maxworkers = template.getMaxWorkers();
    this.delegate = requireNonNull(delegate);
    this.behavior = requireNonNull(behavior);
    this.ctask = newCollectiveTask(behavior, getagentid, minworkers, maxworkers);
  }



  public N getDelegate() {
    return delegate;
  }

  public WonTransporterBehavior<N, B> getBehavior() {
    return behavior;
  }

  public CTask<N, N> getTask() {
    return ctask;
  }

  public int getWorkers() {
    return ctask.getAgents();
  }

  public int getMinWorkers() {
    return ctask.getMinAgents();
  }

  public int getMaxWorkers() {
    return ctask.getMaxAgents();
  }



  public WonGroup<N, B> getGroup() {
    return group;
  }

  public boolean setGroup(final WonGroup<N, B> newgroup) {
    if (group != null && group.removeTransporter(this))
      group = null;
    if (group == null && newgroup != null && newgroup.addTransporter(this))
      group = newgroup;
    return group == newgroup;
  }



  @Override
  public void dispose() {
    super.dispose();
    behavior.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("workers", getWorkers()) //$NON-NLS-1$
        .append("minworkers", getMinWorkers()) //$NON-NLS-1$
        .append("maxworkers", getMaxWorkers()) //$NON-NLS-1$
        .append("behavior", behavior); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .separator() //
        .field("workers").valueInt(this::getWorkers) //$NON-NLS-1$
        .field("minworkers").valueInt(this::getMinWorkers) //$NON-NLS-1$
        .field("maxworkers").valueInt(this::getMaxWorkers) //$NON-NLS-1$
        .separator() //
        .field("behavior").collect(behavior); //$NON-NLS-1$
  }
}
