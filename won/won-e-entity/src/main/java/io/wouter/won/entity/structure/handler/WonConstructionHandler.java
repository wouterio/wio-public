package io.wouter.won.entity.structure.handler;

import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Suspended;
import static io.wouter.lib.task.TaskStatus.Working;
import static java.util.Collections.emptySet;

import java.util.Set;

import com.aoi.won.core.node.Conversion;

import io.wouter.lib.task.TaskStatus;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.task.construction.WonConstructionTask;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public class WonConstructionHandler extends WonEntity implements Conversion<WonItem> {

  private static final long serialVersionUID = 5836071078010344853L;



  private final WonConstructionTask<?> task;



  private boolean enabled = true;



  public WonConstructionHandler(final WonConstructionTask<?> task) {
    super(task.getConstruction());
    this.task = task;
  }



  public WonConstruction getConstruction() {
    return task.getConstruction();
  }

  @Override
  public Set<WonItem> getInput() {
    return getConstruction().getInput().getKeys(); // returns unmodifiable counter keyset
  }

  @Override
  public Set<WonItem> getOutput() {
    return emptySet();
  }



  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
  }



  @Override
  public boolean isActive() {
    return task.isActive();
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      if (enabled && task.getStatus().equals(Suspended))
        task.setStatus(Pending);
    } else if (task.getStatus().equals(Working))
      task.setStatus(Suspended);
  }



  public float getTime() {
    return task.getTime();
  }

  public float getTimeLeft() {
    return task.getTimeLeft();
  }



  @Override
  public void dispose() {
    super.dispose();
    setEnabled(false);
    task.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("enabled", enabled) //$NON-NLS-1$
        .append("task", task); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("production").link(task.getConstruction(), WonConstruction::getType) //$NON-NLS-1$
        .field("enabled").valueBoolean(this::isEnabled, this::setEnabled) //$NON-NLS-1$
        .field("status").valueObject(task::getStatus, TaskStatus::toString) //$NON-NLS-1$
        .field("timeleft").valueFloat(this::getTimeLeft); //$NON-NLS-1$
  }
}
