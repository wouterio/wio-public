package io.wouter.won.entity;

import io.wouter.won.object.WonObject;
import io.wouter.won.util.func.Dispose;

public abstract class WonEntity extends WonObject implements Dispose {

  private static final long serialVersionUID = 3117247965302978909L;



  private boolean disposed = false;



  protected WonEntity(final int index, final String type) {
    super(index, type);
  }

  protected WonEntity(final WonObject reference) {
    super(reference);
  }



  public final boolean isDisposed() {
    return disposed;
  }

  @Override
  public void dispose() {
    disposed = true;
  }
}
