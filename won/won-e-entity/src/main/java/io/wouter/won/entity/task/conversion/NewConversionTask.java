package io.wouter.won.entity.task.conversion;

import java.util.function.BiFunction;

import com.aoi.won.core.Node;

import io.wouter.won.object.product.WonConversion;
import io.wouter.won.object.product.WonItem;

@FunctionalInterface
public interface NewConversionTask<N, B> extends BiFunction<Node<WonItem, N, B>, WonConversion, WonConversionTask<N>> {

  // WonConversionTask<N> newConversionTask(Node<WonItem, N, B> n, WonConversion c);
}
