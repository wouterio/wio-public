package io.wouter.won.entity.task.production;

import io.wouter.lib.task.Task;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.string.WonInfoBuilder;

public interface WonProductionTask<N> extends Task<N, N>, WonInfoBuilder.Collect, Dispose { // TODO extends WonTask interface

  WonProduction getProduction();



  float getTime();

  float getTimeLeft();
}
