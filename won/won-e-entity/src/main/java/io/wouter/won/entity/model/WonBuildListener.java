package io.wouter.won.entity.model;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.WonTransporter;

public interface WonBuildListener<N extends WonEntity, B extends WonEntity> {

  void agentBuilt(WonAgent<N, B> a);

  void agentDestroyed(WonAgent<N, B> a);



  void transporterBuilt(WonTransporter<N, B> t);

  void transporterDestroyed(WonTransporter<N, B> t);



  void structureBuilt(WonStructure<N, B> s);

  void structureDestroyed(WonStructure<N, B> s);
}
