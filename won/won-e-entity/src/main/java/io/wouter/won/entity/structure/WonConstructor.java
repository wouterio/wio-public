package io.wouter.won.entity.structure;

import static com.aoi.won.core.Node.equip;
import static io.wouter.lib.task.Tasks.newCollectiveTask;
import static java.util.Collections.singletonList;

import java.util.function.Function;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;

import io.wouter.lib.task.CTask;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.structure.handler.WonConstructionHandler;
import io.wouter.won.entity.task.construction.NewConstructionTask;
import io.wouter.won.entity.task.construction.WonConstructionTask;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.structure.WonConstructorTemplate;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonConstructor<N extends WonEntity, B extends WonEntity> extends WonStructure<N, B> {

  private static final long serialVersionUID = 1807236913041492217L;



  private final CTask<N, N> ctask;
  private final WonConstructionHandler handler;



  public WonConstructor(final WonConstructorTemplate ct, final Node<WonItem, N, B> node, final Link<WonItem, B> interlink, final NewConstructionTask<N, B> newtask, final Function<N, Object> getagentid) {
    super(ct, node);

    final WonConstructionTask<N> contask = newtask.apply(node, ct.getConstruction());
    final int minworkers = ct.getMinWorkers();
    final int maxworkers = ct.getMaxWorkers();
    ctask = newCollectiveTask(contask, getagentid, minworkers, maxworkers);
    handler = new WonConstructionHandler(contask);

    equip(node, singletonList(handler), interlink, interlink);
  }



  public CTask<N, N> getTask() {
    return ctask;
  }

  public int getWorkers() {
    return ctask.getAgents();
  }

  public int getMinWorkers() {
    return ctask.getMinAgents();
  }

  public int getMaxWorkers() {
    return ctask.getMaxAgents();
  }

  public WonConstructionHandler getHandler() {
    return handler;
  }



  @Override
  public void dispose() {
    super.dispose();
    handler.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("workers", getWorkers()) //$NON-NLS-1$
        .append("minworkers", getMinWorkers()) //$NON-NLS-1$
        .append("maxworkers", getMaxWorkers()) //$NON-NLS-1$
        .append("handler", handler); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .separator() //
        .field("workers").valueInt(this::getWorkers) //$NON-NLS-1$
        .field("minworkers").valueInt(this::getMinWorkers) //$NON-NLS-1$
        .field("maxworkers").valueInt(this::getMaxWorkers) //$NON-NLS-1$
        .separator() //
        .collect(getStructureDelegate()) //
        .separator() //
        .child("handler").collect(handler).parent(); //$NON-NLS-1$
  }
}
