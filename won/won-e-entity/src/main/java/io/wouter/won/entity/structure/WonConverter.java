package io.wouter.won.entity.structure;

import static com.aoi.won.core.Link.newIntraLink;
import static com.aoi.won.core.node.Conversion.getInputs;
import static com.aoi.won.core.node.Conversion.getOutputs;
import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.Tasks.newCollectiveTask;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;
import com.aoi.won.core.node.Buffers;

import io.wouter.lib.task.CTask;
import io.wouter.lib.task.Task;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.model.WonGroup;
import io.wouter.won.entity.structure.handler.WonConversionHandler;
import io.wouter.won.entity.task.conversion.NewConversionTask;
import io.wouter.won.entity.task.conversion.WonConversionTask;
import io.wouter.won.object.product.WonConversion;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.structure.WonConverterTemplate;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonConverter<N extends WonEntity, B extends WonEntity> extends WonStructure<N, B> {

  private static final long serialVersionUID = 827520106854102379L;



  private final CTask<N, N> ctask;
  private final List<WonConversionHandler> handlers = new ArrayList<>();
  private final List<WonConversionHandler> uhandlers = unmodifiableList(handlers);



  private WonGroup<N, B> group = null;



  public WonConverter(final WonConverterTemplate template, final Node<WonItem, N, B> node, final NewConversionTask<N, B> newtask, final Function<N, Object> getagentid) {
    super(template, node);

    final boolean enablehandler = template.getConversions().size() <= 1;
    final Collection<Task<N, N>> tasks = new LinkedList<>();
    for (final WonConversion c : template.getConversions()) {
      final WonConversionTask<N> task = newtask.apply(node, c);
      final WonConversionHandler handler = new WonConversionHandler(task, enablehandler);
      tasks.add(task);
      handlers.add(handler);
    }

    final int minworkers = template.getMinWorkers();
    final int maxworkers = template.getMaxWorkers();
    ctask = newCollectiveTask(tasks, getagentid, minworkers, maxworkers, Pending);

    final Link<WonItem, B> intralink = newIntraLink(uhandlers);
    final Buffers<WonItem, B> input = node.getInput();
    final Buffers<WonItem, B> output = node.getOutput();
    input.setPushLink(intralink);
    output.setPullLink(intralink);
    getInputs(uhandlers).forEach(input::ensureBuffer);
    getOutputs(uhandlers).forEach(output::ensureBuffer);
  }



  public CTask<N, N> getTask() {
    return ctask;
  }

  public List<WonConversionHandler> getHandlers() {
    return uhandlers;
  }

  public int getWorkers() {
    return ctask.getAgents();
  }

  public int getMinWorkers() {
    return ctask.getMinAgents();
  }

  public int getMaxWorkers() {
    return ctask.getMaxAgents();
  }



  public WonGroup<N, B> getGroup() {
    return group;
  }

  public boolean setGroup(final WonGroup<N, B> newgroup) {
    if (group != null && group.removeConverter(this))
      group = null;
    if (group == null && newgroup != null && newgroup.addConverter(this))
      group = newgroup;
    return group == newgroup;
  }



  public void setLink(final Link<WonItem, B> link) {
    node.getInput().setPullLink(link);
    node.getOutput().setPushLink(link);
  }



  @Override
  public void dispose() {
    super.dispose();
    handlers.forEach(WonConversionHandler::dispose);
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("workers", getWorkers()) //$NON-NLS-1$
        .append("minworkers", getMinWorkers()) //$NON-NLS-1$
        .append("maxworkers", getMaxWorkers()) //$NON-NLS-1$
        .appendAll("handlers", uhandlers, WonConversionHandler::toString); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .separator() //
        .field("workers").valueInt(this::getWorkers) //$NON-NLS-1$
        .field("minworkers").valueInt(this::getMinWorkers) //$NON-NLS-1$
        .field("maxworkers").valueInt(this::getMaxWorkers) //$NON-NLS-1$
        .separator() //
        .collect(getStructureDelegate()) //
        .separator() //
        .child("handlers").collectAll(uhandlers).parent(); //$NON-NLS-1$
  }
}
