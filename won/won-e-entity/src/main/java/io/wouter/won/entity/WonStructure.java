package io.wouter.won.entity;

import static java.util.Objects.requireNonNull;

import com.aoi.won.core.Node;

import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.structure.WonStructureTemplate;
import io.wouter.won.util.string.WonStringBuilder;

public abstract class WonStructure<N extends WonEntity, B extends WonEntity> extends WonEntity {

  private static final long serialVersionUID = -7647175018244012906L;



  protected final Node<WonItem, N, B> node;



  protected WonStructure(final WonStructureTemplate st, final Node<WonItem, N, B> node) {
    super(st);
    this.node = requireNonNull(node);
  }



  public final N getStructureDelegate() {
    return node.getNodeDelegate();
  }



  public final boolean hasInput(final WonItem item) {
    return node.getInput().hasBuffer(item);
  }

  public final boolean hasOutput(final WonItem item) {
    return node.getOutput().hasBuffer(item);
  }

  public final B getInputDelegate(final WonItem item) {
    return node.getInput().getBuffer(item).getBufferDelegate();
  }

  public final B getOutputDelegate(final WonItem item) {
    return node.getOutput().getBuffer(item).getBufferDelegate();
  }



  public void inputFilled(final WonItem item, final boolean filled) {
    node.getInput().getBuffer(item).filled(filled);
  }

  public void outputPurged(final WonItem item, final boolean purged) {
    node.getOutput().getBuffer(item).purged(purged);
  }



  @Override
  public void dispose() {
    node.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("node", node); //$NON-NLS-1$
  }
}
