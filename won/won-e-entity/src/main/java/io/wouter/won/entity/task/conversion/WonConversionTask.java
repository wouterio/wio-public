package io.wouter.won.entity.task.conversion;

import io.wouter.lib.task.Task;
import io.wouter.won.object.product.WonConversion;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.string.WonInfoBuilder;

public interface WonConversionTask<N> extends Task<N, N>, WonInfoBuilder.Collect, Dispose { // TODO extends WonTask interface

  WonConversion getConversion();



  float getTime();

  float getTimeLeft();
}
