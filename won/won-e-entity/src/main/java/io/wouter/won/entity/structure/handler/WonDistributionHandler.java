package io.wouter.won.entity.structure.handler;

import com.aoi.won.core.node.Buffer;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.WonObject;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.func.GetItem;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonDistributionHandler extends WonEntity implements GetItem<WonItem> {

  private static final long serialVersionUID = -6580836886514805701L;



  private final Buffer<WonItem, ? extends WonEntity> buffer;



  public WonDistributionHandler(final Buffer<WonItem, ? extends WonEntity> buffer) {
    super(buffer.getItem());
    this.buffer = buffer;
    buffer.setAutoFlow(false);
  }



  @Override
  public WonItem getItem() {
    return buffer.getItem();
  }



  public boolean hasAutoFlow() {
    return buffer.hasAutoFlow();
  }

  public void setAutoFlow(final boolean enabled) {
    buffer.setAutoFlow(enabled);
  }



  public void fill() {
    buffer.fill(true);
  }

  public void purge() {
    buffer.purge(true);
  }



  @Override
  public void dispose() {
    super.dispose();
    buffer.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("item", getItem()) //$NON-NLS-1$
        .append("autoflow", hasAutoFlow()); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("item").link(getItem(), WonObject::getType) //$NON-NLS-1$
        .field("autoflow").valueBoolean(this::hasAutoFlow, this::setAutoFlow) //$NON-NLS-1$
        .field("fill").action(this::fill) //$NON-NLS-1$
        .field("purge").action(this::purge); //$NON-NLS-1$
  }
}
