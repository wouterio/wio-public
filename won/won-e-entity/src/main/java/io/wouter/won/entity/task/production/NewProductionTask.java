package io.wouter.won.entity.task.production;

import java.util.function.BiFunction;

import com.aoi.won.core.Node;

import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;

@FunctionalInterface
public interface NewProductionTask<N, B> extends BiFunction<Node<WonItem, N, B>, WonProduction, WonProductionTask<N>> {

  // WonProductionTask<N> newProductionTask(Node<WonItem, N, B> n, WonProduction p);
}
