package io.wouter.won.entity;

import static java.util.Objects.requireNonNull;

import io.wouter.won.entity.behavior.WonAgentBehavior;
import io.wouter.won.entity.model.WonGroup;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonAgent<N extends WonEntity, B extends WonEntity> extends WonEntity {

  private static final long serialVersionUID = 1973906139499372681L;



  private final N delegate;
  private final WonAgentBehavior<N, B> behavior;



  private WonGroup<N, B> group = null;



  public WonAgent(final N delegate, final WonAgentBehavior<N, B> behavior) {
    super(delegate);
    this.delegate = requireNonNull(delegate);
    this.behavior = requireNonNull(behavior);
  }



  public N getAgentDelegate() {
    return delegate;
  }

  public WonAgentBehavior<N, B> getBehavior() {
    return behavior;
  }



  public WonGroup<N, B> getGroup() {
    return group;
  }

  public boolean setGroup(final WonGroup<N, B> newgroup) {
    if (group != null && group.removeAgent(this))
      group = null;
    if (group == null && newgroup != null && newgroup.addAgent(this))
      group = newgroup;
    return group == newgroup;
  }



  @Override
  public void dispose() {
    super.dispose();
    if (Dispose.class.isInstance(delegate))
      Dispose.class.cast(delegate).dispose();
    behavior.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("delegate", delegate) //$NON-NLS-1$
        .append("behavior", behavior); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .child("delegate").collect(delegate).parent() //$NON-NLS-1$
        .child("behavior").collect(behavior).parent(); //$NON-NLS-1$
  }
}
