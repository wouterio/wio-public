package io.wouter.won.entity.model;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.entity.structure.WonConverter;
import io.wouter.won.util.func.PerformRound;


public interface WonGroup<N extends WonEntity, B extends WonEntity> extends PerformRound {

  boolean addAgent(WonAgent<N, B> agent);

  boolean removeAgent(WonAgent<N, B> agent);



  boolean addConverter(WonConverter<N, B> converter);

  boolean removeConverter(WonConverter<N, B> converter);



  boolean addTransporter(WonTransporter<N, B> transporter);

  boolean removeTransporter(WonTransporter<N, B> transporter);
}
