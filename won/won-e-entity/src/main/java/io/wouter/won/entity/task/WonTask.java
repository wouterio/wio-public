package io.wouter.won.entity.task;

import static io.wouter.lib.task.TaskStatus.Expired;
import static java.util.Objects.requireNonNull;

import io.wouter.lib.task.Task;
import io.wouter.lib.task.TaskStatus;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.WonObject;
import io.wouter.won.util.string.WonStringBuilder;

public abstract class WonTask<N extends WonEntity> extends WonEntity implements Task<N, N> {

  private static final long serialVersionUID = -8581978540315950498L;



  private final N client;



  private TaskStatus status;



  protected WonTask(final int index, final String type, final N client, final TaskStatus status) {
    super(index, type);
    this.client = requireNonNull(client);
    this.status = requireNonNull(status);
  }

  protected WonTask(final WonObject reference, final N client, final TaskStatus status) {
    super(reference);
    this.client = requireNonNull(client);
    this.status = requireNonNull(status);
  }



  @Override
  public N getClient() {
    return client;
  }



  @Override
  public TaskStatus getStatus() {
    return status;
  }

  @Override
  public boolean setStatus(final TaskStatus newstatus) {
    if (newstatus.equals(status))
      return false;
    status = status.checkValidTransition(newstatus);
    return true;
  }



  @Override
  public void dispose() {
    if (isPresent())
      setStatus(Expired);
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("client", client) //$NON-NLS-1$
        .append("status", status); //$NON-NLS-1$
  }
}
