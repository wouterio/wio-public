package io.wouter.won.entity.model;

import java.util.function.BiFunction;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.structure.WonStructureTemplate;

@FunctionalInterface
public interface WonBuilder<N extends WonEntity, B extends WonEntity> extends BiFunction<N, WonStructureTemplate, WonEntity> {

}
