package io.wouter.won.entity.task.construction;

import java.util.function.BiFunction;

import com.aoi.won.core.Node;

import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;

@FunctionalInterface
public interface NewConstructionTask<N, B> extends BiFunction<Node<WonItem, N, B>, WonConstruction, WonConstructionTask<N>> {

  // WonConstructionTask<N> newConstructionTask(Node<WonItem, N, B> n, WonConstruction c);
}
