package io.wouter.won.entity.structure;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;
import com.aoi.won.core.node.Buffer;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.structure.handler.WonDistributionHandler;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.structure.WonDistributorTemplate;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonDistributor<N extends WonEntity, B extends WonEntity> extends WonStructure<N, B> {

  public static <N extends WonEntity, B extends WonEntity> WonDistributor<N, B> newInstance(final WonDistributorTemplate dt, final Node<WonItem, N, B> node) {
    return new WonDistributor<>(dt, node);
  }



  private static final long serialVersionUID = -4394211694320136390L;



  private final List<WonDistributionHandler> handlers = new ArrayList<>();
  private final List<WonDistributionHandler> uhandlers = unmodifiableList(handlers);



  private WonDistributor(final WonDistributorTemplate dt, final Node<WonItem, N, B> node) {
    super(dt, node);

    for (final WonItem i : dt.getItems()) {
      final Buffer<WonItem, B> newbuffer = node.getInput().ensureBuffer(i);
      final WonDistributionHandler newhandler = new WonDistributionHandler(newbuffer);
      handlers.add(newhandler);
    }
  }



  public List<WonDistributionHandler> getHandlers() {
    return uhandlers;
  }



  public void setLink(final Link<WonItem, B> pulllink, final Link<WonItem, B> pushlink) {
    node.getInput().setPullLink(pulllink);
    node.getInput().setPushLink(pushlink); // XXX also input
  }



  @Override
  public void dispose() {
    super.dispose();
    handlers.forEach(WonDistributionHandler::dispose);
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .appendAll("handlers", uhandlers, WonDistributionHandler::toString); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .separator() //
        .collect(getStructureDelegate()) //
        .separator() //
        .child("handlers").collectAll(uhandlers).parent(); //$NON-NLS-1$
  }
}
