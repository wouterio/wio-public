package io.wouter.won.entity;

import java.util.ArrayList;
import java.util.List;

import io.wouter.won.entity.structure.WonConverter;

public class WonConverters<N extends WonEntity, B extends WonEntity> {

  private final List<WonConverter<N, B>> converters = new ArrayList<>();



  public boolean add(final WonConverter<N, B> converter) {
    return converters.add(converter);
  }

  public boolean remove(final WonConverter<N, B> converter) {
    return converters.remove(converter);
  }
}
