package io.wouter.won.entity.structure.handler;

import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Suspended;

import java.util.Set;

import com.aoi.won.core.node.Conversion;

import io.wouter.lib.task.TaskStatus;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.task.production.WonProductionTask;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProductionHandler extends WonEntity implements Conversion<WonItem> {

  private static final long serialVersionUID = 7190749757303379595L;



  private final WonProductionTask<?> task;



  private boolean enabled = false;



  public WonProductionHandler(final WonProductionTask<?> task) {
    super(task.getProduction());
    this.task = task;
  }



  public WonProduction getProduction() {
    return task.getProduction();
  }

  @Override
  public Set<WonItem> getInput() {
    return getProduction().getInput().getKeys(); // returns unmodifiable counter keyset
  }

  @Override
  public Set<WonItem> getOutput() {
    return getProduction().getOutput().getKeys(); // returns unmodifiable counter keyset
  }



  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
    if (enabled != isActive())
      setActive(enabled);
  }



  @Override
  public boolean isActive() {
    return task.isActive();
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      if (enabled && !isActive())
        task.setStatus(Pending);
    } else if (isActive())
      task.setStatus(Suspended);
  }



  public float getTime() {
    return task.getTime();
  }

  public float getTimeLeft() {
    return task.getTimeLeft();
  }



  @Override
  public void dispose() {
    super.dispose();
    setEnabled(false);
    task.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("enabled", enabled) //$NON-NLS-1$
        .append("task", task); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("production").link(task.getProduction(), WonProduction::getType) //$NON-NLS-1$
        .field("enabled").valueBoolean(this::isEnabled, this::setEnabled) //$NON-NLS-1$
        .field("status").valueObject(task::getStatus, TaskStatus::toString) //$NON-NLS-1$
        .field("timeleft").valueFloat(this::getTimeLeft); //$NON-NLS-1$
  }
}
