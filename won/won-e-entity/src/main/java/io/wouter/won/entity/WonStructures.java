package io.wouter.won.entity;

import java.util.ArrayList;
import java.util.List;

public class WonStructures<N extends WonEntity, B extends WonEntity> {

  private final List<WonStructure<N, B>> structures = new ArrayList<>();



  public boolean add(final WonStructure<N, B> structure) {
    return structures.add(structure);
  }

  public boolean remove(final WonStructure<N, B> structure) {
    return structures.remove(structure);
  }
}
