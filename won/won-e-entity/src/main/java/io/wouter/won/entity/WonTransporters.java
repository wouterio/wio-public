package io.wouter.won.entity;

import static io.wouter.lib.rsq.RSQueue.newRSQueue;

import java.util.ArrayDeque;
import java.util.Queue;

import com.aoi.won.core.link.Transfer;

import io.wouter.lib.rsq.RSQueue;
import io.wouter.won.object.product.WonItem;

public class WonTransporters<N extends WonEntity, B extends WonEntity> {

  private final RSQueue<WonTransporter<N, B>> transporters = newRSQueue(t -> t.isDisposed(), t -> t.getBehavior().hasTransfer());
  private final Queue<Transfer<WonItem, B>> transports = new ArrayDeque<>();



  public WonTransporters() { /* nothing */ }



  public boolean add(final WonTransporter<N, B> transporter) {
    return transporters.add(transporter);
  }

  public boolean remove(final WonTransporter<N, B> transporter) {
    return transporters.remove(transporter);
  }



  public boolean offer(final Transfer<WonItem, B> transport) {
    return transports.offer(transport);
  }



  public void distribute() {
    while (true) {
      final WonTransporter<N, B> transporter = transporters.findAndPeek();
      if (transporter == null)
        break;

      final Transfer<WonItem, B> transport = transports.poll();
      if (transport == null)
        break;

      transporters.shift();
      transporter.getBehavior().setTransfer(transport);
    }
  }
}
