package io.wouter.won.entity.structure.handler;

import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.TaskStatus.Suspended;

import java.util.Set;

import com.aoi.won.core.node.Conversion;

import io.wouter.lib.task.TaskStatus;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.task.conversion.WonConversionTask;
import io.wouter.won.object.WonObject;
import io.wouter.won.object.product.WonConversion;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonConversionHandler extends WonEntity implements Conversion<WonItem> {

  private static final long serialVersionUID = 7190749757303379595L;



  private final WonConversionTask<?> task;



  private boolean enabled;



  public WonConversionHandler(final WonConversionTask<?> task, final boolean enabled) {
    super(task.getConversion());
    this.task = task;
    this.enabled = enabled;
  }



  public WonConversion getConversion() {
    return task.getConversion();
  }

  @Override
  public Set<WonItem> getInput() {
    return getConversion().getInput().getKeys(); // returns unmodifiable counter keyset
  }

  @Override
  public Set<WonItem> getOutput() {
    return getConversion().getOutput().getKeys(); // returns unmodifiable counter keyset
  }



  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
    if (enabled != isActive())
      setActive(enabled);
  }



  @Override
  public boolean isActive() {
    return task.isActive();
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      if (enabled && !isActive())
        task.setStatus(Pending);
    } else if (isActive())
      task.setStatus(Suspended);
  }



  public float getTime() {
    return task.getTime();
  }

  public float getTimeLeft() {
    return task.getTimeLeft();
  }



  @Override
  public void dispose() {
    super.dispose();
    setEnabled(false);
    task.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("enabled", enabled) //$NON-NLS-1$
        .append("task", task); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("conversion").link(task.getConversion(), WonObject::getType) //$NON-NLS-1$
        .field("enabled").valueBoolean(this::isEnabled, this::setEnabled) //$NON-NLS-1$
        .field("status").valueObject(task::getStatus, TaskStatus::toString) //$NON-NLS-1$
        .field("timeleft").valueFloat(this::getTimeLeft); //$NON-NLS-1$
  }
}
