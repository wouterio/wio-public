package io.wouter.won.entity.structure;

import static com.aoi.won.core.Node.equip;
import static io.wouter.lib.task.TaskStatus.Pending;
import static io.wouter.lib.task.Tasks.newCollectiveTask;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;

import io.wouter.lib.task.CTask;
import io.wouter.lib.task.Task;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonStructure;
import io.wouter.won.entity.structure.handler.WonProductionHandler;
import io.wouter.won.entity.task.production.NewProductionTask;
import io.wouter.won.entity.task.production.WonProductionTask;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.object.structure.WonProducerTemplate;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProducer<N extends WonEntity, B extends WonEntity> extends WonStructure<N, B> {

  private static final long serialVersionUID = 827520106854102379L;



  private final CTask<N, N> ctask;
  private final List<WonProductionHandler> handlers = new ArrayList<>();
  private final List<WonProductionHandler> uhandlers = unmodifiableList(handlers);



  public WonProducer(final WonProducerTemplate pt, final Node<WonItem, N, B> node, final Link<WonItem, B> interlink, final NewProductionTask<N, B> newtask, final Function<N, Object> getagentid) {
    super(pt, node);

    final Collection<Task<N, N>> tasks = new LinkedList<>();
    for (final WonProduction p : pt.getProductions()) {
      final WonProductionTask<N> task = newtask.apply(node, p);
      final WonProductionHandler handler = new WonProductionHandler(task);
      tasks.add(task);
      handlers.add(handler);
    }

    final int minworkers = pt.getMinWorkers();
    final int maxworkers = pt.getMaxWorkers();
    ctask = newCollectiveTask(tasks, getagentid, minworkers, maxworkers, Pending);

    equip(node, uhandlers, interlink, interlink);
  }



  public CTask<N, N> getTask() {
    return ctask;
  }

  public int getWorkers() {
    return ctask.getAgents();
  }

  public int getMinWorkers() {
    return ctask.getMinAgents();
  }

  public int getMaxWorkers() {
    return ctask.getMaxAgents();
  }

  public List<WonProductionHandler> getHandlers() {
    return uhandlers;
  }



  @Override
  public void dispose() {
    super.dispose();
    handlers.forEach(WonProductionHandler::dispose);
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("workers", getWorkers()) //$NON-NLS-1$
        .append("minworkers", getMinWorkers()) //$NON-NLS-1$
        .append("maxworkers", getMaxWorkers()) //$NON-NLS-1$
        .appendAll("handlers", uhandlers, WonProductionHandler::toString); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .separator() //
        .field("workers").valueInt(this::getWorkers) //$NON-NLS-1$
        .field("minworkers").valueInt(this::getMinWorkers) //$NON-NLS-1$
        .field("maxworkers").valueInt(this::getMaxWorkers) //$NON-NLS-1$
        .separator() //
        .collect(getStructureDelegate()) //
        .separator() //
        .child("handlers").collectAll(uhandlers).parent(); //$NON-NLS-1$
  }
}
