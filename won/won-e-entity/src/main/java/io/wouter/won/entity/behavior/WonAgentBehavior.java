package io.wouter.won.entity.behavior;

import io.wouter.lib.task.CTask;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.func.PerformDelta;
import io.wouter.won.util.string.WonInfoBuilder;

public interface WonAgentBehavior<N, B> extends PerformDelta, WonInfoBuilder.Collect, Dispose {

  boolean hasTask();

  CTask<N, N> getTask();

  void setTask(final CTask<N, N> task);
}
