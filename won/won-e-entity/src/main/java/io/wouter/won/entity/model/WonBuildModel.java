package io.wouter.won.entity.model;

import io.wouter.won.entity.WonAgent;
import io.wouter.won.entity.WonEntity;
import io.wouter.won.entity.WonTransporter;
import io.wouter.won.object.structure.WonStructureTemplate;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.util.func.PerformRound;

public interface WonBuildModel<N extends WonEntity, B extends WonEntity> extends PerformRound {

  N getPlayer();



  WonAgent<N, B> buildAgent(N reference);

  WonTransporter<N, B> buildTransporter(N reference, WonTransporterTemplate template);

  WonEntity buildStructure(N reference, WonStructureTemplate template);



  void addBuildListener(WonBuildListener<N, B> l);

  void removeBuildListener(WonBuildListener<N, B> l);
}
