package io.wouter.won.entity;

import static io.wouter.lib.rsq.RSQueue.newRSQueue;
import static io.wouter.lib.task.Tasks.newCollectiveTaskDistribQueue;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import io.wouter.lib.rsq.RSQueue;
import io.wouter.lib.task.CTask;

public class WonAgents<N extends WonEntity, B extends WonEntity> implements Iterable<WonAgent<N, B>> {

  private final Queue<WonAgent<N, B>> agents = new LinkedList<>();
  private final RSQueue<WonAgent<N, B>> queue = newRSQueue(a -> a.isDisposed(), a -> a.getBehavior().hasTask());
  private final RSQueue<CTask<N, N>> tasks = newCollectiveTaskDistribQueue(new ArrayDeque<>());



  public WonAgents() { /* nothing */ }



  public boolean isEmpty() {
    return agents.isEmpty();
  }

  public int getSize() {
    return agents.size();
  }



  public boolean add(final WonAgent<N, B> agent) {
    if (!agents.add(agent))
      return false;
    queue.add(agent);
    return true;
  }

  public boolean remove(final WonAgent<N, B> agent) {
    if (!agents.remove(agent))
      return false;
    queue.remove(agent);
    return true;
  }

  public WonAgent<N, B> remove() {
    final WonAgent<N, B> removed = agents.remove();
    queue.remove(removed);
    return removed;
  }



  public boolean offer(final CTask<N, N> task) {
    return tasks.offer(task);
  }

  public boolean remove(final CTask<N, N> task) {
    return tasks.remove(task);
  }



  public void distribute() {
    while (true) {
      final WonAgent<N, B> agent = queue.findAndPeek();
      if (agent == null)
        break;

      final CTask<N, N> ctask = tasks.findAndPeek();
      if (ctask == null)
        break;

      agent.getBehavior().setTask(ctask);

      queue.shift();
      if (ctask.hasMinAgents())
        tasks.shift();
    }
  }



  @Override
  public Iterator<WonAgent<N, B>> iterator() {
    return agents.iterator();
  }
}
