package io.wouter.won.entity;

import java.util.Collection;
import java.util.LinkedList;

import com.aoi.won.core.Link;
import com.aoi.won.core.link.PullHandler;
import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushHandler;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.link.SendTransfer;

import io.wouter.won.entity.structure.WonDistributor;
import io.wouter.won.object.product.WonItem;

public class WonDistributors<N extends WonEntity, B extends WonEntity> implements Link<WonItem, B> {

  private final Collection<WonDistributor<N, B>> distributors = new LinkedList<>();
  private final Link<WonItem, B> distribpulllink = Link.newInterLink(SendTransfer.passive());
  private final Link<WonItem, B> distribpushlink = Link.newInterLink(SendTransfer.passive());



  @Override
  public PushHandler<WonItem, B> ensurePushHandler(final WonItem item) {
    return distribpulllink.ensurePushHandler(item);
  }

  @Override
  public PullHandler<WonItem, B> ensurePullHandler(final WonItem item) {
    return distribpushlink.ensurePullHandler(item);
  }



  @Override
  public boolean push(final PushRequest<WonItem, B> request, final SendTransfer<WonItem, B> sendtransfer) {
    return distribpulllink.push(request, sendtransfer);
  }

  @Override
  public boolean pull(final PullRequest<WonItem, B> request, final SendTransfer<WonItem, B> sendtransfer) {
    return distribpushlink.pull(request, sendtransfer);
  }



  public boolean addDistributor(final WonDistributor<N, B> distributor) {
    if (!distributors.add(distributor))
      return false;
    distributor.setLink(distribpulllink, distribpushlink);
    return true;
  }

  public boolean removeDistributor(final WonDistributor<N, B> distributor) {
    if (!distributors.remove(distributor))
      return false;
    distributor.setLink(null, null);
    return true;
  }
}
