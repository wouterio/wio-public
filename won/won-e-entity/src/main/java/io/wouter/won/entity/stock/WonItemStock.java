package io.wouter.won.entity.stock;

import static java.lang.Math.min;
import static java.util.Objects.requireNonNull;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.func.GetItem;
import io.wouter.won.util.stock.AccountStock;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonItemStock extends WonEntity implements GetItem<WonItem> {

  public static int transfer(final WonItemStock src, final WonItemStock tar, final int amount, final int value) {
    final int n = min(amount, min(src.getCount(), tar.getSpace()));
    if (n < 1)
      return 0;

    src.remove(n);
    tar.add(n, value);
    return n;
  }



  private static final long serialVersionUID = 2193940978742098999L;



  private final WonItemPrice price;
  private final AccountStock account;

  private int usesleft = 0;



  public WonItemStock(final WonItem item, final AccountStock account) {
    super(item.getIndex(), item.getType());
    price = new WonItemPrice(item);
    this.account = requireNonNull(account);
  }



  @Override
  public WonItem getItem() {
    return price.getItem();
  }

  public int getPrice() {
    return price.getPrice();
  }

  public int getCost() {
    return price.getCost();
  }



  public boolean isEmpty() {
    return account.isEmpty() && usesleft < 1;
  }

  public boolean isFull() {
    return account.isFull();
  }

  public boolean hasLoCount() {
    return account.hasLoCount();
  }

  public boolean hasLoSpace() {
    return account.hasLoSpace();
  }

  public int getCount() {
    return account.getCount();
  }

  public int getSpace() {
    return account.getSpace();
  }



  public int getCapacity() {
    return account.getCapacity();
  }

  public int getLoCount() {
    return account.getLoCount();
  }

  public int getLoSpace() {
    return account.getLoSpace();
  }



  public void setCapacity(final int cap) {
    account.setCapacity(cap);
  }

  public void setLoCount(final int locount) {
    account.setLoCount(locount);
  }

  public void setLoSpace(final int lospace) {
    account.setLoSpace(lospace);
  }



  public void add(final int n, final float newprice) {
    account.change(n);
    price.supply(newprice, n);
  }

  public void remove(final int n) {
    account.change(-n);
    price.demand(n);
  }



  public int supply(final int items, final float newprice) {
    final int n = Math.min(items, getSpace());
    add(n, newprice);
    return n;
  }

  public int demand(final int items) {
    final int n = Math.min(items, getCount());
    remove(n);
    return n;
  }



  public int produce(final int items, final float newprice) {
    return supply(items, newprice);
  }

  public int consume(final int uses) {
    if (usesleft < uses) {
      final int itemdurability = getItem().getDurability();
      final int itemsrequired = (uses - usesleft) / itemdurability + 1;
      final int itemsdemanded = demand(itemsrequired);
      usesleft += itemsdemanded * itemdurability;
    }
    final int usesdone = Math.min(usesleft, uses);
    usesleft -= usesdone;
    return usesdone;
  }



  @Override
  public void dispose() {
    super.dispose();
    usesleft = 0;
    account.clear();
    price.dispose();
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("item", getItem()) //$NON-NLS-1$
        .append("count", "" + getCount() + '/' + getCapacity() + '-' + usesleft) //$NON-NLS-1$ //$NON-NLS-2$
        .append("price", getPrice()) //$NON-NLS-1$
        .append("cost", getCost()); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("item").link(getItem(), WonItem::getType) //$NON-NLS-1$
        .field("count").valueInt(this::getCount) //$NON-NLS-1$
        .field("capacity").valueInt(this::getCapacity) //$NON-NLS-1$
        .field("price").valueInt(this::getPrice) //$NON-NLS-1$
        .field("cost").valueInt(this::getCost); //$NON-NLS-1$
  }



  // private Object writeReplace() {
  // return new ItemSerial(this);
  // }
  //
  // @SuppressWarnings("static-method")
  // private void readObject(@SuppressWarnings("unused") final ObjectInputStream s) throws InvalidObjectException {
  // throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  // }
}
