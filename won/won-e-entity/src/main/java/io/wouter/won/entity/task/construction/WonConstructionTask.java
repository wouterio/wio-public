package io.wouter.won.entity.task.construction;

import io.wouter.lib.task.Task;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.string.WonInfoBuilder;

public interface WonConstructionTask<N> extends Task<N, N>, WonInfoBuilder.Collect, Dispose { // TODO extends WonTask interface

  WonConstruction getConstruction();



  float getTime();

  float getTimeLeft();
}
