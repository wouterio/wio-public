package io.wouter.won.entity.behavior;

import com.aoi.won.core.link.Transfer;

import io.wouter.lib.task.Task;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.string.WonInfoBuilder;

public interface WonTransporterBehavior<N, B> extends Task<N, N>, WonInfoBuilder.Collect, Dispose {

  boolean hasTransfer();

  Transfer<WonItem, B> getTransfer();

  void setTransfer(final Transfer<WonItem, B> transfer);
}
