package io.wouter.won.entity.stock;

import static java.lang.Math.round;

import io.wouter.won.entity.WonEntity;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonItemPrice extends WonEntity {

  private static final long serialVersionUID = 4466996698315066920L;



  private final WonItem item;

  private int count = 0;
  private float price = 0;
  private float cost = 0;
  private int balance = 0;



  WonItemPrice(final WonItem item) {
    super(item.getIndex(), item.getType());
    this.item = item;
  }



  public WonItem getItem() {
    return item;
  }



  public int getCount() {
    return count;
  }

  public int getPrice() {
    return round(price);
  }

  public int getCost() {
    return round(cost);
  }

  public int getBalance() {
    return balance;
  }



  public void supply(final float prodvalue) {
    ++count;
    ++balance;
    price += (prodvalue - price) / count; // moving average, count increment already done above
    cost = price / item.getDurability();
  }

  public void supply(final float prodvalue, final int amount) {
    count += amount;
    balance += amount;
    price += amount * (prodvalue - price) / count; // moving average, count increment already done above
    cost = price / item.getDurability();
  }

  public void demand() {
    --count;
    --balance;
    // best guess for removed instance value equals average value:
    // value -= (value - value) / count -> no change, count decrement already done above
  }

  public void demand(final int amount) {
    count -= amount;
    balance -= amount;
    // best guess for removed instances value equals average value:
    // value -= amount * (value - value) / count; -> no change, count decrement already done above
  }

  // public void balance() {
  // value -= BalanceFtr * balance;
  // cost = value / item.getDurability();
  // balance -= Integer.signum(balance); // supply/demand effect slowly fades out
  // }



  @Override
  public void dispose() {
    super.dispose();
    count = 0;
    price = 0f;
    cost = 0f;
    balance = 0;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("count", getCount()) //$NON-NLS-1$
        .append("price", getPrice()) //$NON-NLS-1$
        .append("cost", getCost()) //$NON-NLS-1$
        .append("balance", getBalance()); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("item").link(item, WonItem::getType) //$NON-NLS-1$ //
        .field("count").valueInt(this::getCount) //$NON-NLS-1$
        .field("price").valueInt(this::getPrice) //$NON-NLS-1$
        .field("cost").valueInt(this::getCost) //$NON-NLS-1$
        .field("balance").valueInt(this::getBalance); //$NON-NLS-1$
  }
}
