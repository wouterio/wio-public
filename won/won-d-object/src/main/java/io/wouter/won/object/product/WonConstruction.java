package io.wouter.won.object.product;

import static java.util.Objects.requireNonNull;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import io.wouter.won.object.WonObject;
import io.wouter.won.object.WonObjects;
import io.wouter.won.object.structure.WonStructureTemplate;
import io.wouter.won.util.counter.CounterC;
import io.wouter.won.util.props.Properties;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonConstruction extends WonObject {

  private static final long serialVersionUID = -8611222463441682860L;



  private final CounterC<WonItem> input;
  private final WonStructureTemplate template;
  private final float duration;



  public WonConstruction(final Properties props) {
    super(props);
    input = props.get("input", CounterC.class); //$NON-NLS-1$
    template = props.get("template", WonStructureTemplate.class); //$NON-NLS-1$
    duration = props.getInt("duration"); //$NON-NLS-1$
  }

  public WonConstruction(final int index, final String type, final CounterC<WonItem> input, final WonStructureTemplate template, final float duration) {
    super(index, type);
    this.input = requireNonNull(input);
    this.template = requireNonNull(template);
    this.duration = duration;
  }



  public CounterC<WonItem> getInput() {
    return input;
  }

  public WonStructureTemplate getStructureTemplate() {
    return template;
  }

  public float getDuration() {
    return duration;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .appendAll("input", input.getKeys(), WonItem::getType) // //$NON-NLS-1$
        .append("template", template, WonStructureTemplate::getType) // //$NON-NLS-1$
        .append("duration", duration); // //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .field("input").linkAll(input.getKeys(), WonItem::getType) // //$NON-NLS-1$
        .field("template").link(template, WonStructureTemplate::getType) // //$NON-NLS-1$
        .field("duration").value(duration); // //$NON-NLS-1$
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(@SuppressWarnings("unused") final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  //



  private static class Serial implements Serializable {

    private static final long serialVersionUID = -6885362453841557219L;



    private final String type;



    Serial(final WonConstruction obj) {
      type = obj.getType();
    }



    private Object readResolve() {
      return WonObjects.getConstruction(type);
    }
  }
}
