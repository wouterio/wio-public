package io.wouter.won.object.structure;

import io.wouter.won.object.WonObject;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public abstract class WonStructureTemplate extends WonObject {

  private static final long serialVersionUID = -1042045259337471963L;



  private final int minworkers;
  private final int maxworkers;
  private final int cap;



  protected WonStructureTemplate(final int index, final String type, final int minworkers, final int maxworkers, final int cap) {
    super(index, type);
    this.minworkers = minworkers;
    this.maxworkers = maxworkers;
    this.cap = cap;
  }



  public final int getMinWorkers() {
    return minworkers;
  }

  public final int getMaxWorkers() {
    return maxworkers;
  }

  public final int getCapacity() {
    return cap;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("minworkers", minworkers) //$NON-NLS-1$
        .append("maxworkers", maxworkers) //$NON-NLS-1$
        .append("cap", cap); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .field("type").value(getType()) //$NON-NLS-1$ //
        .field("minworkers").value(minworkers) //$NON-NLS-1$
        .field("maxworkers").value(maxworkers) //$NON-NLS-1$
        .field("cap").value(cap); //$NON-NLS-1$
  }
}
