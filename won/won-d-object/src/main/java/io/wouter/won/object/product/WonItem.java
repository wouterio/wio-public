package io.wouter.won.object.product;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import io.wouter.won.object.WonObject;
import io.wouter.won.object.WonObjects;
import io.wouter.won.util.props.Properties;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonItem extends WonObject {

  private static final long serialVersionUID = -7942536724649314602L;



  private final int durability;



  public WonItem(final Properties props) {
    super(props);
    durability = props.getInt("durability"); //$NON-NLS-1$
  }

  public WonItem(final int index, final String type, final int durability) {
    super(index, type);
    this.durability = durability;
  }



  public int getDurability() {
    return durability;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("durability", durability); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .field("durability").value(durability); //$NON-NLS-1$
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(@SuppressWarnings("unused") final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  //



  private static class Serial implements Serializable {

    private static final long serialVersionUID = 2905033486702253444L;



    private final String type;



    Serial(final WonItem obj) {
      type = obj.getType();
    }



    private Object readResolve() {
      return WonObjects.getItem(type);
    }
  }
}
