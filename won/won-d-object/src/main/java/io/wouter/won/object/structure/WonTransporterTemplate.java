package io.wouter.won.object.structure;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import io.wouter.won.object.WonObjects;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonTransporterTemplate extends WonStructureTemplate {

  private static final long serialVersionUID = 8328085155684863580L;



  public WonTransporterTemplate(final int index, final String type, final int minworkers, final int maxworkers, final int cap) {
    super(index, type, minworkers, maxworkers, cap);
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder();
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return super.collect(info);
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  private static class Serial implements Serializable {

    private static final long serialVersionUID = -6270470631062111224L;



    private final String type;



    Serial(final WonTransporterTemplate obj) {
      type = obj.getType();
    }



    private Object readResolve() {
      return WonObjects.getStructure(type);
    }
  }
}
