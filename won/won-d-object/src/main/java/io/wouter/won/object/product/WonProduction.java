package io.wouter.won.object.product;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Optional;

import io.wouter.won.object.WonObject;
import io.wouter.won.object.WonObjects;
import io.wouter.won.object.structure.WonStructureTemplate;
import io.wouter.won.util.counter.CounterC;
import io.wouter.won.util.props.Properties;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProduction extends WonObject {

  private static final long serialVersionUID = 2786326194263020804L;



  private final CounterC<WonItem> input;
  private final CounterC<WonItem> output;
  private final Optional<WonResource> resource;
  private final Optional<WonStructureTemplate> template;
  private final float duration;



  public WonProduction(final Properties props) {
    super(props);
    input = props.get("input", CounterC.class); //$NON-NLS-1$
    output = props.get("output", CounterC.class); //$NON-NLS-1$
    resource = ofNullable(props.get("resource", WonResource.class)); //$NON-NLS-1$
    template = ofNullable(props.get("template", WonStructureTemplate.class)); //$NON-NLS-1$
    duration = props.getInt("duration"); //$NON-NLS-1$
  }

  public WonProduction(final int index, final String type, final CounterC<WonItem> input, final CounterC<WonItem> output, final WonResource resource, final WonStructureTemplate template, final float duration) {
    super(index, type);
    this.input = requireNonNull(input);
    this.output = requireNonNull(output);
    this.resource = ofNullable(resource);
    this.template = ofNullable(template);
    this.duration = duration;
  }



  public CounterC<WonItem> getInput() {
    return input;
  }

  public CounterC<WonItem> getOutput() {
    return output;
  }

  public Optional<WonResource> getResource() {
    return resource;
  }

  public Optional<WonStructureTemplate> getStructureTemplate() {
    return template;
  }

  public float getDuration() {
    return duration;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .appendAll("input", input.getKeys(), WonItem::getType) //$NON-NLS-1$
        .appendAll("output", output.getKeys(), WonItem::getType) //$NON-NLS-1$
        .appendOptional("resource", resource, WonResource::getType) //$NON-NLS-1$
        .appendOptional("template", template, WonStructureTemplate::getType) //$NON-NLS-1$
        .append("duration", duration); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .field("input").linkAll(input.getKeys(), WonItem::getType) //$NON-NLS-1$
        .field("output").linkAll(output.getKeys(), WonItem::getType) //$NON-NLS-1$
        .field("resource").linkOptional(resource, WonResource::getType) //$NON-NLS-1$
        .field("template").linkOptional(template, WonStructureTemplate::getType) //$NON-NLS-1$
        .field("duration").value(duration); //$NON-NLS-1$
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(@SuppressWarnings("unused") final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  //



  private static class Serial implements Serializable {

    private static final long serialVersionUID = -3522215210257275580L;



    private final String type;



    Serial(final WonProduction obj) {
      type = obj.getType();
    }



    private Object readResolve() {
      return WonObjects.getProduction(type);
    }
  }
}
