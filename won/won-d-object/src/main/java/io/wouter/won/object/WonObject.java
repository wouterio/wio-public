package io.wouter.won.object;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Comparator;

import io.wouter.won.util.props.Properties;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public abstract class WonObject implements WonInfoBuilder.Collect, Serializable {

  private static final long serialVersionUID = -1239136482297626708L;



  public static final Comparator<WonObject> CompareIndex = (a, b) -> Integer.compare(a.getIndex(), b.getIndex());
  public static final Comparator<WonObject> CompareType = (a, b) -> a.getType().compareTo(b.getType());



  private final int index;
  private final String type;



  protected WonObject(final Properties props) {
    index = props.getInt("index"); //$NON-NLS-1$
    type = props.getString("type"); //$NON-NLS-1$
  }

  protected WonObject(final int index, final String type) {
    this.index = index;
    this.type = requireNonNull(type);
  }

  protected WonObject(final WonObject reference) {
    index = reference.index;
    type = reference.type;
  }



  public final int getIndex() {
    return index;
  }

  public final String getType() {
    return type;
  }



  @Override
  public final String toString() {
    return toStringBuilder().toString();
  }

  protected WonStringBuilder toStringBuilder() {
    return new WonStringBuilder(getClass(), index, type);
  }



  @Override
  public final boolean equals(final Object other) {
    return super.equals(other); // Only one instance per Item etc. Structures and Agents never equal each other.
  }

  @Override
  public final int hashCode() {
    return index;
  }
}
