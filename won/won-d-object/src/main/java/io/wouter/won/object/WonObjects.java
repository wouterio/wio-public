package io.wouter.won.object;

import static io.wouter.won.object.WonObject.CompareIndex;
import static io.wouter.won.util.counter.Counters.newCounter;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import static java.util.Collections.unmodifiableCollection;
import static java.util.stream.Collectors.toList;

import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Function;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.object.product.WonResource;
import io.wouter.won.object.structure.WonConstructorTemplate;
import io.wouter.won.object.structure.WonDistributorTemplate;
import io.wouter.won.object.structure.WonProducerTemplate;
import io.wouter.won.object.structure.WonStructureTemplate;
import io.wouter.won.object.structure.WonTransporterTemplate;
import io.wouter.won.util.counter.CounterM;
import io.wouter.won.util.enums.LinkType;

public final class WonObjects {

  private static class Holder {

    static final WonObjects Instance = new WonObjects();
  }



  public static WonResource getResource(final String type) {
    return getOrThrow(Holder.Instance.resourcesmap, type);
  }

  public static WonItem getItem(final String type) {
    return getOrThrow(Holder.Instance.itemsmap, type);
  }

  public static WonConstruction getConstruction(final String type) {
    return getOrThrow(Holder.Instance.constructionsmap, type);
  }

  public static WonProduction getProduction(final String type) {
    return getOrThrow(Holder.Instance.productionsmap, type);
  }

  public static WonConstructorTemplate getConstructor(final String type) {
    return getOrThrow(Holder.Instance.constructorsmap, type);
  }

  public static WonProducerTemplate getProducer(final String type) {
    return getOrThrow(Holder.Instance.producersmap, type);
  }

  public static WonStructureTemplate getTransporter(final String type) {
    return getOrThrow(Holder.Instance.transportersmap, type);
  }

  public static WonStructureTemplate getDistributor(final String type) {
    return getOrThrow(Holder.Instance.distributorsmap, type);
  }

  public static WonStructureTemplate getStructure(final String type) {
    return getOrThrow(Holder.Instance.structuresmap, type);
  }



  public static Collection<WonResource> getResources() {
    return Holder.Instance.resources;
  }

  public static Collection<WonItem> getItems() {
    return Holder.Instance.items;
  }

  public static Collection<WonConstruction> getConstructions() {
    return Holder.Instance.constructions;
  }

  public static Collection<WonProduction> getProductions() {
    return Holder.Instance.productions;
  }

  public static Collection<WonConstructorTemplate> getConstructors() {
    return Holder.Instance.constructors;
  }

  public static Collection<WonProducerTemplate> getProducers() {
    return Holder.Instance.producers;
  }

  public static Collection<WonTransporterTemplate> getTransporters() {
    return Holder.Instance.transporters;
  }

  public static Collection<WonDistributorTemplate> getDistributors() {
    return Holder.Instance.distributors;
  }

  public static Collection<WonStructureTemplate> getStructures() {
    return Holder.Instance.structures;
  }



  private static <T> T getOrThrow(final Map<String, T> map, final String key) {
    final T value = map.get(key);
    if (value == null)
      throw new NoSuchElementException(key);
    return value;
  }



  private final Map<String, WonResource> resourcesmap = new HashMap<>();
  private final Map<String, WonItem> itemsmap = new HashMap<>();
  private final Map<String, WonConstruction> constructionsmap = new HashMap<>();
  private final Map<String, WonProduction> productionsmap = new HashMap<>();
  private final Map<String, WonConstructorTemplate> constructorsmap = new HashMap<>();
  private final Map<String, WonProducerTemplate> producersmap = new HashMap<>();
  private final Map<String, WonTransporterTemplate> transportersmap = new HashMap<>();
  private final Map<String, WonDistributorTemplate> distributorsmap = new HashMap<>();
  private final Map<String, WonStructureTemplate> structuresmap = new HashMap<>();

  private final Collection<WonResource> resources;
  private final Collection<WonItem> items;
  private final Collection<WonConstruction> constructions;
  private final Collection<WonProduction> productions;
  private final Collection<WonConstructorTemplate> constructors;
  private final Collection<WonProducerTemplate> producers;
  private final Collection<WonTransporterTemplate> transporters;
  private final Collection<WonDistributorTemplate> distributors;
  private final Collection<WonStructureTemplate> structures;



  WonObjects() {
    final XmlReader reader = new XmlReader();
    resourcesmap.putAll(reader.resources);
    itemsmap.putAll(reader.items);
    constructionsmap.putAll(reader.constructions);
    productionsmap.putAll(reader.productions);
    constructorsmap.putAll(reader.constructors);
    producersmap.putAll(reader.producers);
    transportersmap.putAll(reader.transporters);
    distributorsmap.putAll(reader.distributors);
    structuresmap.putAll(reader.constructors);
    structuresmap.putAll(reader.producers);
    structuresmap.putAll(reader.transporters);
    structuresmap.putAll(reader.distributors);

    resources = unmodifiableCollection(resourcesmap.values().stream().sorted(CompareIndex).collect(toList()));
    items = unmodifiableCollection(itemsmap.values().stream().sorted(CompareIndex).collect(toList()));
    constructions = unmodifiableCollection(constructionsmap.values().stream().sorted(CompareIndex).collect(toList()));
    productions = unmodifiableCollection(productionsmap.values().stream().sorted(CompareIndex).collect(toList()));
    constructors = unmodifiableCollection(constructorsmap.values().stream().sorted(CompareIndex).collect(toList()));
    producers = unmodifiableCollection(producersmap.values().stream().sorted(CompareIndex).collect(toList()));
    transporters = unmodifiableCollection(transportersmap.values().stream().sorted(CompareIndex).collect(toList()));
    distributors = unmodifiableCollection(distributorsmap.values().stream().sorted(CompareIndex).collect(toList()));
    structures = unmodifiableCollection(structuresmap.values().stream().sorted(CompareIndex).collect(toList()));
  }



  //



  private static class XmlReader {

    private static final String Type = "type"; //$NON-NLS-1$
    private static final String Refill = "refill"; //$NON-NLS-1$
    private static final String Extract = "extract"; //$NON-NLS-1$
    private static final String Durability = "durability"; //$NON-NLS-1$
    private static final String Duration = "duration"; //$NON-NLS-1$
    private static final String Amount = "amount"; //$NON-NLS-1$
    private static final String Inputs = "inputs"; //$NON-NLS-1$
    private static final String Input = "input"; //$NON-NLS-1$
    private static final String Outputs = "outputs"; //$NON-NLS-1$
    private static final String Output = "output"; //$NON-NLS-1$
    private static final String Items = "items"; //$NON-NLS-1$
    private static final String Item = "item"; //$NON-NLS-1$
    private static final String Materials = "materials"; //$NON-NLS-1$
    private static final String Material = "material"; //$NON-NLS-1$
    private static final String Productions = "productions"; //$NON-NLS-1$
    private static final String Production = "production"; //$NON-NLS-1$
    private static final String Resource = "resource"; //$NON-NLS-1$
    private static final String Construction = "construction"; //$NON-NLS-1$
    private static final String Structure = "structure"; //$NON-NLS-1$
    private static final String MaxWorkers = "maxworkers"; //$NON-NLS-1$
    private static final String MinWorkers = "minworkers"; //$NON-NLS-1$
    private static final String Capacity = "capacity"; //$NON-NLS-1$
    private static final String Pull = "pull"; //$NON-NLS-1$
    private static final String Push = "push"; //$NON-NLS-1$



    private final Element eresources = root("resources.xml"); //$NON-NLS-1$
    private final Element eitems = root("items.xml"); //$NON-NLS-1$
    private final Element eproductions = root("productions.xml"); //$NON-NLS-1$
    private final Element etransporters = root("transporters.xml"); //$NON-NLS-1$
    private final Element edistributors = root("distributors.xml"); //$NON-NLS-1$
    private final Element eproducers = root("producers.xml"); //$NON-NLS-1$
    private final Element econstructions = root("constructions.xml"); //$NON-NLS-1$
    private final Element econstructors = root("constructors.xml"); //$NON-NLS-1$

    private final Map<String, WonResource> resources = new HashMap<>();
    private final Map<String, WonItem> items = new HashMap<>();
    private final Map<String, WonProduction> productions = new HashMap<>();
    private final Map<String, WonTransporterTemplate> transporters = new HashMap<>();
    private final Map<String, WonDistributorTemplate> distributors = new HashMap<>();
    private final Map<String, WonProducerTemplate> producers = new HashMap<>();
    private final Map<String, WonConstruction> constructions = new HashMap<>();
    private final Map<String, WonConstructorTemplate> constructors = new HashMap<>();



    private int indexer = 0;



    XmlReader() {
      eresources.getChildren().forEach(e -> ensure(e, this::readResource, resources));
      eitems.getChildren().forEach(e -> ensure(e, this::readItem, items));
      eproductions.getChildren().forEach(e -> ensure(e, this::readProduction, productions));
      etransporters.getChildren().forEach(e -> ensure(e, this::readTransporter, transporters));
      edistributors.getChildren().forEach(e -> ensure(e, this::readDistributor, distributors));
      eproducers.getChildren().forEach(e -> ensure(e, this::readProducer, producers));
      econstructions.getChildren().forEach(e -> ensure(e, this::readConstruction, constructions));
      econstructors.getChildren().forEach(e -> ensure(e, this::readConstructor, constructors));
    }



    private WonResource readResource(final Element e) {
      final String type = readType(e);
      final float extractftr = readFloat(e, Extract);
      final float refillftr = readFloat(e, Refill);
      return new WonResource(indexer++, type, extractftr, refillftr);
    }



    private WonItem readItem(final Element e) {
      final String type = readType(e);
      final int durability = readInt(e, Durability);
      return new WonItem(indexer++, type, durability);
    }



    private WonProduction readProduction(final Element e) {
      final String type = readType(e);

      final CounterM<WonItem> input = newCounter(CompareIndex);
      for (final Element ei : e.getChild(Inputs).getChildren(Input)) {
        final String itype = readType(ei);
        final WonItem i = ensureItem(itype);
        final int amount = readInt(ei, Amount);
        input.ensure(i).set(amount);
      }

      final CounterM<WonItem> output = newCounter(CompareIndex);
      for (final Element eo : e.getChild(Outputs).getChildren(Output)) {
        final String otype = readType(eo);
        final WonItem o = ensureItem(otype);
        final int amount = readInt(eo, Amount);
        output.ensure(o).set(amount);
      }

      final String resourcetype = e.getChildTextTrim(Resource);
      final WonResource resource = resourcetype != null ?
          ensureResource(resourcetype) :
          null;

      final String structuretype = e.getChildTextTrim(Structure);
      final WonStructureTemplate structure = structuretype != null ?
          ensureStructure(structuretype) :
          null;

      final int duration = readInt(e, Duration);
      return new WonProduction(indexer++, type, input, output, resource, structure, duration);
    }



    private WonProducerTemplate readProducer(final Element e) {
      final String type = readType(e);

      final List<WonProduction> plist = new LinkedList<>();
      for (final Element ep : e.getChild(Productions).getChildren(Production)) {
        final String ptype = ep.getTextTrim();
        final WonProduction p = ensureProduction(ptype);
        plist.add(p);
      }

      final int cap = readInt(e, Capacity);
      final int minworkers = readInt(e, MinWorkers);
      final int maxworkers = readInt(e, MaxWorkers);
      return new WonProducerTemplate(indexer++, type, plist, minworkers, maxworkers, cap);
    }



    private WonDistributorTemplate readDistributor(final Element e) {
      final String type = readType(e);
      final LinkType pull = readLinkType(e, Pull);
      final LinkType push = readLinkType(e, Push);

      final List<WonItem> ilist = new LinkedList<>();
      for (final Element ei : e.getChild(Items).getChildren(Item)) {
        final String itype = ei.getTextTrim();
        final WonItem i = ensureItem(itype);
        ilist.add(i);
      }

      final int cap = readInt(e, Capacity);
      final int minworkers = readInt(e, MinWorkers);
      final int maxworkers = readInt(e, MaxWorkers);
      return new WonDistributorTemplate(indexer++, type, pull, push, ilist, minworkers, maxworkers, cap);
    }



    private WonTransporterTemplate readTransporter(final Element e) {
      final String type = readType(e);

      final int cap = readInt(e, Capacity);
      final int minworkers = readInt(e, MinWorkers);
      final int maxworkers = readInt(e, MaxWorkers);
      return new WonTransporterTemplate(indexer++, type, minworkers, maxworkers, cap);
    }



    private WonConstruction readConstruction(final Element e) {
      final String type = readType(e);
      final String structuretype = e.getChildTextTrim(Structure);
      final WonStructureTemplate structure = ensureStructure(structuretype);
      final int index = structure.getIndex();

      final CounterM<WonItem> materials = newCounter(CompareIndex);
      for (final Element em : e.getChild(Materials).getChildren(Material)) {
        final String mtype = readType(em);
        final WonItem m = ensureItem(mtype);
        final int amount = readInt(em, Amount);
        materials.ensure(m).set(amount);
      }

      final int duration = readInt(e, Duration);
      return new WonConstruction(index, type, materials, structure, duration);
    }



    private WonConstructorTemplate readConstructor(final Element e) {
      final String type = readType(e);
      final String ctype = e.getChildTextTrim(Construction);
      final WonConstruction c = ensureConstruction(ctype);
      final int index = c.getIndex();

      final int cap = readInt(e, Capacity);
      final int minworkers = readInt(e, MinWorkers);
      final int maxworkers = readInt(e, MaxWorkers);
      return new WonConstructorTemplate(index, type, c, minworkers, maxworkers, cap);
    }



    private static String readType(final Element e) {
      return e.getChildTextTrim(Type);
    }

    private static int readInt(final Element e, final String key) {
      return parseInt(e.getChildTextTrim(key));
    }

    private static float readFloat(final Element e, final String key) {
      return parseFloat(e.getChildTextTrim(key));
    }

    private static LinkType readLinkType(final Element e, final String key) {
      return LinkType.valueOf(e.getChildTextTrim(key));
    }



    private WonResource ensureResource(final String type) {
      return ensure(eresources, type, this::readResource, resources);
    }

    private WonItem ensureItem(final String type) {
      return ensure(eitems, type, this::readItem, items);
    }

    private WonProduction ensureProduction(final String type) {
      return ensure(eproductions, type, this::readProduction, productions);
    }

    private WonConstruction ensureConstruction(final String type) {
      return ensure(econstructions, type, this::readConstruction, constructions);
    }

    private WonStructureTemplate ensureStructure(final String type) {
      if (producers.containsKey(type))
        return producers.get(type);
      if (distributors.containsKey(type))
        return distributors.get(type);
      if (transporters.containsKey(type))
        return transporters.get(type);
      if (constructors.containsKey(type))
        return constructors.get(type);

      Optional<Element> oe = findChild(eproducers, type);
      if (oe.isPresent())
        return ensure(oe.get(), this::readProducer, producers);

      oe = findChild(edistributors, type);
      if (oe.isPresent())
        return ensure(oe.get(), this::readDistributor, distributors);

      oe = findChild(etransporters, type);
      if (oe.isPresent())
        return ensure(oe.get(), this::readTransporter, transporters);

      oe = findChild(econstructors, type);
      if (oe.isPresent())
        return ensure(oe.get(), this::readConstructor, constructors);

      throw new NoSuchElementException(type);
    }



    private static <T> T ensure(final Element e, final Function<Element, T> reader, final Map<String, T> map) {
      final String type = readType(e);
      return map.computeIfAbsent(type, t -> reader.apply(e));
    }

    private static <T> T ensure(final Element parent, final String type, final Function<Element, T> reader, final Map<String, T> map) {
      return map.computeIfAbsent(type, t -> {
        final Optional<Element> oe = findChild(parent, type);
        if (oe.isPresent())
          return reader.apply(oe.get());
        throw new NoSuchElementException(type);
      });
    }



    private static Optional<Element> findChild(final Element parent, final String type) {
      return parent.getChildren().stream().filter(c -> readType(c).equals(type)).findAny();
    }



    private static Element root(final String filename) {
      try (InputStream is = WonObjects.class.getResourceAsStream("/xml/" + filename)) { //$NON-NLS-1$
        return new SAXBuilder().build(is).getDocument().getRootElement();

      } catch (final Exception e) {
        throw new RuntimeException(e);
      }
    }
  }
}
