package io.wouter.won.object.product;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import io.wouter.won.object.WonObject;
import io.wouter.won.object.WonObjects;
import io.wouter.won.util.props.Properties;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonResource extends WonObject {

  private static final long serialVersionUID = 3437820389007194623L;



  private final float extractftr;
  private final float refillftr;



  public WonResource(final Properties props) {
    super(props);
    extractftr = props.getFloat("extractftr"); //$NON-NLS-1$
    refillftr = props.getFloat("refillftr"); //$NON-NLS-1$
  }

  public WonResource(final int index, final String type, final float extractftr, final float refillftr) {
    super(index, type);
    this.extractftr = extractftr;
    this.refillftr = refillftr;
  }



  public float getExtractFactor() {
    return extractftr;
  }

  public float getRefillFactor() {
    return refillftr;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("extractftr", extractftr) //$NON-NLS-1$
        .append("refillftr", refillftr); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return info //
        .header(getType()) //
        .field("extractftr").value(extractftr) //$NON-NLS-1$
        .field("refillftr").value(refillftr); //$NON-NLS-1$
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(@SuppressWarnings("unused") final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  //



  private static class Serial implements Serializable {

    private static final long serialVersionUID = 4892531620097656197L;



    private final String type;



    Serial(final WonResource obj) {
      type = obj.getType();
    }



    private Object readResolve() {
      return WonObjects.getResource(type);
    }
  }
}
