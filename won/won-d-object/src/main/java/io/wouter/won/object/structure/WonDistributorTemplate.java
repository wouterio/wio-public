package io.wouter.won.object.structure;

import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import io.wouter.won.object.WonObjects;
import io.wouter.won.object.product.WonItem;
import io.wouter.won.util.enums.LinkType;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonDistributorTemplate extends WonStructureTemplate {

  private static final long serialVersionUID = 7425820240905199042L;



  private final LinkType pull;
  private final LinkType push;
  private final List<WonItem> items;
  private final int cap;



  public WonDistributorTemplate(final int index, final String type, final LinkType pull, final LinkType push, final Collection<WonItem> items, final int minworkers, final int maxworkers, final int cap) {
    super(index, type, minworkers, maxworkers, cap);
    this.pull = requireNonNull(pull);
    this.push = requireNonNull(push);
    this.items = newItemList(items);
    this.cap = cap;
  }



  private static List<WonItem> newItemList(final Collection<WonItem> items) {
    return unmodifiableList(items.stream().distinct().sorted(CompareIndex).collect(toList()));
  }



  public LinkType getPullType() {
    return pull;
  }

  public LinkType getPushType() {
    return push;
  }

  public List<WonItem> getItems() {
    return items;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("cap", cap) //$NON-NLS-1$
        .append("pull", pull) //$NON-NLS-1$
        .append("push", push) //$NON-NLS-1$
        .appendAll("items", items, WonItem::getType); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return super.collect(info) //
        .field("cap").value(cap) //$NON-NLS-1$
        .field("pull").value(pull) //$NON-NLS-1$
        .field("push").value(push) //$NON-NLS-1$
        .field("items").linkAll(items, WonItem::getType); //$NON-NLS-1$
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  private static class Serial implements Serializable {

    private static final long serialVersionUID = -4415191909143882161L;



    private final String type;



    Serial(final WonDistributorTemplate obj) {
      type = obj.getType();
    }



    private Object readResolve() {
      return WonObjects.getStructure(type);
    }
  }
}
