package io.wouter.won.object.structure;

import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import io.wouter.won.object.WonObjects;
import io.wouter.won.object.product.WonProduction;
import io.wouter.won.object.product.WonResource;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonProducerTemplate extends WonStructureTemplate {

  private static final long serialVersionUID = -3005467363011450094L;



  private final List<WonProduction> productions;
  private final List<WonResource> resources;



  public WonProducerTemplate(final int index, final String type, final Collection<WonProduction> productions, final int minworkers, final int maxworkers, final int cap) {
    super(index, type, minworkers, maxworkers, cap);
    this.productions = newProductionList(productions);
    resources = newResourceList(productions);
  }


  private static List<WonProduction> newProductionList(final Collection<WonProduction> productions) {
    return unmodifiableList(productions.stream().distinct().sorted(CompareIndex).collect(toList()));
  }

  private static List<WonResource> newResourceList(final Collection<WonProduction> productions) {
    return unmodifiableList(productions.stream().map(c -> c.getResource()).filter(Optional::isPresent).map(Optional::get).distinct().sorted(CompareIndex).collect(toList()));
  }



  public List<WonProduction> getProductions() {
    return productions;
  }

  public List<WonResource> getResources() {
    return resources;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .appendAll("resources", resources, WonResource::getType) //$NON-NLS-1$
        .appendAll("productions", productions, WonProduction::getType); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return super.collect(info) //
        .field("resources").linkAll(resources, WonResource::getType) //$NON-NLS-1$
        .field("productions").linkAll(productions, WonProduction::getType); //$NON-NLS-1$
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  private static class Serial implements Serializable {

    private static final long serialVersionUID = 5741204882744947966L;



    private final String type;



    Serial(final WonProducerTemplate obj) {
      type = obj.getType();
    }



    private Object readResolve() {
      return WonObjects.getStructure(type);
    }
  }
}
