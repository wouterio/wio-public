package io.wouter.won.object.structure;

import static java.util.Objects.requireNonNull;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import io.wouter.won.object.WonObjects;
import io.wouter.won.object.product.WonConstruction;
import io.wouter.won.util.string.WonInfoBuilder;
import io.wouter.won.util.string.WonStringBuilder;

public final class WonConstructorTemplate extends WonStructureTemplate {

  private static final long serialVersionUID = 5598855900858437931L;



  private final WonConstruction construction;



  public WonConstructorTemplate(final int index, final String type, final WonConstruction construction, final int minworkers, final int maxworkers, final int cap) {
    super(index, type, minworkers, maxworkers, cap);
    this.construction = requireNonNull(construction);
  }



  public WonConstruction getConstruction() {
    return construction;
  }



  @Override
  protected WonStringBuilder toStringBuilder() {
    return super.toStringBuilder() //
        .append("construction", construction); //$NON-NLS-1$
  }



  @Override
  public WonInfoBuilder collect(final WonInfoBuilder info) {
    return super.collect(info) //
        .field("construction").link(construction, WonConstruction::getType); //$NON-NLS-1$
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  private void readObject(final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  private static class Serial implements Serializable {

    private static final long serialVersionUID = 6360669895667928472L;



    private final String type;



    Serial(final WonConstructorTemplate obj) {
      type = obj.getType();
    }



    private Object readResolve() {
      return WonObjects.getConstructor(type);
    }
  }
}
