package io.wouter.won.object;

import static io.wouter.won.object.WonObjects.getConstructions;
import static io.wouter.won.object.WonObjects.getConstructors;
import static io.wouter.won.object.WonObjects.getDistributors;
import static io.wouter.won.object.WonObjects.getItems;
import static io.wouter.won.object.WonObjects.getProducers;
import static io.wouter.won.object.WonObjects.getProductions;
import static io.wouter.won.object.WonObjects.getResources;
import static io.wouter.won.object.WonObjects.getTransporters;

public class WonObjectsPrint implements Runnable {

  @Override
  public void run() {
    getResources().forEach(r -> System.out.println(r));
    System.out.println();
    getItems().forEach(i -> System.out.println(i));
    System.out.println();
    getProductions().forEach(p -> System.out.println(p));
    System.out.println();
    getProducers().forEach(s -> System.out.println(s));
    System.out.println();
    getTransporters().forEach(s -> System.out.println(s));
    System.out.println();
    getDistributors().forEach(s -> System.out.println(s));
    System.out.println();
    getConstructions().forEach(c -> System.out.println(c));
    System.out.println();
    getConstructors().forEach(c -> System.out.println(c));
  }



  public static void main(final String[] args) {
    new WonObjectsPrint().run();
  }
}
