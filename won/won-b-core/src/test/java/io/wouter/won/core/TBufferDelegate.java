package io.wouter.won.core;


public class TBufferDelegate {

  private int count = 0;



  public int get() {
    return count;
  }

  public void incr(final int amount) {
    count += amount;
  }

  public void decr(final int amount) {
    count -= amount;
  }



  @Override
  public String toString() {
    return getClass().getSimpleName();
  }
}
