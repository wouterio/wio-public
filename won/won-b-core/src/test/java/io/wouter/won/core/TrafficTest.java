package io.wouter.won.core;

import static com.aoi.won.core.Link.newInterLink;
import static com.aoi.won.core.Node.equip;
import static com.aoi.won.core.Node.newNode;
import static io.wouter.lib.task.TaskStatus.Completed;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.testng.Assert.assertEquals;

import java.util.Set;

import org.testng.annotations.Test;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;
import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.link.SendTransfer;
import com.aoi.won.core.node.Buffer;
import com.aoi.won.core.node.Conversion;

public class TrafficTest {

  @Test
  public void testTraffic() {
    final SendTransfer<TItem, TBufferDelegate> sendtransfer = transfer -> {
      final PushRequest<TItem, TBufferDelegate> pushreq = transfer.getPushRequests().poll();
      final PullRequest<TItem, TBufferDelegate> pullreq = transfer.getPullRequests().poll();
      if (pushreq == null || pullreq == null)
        return false;
      final Buffer<TItem, TBufferDelegate> src = pushreq.getSource();
      final Buffer<TItem, TBufferDelegate> tar = pullreq.getTarget();
      final TBufferDelegate srcdel = src.getBufferDelegate();
      final TBufferDelegate tardel = tar.getBufferDelegate();

      final int amount = srcdel.get();
      srcdel.decr(amount);
      tardel.incr(amount);
      src.purged(true);
      tar.filled(true);

      pushreq.setStatus(Completed);
      pullreq.setStatus(Completed);
      return true;
    };



    final Link<TItem, TBufferDelegate> link = newInterLink(sendtransfer);
    final Node<TItem, TNodeDelegate, TBufferDelegate> n1 = newNode(new TNodeDelegate(), i -> new TBufferDelegate(), i -> new TBufferDelegate());
    final Node<TItem, TNodeDelegate, TBufferDelegate> n2 = newNode(new TNodeDelegate(), i -> new TBufferDelegate(), i -> new TBufferDelegate());
    final TItem item = new TItem();



    final Conversion<TItem> c1 = new Conversion<TItem>() {

      private final Buffer<TItem, TBufferDelegate> b1 = n1.getOutput().ensureBuffer(item); // TODO conversion should be defined before node



      private boolean active = false;



      @Override
      public Set<TItem> getInput() {
        return emptySet();
      }

      @Override
      public Set<TItem> getOutput() {
        return singleton(item);
      }



      @Override
      public boolean isActive() {
        return active;
      }

      @Override
      public void setActive(final boolean active) {
        System.out.println(b1.hashCode() + " b1 purged: " + active);
        this.active = active;
        if (active)
          b1.filled(true); // buffer immediatly filled again
      } // no recursive loop because of built-in check
    };



    final Conversion<TItem> c2 = new Conversion<TItem>() {

      private final Buffer<TItem, TBufferDelegate> b2 = n2.getInput().ensureBuffer(item); // TODO conversion should be defined before node



      private boolean active = false;



      @Override
      public Set<TItem> getInput() {
        return singleton(item);
      }

      @Override
      public Set<TItem> getOutput() {
        return emptySet();
      }

      @Override
      public boolean isActive() {
        return active;
      }

      @Override
      public void setActive(final boolean active) {
        System.out.println(b2.hashCode() + " b2 filled: " + active);
        this.active = active;
        if (active)
          b2.purged(true); // buffer immediatly purged
      } // no recursive loop because of built-in check
    };



    equip(n1, singleton(c1), link, link);
    equip(n2, singleton(c2), link, link);
    n1.getOutput().getBuffer(item).getBufferDelegate().incr(1);

    assertEquals(n1.getInput().hasBuffer(item), false);
    assertEquals(n2.getInput().hasBuffer(item), true);
    assertEquals(n1.getOutput().hasBuffer(item), true);
    assertEquals(n2.getOutput().hasBuffer(item), false);
    assertEquals(n1.getOutput().getBuffer(item).getBufferDelegate().get(), 1);
    assertEquals(n2.getInput().getBuffer(item).getBufferDelegate().get(), 0);

    n1.getOutput().getBuffer(item).filled(true);
    n2.getInput().getBuffer(item).purged(true);

    assertEquals(n1.getOutput().getBuffer(item).getBufferDelegate().get(), 0);
    assertEquals(n2.getInput().getBuffer(item).getBufferDelegate().get(), 1);
  }
}
