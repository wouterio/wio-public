package com.aoi.won.core.impl;

import static io.wouter.lib.task.TaskStatus.Expired;
import static io.wouter.lib.task.TaskStatus.Pending;

import java.io.Serializable;

import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.node.Buffer;

import io.wouter.lib.task.TaskStatus;

class BufferRequestI<I, B> implements PushRequest<I, B>, PullRequest<I, B>, Serializable {

  private static final long serialVersionUID = -5821823408944901893L;



  private final Buffer<I, B> buffer;
  private TaskStatus status = Pending;



  BufferRequestI(final Buffer<I, B> buffer) {
    this.buffer = buffer;
  }



  @Override
  public I getItem() {
    return buffer.getItem();
  }

  @Override
  public Buffer<I, B> getSource() {
    return buffer;
  }

  @Override
  public Buffer<I, B> getTarget() {
    return buffer;
  }



  @Override
  public TaskStatus getStatus() {
    return status;
  }

  @Override
  public boolean setStatus(final TaskStatus newstatus) {
    if (newstatus.equals(status))
      return false;
    this.status = status.checkValidTransition(newstatus);
    return true;
  }



  @Override
  public void dispose() {
    setStatus(Expired);
  }



  @Override
  public String toString() {
    return new StringBuilder() //
        .append("<BufferRequest") //$NON-NLS-1$
        .append(" item=").append(getItem()) //$NON-NLS-1$
        .append(" status=").append(status) //$NON-NLS-1$
        .append('>') //
        .toString(); //
  }
}
