package com.aoi.won.core.impl;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;

import com.aoi.won.core.Link;
import com.aoi.won.core.link.PullHandler;
import com.aoi.won.core.link.PushHandler;
import com.aoi.won.core.link.SendTransfer;
import com.aoi.won.core.node.Buffer;

class BufferHandlerI<I, B> implements PushHandler<I, B>, PullHandler<I, B>, Serializable {

  private static final long serialVersionUID = -8283379088163958808L;



  private final Link<I, B> link;
  private final SendTransfer<I, B> sendtransfer;



  private BufferRequestI<I, B> request = null;



  BufferHandlerI(final Link<I, B> link, final SendTransfer<I, B> sendtransfer) {
    this.link = requireNonNull(link);
    this.sendtransfer = requireNonNull(sendtransfer);
  }



  @Override
  public boolean push(final Buffer<I, B> buffer, final boolean push) {
    return push ?
        ensurePushRequest(buffer) :
        disposeRequest();
  }

  @Override
  public boolean pull(final Buffer<I, B> buffer, final boolean pull) {
    return pull ?
        ensurePullRequest(buffer) :
        disposeRequest();
  }



  @Override
  public void dispose() {
    disposeRequest();
  }



  @Override
  public String toString() {
    return new StringBuilder() //
        .append('<') //
        .append("BufferHandler") //$NON-NLS-1$
        .append(" request=").append(request) //$NON-NLS-1$
        .append('>') //
        .toString(); //
  }



  private boolean hasRequest() {
    if (request == null)
      return false;
    if (request.isPresent())
      return true;
    request = null;
    return false;
  }

  private boolean ensurePushRequest(final Buffer<I, B> buffer) {
    if (hasRequest())
      return true;
    request = new BufferRequestI<>(buffer);
    return link.push(request, sendtransfer);
  }

  private boolean ensurePullRequest(final Buffer<I, B> buffer) {
    if (hasRequest())
      return true;
    request = new BufferRequestI<>(buffer);
    return link.pull(request, sendtransfer);
  }

  private boolean disposeRequest() {
    if (request == null)
      return true;
    request.dispose();
    request = null;
    return true;
  }
}
