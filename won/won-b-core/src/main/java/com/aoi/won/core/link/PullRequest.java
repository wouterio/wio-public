package com.aoi.won.core.link;

import com.aoi.won.core.node.Buffer;

import io.wouter.lib.task.TaskBase;
import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.func.GetItem;

public interface PullRequest<I, B> extends TaskBase, GetItem<I>, Dispose {

  Buffer<I, B> getTarget();
}
