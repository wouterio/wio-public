package com.aoi.won.core.node;

import io.wouter.won.util.func.Dispose;
import io.wouter.won.util.func.GetItem;

public interface Buffer<I, B> extends GetItem<I>, Dispose {

  B getBufferDelegate();



  boolean hasAutoFlow();

  void setAutoFlow(boolean autofill);



  void fill(boolean fill);

  void purge(boolean purge);



  void filled(boolean filled);

  void purged(boolean purged);
}
