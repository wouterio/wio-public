package com.aoi.won.core.link;

import com.aoi.won.core.node.Buffer;

import io.wouter.won.util.func.Dispose;

public interface PushHandler<I, B> extends Dispose {

  boolean push(Buffer<I, B> prev, boolean push);
}
