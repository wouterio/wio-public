package com.aoi.won.core.impl;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import com.aoi.won.core.Link;
import com.aoi.won.core.link.PullHandler;
import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushHandler;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.link.SendTransfer;
import com.aoi.won.core.node.Buffer;
import com.aoi.won.core.node.Conversion;

final class IntraLinkI<I, B> implements Link<I, B>, Serializable {

  private static final long serialVersionUID = 8131703065704003665L;



  private final Collection<Conversion<I>> nodeconvs = new ArrayList<>(); // effectively immutable after construction



  IntraLinkI(final Collection<? extends Conversion<I>> nodeconvs) {
    this.nodeconvs.addAll(nodeconvs);
  }



  @Override
  public PushHandler<I, B> ensurePushHandler(final I item) {
    return new BufferHandlerI<>(filterConversionsInput(item)); // push handler for input layer
  }

  @Override
  public PullHandler<I, B> ensurePullHandler(final I item) {
    return new BufferHandlerI<>(filterConversionsOutput(item)); // pull handler for output layer
  }



  @Override
  public boolean push(final PushRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
    throw new UnsupportedOperationException("IntraLink does not take push requests."); //$NON-NLS-1$
  }

  @Override
  public boolean pull(final PullRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
    throw new UnsupportedOperationException("IntraLink does not take pull requests."); //$NON-NLS-1$
  }



  @Override
  public String toString() {
    return "IntraLink"; //$NON-NLS-1$
  }



  private Stream<Conversion<I>> filterConversionsInput(final I item) {
    return nodeconvs.stream().filter(c -> c.hasInput(item));
  }

  private Stream<Conversion<I>> filterConversionsOutput(final I item) {
    return nodeconvs.stream().filter(c -> c.hasOutput(item));
  }



  //



  private static final class BufferHandlerI<I, B> implements PushHandler<I, B>, PullHandler<I, B>, Serializable {

    private static final long serialVersionUID = -4768471772459794811L;



    private final Collection<Conversion<I>> bufferconvs; // effectively immutable after construction



    BufferHandlerI(final Stream<Conversion<I>> bufferconvs) {
      this.bufferconvs = bufferconvs.collect(toList());
    }



    @Override
    public boolean push(final Buffer<I, B> unused, final boolean push) {
      return activate(push);
    }

    @Override
    public boolean pull(final Buffer<I, B> unused, final boolean pull) {
      return activate(pull);
    }



    @Override
    public void dispose() {
      activate(false);
    }



    @Override
    public String toString() {
      return "IntraLink.BufferHandler"; //$NON-NLS-1$
    }



    private boolean activate(final boolean activate) {
      bufferconvs.forEach(c -> c.setActive(activate));
      return true;
    }
  }
}
