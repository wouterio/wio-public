package com.aoi.won.core.node;

import java.util.Collection;
import java.util.stream.Stream;

public interface Conversion<I> {

  static <I> Stream<I> getInputs(final Collection<? extends Conversion<I>> col) {
    return col.stream().flatMap(c -> c.getInput().stream()).distinct();
  }

  static <I> Stream<I> getOutputs(final Collection<? extends Conversion<I>> col) {
    return col.stream().flatMap(c -> c.getOutput().stream()).distinct();
  }



  default boolean hasInput(final I item) {
    return getInput().contains(item);
  }

  default boolean hasOutput(final I item) {
    return getOutput().contains(item);
  }



  Collection<I> getInput();

  Collection<I> getOutput();



  boolean isActive();

  void setActive(boolean b);
}
