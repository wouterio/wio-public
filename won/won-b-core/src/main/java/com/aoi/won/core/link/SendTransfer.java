package com.aoi.won.core.link;

public interface SendTransfer<I, B> {

  static <I, B> SendTransfer<I, B> passive() {
    return t -> false;
  }



  boolean send(Transfer<I, B> transfer);
}
