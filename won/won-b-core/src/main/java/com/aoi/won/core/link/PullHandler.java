package com.aoi.won.core.link;

import com.aoi.won.core.node.Buffer;

import io.wouter.won.util.func.Dispose;

public interface PullHandler<I, B> extends Dispose {

  boolean pull(Buffer<I, B> next, boolean pull);
}
