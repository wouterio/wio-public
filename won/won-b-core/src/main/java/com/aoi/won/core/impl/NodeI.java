package com.aoi.won.core.impl;

import static com.aoi.won.core.Link.getIdleLink;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;
import com.aoi.won.core.link.PullHandler;
import com.aoi.won.core.link.PushHandler;
import com.aoi.won.core.node.Buffer;
import com.aoi.won.core.node.Buffers;

import io.wouter.won.util.func.Dispose;

final class NodeI<I, N, B> implements Node<I, N, B>, Serializable {

  private static final long serialVersionUID = 6145676057805065799L;



  private final N delegate;
  private final Buffers<I, B> input;
  private final Buffers<I, B> output;



  NodeI(final N delegate, final Function<I, B> newinputbufferdelegate, final Function<I, B> newoutputbufferdelegate) {
    this.delegate = requireNonNull(delegate);
    this.input = new BuffersI<>(newinputbufferdelegate, new HashMap<>());
    this.output = new BuffersI<>(newoutputbufferdelegate, new HashMap<>());
  }

  NodeI(final N delegate, final Function<I, B> newinputbufferdelegate, final Function<I, B> newoutputbufferdelegate, final Comparator<? super I> comp) {
    this.delegate = requireNonNull(delegate);
    this.input = new BuffersI<>(newinputbufferdelegate, new TreeMap<>(comp));
    this.output = new BuffersI<>(newoutputbufferdelegate, new TreeMap<>(comp));
  }



  @Override
  public N getNodeDelegate() {
    return delegate;
  }

  @Override
  public Buffers<I, B> getInput() {
    return input;
  }

  @Override
  public Buffers<I, B> getOutput() {
    return output;
  }



  @Override
  public void dispose() {
    if (Dispose.class.isInstance(delegate))
      Dispose.class.cast(delegate).dispose();
    input.dispose();
    output.dispose();
  }



  @Override
  public String toString() {
    return new StringBuilder().append('<') //
        .append("Node") //$NON-NLS-1$
        .append(' ').append(delegate) //
        .append(" i=").append(input) //$NON-NLS-1$
        .append(" o=").append(output) //$NON-NLS-1$
        .append('>').toString();
  }



  private static class BuffersI<I, B> implements Buffers<I, B>, Serializable {

    private static final long serialVersionUID = -4047582392832805480L;



    private final Function<I, B> newbufferdelegate;
    private final Map<I, BufferI<I, B>> buffers;
    private final Map<I, Buffer<I, B>> ubuffers;



    private Link<I, B> pulllink = getIdleLink();
    private Link<I, B> pushlink = getIdleLink();



    BuffersI(final Function<I, B> newbufferdelegate, final Map<I, BufferI<I, B>> buffers) {
      this.newbufferdelegate = requireNonNull(newbufferdelegate);
      this.buffers = requireNonNull(buffers);
      this.ubuffers = unmodifiableMap(this.buffers);
    }



    @Override
    public boolean hasBuffers() {
      return !buffers.isEmpty();
    }

    @Override
    public boolean hasBuffer(final I item) {
      return buffers.containsKey(item);
    }

    @Override
    public Buffer<I, B> ensureBuffer(final I item) {
      return buffers.computeIfAbsent(item, i -> newBuffer(i));
    }

    @Override
    public Buffer<I, B> getBuffer(final I item) {
      return buffers.get(item);
    }

    @Override
    public Buffer<I, B> removeBuffer(final I item) {
      final Buffer<I, B> removed = buffers.remove(item);
      removed.dispose();
      return removed;
    }



    @Override
    public Set<I> getItems() {
      return ubuffers.keySet();
    }

    @Override
    public Collection<Buffer<I, B>> getBuffers() {
      return ubuffers.values();
    }



    @Override
    public void setPullLink(final Link<I, B> newpulllink) {
      pulllink = newpulllink != null ?
          newpulllink :
          getIdleLink();
      buffers.values().forEach(b -> b.obtainPullHandler(pulllink));
    }

    @Override
    public void setPushLink(final Link<I, B> newpushlink) {
      pushlink = newpushlink != null ?
          newpushlink :
          getIdleLink();
      buffers.values().forEach(b -> b.obtainPushHandler(pushlink));
    }



    @Override
    public String toString() {
      return new StringBuilder().append('<') //
          .append("Buffers") //$NON-NLS-1$
          .append(' ').append(getBuffers()) //
          .append('>').toString();
    }



    private BufferI<I, B> newBuffer(final I item) {
      final B bufferdelegate = newbufferdelegate.apply(item);
      final Dispose metadispose = () -> removeBuffer(item);
      return new BufferI<>(item, bufferdelegate, pulllink, pushlink, metadispose);
    }



    private static class BufferI<I, B> implements Buffer<I, B>, Serializable {

      private static final long serialVersionUID = 6074157223270462611L;



      private final I item;
      private final B delegate;
      private final Dispose metadispose;



      private PullHandler<I, B> pull = null;
      private PushHandler<I, B> push = null;
      private boolean blockfill = false;
      private boolean blockpurge = false;
      private boolean autoflow = true;



      BufferI(final I item, final B delegate, final Link<I, B> pulllink, final Link<I, B> pushlink, final Dispose metadispose) {
        this.item = requireNonNull(item);
        this.delegate = requireNonNull(delegate);
        this.metadispose = requireNonNull(metadispose);
        obtainPullHandler(pulllink);
        obtainPushHandler(pushlink);
      }



      @Override
      public I getItem() {
        return item;
      }

      @Override
      public B getBufferDelegate() {
        return delegate;
      }



      @Override
      public boolean hasAutoFlow() {
        return autoflow;
      }

      @Override
      public void setAutoFlow(final boolean autoflow) {
        this.autoflow = autoflow;
      }



      @Override
      public void fill(final boolean fill) {
        if (fill != blockfill) {
          blockfill = fill;
          pull.pull(this, fill); // if fill is true, eventually invokes filled();
        }
      }

      @Override
      public void purge(final boolean purge) {
        if (purge != blockpurge) {
          blockpurge = purge;
          push.push(this, purge); // if purge is true, eventually invokes purged();
        }
      }



      @Override
      public void filled(final boolean filled) { // eventually invoked by fill()
        if (!filled) { // if fill failed, try again
          blockfill = false;
          fill(true);
        } else if (autoflow) {
          purge(true);
          blockfill = false; // setting blockfill false after purge(true) prevents recursive loop
        }
      }

      @Override
      public void purged(final boolean purged) { // eventually invoked by purge()
        if (!purged) { // if purge failed, try again
          blockpurge = false;
          purge(true);
        } else if (autoflow) {
          fill(true);
          blockpurge = false; // setting blockpurge false after fill(true) prevents recursive loop
        }
      }



      void obtainPullHandler(final Link<I, B> pulllink) {
        if (pull != null)
          pull.dispose();
        pull = pulllink.ensurePullHandler(item);
      }

      void obtainPushHandler(final Link<I, B> pushlink) {
        if (push != null)
          push.dispose();
        push = pushlink.ensurePushHandler(item);
      }



      @Override
      public void dispose() {
        if (Dispose.class.isInstance(delegate))
          Dispose.class.cast(delegate).dispose();
        metadispose.dispose();
        pull.dispose();
        push.dispose();
        pull = null;
        push = null;
      }



      @Override
      public String toString() {
        return new StringBuilder().append('<') //
            .append("Buffer") //$NON-NLS-1$
            .append(' ').append(item) ///
            .append(' ').append(delegate) ///
            .append('>').toString(); //
      }
    }
  }
}
