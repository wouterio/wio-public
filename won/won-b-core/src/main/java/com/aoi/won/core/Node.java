package com.aoi.won.core;

import static com.aoi.won.core.Link.newIntraLink;
import static com.aoi.won.core.node.Conversion.getInputs;
import static com.aoi.won.core.node.Conversion.getOutputs;

import java.util.Collection;
import java.util.Comparator;
import java.util.function.Function;

import com.aoi.won.core.impl.ImplFactory;
import com.aoi.won.core.node.Buffers;
import com.aoi.won.core.node.Conversion;

import io.wouter.won.util.func.Dispose;

/**
 * @param <I> item type
 * @param <N> node delegate type
 * @param <B> buffer delegate type
 */

public interface Node<I, N, B> extends Dispose {

  static <I, N, B> Node<I, N, B> newNode(final N delegate, final Function<I, B> newinputbufferdelegate, final Function<I, B> newoutputbufferdelegate) {
    return ImplFactory.newNode(delegate, newinputbufferdelegate, newoutputbufferdelegate);
  }

  static <I, N, B> Node<I, N, B> newNode(final N delegate, final Function<I, B> newinputbufferdelegate, final Function<I, B> newoutputbufferdelegate, final Comparator<? super I> comp) {
    return ImplFactory.newNode(delegate, newinputbufferdelegate, newoutputbufferdelegate, comp);
  }

  static <I, N, B> Node<I, N, B> equip(final Node<I, N, B> node, final Collection<? extends Conversion<I>> convs, final Link<I, B> inputlink, final Link<I, B> outputlink) {
    final Link<I, B> intralink = newIntraLink(convs);
    final Buffers<I, B> input = node.getInput();
    final Buffers<I, B> output = node.getOutput();
    input.setPullLink(inputlink);
    input.setPushLink(intralink);
    output.setPullLink(intralink);
    output.setPushLink(outputlink);
    getInputs(convs).forEach(input::ensureBuffer);
    getOutputs(convs).forEach(output::ensureBuffer);
    return node;
  }



  N getNodeDelegate();

  Buffers<I, B> getInput();

  Buffers<I, B> getOutput();
}
