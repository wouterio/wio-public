package com.aoi.won.core.impl;

import static io.wouter.lib.rsq.RSQueue.newRSQueue;
import static io.wouter.lib.task.TaskStatus.Failed;
import static io.wouter.lib.task.TaskStatus.Pending;
import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.aoi.won.core.Link;
import com.aoi.won.core.link.PullHandler;
import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushHandler;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.link.SendTransfer;
import com.aoi.won.core.link.Transfer;

import io.wouter.lib.rsq.RSQueue;
import io.wouter.lib.task.TaskBase;

final class InterLinkI<I, B> implements Link<I, B>, Serializable {

  private static final long serialVersionUID = 1434366838507618202L;



  private final Map<I, Link<I, B>> itemlinks = new HashMap<>();
  private final SendTransfer<I, B> pushsendtransfer;
  private final SendTransfer<I, B> pullsendtransfer;



  InterLinkI(final SendTransfer<I, B> pushsendtransfer, final SendTransfer<I, B> pullsendtransfer) {
    this.pushsendtransfer = requireNonNull(pushsendtransfer);
    this.pullsendtransfer = requireNonNull(pullsendtransfer);
  }



  @Override
  public PushHandler<I, B> ensurePushHandler(final I item) {
    return ensureItemLink(item).ensurePushHandler(item);
  }

  @Override
  public PullHandler<I, B> ensurePullHandler(final I item) {
    return ensureItemLink(item).ensurePullHandler(item);
  }



  @Override
  public boolean push(final PushRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
    final I item = request.getItem();
    return itemlinks.containsKey(item) && itemlinks.get(item).push(request, sendtransfer);
  }

  @Override
  public boolean pull(final PullRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
    final I item = request.getItem();
    return itemlinks.containsKey(item) && itemlinks.get(item).pull(request, sendtransfer);
  }



  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Link\n"); //$NON-NLS-1$
    itemlinks.values().forEach(l -> sb.append(l.toString()).append('\n'));
    return sb.toString();
  }



  private Link<I, B> ensureItemLink(final I item) {
    return itemlinks.computeIfAbsent(item, i -> new ItemLinkI<>(i, pushsendtransfer, pullsendtransfer));
  }



  //



  private static class ItemLinkI<I, B> implements Link<I, B>, Transfer<I, B>, Serializable {

    private static final long serialVersionUID = 8276758245704510990L;



    private final I item;
    private final SendTransfer<I, B> pushsendtransfer;
    private final SendTransfer<I, B> pullsendtransfer;
    private final RSQueue<PushRequest<I, B>> pushreqs = newRSQueue(ItemLinkI::resetFailed, ItemLinkI::shouldRemove, ItemLinkI::shouldShift);
    private final RSQueue<PullRequest<I, B>> pullreqs = newRSQueue(ItemLinkI::resetFailed, ItemLinkI::shouldRemove, ItemLinkI::shouldShift);



    ItemLinkI(final I item, final SendTransfer<I, B> pushsendtransfer, final SendTransfer<I, B> pullsendtransfer) {
      this.item = requireNonNull(item);
      this.pushsendtransfer = requireNonNull(pushsendtransfer);
      this.pullsendtransfer = requireNonNull(pullsendtransfer);
    }



    @Override
    public I getItem() {
      return item;
    }



    @Override
    public PushHandler<I, B> ensurePushHandler(final I unused) {
      return new BufferHandlerI<>(this, pushsendtransfer);
    }

    @Override
    public PullHandler<I, B> ensurePullHandler(final I unused) {
      return new BufferHandlerI<>(this, pullsendtransfer);
    }



    @Override
    public RSQueue<PushRequest<I, B>> getPushRequests() {
      return pushreqs;
    }

    @Override
    public RSQueue<PullRequest<I, B>> getPullRequests() {
      return pullreqs;
    }



    @Override
    public boolean push(final PushRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
      return pushreqs.add(request) && pullreqs.findAndPeek() != null && sendtransfer.send(this);
    }

    @Override
    public boolean pull(final PullRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
      return pullreqs.add(request) && pushreqs.findAndPeek() != null && sendtransfer.send(this);
    }



    @Override
    public String toString() {
      return new StringBuilder() //
          .append('<') //
          .append("ItemLink") // //$NON-NLS-1$
          .append(" item=").append(getItem()) //$NON-NLS-1$
          .append(" pushreqs=").append(pushreqs) //$NON-NLS-1$
          .append(" pullreqs=").append(pullreqs) //$NON-NLS-1$
          .append('>') //
          .toString(); //
    }



    private static void resetFailed(final TaskBase r) {
      if (r.getStatus().equals(Failed))
        r.setStatus(Pending);
    }

    private static boolean shouldRemove(final TaskBase r) {
      return !r.isPresent();
    }

    private static boolean shouldShift(final TaskBase r) {
      return !r.getStatus().equals(Pending);
    }
  }
}
