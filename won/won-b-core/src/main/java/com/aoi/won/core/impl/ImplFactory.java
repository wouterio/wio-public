package com.aoi.won.core.impl;

import java.util.Collection;
import java.util.Comparator;
import java.util.function.Function;

import com.aoi.won.core.Link;
import com.aoi.won.core.Node;
import com.aoi.won.core.link.SendTransfer;
import com.aoi.won.core.node.Conversion;

public class ImplFactory {

  public static <I, N, B> Node<I, N, B> newNode(final N delegate, final Function<I, B> newinputbufferdelegate, final Function<I, B> newoutputbufferdelegate) {
    return new NodeI<>(delegate, newinputbufferdelegate, newoutputbufferdelegate);
  }

  public static <I, N, B> Node<I, N, B> newNode(final N delegate, final Function<I, B> newinputbufferdelegate, final Function<I, B> newoutputbufferdelegate, final Comparator<? super I> comp) {
    return new NodeI<>(delegate, newinputbufferdelegate, newoutputbufferdelegate, comp);
  }



  public static <I, B> Link<I, B> getIdleLink() {
    return IdleLinkI.getInstance();
  }

  public static <I, B> Link<I, B> newInterLink(final SendTransfer<I, B> sendtransfer) {
    return new InterLinkI<>(sendtransfer, sendtransfer);
  }

  public static <I, B> Link<I, B> newInterLink(final SendTransfer<I, B> pushsendtransfer, final SendTransfer<I, B> pullsendtransfer) {
    return new InterLinkI<>(pushsendtransfer, pullsendtransfer);
  }

  public static <I, B> Link<I, B> newIntraLink(final Collection<? extends Conversion<I>> convs) {
    return new IntraLinkI<>(convs);
  }

  public static <I, B> LinkMultiplexerI<I, B> newLinkMultiplexer(final Collection<Link<I, B>> links) {
    return new LinkMultiplexerI<>(links);
  }



  private ImplFactory() {
    throw new AssertionError("static class"); //$NON-NLS-1$
  }
}
