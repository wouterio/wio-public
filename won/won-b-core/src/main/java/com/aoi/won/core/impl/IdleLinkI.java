package com.aoi.won.core.impl;

import java.io.Serializable;

import com.aoi.won.core.Link;
import com.aoi.won.core.link.PullHandler;
import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushHandler;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.link.SendTransfer;
import com.aoi.won.core.node.Buffer;

final class IdleLinkI<I, B> implements Link<I, B>, Serializable {

  private static final long serialVersionUID = -3314745281616204148L;



  private static final Link<?, ?> instance = new IdleLinkI<>();



  public static <I, B> Link<I, B> getInstance() {
    return (Link<I, B>) instance;
  }



  private transient final IdleHandlerI<I, B> handler = new IdleHandlerI<>();



  IdleLinkI() {}



  @Override
  public PushHandler<I, B> ensurePushHandler(final I item) {
    return handler;
  }

  @Override
  public PullHandler<I, B> ensurePullHandler(final I item) {
    return handler;
  }



  @Override
  public boolean push(final PushRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
    return false;
  }

  @Override
  public boolean pull(final PullRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
    return false;
  }



  @Override
  public String toString() {
    return "IdleLink"; //$NON-NLS-1$
  }



  //



  private static class IdleHandlerI<I, B> implements PushHandler<I, B>, PullHandler<I, B> {

    IdleHandlerI() {}



    @Override
    public boolean pull(final Buffer<I, B> next, final boolean pull) {
      return false;
    }

    @Override
    public boolean push(final Buffer<I, B> prev, final boolean push) {
      return false;
    }



    @Override
    public void dispose() {
      // empty
    }



    @Override
    public String toString() {
      return "IdleHandler"; //$NON-NLS-1$
    }
  }
}
