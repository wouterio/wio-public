package com.aoi.won.core.link;

import io.wouter.lib.rsq.RSQueue;
import io.wouter.won.util.func.GetItem;

public interface Transfer<I, B> extends GetItem<I> {

  RSQueue<PushRequest<I, B>> getPushRequests();

  RSQueue<PullRequest<I, B>> getPullRequests();
}
