package com.aoi.won.core.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import com.aoi.won.core.Link;
import com.aoi.won.core.LinkMultiplexer;
import com.aoi.won.core.link.PullHandler;
import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushHandler;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.link.SendTransfer;

final class LinkMultiplexerI<I, B> implements LinkMultiplexer<I, B>, Serializable {

  private static final long serialVersionUID = -2350455583348843245L;



  private final Collection<Link<I, B>> links = new LinkedList<>();



  LinkMultiplexerI(final Collection<Link<I, B>> links) {
    this.links.addAll(links);
  }



  @Override
  public boolean addLink(final Link<I, B> link) {
    return links.add(link);
  }

  @Override
  public boolean removeLink(final Link<I, B> link) {
    return links.remove(link);
  }



  @Override
  public PushHandler<I, B> ensurePushHandler(final I item) {
    throw new UnsupportedOperationException("LinkMultiplexer does not have push handlers."); //$NON-NLS-1$
  }

  @Override
  public PullHandler<I, B> ensurePullHandler(final I item) {
    throw new UnsupportedOperationException("LinkMultiplexer does not have pull handlers."); //$NON-NLS-1$
  }



  @Override
  public boolean push(final PushRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
    for (final Link<I, B> link : links)
      if (link.push(request, sendtransfer))
        return true;
    return false;
  }

  @Override
  public boolean pull(final PullRequest<I, B> request, final SendTransfer<I, B> sendtransfer) {
    for (final Link<I, B> link : links)
      if (link.pull(request, sendtransfer))
        return true;
    return false;
  }
}
