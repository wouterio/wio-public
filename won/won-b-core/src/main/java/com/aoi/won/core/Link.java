package com.aoi.won.core;

import static java.util.Arrays.asList;

import java.util.Collection;

import com.aoi.won.core.impl.ImplFactory;
import com.aoi.won.core.link.PullHandler;
import com.aoi.won.core.link.PullRequest;
import com.aoi.won.core.link.PushHandler;
import com.aoi.won.core.link.PushRequest;
import com.aoi.won.core.link.SendTransfer;
import com.aoi.won.core.node.Conversion;

public interface Link<I, B> {

  static <I, B> Link<I, B> getIdleLink() {
    return ImplFactory.getIdleLink();
  }

  static <I, B> Link<I, B> newInterLink(final SendTransfer<I, B> sendtransfer) {
    return ImplFactory.newInterLink(sendtransfer);
  }



  @SafeVarargs
  static <I, B> Link<I, B> newIntraLink(final Conversion<I>... conv) {
    return ImplFactory.newIntraLink(asList(conv));
  }

  static <I, B> Link<I, B> newIntraLink(final Collection<? extends Conversion<I>> convs) {
    return ImplFactory.newIntraLink(convs);
  }



  @SafeVarargs
  static <I, B> LinkMultiplexer<I, B> newLinkMultiplexer(final Link<I, B>... links) {
    return ImplFactory.newLinkMultiplexer(asList(links));
  }

  static <I, B> LinkMultiplexer<I, B> newLinkMultiplexer(final Collection<Link<I, B>> links) {
    return ImplFactory.newLinkMultiplexer(links);
  }



  PushHandler<I, B> ensurePushHandler(I item);

  PullHandler<I, B> ensurePullHandler(I item);



  boolean push(PushRequest<I, B> request, SendTransfer<I, B> sendtransfer);

  boolean pull(PullRequest<I, B> request, SendTransfer<I, B> sendtransfer);
}
