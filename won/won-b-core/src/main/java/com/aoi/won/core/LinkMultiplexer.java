package com.aoi.won.core;

public interface LinkMultiplexer<I, B> extends Link<I, B> {

  boolean addLink(Link<I, B> link);

  boolean removeLink(Link<I, B> link);
}
