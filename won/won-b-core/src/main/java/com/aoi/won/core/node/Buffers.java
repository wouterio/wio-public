package com.aoi.won.core.node;

import java.util.Collection;
import java.util.Iterator;

import com.aoi.won.core.Link;

import io.wouter.won.util.func.Dispose;

public interface Buffers<I, B> extends Dispose, Iterable<Buffer<I, B>> {

  boolean hasBuffers();

  boolean hasBuffer(I item);

  Buffer<I, B> ensureBuffer(I item);

  Buffer<I, B> getBuffer(I item);

  Buffer<I, B> removeBuffer(final I item);



  Collection<I> getItems();

  Collection<Buffer<I, B>> getBuffers();



  void setPullLink(Link<I, B> pulllink);

  void setPushLink(Link<I, B> pushlink);



  @Override
  default void dispose() {
    forEach(Buffer::dispose);
  }

  @Override
  default Iterator<Buffer<I, B>> iterator() {
    return getBuffers().iterator();
  }
}
