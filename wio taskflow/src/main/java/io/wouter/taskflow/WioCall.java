package io.wouter.taskflow;

import static java.util.Objects.requireNonNull;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @param <I> Input type
 * @param <O> Output type
 * @param <R> Result type
 */

@FunctionalInterface
public interface WioCall<I, O, R> {

  /**
   * Returns a call based on the provided function.
   *
   * @param <T> Input and Output type
   * @param <R> Result type
   * @param f Function to be applied
   * @return Call based on provided function
   */

  static <T, R> WioCall<T, T, R> apply(final Function<T, R> f) {
    requireNonNull(f);
    return (arg, cb) -> cb.callback(arg, f.apply(arg));
  }

  /**
   * Returns a call based on the provided predicate.
   *
   * @param <T> Input and Output type
   * @param p Predicate to be tested
   * @return Call based on provided predicate
   */

  static <T> WioCall<T, T, Boolean> test(final Predicate<T> p) {
    requireNonNull(p);
    return (arg, cb) -> cb.callback(arg, p.test(arg));
  }

  /**
   * Returns a call that does not invoke the callback and dismisses its input. Used for terminating a task flow.
   *
   * @param <T> Input and Output type
   * @return Call that dismisses its input
   */

  static <T> WioCall<T, T, Object> dismiss() {
    // no argument to test for null
    return (arg, cb) -> {};
  }

  /**
   * Returns a call that invokes the provided consumer but does not invoke the callback. Used for terminating a task
   * flow.
   *
   * @param <T>
   * @param c
   * @return
   */

  static <T> WioCall<T, Object, Object> consume(final Consumer<T> c) {
    requireNonNull(c);
    return (arg, cb) -> c.accept(arg);
  }



  /**
   * Processes the provided input and invokes the provided callback with parameters output and result. The callback's
   * result parameter determines which task will be executed next and the output parameter will be the input for that
   * next task. Input and output may or may not be the same object or even of the same type.<br>
   * <br>
   * The task flow does not proceed as long as the callback is not invoked. Invoking the callback should be the very
   * last thing this method does. See {@link WioCallBack#callback(Object, Object)}.<br>
   * <br>
   * Any exception thrown will be caught and passed on to the task flow's exception handler.
   *
   * @param input Input object
   * @param cb Callback
   * @throws Exception
   */

  void call(I input, WioCallBack<O, R> cb) throws Exception;
}
