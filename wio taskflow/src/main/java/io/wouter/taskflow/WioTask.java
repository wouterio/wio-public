package io.wouter.taskflow;

import static java.util.Objects.requireNonNull;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @param <I> Input type
 * @param <O> Output type
 * @param <R> Result type
 */

public final class WioTask<I, O, R> {

  private final Executor exec;
  private final Consumer<Exception> exceptionhandler;
  private final WioCall<I, O, R> call;
  private final WioCallBack<O, R> callback = (output, result) -> propagate(output, result);
  private final Map<R, WioTask<O, ?, ?>> nexts = new HashMap<>();
  private final WioTask<O, ?, ?> defaultnext;



  WioTask(final Executor exec, final Consumer<Exception> exceptionhandler, final WioCall<I, O, R> call, final WioTask<O, ?, ?> defaultnext) {
    this.exec = requireNonNull(exec);
    this.exceptionhandler = requireNonNull(exceptionhandler);
    this.call = requireNonNull(call);
    this.defaultnext = defaultnext;
  }



  void execute(final I input) {
    exec.execute(() -> {
      try {
        call.call(input, callback);
      } catch (final Exception e) {
        exceptionhandler.accept(e);
      }
    });
  }



  private void propagate(final O output, final R result) {
    if (nexts.containsKey(result))
      nexts.get(result).execute(output);
    else if (defaultnext != null)
      defaultnext.execute(output);
    else
      exceptionhandler.accept(new IllegalStateException("Unmapped result and no default: " + result)); //$NON-NLS-1$
  }



  //



  /**
   * @param <I> Input type
   * @param <O> Output type
   * @param <R> Result type
   */

  public static final class Builder<I, O, R> {

    private final Executor exec;
    private final Consumer<Exception> exceptionhandler;
    private final WioCall<I, O, R> call;
    private final Map<R, WioTask.Builder<O, ?, ?>> nextbuilders = new HashMap<>();



    private WioTask.Builder<O, ?, ?> defaultnextbuilder = null;
    private WioTask<I, O, R> task = null;



    Builder(final Executor exec, final Consumer<Exception> exceptionhandler, final WioCall<I, O, R> call) {
      this.exec = requireNonNull(exec);
      this.exceptionhandler = requireNonNull(exceptionhandler);
      this.call = requireNonNull(call);
    }



    /**
     * Extend this task with the provided result and next task. If this task is invoked and completes with the provided
     * result, then it will invoke next task. This task's output type O and next task's input type must match each
     * other.
     *
     * @param <P> Output type for next task
     * @param <S> Result type for next task
     * @param result Result required to invoke next task
     * @param next Next task to be invoked
     * @return Next task
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has the result mapped to another next task.
     * @throws NullPointerException If next task is null
     */

    public <P, S> WioTask.Builder<O, P, S> next(final R result, final WioTask.Builder<O, P, S> next) {
      checkBuilt();
      checkResult(result);
      nextbuilders.put(result, requireNonNull(next));
      return next;
    }

    /**
     * Extend this task with the provided default next task. If this task is invoked and completes with an unmapped
     * result, then it will invoke the default next task. This task's output type O and next task's input type must
     * match each other.
     *
     * @param <P> Output type for next task
     * @param <S> Result type for next task
     * @param next Next task to be invoked
     * @return Next task
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has another default next task.
     * @throws NullPointerException If next task is null
     */

    public <P, S> WioTask.Builder<O, P, S> defaultNext(final WioTask.Builder<O, P, S> next) {
      checkBuilt();
      checkDefault();
      defaultnextbuilder = requireNonNull(next);
      return next;
    }



    /**
     * Extend this task with the provided result and a next task based on the provided call. If this task is invoked and
     * completes with the provided result, then it will invoke next task. This task's output type O and next task's
     * input type must match each other.
     *
     * @param <P> Output type for next task
     * @param <S> Result type for next task
     * @param result Result required to invoke next task
     * @param next Next task to be invoked
     * @return Next task
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has the result mapped to another next task.
     * @throws NullPointerException If next call is null
     */

    public <P, S> WioTask.Builder<O, P, S> next(final R result, final WioCall<O, P, S> next) {
      return next(result, new WioTask.Builder<>(exec, exceptionhandler, next));
    }

    /**
     * Extend this task with the provided result and a next task based on the provided function. If this task is invoked
     * and completes with the provided result, then it will invoke next task. This task's output type O and next task's
     * input type must match each other.
     *
     * @param <S> Result type for next task
     * @param result Result required to invoke next task
     * @param next Next task to be invoked
     * @return Next task
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has the result mapped to another next task.
     * @throws NullPointerException If next function is null
     */

    public <S> WioTask.Builder<O, O, S> next(final R result, final Function<O, S> next) {
      return next(result, WioCall.apply(next));
    }

    /**
     * Extend this task with the provided result and a next task based on the provided predicate. If this task is
     * invoked and completes with the provided result, then it will invoke next task. This task's output type O and next
     * task's input type must match each other.
     *
     * @param result Result required to invoke next task
     * @param next Next task to be invoked
     * @return Next task
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has the result mapped to another next task.
     * @throws NullPointerException If next predicate is null
     */

    public WioTask.Builder<O, O, Boolean> next(final R result, final Predicate<O> next) {
      return next(result, WioCall.test(next));
    }

    /**
     * Extend this task with the provided result and an empty terminal task. If this task is invoked and completes with
     * the provided result, then it will terminate the task flow.
     *
     * @param result Result required to terminate task flow
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has the result mapped to another next task.
     */

    public void terminate(final R result) {
      next(result, WioCall.dismiss());
    }

    /**
     * Extend this task with the provided result and a terminal task based on the provided consumer. If this task is
     * invoked and completes with the provided result, then it will invoke the consumer and terminate the task flow.
     *
     * @param result Result required to terminate task flow
     * @param c Consumer to be invoked
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has the result mapped to another next task.
     * @throws NullPointerException If next consumer is null
     */

    public void terminate(final R result, final Consumer<O> c) {
      next(result, WioCall.consume(c));
    }



    /**
     * Extend this task with a default next task based on the provided call. If this task is invoked and completes with
     * an unmapped result, then it will invoke the default next task. This task's output type O and next task's input
     * type must match each other.
     *
     * @param <P> Output type for next task
     * @param <S> Result type for next task
     * @param next Next task to be invoked
     * @return Next task
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has another default next task.
     * @throws NullPointerException If next call is null
     */

    public <P, S> WioTask.Builder<O, P, S> defaultNext(final WioCall<O, P, S> next) {
      return defaultNext(new WioTask.Builder<>(exec, exceptionhandler, next));
    }

    /**
     * Extend this task with a default next task based on the provided function. If this task is invoked and completes
     * with an unmapped result, then it will invoke the default next task. This task's output type O and next task's
     * input type must match each other.
     *
     * @param <S> Result type for next task
     * @param next Next task to be invoked
     * @return Next task
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has another default next task.
     * @throws NullPointerException If next function is null
     */

    public <S> WioTask.Builder<O, O, S> defaultNext(final Function<O, S> next) {
      return defaultNext(WioCall.apply(next));
    }

    /**
     * Extend this task with a default next task based on the provided predicate. If this task is invoked and completes
     * with an unmapped result, then it will invoke the default next task. This task's output type O and next task's
     * input type must match each other.
     *
     * @param next Next task to be invoked
     * @return Next task
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has another default next task.
     * @throws NullPointerException If next predicate is null
     */

    public WioTask.Builder<O, O, Boolean> defaultNext(final Predicate<O> next) {
      return defaultNext(WioCall.test(next));
    }

    /**
     * Extend this task with a default empty terminal task. If this task is invoked and completes with an unmapped
     * result, then it will terminate the task flow.
     *
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has another default next task.
     */

    public void defaultTerminate() {
      defaultNext(WioCall.dismiss());
    }

    /**
     * Extend this task with a default terminal task based on the provided consumer. If this task is invoked and
     * completes with an unmapped result, then it will invoke the consumer and terminate the task flow.
     *
     * @param c Consumer to be invoked
     * @throws IllegalStateException If this task was built already.
     * @throws IllegalStateException If this task already has another default next task.
     * @throws NullPointerException If next consumer is null
     */

    public void defaultTerminate(final Consumer<O> c) {
      defaultNext(WioCall.consume(c));
    }



    WioTask<I, O, R> build() {
      if (task != null)
        return task;
      final WioTask<O, ?, ?> defaultnexttask = defaultnextbuilder != null ?
          defaultnextbuilder.build() :
          null;
      task = new WioTask<>(exec, exceptionhandler, call, defaultnexttask);
      nextbuilders.entrySet().forEach(e -> task.nexts.put(e.getKey(), e.getValue().build()));
      return task;
    }



    private void checkBuilt() {
      if (task != null)
        throw new IllegalStateException("Already built"); //$NON-NLS-1$
    }

    private void checkResult(final R result) {
      if (nextbuilders.containsKey(result))
        throw new IllegalStateException("Result already set: " + result); //$NON-NLS-1$
    }

    private void checkDefault() {
      if (defaultnextbuilder != null)
        throw new IllegalStateException("Default already set"); //$NON-NLS-1$
    }

  }
}
