package io.wouter.taskflow;

/**
 * @param <O> Output type
 * @param <R> Result type
 */

@FunctionalInterface
public interface WioCallBack<O, R> {

  /**
   * Invoked by {@link WioCall#call(Object, WioCallBack)}. The result parameter determines which task will be executed
   * next and the output parameter will be the input for that next task.<br>
   *
   * @param output
   * @param result
   */

  void callback(O output, R result);
}
