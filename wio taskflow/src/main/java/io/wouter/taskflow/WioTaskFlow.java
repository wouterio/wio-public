package io.wouter.taskflow;

import static java.util.Objects.requireNonNull;

import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @param <I> Input type
 */

public final class WioTaskFlow<I> {

  /**
   * Construct a new task flow builder based on provided executor.
   *
   * @param <I> Task flow input type
   * @param exec Executor for executing task flow
   * @param exceptionhandler
   * @return A new task flow builder
   */

  public static <I> WioTaskFlow.Builder<I> builder(final Executor exec, final Consumer<Exception> exceptionhandler) {
    return new WioTaskFlow.Builder<>(exec, exceptionhandler);
  }



  private final WioTask<I, ?, ?> head;



  WioTaskFlow(final WioTask<I, ?, ?> head) {
    this.head = requireNonNull(head);
  }



  /**
   * Execute task flow for provided input.
   *
   * @param input
   */

  public void execute(final I input) {
    head.execute(input);
  }

  /**
   * Execute task flow for each provided input.
   *
   * @param inputs
   */

  public void execute(final Iterable<? extends I> inputs) {
    inputs.forEach(i -> execute(i));
  }



  //



  /**
   * @param <I> Input type
   */

  public static final class Builder<I> {

    private final Executor exec;
    private final Consumer<Exception> exceptionhandler;



    private WioTask.Builder<I, ?, ?> headbuilder = null;



    Builder(final Executor exec, final Consumer<Exception> exceptionhandler) {
      this.exec = requireNonNull(exec);
      this.exceptionhandler = requireNonNull(exceptionhandler);
    }



    /**
     * Construct the task flow's head based on the provided call. A task flow can have only one head.
     *
     * @param <O> Output type
     * @param <R> Result type
     * @param call
     * @return The constructed head
     * @throws IllegalStateException If attempting to construct a second head
     */

    public <O, R> WioTask.Builder<I, O, R> head(final WioCall<I, O, R> call) {
      checkHead();
      final WioTask.Builder<I, O, R> builder = new WioTask.Builder<>(exec, exceptionhandler, call);
      headbuilder = builder;
      return builder;
    }

    /**
     * Construct the task flow's head based on the provided function. A task flow can have only one head.
     *
     * @param <R> Result type
     * @param f
     * @return The constructed head
     * @throws IllegalStateException If attempting to construct a second head
     */

    public <R> WioTask.Builder<I, I, R> headApply(final Function<I, R> f) {
      return head(WioCall.apply(f));
    }

    /**
     * Construct the task flow's head based on the provided predicate. A task flow can have only one head.
     *
     * @param p
     * @return The constructed head
     * @throws IllegalStateException If attempting to construct a second head
     */

    public WioTask.Builder<I, I, Boolean> headTest(final Predicate<I> p) {
      return head(WioCall.test(p));
    }



    /**
     * Build a task flow.
     *
     * @return The task flow
     * @throws IllegalStateException If attempting to build a task flow with no head defined
     */

    public WioTaskFlow<I> build() {
      checkNoHead();
      final WioTask<I, ?, ?> head = headbuilder.build();
      return new WioTaskFlow<>(head);
    }



    private void checkHead() {
      if (headbuilder != null)
        throw new IllegalStateException("Head defined already"); //$NON-NLS-1$
    }

    private void checkNoHead() {
      if (headbuilder == null)
        throw new IllegalStateException("No head defined yet"); //$NON-NLS-1$
    }
  }
}
