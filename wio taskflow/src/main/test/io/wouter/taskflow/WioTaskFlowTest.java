package io.wouter.taskflow;

import static java.util.Arrays.asList;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class WioTaskFlowTest {

  public static void main(final String[] args) {
    final WioCall<Integer, Integer, Boolean> incr = (arg, cb) -> {
      System.out.println(arg);
      cb.callback(arg + 1, arg < 9);
    };

    // final Executor exec = Runnable::run;
    final Executor exec = Executors.newSingleThreadExecutor();
    // final Executor exec = new ThreadPoolExecutor(2, 2, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(2), new
    // ThreadPoolExecutor.CallerRunsPolicy());

    final WioTaskFlow.Builder<Integer> flowbuilder = WioTaskFlow.builder(exec, Exception::printStackTrace);
    final WioTask.Builder<Integer, Integer, Boolean> taskbuilder = flowbuilder.head(incr);
    taskbuilder.next(true, taskbuilder); // keep incrementing while true
    taskbuilder.terminate(false, arg -> System.out.println("done"));
    final WioTaskFlow<Integer> flow = flowbuilder.build();

    flow.execute(0);
    flow.execute(1);
    flow.execute(asList(2, 3, 4, 5));
  }
}
