package io.wouter.lib.grid;

public enum Dir {

  Fw(0),
  FwRg(2),
  Rg(2),
  BwRg(3),
  Bw(1),
  BwLf(3),
  Lf(2),
  FwLf(2),
  X(0);



  public final int ticks;



  private Dir(final int ticks) {
    this.ticks = ticks;
  }



  public Dis transpose(final Ori o) {
    return Transforms.transpose(o, this);
  }

  public Dir opposite() {
    return Transforms.opposite(this);
  }
}
