package io.wouter.lib.grid;

import static java.lang.Integer.valueOf;
import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Pos implements Serializable {

  private static final long serialVersionUID = -6214387846063871413L;



  private static final Map<Integer, Map<Integer, Pos>> PosMap = new HashMap<>();
  private static final AtomicInteger Hasher = new AtomicInteger();



  public static Pos get(final int i, final int j) {
    return PosMap.computeIfAbsent(valueOf(i), k -> new HashMap<>()).computeIfAbsent(valueOf(j), key -> new Pos(i, j, Hasher.getAndIncrement()));
  }

  public static Iterator<Pos> iterator(final int ai, final int aj, final int bi, final int bj) {
    return new PosItr(ai, aj, bi, bj);
  }

  public static Iterator<Pos> iterator(final Pos a, final Pos b) {
    return new PosItr(a.i, a.j, b.i, b.j);
  }



  private static final float Sqrt2 = (float) sqrt(2d);



  public final int i;
  public final int j;

  private final float sqrdistorigin; // square distance to origin
  private final float distorigin; // distance to origin

  private final int hash;



  private Pos(final int i, final int j, final int hash) {
    this.i = i;
    this.j = j;

    sqrdistorigin = i * i + j * j;
    distorigin = (float) sqrt(sqrdistorigin);

    this.hash = hash;
  }



  public Pos add(final Dis d) {
    return Pos.get(i + d.di, j + d.dj);
  }

  public Dis sub(final Pos p) {
    return Transforms.sub(this, p);
  }



  @Override
  public boolean equals(final Object o) {
    return this == o;
  }

  @Override
  public int hashCode() {
    return hash;
  }

  @Override
  public String toString() {
    return new StringBuilder().append('(').append(i).append(',').append(j).append(')').toString();
  }



  public static float sqreucdist(final Pos a, final Pos b) { // square euclidian distance
    final int di = abs(b.i - a.i);
    final int dj = abs(b.j - a.j);
    return get(di, dj).sqrdistorigin;
  }

  public static float eucdist(final Pos a, final Pos b) { // euclidian distance
    final int di = abs(b.i - a.i);
    final int dj = abs(b.j - a.j);
    return get(di, dj).distorigin;
  }

  public static float mandist(final Pos a, final Pos b) { // manhattan distance
    final float di = abs(b.i - a.i);
    final float dj = abs(b.j - a.j);
    return di + dj;
  }

  public static float diagdist(final Pos a, final Pos b) { // diagonal shortcut
    final float di = abs(b.i - a.i);
    final float dj = abs(b.j - a.j);
    return di > dj ?
        di - dj + Sqrt2 * dj :
        dj - di + Sqrt2 * di;
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  @SuppressWarnings("static-method")
  private void readObject(@SuppressWarnings("unused") final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  //



  private static class PosItr implements Iterator<Pos> {

    private final int imin;
    private final int imax;
    private final int jmin;
    private final int jmax;



    private int i;
    private int j;



    PosItr(final int ai, final int aj, final int bi, final int bj) {
      if (ai < bi) {
        imin = ai;
        imax = bi;
      } else {
        imin = bi;
        imax = ai;
      }

      if (aj < bj) {
        jmin = aj;
        jmax = bj;
      } else {
        jmin = bj;
        jmax = aj;
      }

      i = imin;
      j = jmin;
    }



    @Override
    public boolean hasNext() {
      return j < jmax;
    }

    @Override
    public Pos next() {
      ++i;
      if (i >= imax) {
        i = imin;
        ++j;
      }
      return Pos.get(i, j);
    }
  }



  //



  private static class Serial implements Serializable {

    private static final long serialVersionUID = 5298452645833933426L;



    private final int i;
    private final int j;



    Serial(final Pos p) {
      i = p.i;
      j = p.j;
    }



    private Object readResolve() {
      return Pos.get(i, j);
    }
  }
}
