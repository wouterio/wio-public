package io.wouter.lib.grid;

import static java.lang.Math.sqrt;

public enum Ori {

  N(0, -1),
  NE(1, -1),
  E(1, 0),
  SE(1, 1),
  S(0, 1),
  SW(-1, 1),
  W(-1, 0),
  NW(-1, -1);



  public final float cos;
  public final float sin;



  private Ori(final int di, final int dj) {
    final float dist = (float) sqrt(di * di + dj * dj);
    cos = di / dist;
    sin = dj / dist;
  }



  public Ori add(final Rot r) {
    return Transforms.add(this, r);
  }

  public Rot sub(final Ori o) {
    return Transforms.sub(this, o);
  }

  public Dis transpose(final Dir d) {
    return Transforms.transpose(this, d);
  }

  public Ori opposite() {
    return Transforms.opposite(this);
  }
}
