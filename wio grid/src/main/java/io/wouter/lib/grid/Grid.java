package io.wouter.lib.grid;

public interface Grid {

  int getWidth();

  int getHeight();



  default boolean includes(final int i, final int j) {
    return i >= 0 && i < getWidth() && j >= 0 && j < getHeight();
  }

  default boolean includes(final Pos p) {
    return includes(p.i, p.j);
  }
}
