package io.wouter.lib.grid;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Objects.requireNonNull;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;

public final class Move implements Serializable {

  private static final long serialVersionUID = -715676769997489408L;



  private static final Move[][] Moves = new Move[Rot.values().length][Dir.values().length];

  static {
    int hasher = 0;
    for (final Rot r : Rot.values())
      for (final Dir d : Dir.values())
        Moves[r.ordinal()][d.ordinal()] = new Move(d, r, ++hasher);
  }



  public static Move get(final Dir d, final Rot r) {
    return Moves[r.ordinal()][d.ordinal()];
  }

  public static Collection<Move> collect(final Move... args) {
    return unmodifiableCollection(asList(args));
  }



  public static final Move Fw = get(Dir.Fw, Rot.Fw000);
  public static final Move Bw = get(Dir.Bw, Rot.Fw000);

  public static final Move Lf45 = get(Dir.X, Rot.Lf045);
  public static final Move Lf90 = get(Dir.X, Rot.Lf090);
  public static final Move Rg45 = get(Dir.X, Rot.Rg045);
  public static final Move Rg90 = get(Dir.X, Rot.Rg090);

  public static final Move FwLf45 = get(Dir.FwLf, Rot.Lf045);
  public static final Move FwLf90 = get(Dir.FwLf, Rot.Lf090);
  public static final Move FwRg45 = get(Dir.FwRg, Rot.Rg045);
  public static final Move FwRg90 = get(Dir.FwRg, Rot.Rg090);

  public static final Move BwLf45 = get(Dir.BwLf, Rot.Rg045);
  public static final Move BwRg45 = get(Dir.BwRg, Rot.Lf045);
  public static final Move BwLf90 = get(Dir.BwLf, Rot.Rg090);
  public static final Move BwRg90 = get(Dir.BwRg, Rot.Lf090);

  public static final Collection<Move> WheelMoves = collect(Fw, Bw, FwLf45, FwRg45, FwLf90, FwRg90, BwLf45, BwRg45, BwLf90, BwRg90);
  public static final Collection<Move> TrackMoves = collect(Fw, Bw, FwLf45, FwRg45, FwLf90, FwRg90, BwLf45, BwRg45, BwLf90, BwRg90, Lf45, Rg45);
  public static final Collection<Move> PlaneMoves = collect(Fw, FwLf45, FwRg45);
  public static final Collection<Move> HeliMoves = collect(Fw, Bw, Lf45, Rg45, FwLf45, FwRg45, get(Dir.FwRg, Rot.Fw000), get(Dir.Rg, Rot.Fw000), get(Dir.BwRg, Rot.Fw000), get(Dir.BwLf, Rot.Fw000), get(Dir.Lf, Rot.Fw000), get(Dir.FwLf, Rot.Fw000));



  public final Dir dir;
  public final Rot rot;
  private final int hash;



  private Move(final Dir dir, final Rot rot, final int hash) {
    this.dir = requireNonNull(dir);
    this.rot = requireNonNull(rot);
    this.hash = hash;
  }



  @Override
  public boolean equals(final Object o) {
    return this == o;
  }

  @Override
  public int hashCode() {
    return hash;
  }

  @Override
  public String toString() {
    return new StringBuilder().append('(').append(dir).append('-').append(rot).append(')').toString();
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  @SuppressWarnings("static-method")
  private void readObject(@SuppressWarnings("unused") final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  //



  private static class Serial implements Serializable {

    private static final long serialVersionUID = -4806747816925538302L;



    private final Dir dir;
    private final Rot rot;



    Serial(final Move m) {
      dir = m.dir;
      rot = m.rot;
    }



    private Object readResolve() {
      return Move.get(dir, rot);
    }
  }
}
