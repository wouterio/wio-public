package io.wouter.lib.grid;

import static java.lang.Integer.signum;

import java.util.EnumMap;
import java.util.Map;

final class Transforms {

  private static final Dis[][] DisGrid = {{Dis.SW, Dis.S, Dis.SE}, {Dis.W, Dis.X, Dis.E}, {Dis.NW, Dis.N, Dis.NE}};
  private static final Map<Ori, Map<Rot, Ori>> RotationsAdd = new EnumMap<>(Ori.class);
  private static final Map<Ori, Map<Ori, Rot>> RotationsSub = new EnumMap<>(Ori.class);
  private static final Map<Ori, Map<Dir, Dis>> Transposes = new EnumMap<>(Ori.class);
  private static final Map<Dir, Dir> DirOpposites = new EnumMap<>(Dir.class);
  private static final Map<Dis, Dis> DisOpposites = new EnumMap<>(Dis.class);
  private static final Map<Ori, Ori> OriOpposites = new EnumMap<>(Ori.class);
  private static final Map<Rot, Rot> RotOpposites = new EnumMap<>(Rot.class);
  private static final Map<Dis, Ori> DisToOris = new EnumMap<>(Dis.class);

  static {
    // rotations add

    final Map<Rot, Ori> raddmapN = new EnumMap<>(Rot.class);
    raddmapN.put(Rot.Fw000, Ori.N);
    raddmapN.put(Rot.Rg045, Ori.NE);
    raddmapN.put(Rot.Rg090, Ori.E);
    raddmapN.put(Rot.Rg135, Ori.SE);
    raddmapN.put(Rot.Bw180, Ori.S);
    raddmapN.put(Rot.Lf135, Ori.SW);
    raddmapN.put(Rot.Lf090, Ori.W);
    raddmapN.put(Rot.Lf045, Ori.NW);
    RotationsAdd.put(Ori.N, raddmapN);

    final Map<Rot, Ori> raddmapNE = new EnumMap<>(Rot.class);
    raddmapNE.put(Rot.Fw000, Ori.NE);
    raddmapNE.put(Rot.Rg045, Ori.E);
    raddmapNE.put(Rot.Rg090, Ori.SE);
    raddmapNE.put(Rot.Rg135, Ori.S);
    raddmapNE.put(Rot.Bw180, Ori.SW);
    raddmapNE.put(Rot.Lf135, Ori.W);
    raddmapNE.put(Rot.Lf090, Ori.NW);
    raddmapNE.put(Rot.Lf045, Ori.N);
    RotationsAdd.put(Ori.NE, raddmapNE);

    final Map<Rot, Ori> raddmapE = new EnumMap<>(Rot.class);
    raddmapE.put(Rot.Fw000, Ori.E);
    raddmapE.put(Rot.Rg045, Ori.SE);
    raddmapE.put(Rot.Rg090, Ori.S);
    raddmapE.put(Rot.Rg135, Ori.SW);
    raddmapE.put(Rot.Bw180, Ori.W);
    raddmapE.put(Rot.Lf135, Ori.NW);
    raddmapE.put(Rot.Lf090, Ori.N);
    raddmapE.put(Rot.Lf045, Ori.NE);
    RotationsAdd.put(Ori.E, raddmapE);

    final Map<Rot, Ori> raddmapSE = new EnumMap<>(Rot.class);
    raddmapSE.put(Rot.Fw000, Ori.SE);
    raddmapSE.put(Rot.Rg045, Ori.S);
    raddmapSE.put(Rot.Rg090, Ori.SW);
    raddmapSE.put(Rot.Rg135, Ori.W);
    raddmapSE.put(Rot.Bw180, Ori.NW);
    raddmapSE.put(Rot.Lf135, Ori.N);
    raddmapSE.put(Rot.Lf090, Ori.NE);
    raddmapSE.put(Rot.Lf045, Ori.E);
    RotationsAdd.put(Ori.SE, raddmapSE);

    final Map<Rot, Ori> raddmapS = new EnumMap<>(Rot.class);
    raddmapS.put(Rot.Fw000, Ori.S);
    raddmapS.put(Rot.Rg045, Ori.SW);
    raddmapS.put(Rot.Rg090, Ori.W);
    raddmapS.put(Rot.Rg135, Ori.NW);
    raddmapS.put(Rot.Bw180, Ori.N);
    raddmapS.put(Rot.Lf135, Ori.NE);
    raddmapS.put(Rot.Lf090, Ori.E);
    raddmapS.put(Rot.Lf045, Ori.SE);
    RotationsAdd.put(Ori.S, raddmapS);

    final Map<Rot, Ori> raddmapSW = new EnumMap<>(Rot.class);
    raddmapSW.put(Rot.Fw000, Ori.SW);
    raddmapSW.put(Rot.Rg045, Ori.W);
    raddmapSW.put(Rot.Rg090, Ori.NW);
    raddmapSW.put(Rot.Rg135, Ori.N);
    raddmapSW.put(Rot.Bw180, Ori.NE);
    raddmapSW.put(Rot.Lf135, Ori.E);
    raddmapSW.put(Rot.Lf090, Ori.SE);
    raddmapSW.put(Rot.Lf045, Ori.S);
    RotationsAdd.put(Ori.SW, raddmapSW);

    final Map<Rot, Ori> raddmapW = new EnumMap<>(Rot.class);
    raddmapW.put(Rot.Fw000, Ori.W);
    raddmapW.put(Rot.Rg045, Ori.NW);
    raddmapW.put(Rot.Rg090, Ori.N);
    raddmapW.put(Rot.Rg135, Ori.NE);
    raddmapW.put(Rot.Bw180, Ori.E);
    raddmapW.put(Rot.Lf135, Ori.SE);
    raddmapW.put(Rot.Lf090, Ori.S);
    raddmapW.put(Rot.Lf045, Ori.SW);
    RotationsAdd.put(Ori.W, raddmapW);

    final Map<Rot, Ori> raddmapNW = new EnumMap<>(Rot.class);
    raddmapNW.put(Rot.Fw000, Ori.NW);
    raddmapNW.put(Rot.Rg045, Ori.N);
    raddmapNW.put(Rot.Rg090, Ori.NE);
    raddmapNW.put(Rot.Rg135, Ori.E);
    raddmapNW.put(Rot.Bw180, Ori.SE);
    raddmapNW.put(Rot.Lf135, Ori.S);
    raddmapNW.put(Rot.Lf090, Ori.SW);
    raddmapNW.put(Rot.Lf045, Ori.W);
    RotationsAdd.put(Ori.NW, raddmapNW);

    // rotations sub

    final Map<Ori, Rot> rsubmapN = new EnumMap<>(Ori.class);
    rsubmapN.put(Ori.N, Rot.Fw000);
    rsubmapN.put(Ori.NE, Rot.Lf045);
    rsubmapN.put(Ori.E, Rot.Lf090);
    rsubmapN.put(Ori.SE, Rot.Lf135);
    rsubmapN.put(Ori.S, Rot.Bw180);
    rsubmapN.put(Ori.SW, Rot.Rg135);
    rsubmapN.put(Ori.W, Rot.Rg090);
    rsubmapN.put(Ori.NW, Rot.Rg045);
    RotationsSub.put(Ori.N, rsubmapN);

    final Map<Ori, Rot> rsubmapNE = new EnumMap<>(Ori.class);
    rsubmapNE.put(Ori.N, Rot.Rg045);
    rsubmapNE.put(Ori.NE, Rot.Fw000);
    rsubmapNE.put(Ori.E, Rot.Lf045);
    rsubmapNE.put(Ori.SE, Rot.Lf090);
    rsubmapNE.put(Ori.S, Rot.Lf135);
    rsubmapNE.put(Ori.SW, Rot.Bw180);
    rsubmapNE.put(Ori.W, Rot.Rg135);
    rsubmapNE.put(Ori.NW, Rot.Rg090);
    RotationsSub.put(Ori.NE, rsubmapNE);

    final Map<Ori, Rot> rsubmapE = new EnumMap<>(Ori.class);
    rsubmapE.put(Ori.N, Rot.Rg090);
    rsubmapE.put(Ori.NE, Rot.Rg045);
    rsubmapE.put(Ori.E, Rot.Fw000);
    rsubmapE.put(Ori.SE, Rot.Lf045);
    rsubmapE.put(Ori.S, Rot.Lf090);
    rsubmapE.put(Ori.SW, Rot.Lf135);
    rsubmapE.put(Ori.W, Rot.Bw180);
    rsubmapE.put(Ori.NW, Rot.Rg135);
    RotationsSub.put(Ori.E, rsubmapE);

    final Map<Ori, Rot> rsubmapSE = new EnumMap<>(Ori.class);
    rsubmapSE.put(Ori.N, Rot.Rg135);
    rsubmapSE.put(Ori.NE, Rot.Rg090);
    rsubmapSE.put(Ori.E, Rot.Rg045);
    rsubmapSE.put(Ori.SE, Rot.Fw000);
    rsubmapSE.put(Ori.S, Rot.Lf045);
    rsubmapSE.put(Ori.SW, Rot.Lf090);
    rsubmapSE.put(Ori.W, Rot.Lf135);
    rsubmapSE.put(Ori.NW, Rot.Bw180);
    RotationsSub.put(Ori.SE, rsubmapSE);

    final Map<Ori, Rot> rsubmapS = new EnumMap<>(Ori.class);
    rsubmapS.put(Ori.N, Rot.Bw180);
    rsubmapS.put(Ori.NE, Rot.Rg135);
    rsubmapS.put(Ori.E, Rot.Rg090);
    rsubmapS.put(Ori.SE, Rot.Rg045);
    rsubmapS.put(Ori.S, Rot.Fw000);
    rsubmapS.put(Ori.SW, Rot.Lf045);
    rsubmapS.put(Ori.W, Rot.Lf090);
    rsubmapS.put(Ori.NW, Rot.Lf135);
    RotationsSub.put(Ori.S, rsubmapS);

    final Map<Ori, Rot> rsubmapSW = new EnumMap<>(Ori.class);
    rsubmapSW.put(Ori.N, Rot.Lf135);
    rsubmapSW.put(Ori.NE, Rot.Bw180);
    rsubmapSW.put(Ori.E, Rot.Rg135);
    rsubmapSW.put(Ori.SE, Rot.Rg090);
    rsubmapSW.put(Ori.S, Rot.Rg045);
    rsubmapSW.put(Ori.SW, Rot.Fw000);
    rsubmapSW.put(Ori.W, Rot.Lf045);
    rsubmapSW.put(Ori.NW, Rot.Lf090);
    RotationsSub.put(Ori.SW, rsubmapSW);

    final Map<Ori, Rot> rsubmapW = new EnumMap<>(Ori.class);
    rsubmapW.put(Ori.N, Rot.Lf090);
    rsubmapW.put(Ori.NE, Rot.Lf135);
    rsubmapW.put(Ori.E, Rot.Bw180);
    rsubmapW.put(Ori.SE, Rot.Rg135);
    rsubmapW.put(Ori.S, Rot.Rg090);
    rsubmapW.put(Ori.SW, Rot.Rg045);
    rsubmapW.put(Ori.W, Rot.Fw000);
    rsubmapW.put(Ori.NW, Rot.Lf045);
    RotationsSub.put(Ori.W, rsubmapW);

    final Map<Ori, Rot> rsubmapNW = new EnumMap<>(Ori.class);
    rsubmapNW.put(Ori.N, Rot.Lf045);
    rsubmapNW.put(Ori.NE, Rot.Lf090);
    rsubmapNW.put(Ori.E, Rot.Lf135);
    rsubmapNW.put(Ori.SE, Rot.Bw180);
    rsubmapNW.put(Ori.S, Rot.Rg135);
    rsubmapNW.put(Ori.SW, Rot.Rg090);
    rsubmapNW.put(Ori.W, Rot.Rg045);
    rsubmapNW.put(Ori.NW, Rot.Fw000);
    RotationsSub.put(Ori.NW, rsubmapNW);

    // transposes

    final Map<Dir, Dis> tmapN = new EnumMap<>(Dir.class);
    tmapN.put(Dir.Fw, Dis.N);
    tmapN.put(Dir.FwRg, Dis.NE);
    tmapN.put(Dir.Rg, Dis.E);
    tmapN.put(Dir.BwRg, Dis.SE);
    tmapN.put(Dir.Bw, Dis.S);
    tmapN.put(Dir.BwLf, Dis.SW);
    tmapN.put(Dir.Lf, Dis.W);
    tmapN.put(Dir.FwLf, Dis.NW);
    tmapN.put(Dir.X, Dis.X);
    Transposes.put(Ori.N, tmapN);

    final Map<Dir, Dis> tmapNE = new EnumMap<>(Dir.class);
    tmapNE.put(Dir.Fw, Dis.NE);
    tmapNE.put(Dir.FwRg, Dis.E);
    tmapNE.put(Dir.Rg, Dis.SE);
    tmapNE.put(Dir.BwRg, Dis.S);
    tmapNE.put(Dir.Bw, Dis.SW);
    tmapNE.put(Dir.BwLf, Dis.W);
    tmapNE.put(Dir.Lf, Dis.NW);
    tmapNE.put(Dir.FwLf, Dis.N);
    tmapNE.put(Dir.X, Dis.X);
    Transposes.put(Ori.NE, tmapNE);

    final Map<Dir, Dis> tmapE = new EnumMap<>(Dir.class);
    tmapE.put(Dir.Fw, Dis.E);
    tmapE.put(Dir.FwRg, Dis.SE);
    tmapE.put(Dir.Rg, Dis.S);
    tmapE.put(Dir.BwRg, Dis.SW);
    tmapE.put(Dir.Bw, Dis.W);
    tmapE.put(Dir.BwLf, Dis.NW);
    tmapE.put(Dir.Lf, Dis.N);
    tmapE.put(Dir.FwLf, Dis.NE);
    tmapE.put(Dir.X, Dis.X);
    Transposes.put(Ori.E, tmapE);

    final Map<Dir, Dis> tmapSE = new EnumMap<>(Dir.class);
    tmapSE.put(Dir.Fw, Dis.SE);
    tmapSE.put(Dir.FwRg, Dis.S);
    tmapSE.put(Dir.Rg, Dis.SW);
    tmapSE.put(Dir.BwRg, Dis.W);
    tmapSE.put(Dir.Bw, Dis.NW);
    tmapSE.put(Dir.BwLf, Dis.N);
    tmapSE.put(Dir.Lf, Dis.NE);
    tmapSE.put(Dir.FwLf, Dis.E);
    tmapSE.put(Dir.X, Dis.X);
    Transposes.put(Ori.SE, tmapSE);

    final Map<Dir, Dis> tmapS = new EnumMap<>(Dir.class);
    tmapS.put(Dir.Fw, Dis.S);
    tmapS.put(Dir.FwRg, Dis.SW);
    tmapS.put(Dir.Rg, Dis.W);
    tmapS.put(Dir.BwRg, Dis.NW);
    tmapS.put(Dir.Bw, Dis.N);
    tmapS.put(Dir.BwLf, Dis.NE);
    tmapS.put(Dir.Lf, Dis.E);
    tmapS.put(Dir.FwLf, Dis.SE);
    tmapS.put(Dir.X, Dis.X);
    Transposes.put(Ori.S, tmapS);

    final Map<Dir, Dis> mapSW = new EnumMap<>(Dir.class);
    mapSW.put(Dir.Fw, Dis.SW);
    mapSW.put(Dir.FwRg, Dis.W);
    mapSW.put(Dir.Rg, Dis.NW);
    mapSW.put(Dir.BwRg, Dis.N);
    mapSW.put(Dir.Bw, Dis.NE);
    mapSW.put(Dir.BwLf, Dis.E);
    mapSW.put(Dir.Lf, Dis.SE);
    mapSW.put(Dir.FwLf, Dis.S);
    mapSW.put(Dir.X, Dis.X);
    Transposes.put(Ori.SW, mapSW);

    final Map<Dir, Dis> tmapW = new EnumMap<>(Dir.class);
    tmapW.put(Dir.Fw, Dis.W);
    tmapW.put(Dir.FwRg, Dis.NW);
    tmapW.put(Dir.Rg, Dis.N);
    tmapW.put(Dir.BwRg, Dis.NE);
    tmapW.put(Dir.Bw, Dis.E);
    tmapW.put(Dir.BwLf, Dis.SE);
    tmapW.put(Dir.Lf, Dis.S);
    tmapW.put(Dir.FwLf, Dis.SW);
    tmapW.put(Dir.X, Dis.X);
    Transposes.put(Ori.W, tmapW);

    final Map<Dir, Dis> tmapNW = new EnumMap<>(Dir.class);
    tmapNW.put(Dir.Fw, Dis.NW);
    tmapNW.put(Dir.FwRg, Dis.N);
    tmapNW.put(Dir.Rg, Dis.NE);
    tmapNW.put(Dir.BwRg, Dis.E);
    tmapNW.put(Dir.Bw, Dis.SE);
    tmapNW.put(Dir.BwLf, Dis.S);
    tmapNW.put(Dir.Lf, Dis.SW);
    tmapNW.put(Dir.FwLf, Dis.W);
    tmapNW.put(Dir.X, Dis.X);
    Transposes.put(Ori.NW, tmapNW);

    // opposites

    DirOpposites.put(Dir.Fw, Dir.Bw);
    DirOpposites.put(Dir.FwRg, Dir.BwLf);
    DirOpposites.put(Dir.Rg, Dir.Lf);
    DirOpposites.put(Dir.BwRg, Dir.FwLf);
    DirOpposites.put(Dir.Bw, Dir.Fw);
    DirOpposites.put(Dir.BwLf, Dir.FwRg);
    DirOpposites.put(Dir.Lf, Dir.Rg);
    DirOpposites.put(Dir.FwLf, Dir.BwRg);
    DirOpposites.put(Dir.X, Dir.X);

    DisOpposites.put(Dis.N, Dis.S);
    DisOpposites.put(Dis.NE, Dis.SW);
    DisOpposites.put(Dis.E, Dis.W);
    DisOpposites.put(Dis.SE, Dis.NW);
    DisOpposites.put(Dis.S, Dis.N);
    DisOpposites.put(Dis.SW, Dis.NE);
    DisOpposites.put(Dis.W, Dis.E);
    DisOpposites.put(Dis.NW, Dis.SE);
    DisOpposites.put(Dis.X, Dis.X);

    OriOpposites.put(Ori.N, Ori.S);
    OriOpposites.put(Ori.NE, Ori.SW);
    OriOpposites.put(Ori.E, Ori.W);
    OriOpposites.put(Ori.SE, Ori.NW);
    OriOpposites.put(Ori.S, Ori.N);
    OriOpposites.put(Ori.SW, Ori.NE);
    OriOpposites.put(Ori.W, Ori.E);
    OriOpposites.put(Ori.NW, Ori.SE);

    RotOpposites.put(Rot.Fw000, Rot.Fw000);
    RotOpposites.put(Rot.Rg045, Rot.Lf045);
    RotOpposites.put(Rot.Rg090, Rot.Lf090);
    RotOpposites.put(Rot.Rg135, Rot.Lf135);
    RotOpposites.put(Rot.Bw180, Rot.Bw180);
    RotOpposites.put(Rot.Lf135, Rot.Rg135);
    RotOpposites.put(Rot.Lf090, Rot.Rg090);
    RotOpposites.put(Rot.Lf045, Rot.Rg045);

    DisToOris.put(Dis.N, Ori.N);
    DisToOris.put(Dis.NE, Ori.NE);
    DisToOris.put(Dis.E, Ori.E);
    DisToOris.put(Dis.SE, Ori.SE);
    DisToOris.put(Dis.S, Ori.S);
    DisToOris.put(Dis.SW, Ori.SW);
    DisToOris.put(Dis.W, Ori.W);
    DisToOris.put(Dis.NW, Ori.NW);
  }



  public static Dis sub(final Pos a, final Pos b) {
    return DisGrid[signum(a.i - b.i) + 1][signum(a.j - b.j) + 1];
  }

  public static Dis transpose(final Ori o, final Dir d) {
    return Transposes.get(o).get(d);
  }

  public static Ori add(final Ori o, final Rot r) {
    return RotationsAdd.get(o).get(r);
  }

  public static Rot sub(final Ori a, final Ori b) {
    return RotationsSub.get(a).get(b);
  }

  public static Dir opposite(final Dir d) {
    return DirOpposites.get(d);
  }

  public static Dis opposite(final Dis d) {
    return DisOpposites.get(d);
  }

  public static Ori opposite(final Ori o) {
    return OriOpposites.get(o);
  }

  public static Rot opposite(final Rot r) {
    return RotOpposites.get(r);
  }

  public static Ori convert(final Dis d, final Ori def) {
    return DisToOris.getOrDefault(d, def);
  }



  private Transforms() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
