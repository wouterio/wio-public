package io.wouter.lib.grid;

import static java.lang.Math.sqrt;

public enum Dis {

  N(0, 1),
  NE(1, 1),
  E(1, 0),
  SE(1, -1),
  S(0, -1),
  SW(-1, -1),
  W(-1, 0),
  NW(-1, 1),
  X(0, 0);



  public final int di;
  public final int dj;
  public final float dist;
  public final float cos;
  public final float sin;



  private Dis(final int di, final int dj) {
    this.di = di;
    this.dj = dj;

    dist = (float) sqrt(di * di + dj * dj);
    cos = di / dist;
    sin = dj / dist;
  }



  public Dis opposite() {
    return Transforms.opposite(this);
  }



  public Ori toOri(final Ori def) {
    return Transforms.convert(this, def);
  }
}
