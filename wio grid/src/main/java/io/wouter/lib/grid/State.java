package io.wouter.lib.grid;

import static java.util.Objects.hash;
import static java.util.Objects.requireNonNull;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Objects;

public final class State {

  public final Pos pos;
  public final Ori ori;

  private final int hash;



  public State(final Pos p, final Ori o) {
    pos = requireNonNull(p);
    ori = requireNonNull(o);
    hash = hash(pos, ori);
  }



  public State move(final Move m) {
    final Dis dis = ori.transpose(m.dir);
    final Pos newpos = pos.add(dis);
    final Ori newori = ori.add(m.rot);
    return new State(newpos, newori);
  }



  @Override
  public boolean equals(final Object o) {
    return o instanceof State && equals(this, (State) o);
  }

  @Override
  public int hashCode() {
    return hash;
  }

  @Override
  public String toString() {
    return new StringBuilder().append('(').append(pos).append('-').append(ori).append(')').toString();
  }



  private static boolean equals(final State a, final State b) {
    return Objects.equals(a.pos, b.pos) && Objects.equals(a.ori, b.ori);
  }



  private Object writeReplace() {
    return new Serial(this);
  }

  @SuppressWarnings("static-method")
  private void readObject(@SuppressWarnings("unused") final ObjectInputStream s) throws InvalidObjectException {
    throw new InvalidObjectException("Serial proxy needed."); //$NON-NLS-1$
  }



  //



  private static class Serial implements Serializable {

    private static final long serialVersionUID = 7937334762629716295L;



    public final Pos pos;
    public final Ori ori;



    Serial(final State s) {
      pos = s.pos;
      ori = s.ori;
    }



    private Object readResolve() {
      return new State(pos, ori);
    }
  }
}
