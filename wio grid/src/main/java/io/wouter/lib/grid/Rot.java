package io.wouter.lib.grid;

public enum Rot {

  Fw000(0),
  Rg045(1),
  Rg090(2),
  Rg135(3),
  Bw180(4),
  Lf135(3),
  Lf090(2),
  Lf045(1);



  public final int ticks;



  private Rot(final int ticks) {
    this.ticks = ticks;
  }



  public Rot opposite() {
    return Transforms.opposite(this);
  }
}
