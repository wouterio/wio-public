package io.wouter.lib.grid;

public class OriTest {

  public static void main(final String[] args) {
    for (final Ori o : Ori.values())
      System.out.println(o + " cos=" + o.cos + " sin=" + o.sin); //$NON-NLS-1$ //$NON-NLS-2$

    System.out.println("---- add"); //$NON-NLS-1$

    for (final Ori o1 : Ori.values())
      for (final Rot r1 : Rot.values()) {
        final Ori o2 = o1.add(r1);
        System.out.println(o1 + " add " + r1 + " -> Ori." + o2); //$NON-NLS-1$ //$NON-NLS-2$
        final Rot r2 = r1.opposite();
        System.out.println(o2 + " add " + r2 + " -> Ori." + o2.add(r2)); //$NON-NLS-1$ //$NON-NLS-2$
        System.out.println();
      }

    System.out.println("---- sub"); //$NON-NLS-1$

    for (final Ori o1 : Ori.values())
      for (int i = 0; i < Ori.values().length; ++i) {
        final Ori o2 = Ori.values()[i];
        final Rot r1 = o1.sub(o2);
        System.out.println(o1 + " sub " + o2 + " -> Rot." + r1); //$NON-NLS-1$ //$NON-NLS-2$
        System.out.println(o2 + " add " + r1 + " -> Ori." + o2.add(r1)); //$NON-NLS-1$ //$NON-NLS-2$
        System.out.println();
      }

    System.out.println("---- transpose"); //$NON-NLS-1$

    for (final Ori o : Ori.values())
      for (final Dir d : Dir.values())
        System.out.println(o + " transpose " + d + " -> Dis." + o.transpose(d)); //$NON-NLS-1$ //$NON-NLS-2$

    System.out.println("---- opposite"); //$NON-NLS-1$

    for (final Ori o : Ori.values())
      System.out.println(o + " opp -> " + o.opposite()); //$NON-NLS-1$
  }
}
