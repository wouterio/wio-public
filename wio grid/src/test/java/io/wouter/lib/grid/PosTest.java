package io.wouter.lib.grid;

import io.wouter.lib.grid.Dis;
import io.wouter.lib.grid.Pos;

public class PosTest {

  public static void main(final String[] args) {
    final Pos p1 = Pos.get(0, 0);
    for (final Dis d : Dis.values()) {
      final Pos p2 = p1.add(d);
      System.out.println(p1 + " add " + d + " -> " + p2); //$NON-NLS-1$ //$NON-NLS-2$
      System.out.println(p2 + " sub " + p1 + " -> " + p2.sub(p1)); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }
}
