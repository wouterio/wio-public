package io.wouter.lib.grid;

public class DisTest {

  public static void main(final String[] args) {
    for (final Dis d : Dis.values())
      System.out.println(d + " di=" + d.di + " dj=" + d.dj + " d=" + d.dist + " cos=" + d.cos + " sin=" + d.sin); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

    System.out.println("---- opposite"); //$NON-NLS-1$

    for (final Dis d : Dis.values())
      System.out.println(d + " opp -> " + d.opposite()); //$NON-NLS-1$
  }
}
