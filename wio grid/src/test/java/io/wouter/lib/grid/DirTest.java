package io.wouter.lib.grid;

import io.wouter.lib.grid.Dir;
import io.wouter.lib.grid.Ori;

public class DirTest {

  public static void main(final String[] args) {
    for (final Dir d : Dir.values())
      System.out.println(d + " ticks=" + d.ticks); //$NON-NLS-1$

    System.out.println("---- transpose"); //$NON-NLS-1$

    for (final Ori o : Ori.values())
      for (final Dir d : Dir.values())
        System.out.println(o + " transpose " + d + " -> Dis." + d.transpose(o)); //$NON-NLS-1$ //$NON-NLS-2$

    System.out.println("---- opposite"); //$NON-NLS-1$

    for (final Dir d : Dir.values())
      System.out.println(d + " opp -> " + d.opposite()); //$NON-NLS-1$
  }
}
