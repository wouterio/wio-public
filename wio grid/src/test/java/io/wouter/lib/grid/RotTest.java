package io.wouter.lib.grid;

public class RotTest {

  public static void main(final String[] args) {
    for (final Rot r : Rot.values())
      System.out.println(r + " ticks=" + r.ticks); //$NON-NLS-1$

    System.out.println("---- opposite (mirrored)"); //$NON-NLS-1$

    for (final Rot r : Rot.values())
      System.out.println(r + " opp -> " + r.opposite()); //$NON-NLS-1$
  }
}
