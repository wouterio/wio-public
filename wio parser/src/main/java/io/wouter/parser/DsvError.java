package io.wouter.parser;

public interface DsvError {

  long getRow();

  int getColumn();

  DsvValue getValue();

  String getMessage();
}
