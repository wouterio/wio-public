package io.wouter.parser;

import java.util.Iterator;

public interface DsvReader extends Iterator<DsvValue> {

  /**
   * Read current value.
   *
   * @return current value
   */
  DsvValue readValue();

  /**
   * Skip current value.
   */
  void skipValue();

  /**
   * Skip rest of line and move to next.
   */
  void nextLine();
}
