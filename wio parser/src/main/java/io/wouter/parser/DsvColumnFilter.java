package io.wouter.parser;

import static java.util.Objects.requireNonNull;

import java.util.function.BiConsumer;
import java.util.function.Predicate;

/**
 * A WioColumnFilter may serve several purposes. Typically it may:<br>
 * - convert a value to a certain type<br>
 * - test the converted value<br>
 * - assign the converted value to the output<br>
 * - test the output<br>
 *
 * @param <T> output type
 */
@FunctionalInterface
public interface DsvColumnFilter<T> {

  /**
   * Processes a value with regard to an output object. A WioColumnFilter typically may:<br>
   * - test the value parameter<br>
   * - convert the value parameter to a primitive value<br>
   * - test the primitive value<br>
   * - assign the primitive to the output object parameter<br>
   * - test the output object<br>
   * Note that there is no point in keeping a reference to the value parameter itself, since its content changes when reading a next value.
   *
   * @param out output from current row.
   * @param value value of current column in current row.
   * @return whether to keep processing this row (true) or not (false).
   */
  boolean process(T out, DsvValue value);



  static <T> DsvColumnFilter<T> testValue(final Predicate<DsvValue> valuepredicate) {
    requireNonNull(valuepredicate);
    return (out, value) -> valuepredicate.test(value);
  }

  static <T> DsvColumnFilter<T> perform(final BiConsumer<T, DsvValue> action) {
    requireNonNull(action);
    return (out, value) -> {
      action.accept(out, value);
      return true;
    };
  }

  static <T> DsvColumnFilter<T> testObject(final Predicate<T> objectpredicate) {
    requireNonNull(objectpredicate);
    return (out, value) -> objectpredicate.test(out);
  }



  default DsvColumnFilter<T> andThen(final DsvColumnFilter<T> next) {
    requireNonNull(next);
    return (out, value) -> process(out, value) && next.process(out, value);
  }

  default DsvColumnFilter<T> andThenTestValue(final Predicate<DsvValue> valuefilter) {
    return andThen(testValue(valuefilter));
  }

  default DsvColumnFilter<T> andThenPerform(final BiConsumer<T, DsvValue> action) {
    return andThen(perform(action));
  }

  default DsvColumnFilter<T> andThenTestObject(final Predicate<T> objectfilter) {
    return andThen(testObject(objectfilter));
  }
}
