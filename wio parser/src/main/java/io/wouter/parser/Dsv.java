package io.wouter.parser;

import java.io.InputStream;

import io.wouter.parser.impl.ImplFactory;
import io.wouter.parser.settings.DsvArrayParserSettings;
import io.wouter.parser.settings.DsvObjectParserSettings;

public final class Dsv {

  public static DsvArrayParserSettings newArraySettings() {
    return new DsvArrayParserSettings();
  }

  public static <T> DsvObjectParserSettings<T> newObjectSettings() {
    return new DsvObjectParserSettings<>();
  }



  public static DsvReader newReader(final InputStream is, final DsvSettings settings) {
    return ImplFactory.newReader(is, settings);
  }

  public static DsvParser<String[]> newParser(final InputStream is, final DsvArrayParserSettings settings) {
    return ImplFactory.newParser(is, settings);
  }

  public static <T> DsvParser<T> newParser(final InputStream is, final DsvObjectParserSettings<T> settings) {
    return ImplFactory.newParser(is, settings);
  }



  private Dsv() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
