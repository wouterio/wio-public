package io.wouter.parser;

/**
 * Class representing a value in a DSV text value.<br>
 * Note that there is no point in keeping a reference to a WioValue instance, as its content changes when reading a next value.
 */
public interface DsvValue {

  /**
   * @return boolean value
   */
  boolean toBoolean();

  /**
   * @return int value
   * @throws NumberFormatException if the value does not contain a parsable integer.
   */
  int toInt();

  /**
   * @return long value
   * @throws NumberFormatException if the string does not contain a parsable long.
   */
  long toLong();

  /**
   * @return float value
   * @throws NumberFormatException if the string does not contain a parsable float.
   */
  float toFloat();

  /**
   * @return double value
   * @throws NumberFormatException if the string does not contain a parsable double.
   */
  double toDouble();

  /**
   * Returns a CharSequence instance<br>
   * <br>
   * Whereas toString() creates a new String instance with a new backing array, toCharSequence() reuses the same CharSequence instance with
   * the same backing array. This makes toCharSequence() much more efficient than toString().<br>
   * <br>
   * There is no point in keeping a reference to a CharSequence instance, as its content changes when reading a next value. Use toString()
   * instead for a persistent reference to a text value.
   *
   * @return CharSequence instance
   */
  CharSequence toCharSequence();
}
