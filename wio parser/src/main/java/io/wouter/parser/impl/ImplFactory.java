package io.wouter.parser.impl;

import java.io.InputStream;
import java.io.InputStreamReader;

import io.wouter.parser.DsvError;
import io.wouter.parser.DsvParser;
import io.wouter.parser.DsvReader;
import io.wouter.parser.DsvSettings;
import io.wouter.parser.DsvValue;
import io.wouter.parser.settings.DsvArrayParserSettings;
import io.wouter.parser.settings.DsvObjectParserSettings;

public class ImplFactory {

  public static DsvReader newReader(final InputStream is, final DsvSettings settings) {
    return new DsvReaderI(new InputStreamReader(is, settings.getCharset()), settings);
  }



  public static DsvParser<String[]> newParser(final InputStream is, final DsvArrayParserSettings settings) {
    return new DsvArrayParserI(newReader(is, settings), settings);
  }

  public static <T> DsvParser<T> newParser(final InputStream is, final DsvObjectParserSettings<T> settings) {
    return new DsvObjectParserI<>(newReader(is, settings), settings);
  }



  public static DsvError newError(final long rowidx, final int colidx, final DsvValue value, final String message) {
    return new DsvErrorI(rowidx, colidx, value, message);
  }



  private ImplFactory() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
