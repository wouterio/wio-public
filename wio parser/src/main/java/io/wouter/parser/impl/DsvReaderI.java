package io.wouter.parser.impl;

import static java.lang.System.arraycopy;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.util.NoSuchElementException;

import io.wouter.parser.DsvReader;
import io.wouter.parser.DsvSettings;
import io.wouter.parser.DsvValue;

final class DsvReaderI implements DsvReader {

  private final Reader reader;
  private final DsvValueI value;
  private final char delimiter;
  private final char newline;
  private final char quote;

  private char[] buffer;
  private int nbufferchars = 0;

  private int delimLFidx = -1;
  private int delimRGidx;
  private int newlineidx = 0;
  private boolean eol;



  DsvReaderI(final Reader reader, final DsvSettings settings) {
    this.reader = requireNonNull(reader);
    value = new DsvValueI(settings);
    delimiter = settings.getDelimiter();
    newline = settings.getNewLine();
    quote = settings.getQuote();
    buffer = new char[settings.getBufferSize()];
  }



  @Override
  public boolean hasNext() {
    return delimLFidx < nbufferchars;
  }

  @Override
  public DsvValue next() {
    return readValue();
  }



  @Override
  public DsvValue readValue() {
    checkColumn();
    checkNext();
    findNextDelimiter(); // move right delimiter index to next unquoted delimiter or newline
    value.set(buffer, delimLFidx, delimRGidx);
    delimLFidx = delimRGidx; // move left delimiter index to right delimiter index
    return value;
  }

  @Override
  public void skipValue() {
    checkColumn();
    checkNext();
    findNextDelimiter(); // move right delimiter index to next unquoted delimiter or newline
    delimLFidx = delimRGidx; // move left delimiter index to right delimiter index
  }

  @Override
  public void nextLine() {
    findNextLine(); // move left delimiter index to next unquoted newline
  }



  private void findNextDelimiter() {
    value.resetEscapes();

    boolean cr = false;
    DsvQuoteState quotestate = DsvQuoteState.Unquoted;
    for (delimRGidx = delimLFidx + 1; true; ++delimRGidx) { // +1 to prevent looking at delimLFidx again
      // if (delimRGidx >= nbufferchars && !fill())
      if (checkFill(delimRGidx))
        return;

      final char c = buffer[delimRGidx];

      if (quotestate.check) {
        if (c == '\r') {
          cr = true;
          continue;
        }
        if (c == newline) {
          eol = true;
          if (cr)
            --delimRGidx;
          return;
        }
        if (c == delimiter)
          return;
        if (c != quote)
          continue;
      }

      final boolean isquote = c == quote;
      if (quotestate.change || isquote)
        quotestate = quotestate.change(isquote);
      if (quotestate.escape)
        value.addEscape();
    }
  }

  private void findNextLine() {
    eol = false;

    boolean found = false;
    DsvQuoteState quotestate = DsvQuoteState.Unquoted;
    for (newlineidx = delimRGidx; true; ++newlineidx) { // no +1 since delimRGidx might be the newline to look for
      // if (newlineidx >= nbufferchars && !fill()) {
      if (checkFill(newlineidx)) {
        delimLFidx = nbufferchars;
        return;
      }

      final char c = buffer[newlineidx];

      if (quotestate.check) {
        if (c == newline) {
          found = true;
          delimLFidx = newlineidx;
          continue;
        }
        if (found && !isWhitespace(c))
          return;
        if (c != quote)
          continue;
      }

      final boolean isquote = c == quote;
      if (quotestate.change || isquote)
        quotestate = quotestate.change(isquote);
    }
  }



  private boolean checkFill(final int idx) {
    return idx >= nbufferchars && !fill();
  }

  private boolean fill() {
    final int buffershift = delimLFidx; // buffershift == -1 at first fill
    if (buffershift > 0) { // if buffer can be shifted ...
      nbufferchars -= buffershift; // ... shift all indices ...
      newlineidx -= buffershift;
      delimLFidx -= buffershift;
      delimRGidx -= buffershift;
      arraycopy(buffer, buffershift, buffer, 0, nbufferchars); // ... and shift buffer
    } else if (delimRGidx >= buffer.length) { // buffershift <= 0, if unable to shift but need more buffer ...
      final char[] newbuffer = new char[buffer.length << 1]; // ... double buffer size ...
      arraycopy(buffer, 0, newbuffer, 0, nbufferchars); // ... copy buffer ...
      buffer = newbuffer; // ... and replace buffer
    }

    int ncharsread;
    try {
      do
        ncharsread = reader.read(buffer, nbufferchars, buffer.length - nbufferchars);
      while (ncharsread == 0);
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }

    if (ncharsread < 0)
      return false;
    nbufferchars += ncharsread;
    return true;
  }



  private void checkColumn() {
    if (eol)
      throw new NoSuchElementException("No more columns."); //$NON-NLS-1$
  }

  private void checkNext() {
    if (!hasNext())
      throw new NoSuchElementException("No more values."); //$NON-NLS-1$
  }



  private static boolean isWhitespace(final char c) { // somehow Character.isWhiteSpace seems quite slow
    return c == ' ' || c == '\t' || c == '\r' || c == '\n';
  }
}
