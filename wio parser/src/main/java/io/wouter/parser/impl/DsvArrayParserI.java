package io.wouter.parser.impl;

import static java.util.Objects.requireNonNull;
import static java.util.Spliterator.DISTINCT;
import static java.util.Spliterator.ORDERED;
import static java.util.Spliterators.spliteratorUnknownSize;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import io.wouter.parser.DsvError;
import io.wouter.parser.DsvParser;
import io.wouter.parser.DsvReader;
import io.wouter.parser.settings.DsvArrayParserSettings;

final class DsvArrayParserI implements DsvParser<String[]> {

  private final DsvReader reader;
  private final WioColumn[] columns;
  private final Collection<DsvError> errors = new LinkedList<>();



  private boolean hasnext;
  private long rowidx = 0;



  DsvArrayParserI(final DsvReader reader, final DsvArrayParserSettings settings) {
    this.reader = requireNonNull(reader);
    columns = settings //
        .getColumns() //
        .entrySet() //
        .stream() //
        .map(e -> new WioColumn(e.getKey().intValue(), e.getValue())) //
        .toArray(WioColumn[]::new); //
    Arrays.sort(columns, (a, b) -> Integer.compare(a.idx, b.idx));
    if (settings.hasHeaders())
      reader.nextLine();
  }



  @Override
  public boolean hasNext() {
    hasnext = reader.hasNext();
    return hasnext;
  }

  @Override
  public String[] next() {
    if (!hasnext)
      throw new NoSuchElementException();

    final String[] out = new String[columns.length]; // output array for this line

    ++rowidx;
    int colidx = 0; // current column index
    int outidx = 0; // output array index

    try {
      for (final WioColumn nxtcol : columns) { // next column index

        for (; colidx < nxtcol.idx; ++colidx) // skip current column until next column
          reader.skipValue();

        out[outidx] = reader.readValue().toString(); // read column

        if (nxtcol.hasfilter && !nxtcol.filter.test(out)) { // if not happy with output so far ...
          reader.nextLine(); // ... don't waste any more cpu on this line ...
          return null; // ... and quite
        }

        ++colidx; // step over column just read
        ++outidx; // move output array index
      }

      reader.nextLine(); // done processing this line ...
      return out; // .. and return output

    } catch (final NoSuchElementException e) {
      errors.add(ImplFactory.newError(rowidx, colidx, null, e.getMessage()));
      return null;
    }
  }



  @Override
  public Stream<String[]> stream() {
    return StreamSupport.stream(spliteratorUnknownSize(this, ORDERED | DISTINCT), false) //
        .filter(row -> row != null); //
  }

  @Override
  public Collection<DsvError> errors() {
    return Collections.unmodifiableCollection(errors);
  }



  private static class WioColumn {

    final int idx;
    final Predicate<String[]> filter;
    final boolean hasfilter;



    WioColumn(final int idx, final Predicate<String[]> filter) {
      this.idx = idx;
      this.filter = filter;
      hasfilter = filter != null;
    }
  }
}
