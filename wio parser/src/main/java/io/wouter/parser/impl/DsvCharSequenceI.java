package io.wouter.parser.impl;

import io.wouter.parser.DsvSettings;

final class DsvCharSequenceI implements CharSequence {

  private final char[] chars;
  private int length = 0;



  DsvCharSequenceI(final DsvSettings settings) {
    chars = new char[settings.getCharSequenceSize()];
  }



  CharSequence set(final char[] buffer, final int beg, final int end) {
    length = end - beg;
    System.arraycopy(buffer, beg, chars, 0, length);
    return this;
  }



  @Override
  public int length() {
    return length;
  }

  @Override
  public char charAt(final int idx) {
    return chars[idx];
  }

  @Override
  public CharSequence subSequence(final int subbeg, final int subend) {
    return new String(chars, subbeg, subend);
  }

  @Override
  public String toString() {
    return new String(chars, 0, length);
  }



  @Override
  public boolean equals(final Object o) {
    return super.equals(o) || o instanceof CharSequence && contentEquals((CharSequence) o);
  }

  @Override
  public int hashCode() {
    return super.hashCode(); // nothing else to base hashcode on
  }


  private boolean contentEquals(final CharSequence other) {
    if (length != other.length())
      return false;
    for (int i = 0; i < length; ++i)
      if (chars[i] != other.charAt(i))
        return false;
    return true;
  }
}
