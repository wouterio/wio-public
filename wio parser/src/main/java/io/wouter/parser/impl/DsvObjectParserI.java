package io.wouter.parser.impl;

import static java.util.Objects.requireNonNull;
import static java.util.Spliterator.DISTINCT;
import static java.util.Spliterator.ORDERED;
import static java.util.Spliterators.spliteratorUnknownSize;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import io.wouter.parser.DsvColumnFilter;
import io.wouter.parser.DsvError;
import io.wouter.parser.DsvParser;
import io.wouter.parser.DsvReader;
import io.wouter.parser.DsvValue;
import io.wouter.parser.settings.DsvObjectParserSettings;

final class DsvObjectParserI<T> implements DsvParser<T> {

  private final DsvReader reader;
  private final Supplier<T> supplier;
  private final WioColumn<T>[] columns;
  private final Collection<DsvError> errors = new LinkedList<>();



  private boolean hasnext;
  private long rowidx = 0;



  DsvObjectParserI(final DsvReader reader, final DsvObjectParserSettings<T> settings) {
    this.reader = requireNonNull(reader);
    this.supplier = requireNonNull(settings.getSupplier());
    this.columns = settings //
        .getColumns() //
        .entrySet() //
        .stream() //
        .map(e -> new WioColumn<>(e.getKey().intValue(), e.getValue())) //
        .toArray(WioColumn[]::new); //
    Arrays.sort(columns, (a, b) -> Integer.compare(a.idx, b.idx));
    if (settings.hasHeaders())
      reader.nextLine();
  }



  @Override
  public boolean hasNext() {
    hasnext = reader.hasNext();
    return hasnext;
  }

  @Override
  public T next() {
    if (!hasnext)
      throw new NoSuchElementException();

    final T out = supplier.get(); // output for this line

    ++rowidx;
    int colidx = 0; // current column index
    DsvValue value = null;

    try {
      for (final WioColumn<T> nxtcol : columns) { // next column index

        for (; colidx < nxtcol.idx; ++colidx) // skip columns until next column
          reader.skipValue();

        value = reader.readValue(); // read column

        if (nxtcol.hasfilter && !nxtcol.filter.process(out, value)) { // if not happy with output so far ...
          reader.nextLine(); // ... don't waste any more cpu on this line ...
          return null; // ... and quite
        }

        ++colidx; // step over column just read
      }

      reader.nextLine(); // done processing this line ...
      return out; // .. and return output

    } catch (final NoSuchElementException e) {
      errors.add(ImplFactory.newError(rowidx, colidx, null, e.getMessage()));
      return null;

    } catch (final NumberFormatException e) {
      errors.add(ImplFactory.newError(rowidx, colidx, value, e.getMessage()));
      return null;
    }
  }



  @Override
  public Stream<T> stream() {
    return StreamSupport.stream(spliteratorUnknownSize(this, ORDERED | DISTINCT), false) //
        .filter(row -> row != null); //
  }

  @Override
  public Collection<DsvError> errors() {
    return Collections.unmodifiableCollection(errors);
  }



  private static class WioColumn<T> {

    final int idx;
    final DsvColumnFilter<T> filter;
    final boolean hasfilter;



    WioColumn(final int idx, final DsvColumnFilter<T> filter) {
      this.idx = idx;
      this.filter = filter;
      hasfilter = filter != null;
    }
  }
}
