package io.wouter.parser.impl;

import static java.lang.System.arraycopy;

import io.wouter.parser.DsvSettings;
import io.wouter.parser.DsvValue;

final class DsvValueI implements DsvValue {

  private final DsvCharSequenceI charseq;
  private final char quote;
  private final boolean removeleading;
  private final boolean removetrailing;
  private final boolean removequotes;
  private final boolean removeescapes;

  private char[] buffer;
  private int valueLFidx;
  private int valueRGidx;
  private int nescapes = 0;



  DsvValueI(final DsvSettings settings) {
    charseq = new DsvCharSequenceI(settings);
    quote = settings.getQuote();
    removequotes = settings.doesRemoveQuotes();
    removeescapes = settings.doesRemoveEscapes();
    removeleading = removequotes || settings.doesRemoveLeadingWhiteSpace();
    removetrailing = removequotes || settings.doesRemoveTrailingWhiteSpace();
  }



  void resetEscapes() {
    nescapes = 0;
  }

  void addEscape() {
    ++nescapes;
  }



  DsvValue set(final char[] buffer, final int delimLFidx, final int delimRGidx) {
    this.buffer = buffer;
    valueLFidx = delimLFidx;
    valueRGidx = delimRGidx;
    ++valueLFidx; // step over left delimiter

    if (removeleading)
      removeLeadingWhiteSpace();
    if (removetrailing)
      removeTrailingWhiteSpace();
    if (removequotes)
      removeQuotes();
    if (removeescapes)
      removeEscapes();

    return this;
  }



  @Override
  public boolean toBoolean() {
    return Boolean.parseBoolean(toString()); // TODO avoid intermediate string
  }

  @Override
  public int toInt() {
    return Integer.parseInt(toString()); // TODO avoid intermediate string
  }

  @Override
  public long toLong() {
    return Long.parseLong(toString()); // TODO avoid intermediate string
  }

  @Override
  public float toFloat() {
    return Float.parseFloat(toString()); // TODO avoid intermediate string
  }

  @Override
  public double toDouble() {
    return Double.parseDouble(toString()); // TODO avoid intermediate string
  }

  @Override
  public CharSequence toCharSequence() {
    return charseq.set(buffer, valueLFidx, valueRGidx);
  }

  @Override
  public String toString() {
    return new String(buffer, valueLFidx, valueRGidx - valueLFidx);
  }



  private void removeLeadingWhiteSpace() {
    while (valueLFidx < valueRGidx && isWhitespace(buffer[valueLFidx]))
      ++valueLFidx;
  }

  private void removeTrailingWhiteSpace() {
    while (valueRGidx > valueLFidx && isWhitespace(buffer[valueRGidx - 1]))
      --valueRGidx;
  }

  private void removeQuotes() {
    if (buffer[valueLFidx] == quote) { // if there's a left quote, then also a right quote
      ++valueLFidx;
      --valueRGidx;
    }
  }

  private void removeEscapes() {
    if (nescapes > 0)
      for (int i = valueLFidx, j = i + 1; nescapes > 0; ++i, ++j)
        if (buffer[i] == quote && buffer[j] == quote) { // if two consecutive quotes ...
          arraycopy(buffer, j, buffer, i, valueRGidx - i); // ... shift char array to the left ...
          --valueRGidx; // ... shorten the value ...
          --nescapes; // ... and one escape less to deal with
        }
  }



  private static boolean isWhitespace(final char c) {
    return c == ' ' || c == '\t'; // only care about spaces and tabs
  }
}
