package io.wouter.parser.impl;

import io.wouter.parser.DsvError;
import io.wouter.parser.DsvValue;

final class DsvErrorI implements DsvError {

  private final long rowidx;
  private final int colidx;
  private final DsvValue value;
  private final String message;



  DsvErrorI(final long rowidx, final int colidx, final DsvValue value, final String message) {
    this.rowidx = rowidx;
    this.colidx = colidx;
    this.value = value;
    this.message = message;
  }



  @Override
  public long getRow() {
    return rowidx;
  }

  @Override
  public int getColumn() {
    return colidx;
  }

  @Override
  public DsvValue getValue() {
    return value;
  }

  @Override
  public String getMessage() {
    return message;
  }
}
