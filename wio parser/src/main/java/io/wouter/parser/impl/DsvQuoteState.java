package io.wouter.parser.impl;

enum DsvQuoteState {

  Unquoted(false, true, false),
  Quoted1(false, false, false),
  Quoted2(true, true, false),
  Quoted3(true, false, true);



  final boolean change;
  final boolean check;
  final boolean escape;



  private DsvQuoteState(final boolean change, final boolean check, final boolean escape) {
    this.change = change;
    this.check = check;
    this.escape = escape;
  }



  DsvQuoteState change(final boolean isquote) {
    switch (this) {
      case Unquoted:
        return isquote ?
            Quoted1 :
            Unquoted;
      case Quoted1:
        return isquote ?
            Quoted2 :
            Quoted1;
      case Quoted2:
        return isquote ?
            Quoted3 :
            Unquoted;
      case Quoted3:
        return isquote ?
            Quoted2 :
            Quoted1;
      default:
        throw new AssertionError();
    }
  }
}
