package io.wouter.parser;

import java.nio.charset.Charset;

public interface DsvSettings {

  Charset getCharset();



  int getBufferSize();

  int getCharSequenceSize();

  char getDelimiter();

  char getNewLine();

  char getQuote();



  boolean doesRemoveLeadingWhiteSpace();

  boolean doesRemoveTrailingWhiteSpace();

  boolean doesRemoveQuotes();

  boolean doesRemoveEscapes();
}
