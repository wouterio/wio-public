package io.wouter.parser;

import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Stream;

public interface DsvParser<T> extends Iterator<T> {

  Stream<T> stream();

  Collection<DsvError> errors();
}
