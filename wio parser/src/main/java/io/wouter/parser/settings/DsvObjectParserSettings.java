package io.wouter.parser.settings;

import static java.util.Objects.requireNonNull;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import io.wouter.parser.DsvColumnFilter;
import io.wouter.parser.DsvSettings;
import io.wouter.parser.DsvValue;

public final class DsvObjectParserSettings<T> implements DsvSettings {

  private Charset charset = StandardCharsets.UTF_8;

  private int buffersize = 1024 * 1024;
  private int charseqsize = 512;
  private char delimiter = ',';
  private char newline = '\n';
  private char quote = '"';

  private boolean removeleading = false;
  private boolean removetrailing = false;
  private boolean removequotes = false;
  private boolean removeescapes = false;

  private boolean hasheaders = false;
  private Supplier<T> supplier;
  private final Map<Integer, DsvColumnFilter<T>> columns = new HashMap<>();



  public DsvObjectParserSettings<T> setCharset(final Charset charset) {
    this.charset = charset;
    return this;
  }



  public DsvObjectParserSettings<T> setBufferSize(final int buffersize) {
    this.buffersize = buffersize;
    return this;
  }

  public DsvObjectParserSettings<T> setCharSequenceSize(final int charseqsize) {
    this.charseqsize = charseqsize;
    return this;
  }

  public DsvObjectParserSettings<T> setDelimiter(final char delimiter) {
    this.delimiter = delimiter;
    return this;
  }

  public DsvObjectParserSettings<T> setNewLine(final char newline) {
    this.newline = newline;
    return this;
  }

  public DsvObjectParserSettings<T> setQuote(final char quote) {
    this.quote = quote;
    return this;
  }

  public DsvObjectParserSettings<T> setRemoveLeadingWhiteSpace(final boolean removeleading) {
    this.removeleading = removeleading;
    return this;
  }

  public DsvObjectParserSettings<T> setRemoveTrailingWhiteSpace(final boolean removetrailing) {
    this.removetrailing = removetrailing;
    return this;
  }

  public DsvObjectParserSettings<T> setRemoveQuotes(final boolean removequotes) {
    this.removequotes = removequotes;
    return this;
  }

  public DsvObjectParserSettings<T> setRemoveEscapes(final boolean removeescapes) {
    this.removeescapes = removeescapes;
    return this;
  }



  public DsvObjectParserSettings<T> setHasHeaders(final boolean hasheaders) {
    this.hasheaders = hasheaders;
    return this;
  }

  public DsvObjectParserSettings<T> setSupplier(final Supplier<T> supplier) {
    this.supplier = requireNonNull(supplier);
    return this;
  }

  public DsvObjectParserSettings<T> addColumn(final int index, final DsvColumnFilter<T> filter) {
    columns.put(Integer.valueOf(index), requireNonNull(filter));
    return this;
  }

  public DsvObjectParserSettings<T> addColumn(final int index, final BiConsumer<T, DsvValue> filter) {
    requireNonNull(filter);
    columns.put(Integer.valueOf(index), (out, value) -> {
      filter.accept(out, value);
      return true;
    });
    return this;
  }



  @Override
  public Charset getCharset() {
    return charset;
  }

  @Override
  public int getBufferSize() {
    return buffersize;
  }

  @Override
  public int getCharSequenceSize() {
    return charseqsize;
  }

  @Override
  public char getDelimiter() {
    return delimiter;
  }

  @Override
  public char getNewLine() {
    return newline;
  }

  @Override
  public char getQuote() {
    return quote;
  }

  @Override
  public boolean doesRemoveLeadingWhiteSpace() {
    return removeleading;
  }

  @Override
  public boolean doesRemoveTrailingWhiteSpace() {
    return removetrailing;
  }

  @Override
  public boolean doesRemoveQuotes() {
    return removequotes;
  }

  @Override
  public boolean doesRemoveEscapes() {
    return removeescapes;
  }



  public boolean hasHeaders() {
    return hasheaders;
  }

  public Supplier<T> getSupplier() {
    return requireNonNull(supplier);
  }

  public Map<Integer, DsvColumnFilter<T>> getColumns() {
    return columns;
  }
}
