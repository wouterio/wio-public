package io.wouter.parser.settings;

import static java.util.Objects.requireNonNull;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import io.wouter.parser.DsvSettings;

public final class DsvArrayParserSettings implements DsvSettings {

  private Charset charset = StandardCharsets.UTF_8;

  private int buffersize = 1024 * 1024;
  private int charseqsize = 512;
  private char delimiter = ',';
  private char newline = '\n';
  private char quote = '"';

  private boolean removeleading = false;
  private boolean removetrailing = false;
  private boolean removequotes = false;
  private boolean removeescapes = false;

  private boolean headers = false;
  private final Map<Integer, Predicate<String[]>> columns = new HashMap<>();



  public DsvArrayParserSettings setCharset(final Charset charset) {
    this.charset = requireNonNull(charset);
    return this;
  }



  public DsvArrayParserSettings setBufferSize(final int buffersize) {
    this.buffersize = buffersize;
    return this;
  }

  public DsvArrayParserSettings setCharSequenceSize(final int charseqsize) {
    this.charseqsize = charseqsize;
    return this;
  }

  public DsvArrayParserSettings setDelimiter(final char delimiter) {
    this.delimiter = delimiter;
    return this;
  }

  public DsvArrayParserSettings setNewLine(final char newline) {
    this.newline = newline;
    return this;
  }

  public DsvArrayParserSettings setQuote(final char quote) {
    this.quote = quote;
    return this;
  }

  public DsvArrayParserSettings setRemoveLeadingWhiteSpace(final boolean removeleading) {
    this.removeleading = removeleading;
    return this;
  }

  public DsvArrayParserSettings setRemoveTrailingWhiteSpace(final boolean removetrailing) {
    this.removetrailing = removetrailing;
    return this;
  }

  public DsvArrayParserSettings setRemoveQuotes(final boolean removequotes) {
    this.removequotes = removequotes;
    return this;
  }

  public DsvArrayParserSettings setRemoveEscapes(final boolean removeescapes) {
    this.removeescapes = removeescapes;
    return this;
  }



  public DsvArrayParserSettings setHeaders(final boolean hasheaders) {
    headers = hasheaders;
    return this;
  }

  public DsvArrayParserSettings addColumn(final int col) {
    return addColumn(col, null);
  }

  public DsvArrayParserSettings addColumns(final int... cols) {
    for (final int col : cols)
      addColumn(col, null);
    return this;
  }

  public DsvArrayParserSettings addAllColumns(final int ncols) {
    for (int col = 0; col < ncols; ++col)
      addColumn(col, null);
    return this;
  }

  public DsvArrayParserSettings addColumn(final int col, final Predicate<String[]> filter) {
    columns.put(Integer.valueOf(col), filter);
    return this;
  }



  @Override
  public Charset getCharset() {
    return charset;
  }

  @Override
  public int getBufferSize() {
    return buffersize;
  }

  @Override
  public int getCharSequenceSize() {
    return charseqsize;
  }

  @Override
  public char getDelimiter() {
    return delimiter;
  }

  @Override
  public char getNewLine() {
    return newline;
  }

  @Override
  public char getQuote() {
    return quote;
  }

  @Override
  public boolean doesRemoveLeadingWhiteSpace() {
    return removeleading;
  }

  @Override
  public boolean doesRemoveTrailingWhiteSpace() {
    return removetrailing;
  }

  @Override
  public boolean doesRemoveQuotes() {
    return removequotes;
  }

  @Override
  public boolean doesRemoveEscapes() {
    return removeescapes;
  }



  public boolean hasHeaders() {
    return headers;
  }

  public Map<Integer, Predicate<String[]>> getColumns() {
    return columns;
  }
}
