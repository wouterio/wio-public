package io.wouter.parser;

import java.io.IOException;

import org.testng.annotations.Test;

import io.wouter.parser.settings.DsvArrayParserSettings;
import io.wouter.parser.settings.DsvObjectParserSettings;

public class DsvTests {

  @Test
  static void checkReaderOutputEquals1() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().setBufferSize(32);
    DsvTestUtils.checkReaderOutputEquals("quotetest.txt", "quotetest-ok.txt", settings, 10); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkReaderOutputEquals2() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().setBufferSize(32);
    DsvTestUtils.checkReaderOutputEquals("test1.txt", "test1-reader-ok.txt", settings, 4); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkReaderOutputEquals3() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().setBufferSize(32);
    DsvTestUtils.checkReaderOutputEquals("test2.txt", "test2-reader-ok.txt", settings, 4); //$NON-NLS-1$ //$NON-NLS-2$
  }



  @Test
  static void checkArrayOutputEquals1() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().addAllColumns(4);
    DsvTestUtils.checkArrayParserOutputEquals("test1.txt", "test1-parser-ok1.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkArrayOutputEquals2() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().addColumns(1, 3);
    DsvTestUtils.checkArrayParserOutputEquals("test1.txt", "test1-parser-ok2.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkArrayOutputEquals3() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().addAllColumns(4).addColumn(0, line -> !line[0].contains("E")); //$NON-NLS-1$
    DsvTestUtils.checkArrayParserOutputEquals("test1.txt", "test1-parser-ok3.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkArrayOutputEquals4() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().addColumn(0, line -> !line[0].contains("E")).addColumn(3); //$NON-NLS-1$
    DsvTestUtils.checkArrayParserOutputEquals("test1.txt", "test1-parser-ok4.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }



  @Test
  static void checkArrayOutputEquals5() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().setRemoveQuotes(true).setRemoveEscapes(true).addAllColumns(4);
    DsvTestUtils.checkArrayParserOutputEquals("test2.txt", "test2-parser-ok1.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkArrayOutputEquals6() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().setRemoveQuotes(true).setRemoveEscapes(true).addColumns(1, 3);
    DsvTestUtils.checkArrayParserOutputEquals("test2.txt", "test2-parser-ok2.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkArrayOutputEquals7() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().setRemoveQuotes(true).setRemoveEscapes(true).addAllColumns(4).addColumn(0, line -> !line[0].contains("E")); //$NON-NLS-1$
    DsvTestUtils.checkArrayParserOutputEquals("test2.txt", "test2-parser-ok3.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkArrayOutputEquals8() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().setRemoveQuotes(true).setRemoveEscapes(true).addColumn(0, line -> !line[0].contains("E")).addColumn(3); //$NON-NLS-1$
    DsvTestUtils.checkArrayParserOutputEquals("test2.txt", "test2-parser-ok4.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }



  @Test
  static void checkArrayOutputEquals9() throws IOException {
    final DsvArrayParserSettings settings = Dsv.newArraySettings().setRemoveQuotes(true).setRemoveEscapes(true).addAllColumns(5);
    DsvTestUtils.checkArrayParserOutputEquals("correctness.txt", "correctness-ok.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }



  @Test
  static void checkObjectOutputEquals1() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setSupplier(() -> new String[4]);
    settings.addColumn(0, (out, value) -> {
      out[0] = value.toString();
    });
    settings.addColumn(1, (out, value) -> {
      out[1] = value.toString();
    });
    settings.addColumn(2, (out, value) -> {
      out[2] = value.toString();
    });
    settings.addColumn(3, (out, value) -> {
      out[3] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("test1.txt", "test1-parser-ok1.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkObjectOutputEquals2() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setSupplier(() -> new String[2]);

    settings.addColumn(1, (out, value) -> {
      out[0] = value.toString();
    });
    settings.addColumn(3, (out, value) -> {
      out[1] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("test1.txt", "test1-parser-ok2.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }


  @Test
  static void checkObjectOutputEquals3() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setSupplier(() -> new String[4]);
    settings.addColumn(0, (out, value) -> {
      final String s = value.toString();
      final boolean valid = !s.contains("E"); //$NON-NLS-1$
      if (valid)
        out[0] = s;
      return valid;
    });
    settings.addColumn(1, (out, value) -> {
      out[1] = value.toString();
    });
    settings.addColumn(2, (out, value) -> {
      out[2] = value.toString();
    });
    settings.addColumn(3, (out, value) -> {
      out[3] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("test1.txt", "test1-parser-ok3.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkObjectOutputEquals4() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setSupplier(() -> new String[2]);
    settings.addColumn(0, (out, value) -> {
      final String s = value.toString();
      final boolean valid = !s.contains("E"); //$NON-NLS-1$
      if (valid)
        out[0] = s;
      return valid;
    });
    settings.addColumn(3, (out, value) -> {
      out[1] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("test1.txt", "test1-parser-ok4.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }



  @Test
  static void checkObjectOutputEquals5() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setRemoveQuotes(true);
    settings.setRemoveEscapes(true);
    settings.setSupplier(() -> new String[4]);
    settings.addColumn(0, (out, value) -> {
      out[0] = value.toString();
    });
    settings.addColumn(1, (out, value) -> {
      out[1] = value.toString();
    });
    settings.addColumn(2, (out, value) -> {
      out[2] = value.toString();
    });
    settings.addColumn(3, (out, value) -> {
      out[3] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("test2.txt", "test2-parser-ok1.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkObjectOutputEquals6() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setRemoveQuotes(true);
    settings.setRemoveEscapes(true);
    settings.setSupplier(() -> new String[2]);
    settings.addColumn(1, (out, value) -> {
      out[0] = value.toString();
    });
    settings.addColumn(3, (out, value) -> {
      out[1] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("test2.txt", "test2-parser-ok2.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }


  @Test
  static void checkObjectOutputEquals7() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setRemoveQuotes(true);
    settings.setRemoveEscapes(true);
    settings.setSupplier(() -> new String[4]);
    settings.addColumn(0, (out, value) -> {
      final String s = value.toString();
      final boolean valid = !s.contains("E"); //$NON-NLS-1$
      if (valid)
        out[0] = s;
      return valid;
    });
    settings.addColumn(1, (out, value) -> {
      out[1] = value.toString();
    });
    settings.addColumn(2, (out, value) -> {
      out[2] = value.toString();
    });
    settings.addColumn(3, (out, value) -> {
      out[3] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("test2.txt", "test2-parser-ok3.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }

  @Test
  static void checkObjectOutputEquals8() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setRemoveQuotes(true);
    settings.setRemoveEscapes(true);
    settings.setSupplier(() -> new String[2]);
    settings.addColumn(0, (out, value) -> {
      final String s = value.toString();
      final boolean valid = !s.contains("E"); //$NON-NLS-1$
      if (valid)
        out[0] = s;
      return valid;
    });
    settings.addColumn(3, (out, value) -> {
      out[1] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("test2.txt", "test2-parser-ok4.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }



  @Test
  static void checkObjectOutputEquals9() throws IOException {
    final DsvObjectParserSettings<String[]> settings = Dsv.newObjectSettings();
    settings.setRemoveQuotes(true);
    settings.setRemoveEscapes(true);
    settings.setSupplier(() -> new String[5]);
    settings.addColumn(0, (out, value) -> {
      out[0] = value.toString();
    });
    settings.addColumn(1, (out, value) -> {
      out[1] = value.toString();
    });
    settings.addColumn(2, (out, value) -> {
      out[2] = value.toString();
    });
    settings.addColumn(3, (out, value) -> {
      out[3] = value.toString();
    });
    settings.addColumn(4, (out, value) -> {
      out[4] = value.toString();
    });
    DsvTestUtils.checkObjectParserOutputEquals("correctness.txt", "correctness-ok.txt", settings); //$NON-NLS-1$ //$NON-NLS-2$
  }
}
