package io.wouter.parser;

import static io.wouter.parser.DsvTestUtils.newInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

import io.wouter.parser.settings.DsvArrayParserSettings;

public class DsvPerformance implements Runnable {

  @Override
  public void run() {
    final DsvArrayParserSettings settings = Dsv.newArraySettings();
    settings.addAllColumns(7);
    settings.setBufferSize(1024 * 1024);
    for (int i = 0; i < 20; ++i)
      parse(settings);
  }



  private static void parse(final DsvArrayParserSettings settings) {
    try (InputStream is = newInputStream("worldcitiespop.txt")) { //$NON-NLS-1$
      final DsvParser<String[]> p = Dsv.newParser(is, settings);
      final Stream<String[]> stream = p.stream();
      final long t0 = System.currentTimeMillis();
      final long count = stream.count();
      final long dt = System.currentTimeMillis() - t0;
      System.out.println("Parsing took " + dt + " ms to read " + count + " rows. "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    } catch (final IOException e) {
      e.printStackTrace();
    }
  }



  public static void main(final String[] args) {
    new DsvPerformance().run();
  }
}
