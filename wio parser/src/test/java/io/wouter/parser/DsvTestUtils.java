package io.wouter.parser;

import static org.testng.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

import io.wouter.parser.settings.DsvArrayParserSettings;
import io.wouter.parser.settings.DsvObjectParserSettings;

public class DsvTestUtils {

  static InputStream newInputStream(final String filename) {
    return ClassLoader.getSystemClassLoader().getResourceAsStream(filename);
  }

  static String newExpectedStringFromFile(final String filename) {
    return new BufferedReader(new InputStreamReader(newInputStream(filename))).lines().collect(Collectors.joining(System.lineSeparator())) + System.lineSeparator();
  }



  static void checkReaderOutputEquals(final String actfilename, final String expfilename, final DsvSettings settings, final int ncols) throws IOException {
    try (InputStream is = newInputStream(actfilename)) {
      final DsvReader r = Dsv.newReader(is, settings);
      final String actual = newActualStringFromReader(r, ncols);
      final String expected = newExpectedStringFromFile(expfilename);
      assertEquals(actual, expected);
    }
  }

  static void checkArrayParserOutputEquals(final String actfilename, final String expfilename, final DsvArrayParserSettings settings) throws IOException {
    try (InputStream is = newInputStream(actfilename)) {
      final DsvParser<String[]> p = Dsv.newParser(is, settings);
      final String actual = newActualStringFromParser(p);
      final String expected = newExpectedStringFromFile(expfilename);
      assertEquals(actual, expected);
    }
  }

  static void checkObjectParserOutputEquals(final String actfilename, final String expfilename, final DsvObjectParserSettings<String[]> settings) throws IOException {
    try (InputStream is = newInputStream(actfilename)) {
      final DsvParser<String[]> p = Dsv.newParser(is, settings);
      final String actual = newActualStringFromParser(p);
      final String expected = newExpectedStringFromFile(expfilename);
      assertEquals(actual, expected);
    }
  }



  static String newActualStringFromReader(final DsvReader r, final int ncols) {
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; r.hasNext();) {
      sb.append('<').append(r.readValue().toString()).append('>').append(System.lineSeparator());
      if (++i % ncols == 0)
        r.nextLine();
    }
    return sb.toString();
  }

  static String newActualStringFromParser(final DsvParser<String[]> p) {
    final StringBuilder sb = new StringBuilder();
    p.stream().forEach(line -> sb.append(Arrays.toString(line)).append(System.lineSeparator()));
    return sb.toString();
  }



  private DsvTestUtils() {
    throw new AssertionError("Static class."); //$NON-NLS-1$
  }
}
